<?php
ob_start();
session_start();

require '../functions/login.php';
include '../functions/conexao.php';
include '../functions/crud.php';

$acao = filter_input(INPUT_POST, 'acao', FILTER_SANITIZE_STRING);
switch ($acao) {

		case 'carregaCampeonatos' :
			$campeonatoNome = filter_input(INPUT_POST, 'filtroCampeonatoNome', FILTER_SANITIZE_STRING);
			if ($resultado = retornaCampeonatos($campeonatoNome, 'T')){
				foreach($resultado as $result){
                    $dados[] = array(
                        'id_campeonato' => $result -> id,
                        'nome_campeonato' => $result -> descricao,
                        'ativo' => $result->ativo
                    );
				}
                echo json_encode(array('data' => $dados));
			}else{
				echo json_encode(array('data' => []));
			}
		break;

        case 'carregaColetores' :
            $coletorNome = filter_input(INPUT_POST, 'filtroColetorNome', FILTER_SANITIZE_STRING);
            if ($resultado = retornaColetores($coletorNome)){
                foreach($resultado as $result){
                    $dados[] = array(
                        'id_coletor' => $result -> id,
                        'nome_coletor' => $result -> descricao,
                        'ativo' => $result -> ativo
                    );
                }
                echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }
        break;

		case 'carregaModalidades' :
			$modalidadeNome = filter_input(INPUT_POST, 'filtroModalidadeNome', FILTER_SANITIZE_STRING);
			if ($resultado = retornaModalidades($modalidadeNome)){
				foreach($resultado as $result){
                    $dados[] = array(
                        'id_modalidade' => $result -> id,
                        'nome_modalidade' => $result -> descricao
                    );
				}
                echo json_encode(array('data' => $dados));
			}else{
				echo json_encode(array('data' => []));
			}
		break;

		case 'carregaIscas' :
			$iscaNome = filter_input(INPUT_POST, 'filtroIscaNome', FILTER_SANITIZE_STRING);
			if ($resultado = retornaIscas($iscaNome)){
				foreach($resultado as $result){
                    $dados[] = array(
                        'id_isca' => $result -> id,
                        'nome_isca' => $result -> descricao
                    );
				}
                echo json_encode(array('data' => $dados));
			}else{
				echo json_encode(array('data' => []));
			}
		break;

        case 'carregaFiscais' :
            $fiscalNome = filter_input(INPUT_POST, 'filtroFiscalNome', FILTER_SANITIZE_STRING);
            if ($resultado = retornaFiscais($fiscalNome)){
                foreach($resultado as $result){
                    $dados[] = array(
                        'id_fiscal' => $result -> id,
                        'nome_fiscal' => $result -> nome
                    );
                }
                echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }
        break;

        case 'retornaFiscaisDisponiveisEtapa' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            if ($resultado = retornaFiscaisDisponiveisEtapa($idEtapa)){
                foreach($resultado as $result){
                    $dados[] = array(
                        'id_fiscal' => $result -> id,
                        'nome_fiscal' => $result -> nome,
                        'cpf_fiscal' => $result -> cpf
                    );
                }
                echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }
        break;

		case 'carregaEspecies' :
			$especieNome = filter_input(INPUT_POST, 'filtroEspecieNome', FILTER_SANITIZE_STRING);
			$prioridade = filter_input(INPUT_POST, 'filtroEspeciePrioridade', FILTER_SANITIZE_STRING);
			if ($resultado = retornaEspecies($especieNome, $prioridade)){
				foreach($resultado as $result){
					$dados[] = array(
                        'id_especie' => $result -> id,
                        'especie' => $result -> descricao,
                        'prioridade' => $result -> prioridade
                    );
				}
                echo json_encode(array('data' => $dados));
			}else{
				echo json_encode(array('data' => []));
			}
		break;

		case 'carregaEtapas' :
			$etapaNome = filter_input(INPUT_POST, 'filtroEtapaNome', FILTER_SANITIZE_STRING);
            $idCampeonato = filter_input(INPUT_POST, 'filtroEtapaCampeonato', FILTER_SANITIZE_STRING);
			if ($resultado = retornaEtapas($etapaNome, $idCampeonato)){
				foreach($resultado as $result){
                    $dados[] = array(
                        'id_etapa' => trim($result -> id),
                        'campeonato' => trim($result -> campeonato),
                        'nome_etapa' => trim($result -> DESCRICAO),
                        'data_etapa' => date("d/m/Y", strtotime($result -> DATA_ETAPA)),
                    );
                }
                echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }
		break;

		case 'listaEtapas' :
			$idCampeonato = filter_input(INPUT_POST, 'idCampeonato', FILTER_SANITIZE_STRING);
			if ($resultado = retornaEtapas('', $idCampeonato)){
                foreach($resultado as $result){
                    echo '<option value="'.$result->id.'">'.$result->DESCRICAO.' ('.date("d/m/Y", strtotime($result -> DATA_ETAPA)).')</option>';
                }
			}else{
                echo '<option value="-2">Nenhuma etapa encontrada</option>';
			}
		break;

		case 'pegaDataEtapa' :
			$idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);

			if ($resultado = retornaEtapa($idEtapa)){
				echo date("Y-m-d", strtotime($resultado -> DATA_ETAPA));
			}else{
				echo date("Y-m-d", time());
			}
		break;


        case 'pegaDataEtapa2' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            if ($resultado = retornaEtapa($idEtapa)){
                echo date("Y/m/d", strtotime($resultado -> DATA_ETAPA));
            }else{
                echo date("Y/m/d", time());
            }
        break;

		case 'excluiEquipeComposicao' :
			$idComposicaoEquipe = filter_input(INPUT_POST, 'idComposicaoEquipe', FILTER_SANITIZE_STRING);
			if (excluiEquipeComposicao($idComposicaoEquipe)){
                echo 'ok';
			}else{
                echo 'erro';
			}
		break;

        case 'salvaHistoriaEquipe' :
            $idEquipe = filter_input(INPUT_POST, 'idEquipe', FILTER_SANITIZE_STRING);
            $historia = filter_input(INPUT_POST, 'historia', FILTER_SANITIZE_STRING);

            if (salvaHistoriaEquipe($idEquipe, $historia)){
                echo 'ok';
            }else{
                echo 'erro';
            }
        break;

        case 'salvaCuriosidadesEquipe' :
            $idEquipe = filter_input(INPUT_POST, 'idEquipe', FILTER_SANITIZE_STRING);
            $curiosidades = filter_input(INPUT_POST, 'curiosidades', FILTER_SANITIZE_STRING);

            if (salvaCuriosidadesEquipe($idEquipe, $curiosidades)){
                echo 'ok';
            }else{
                echo 'erro';
            }
        break;

		case 'insertEquipeComposicao' :
            $idEquipe = filter_input(INPUT_POST, 'idEquipe', FILTER_SANITIZE_STRING);
            $idCompetidor = filter_input(INPUT_POST, 'idCompetidor', FILTER_SANITIZE_STRING);

            if (insereComposicaoEquipe($idEquipe, $idCompetidor)){
                echo 'ok';
            }else{
                echo 'erro';
            }
		break;

        case 'alteraTipoOrganizacaoRaias' :
            $idTipoOrgRaias = filter_input(INPUT_POST, 'idTipoOrgRaias', FILTER_SANITIZE_STRING);
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);

            if (insereTipoOrgRaiasEtapa($idTipoOrgRaias, $idEtapa)){
                echo 'ok';
            }else{
                echo 'erro';
            }
        break;

        case 'insertColetorComposicao' :
            $idColetor = filter_input(INPUT_POST, 'idColetor', FILTER_SANITIZE_STRING);
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);

            if (insereComposicaoColetor($idColetor, $idEtapa)){
                echo 'ok';
            }else{
                echo 'erro';
            }
        break;

        case 'insertBateriaEtapa' :
            $horario_inicio = filter_input(INPUT_POST, 'horario_inicio', FILTER_SANITIZE_STRING);
            $horario_termino = filter_input(INPUT_POST, 'horario_termino', FILTER_SANITIZE_STRING);
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);

            if (insereBateria($idEtapa, $horario_inicio, $horario_termino)){
                echo 'ok';
            }else{
                echo 'erro';
            }
        break;

        case 'insertFiscalComposicao' :
            $idFiscal = filter_input(INPUT_POST, 'idFiscal', FILTER_SANITIZE_STRING);
            $idComposicao = filter_input(INPUT_POST, 'idComposicao', FILTER_SANITIZE_STRING);

            if (insereComposicaoFiscal($idFiscal, $idComposicao)){
                echo 'ok';
            }else{
                echo 'erro';
            }
        break;

        case 'deleteColetorComposicao' :
            $idColetor = filter_input(INPUT_POST, 'idColetor', FILTER_SANITIZE_STRING);
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);

            if (excluiComposicaoColetor($idColetor, $idEtapa)){
                echo 'ok';
            }else{
                echo 'erro';
            }
        break;

        case 'deleteBateriaEtapa' :
            $idBateria = filter_input(INPUT_POST, 'idBateria', FILTER_SANITIZE_STRING);
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);

            if (excluiBateriaEtapa($idBateria, $idEtapa)){
                echo 'ok';
            }else{
                echo 'erro';
            }
        break;

		case 'insertEquipeComposicaoIdentificacao' :
            $idComposicao = filter_input(INPUT_POST, 'idComposicao', FILTER_SANITIZE_STRING);
            $idPulseira = filter_input(INPUT_POST, 'idPulseira', FILTER_SANITIZE_STRING);

            if(retornaSePulseiraJaCadastrada($idPulseira)){
                echo 'duplicado';
            }else{
                if (insereComposicaoIdentificacaoCompetidor($idComposicao, $idPulseira)){
                    echo 'ok';
                }else{
                    echo 'erro';
                }
            }
		break;

        case 'updateApelidoCompetidor' :
            $idCompetidor = filter_input(INPUT_POST, 'idCompetidor', FILTER_SANITIZE_STRING);
            $novoApelido = filter_input(INPUT_POST, 'novoApelido', FILTER_SANITIZE_STRING);
            $anteriorApelido = filter_input(INPUT_POST, 'anteriorApelido', FILTER_SANITIZE_STRING);
            if (updateApelidoCompetidor($idCompetidor, $novoApelido, $anteriorApelido)){
                echo 'ok';
            }else{
                echo 'erro';
            }

        break;

        case 'marcaEquipeComposicaoSuplente' :
            $idComposicao = filter_input(INPUT_POST, 'idComposicao', FILTER_SANITIZE_STRING);
            $marcacao = filter_input(INPUT_POST, 'marcacao', FILTER_SANITIZE_STRING);

            if (marcaComposicaoSuplenteCompetidor($idComposicao, $marcacao)){
                echo 'ok';
            }else{
                echo 'erro';
            }
        break;

        case 'informaPatrocinadorEquipe' :
            $idEquipe = filter_input(INPUT_POST, 'idEquipe', FILTER_SANITIZE_STRING);
            $patrocinador = filter_input(INPUT_POST, 'patrocinador', FILTER_SANITIZE_STRING);

            if (alteraPatrocinadorEquipe($idEquipe, $patrocinador)){
                echo 'ok';
            }else{
                echo 'erro';
            }
        break;

        case 'insertNumeroRaiaComposicaoEquipe' :
            $idComposicao = filter_input(INPUT_POST, 'idComposicao', FILTER_SANITIZE_STRING);
            $idRaiaInicial = filter_input(INPUT_POST, 'idRaiaInicial', FILTER_SANITIZE_STRING);

            if ($erro = insereComposicaoIdentificacaoRaia($idComposicao, $idRaiaInicial)){
                echo 'ok';
            }else{
                echo $erro;
            }
        break;

		case 'resultadosEtapa' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $horario = filter_input(INPUT_POST, 'horario', FILTER_SANITIZE_STRING);
            $unidadeMedida = retornaUnidadeMedidaEtapa($idEtapa);
            if($unidadeMedida){
                $unidadeMedida = $unidadeMedida->abreviacao;
            }else{
                $unidadeMedida ='';
            }

            if ($resultado = retonaResultadoEtapaGeral($idEtapa, $horario)){

                    $dados[] = array(
                        'total_peso' => number_format(floatval(str_replace(",","",$resultado->total_pesado)), 3, ',', '.').' '.$unidadeMedida,
                        'total_quantidade' => $resultado->total_peixes
                    );
                    echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }
		break;

        case 'resultadosCampeonatos' :
            $idCampeonato = filter_input(INPUT_POST, 'idCampeonato', FILTER_SANITIZE_STRING);

            if ($resultados = retonaResultadoCampeonato($idCampeonato)){
                foreach($resultados as $resultado){
                    $unidadeMedida = retornaUnidadeMedidaEtapa($resultado -> ID_ETAPA);
                    if($unidadeMedida){
                        $unidadeMedida = $unidadeMedida->abreviacao;
                    }else{
                        $unidadeMedida ='';
                    }

                    $dados[] = array(
                        'descricao_etapa' => $resultado -> DESCRICAO_ETAPA,
                        'uf_realizacao' => $resultado -> UF,
                        'data_etapa' => $resultado -> DATA_ETAPA,
                        'total_peso' => number_format(floatval(str_replace(",","",$resultado -> TOTAL_PESADO)), 3, ',', '.').' '.$unidadeMedida,
                        'total_quantidade' => $resultado -> TOTAL_PEIXES
                    );
                }

                echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }
        break;

        case 'resultadosEtapa' :
            $idEquipe = filter_input(INPUT_POST, 'idEquipe', FILTER_SANITIZE_STRING);
            if ($resultado = retonaResultadoEtapaGeral($idEtapa, $horario)){

                    $dados[] = array(
                        'total_peso' => number_format(floatval(str_replace(",","",$resultado->total_pesado)), 3, ',', '.').' '.$unidadeMedida,
                        'total_quantidade' => $resultado->total_peixes
                    );
                    echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }

        break;

        case 'resultadosEtapaLiveNew2' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $horario = filter_input(INPUT_POST, 'horario', FILTER_SANITIZE_STRING);
            $unidadeMedida = retornaUnidadeMedidaEtapa($idEtapa);
            if($unidadeMedida){
                $unidadeMedida = $unidadeMedida->abreviacao;
            }else{
                $unidadeMedida ='';
            }

            if ($resultado = retonaResultadoEtapaGeral($idEtapa, $horario)){

                    $dados[] = array(
                        'total_peso' => number_format(floatval(str_replace(",","",$resultado->total_pesado)), 3, ',', '.').' '.$unidadeMedida,
                        'total_quantidade' => $resultado->total_peixes
                    );
                    echo json_encode($dados);
            }else{
                echo json_encode(array('data' => []));
            }

        break;

        case 'retornaColetoresDisponiveisEtapa' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);

            if($resultados = retornaColetoresDisponiveisEtapa($idEtapa)){
                foreach($resultados as $resultado){
                    $dados[] = array(
                        'id_coletor' => $resultado -> id,
                        'nome_coletor' => $resultado -> descricao,
                        'serial_coletor' => $resultado -> serial
                    );
                }
                echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }
        break;

        case 'retornaColetoresEtapa' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);

            if($resultados = retornaColetoresEtapa($idEtapa)){
                foreach($resultados as $resultado){
                    $dados[] = array(
                        'id_coletor' => $resultado -> id,
                        'nome_coletor' => $resultado -> descricao,
                        'serial_coletor' => $resultado -> serial,
                        'id_cad_fiscal' => $resultado -> id_cad_fiscal,
                        'nome_fiscal' => $resultado -> fiscal_nome,
                        'id_composicao' => $resultado -> id_composicao
                    );
                }
                echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }
        break;

        case 'retornaBateriasEtapa' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);

            if($resultados = retornaBateriasEtapa($idEtapa)){
                foreach($resultados as $resultado){
                    $dados[] = array(
                        'id_bateria' => $resultado -> id,
                        'id_cad_etapa' => $resultado -> id_cad_etapa,
                        'horario_inicio' => $resultado -> horario_inicio,
                        'horario_termino' => $resultado -> horario_termino
                    );
                }
                echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }
        break;

        case 'unidadeMedidaEtapa' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            if ($resultado = retornaUnidadeMedidaEtapa($idEtapa)){
                    $dados[] = array(
                        'descricao' => $resultado->descricao,
                        'abreviacao' => $resultado->abreviacao
                    );
                    echo json_encode($dados);
            }else{
                echo json_encode([]);
            }
        break;

		case 'retornaBuscaEquipesComposicaoCompetidores' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);

            if ($resultado = retornaBuscaEquipesComposicaoCompetidores($idEtapa, $nome)){
                foreach($resultado as $result){
                    echo '
                    <a class="list-group-item clearfix">
                        ' . $result -> nome . '
                        <span class="pull-right">
                            <span class="btn btn-xs btn-default" onclick="insereComposicaoEquipe('. $result -> id .'); event.stopPropagation();">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            </span>
                        </span>
                    </a>';
                }
            }else{
                echo 'Nenhum competidor encontrado!';
            }
		break;

		case 'resultadosEtapaMaiorPeixe' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $horario = filter_input(INPUT_POST, 'horario', FILTER_SANITIZE_STRING);
            $unidadeMedida = retornaUnidadeMedidaEtapa($idEtapa);
            if ($resultado = retornaResultadoEtapaMaiorPeixe($idEtapa, $horario, 0)){
                echo '<tr>';
                    echo '<td>_____________________________</td>';
                    echo '<td>'.number_format($resultado->peso, 3, ',', ' ').' '.$unidadeMedida->abreviacao.'</td>';
                    echo '<td>'.$resultado->especie.'</td>';

                    $competidores = retornaNomesEquipe($resultado->id_equipe);
                    $nomes = '';
                    foreach($competidores as $result){
                        if($result->id_competidor == $resultado->ID_CAD_COMPETIDOR){
                            $nomes .= ' - <b>'.$result->competidor.'</b>';
                        }else{
                            $nomes .= ' - '.$result->competidor;
                        }
                    }
                    echo '<td>'.$resultado->modalidade.'</td>';
                    echo '<td>'.$resultado->isca.'</td>';
                    echo '<td>'.$resultado->equipe .$nomes.'</td>';

                echo '</tr>';
            }else{
                echo '<tr>';
                    echo '<td colspan="6">Sem Resultados</td>';
                echo '</tr>';
            }
		break;

		case 'resultadosEtapaMaiorPeixeSemEntregaTrofeu' :
			$idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
			$horario = filter_input(INPUT_POST, 'horario', FILTER_SANITIZE_STRING);
            $mostraNome = filter_input(INPUT_POST, 'mostraNome', FILTER_SANITIZE_STRING);

            $unidadeMedida = retornaUnidadeMedidaEtapa($idEtapa);
			if ($resultado = retornaResultadoEtapaMaiorPeixe($idEtapa, $horario, 0)){
                $competidores = retornaNomesEquipe($resultado->id_equipe);
                $nomes = '';
                foreach($competidores as $result){
                    if($result->id_competidor == $resultado->ID_CAD_COMPETIDOR){
                        if($mostraNome == 'completo'){
                            if($nomes == ''){
                                $nomes .= ' - <b>'.$result->competidor.'</b>';
                            }else{
                                $nomes .= ' & <b>'.$result->competidor.'</b>';
                            }
                        }else{
                            if($mostraNome == 'primeiro'){
                                if($nomes == ''){
                                    $nomes .= ' - <b>'.$result->primeiro_nome.'</b>';
                                }else{
                                    $nomes .= ' & <b>'.$result->primeiro_nome.'</b>';
                                }
                            }else{
                                if($mostraNome == 'apelido'){
                                    if($nomes == ''){
                                        $nomes .= ' - <b>'.$result->apelido.'</b>';
                                    }else{
                                        $nomes .= ' & <b>'.$result->apelido.'</b>';
                                    }
                                }
                            }
                        }
                    }else{
                        if($mostraNome == 'completo'){
                            if($nomes == ''){
                                $nomes .= ' - '.$result->competidor;
                            }else{
                                $nomes .= ' & '.$result->competidor;
                            }
                        }else{
                            if($mostraNome == 'primeiro'){
                                if($nomes == ''){
                                    $nomes .= ' - '.$result->primeiro_nome;
                                }else{
                                    $nomes .= ' & '.$result->primeiro_nome;
                                }
                            }else{
                                if($mostraNome == 'apelido'){
                                    if($nomes == ''){
                                        $nomes .= ' - '.$result->apelido;
                                    }else{
                                        $nomes .= ' & '.$result->apelido;
                                    }
                                }
                            }
                        }
                    }
                }

                $dados[] = array(
                    'peso' => number_format(floatval(str_replace(",","",$resultado -> peso)), 3, ',', '.').' '.$unidadeMedida -> abreviacao,
                    'especie' => $resultado -> especie,
                    'modalidade' => $resultado -> modalidade,
                    'isca' => $resultado -> isca,
                    'nome_equipe' => $resultado -> equipe,
                    'competidores' => $resultado -> equipe .$nomes,
                    'competidores_completo' => array($competidores),
                    'id_equipe' => $resultado -> id_equipe
                );

                echo json_encode(array('data' => $dados));

			}else{
                echo json_encode(array('data' => []));
			}
		break;

        case 'resultadosEtapaMaiorPeixeSemEntregaTrofeuLiveNew2' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $horario = filter_input(INPUT_POST, 'horario', FILTER_SANITIZE_STRING);
            $mostraNome = filter_input(INPUT_POST, 'mostraNome', FILTER_SANITIZE_STRING);

            $unidadeMedida = retornaUnidadeMedidaEtapa($idEtapa);
            if ($resultado = retornaResultadoEtapaMaiorPeixe($idEtapa, $horario, 0)){
                $competidores = retornaNomesEquipe($resultado->id_equipe);
                $nomes = '';
                foreach($competidores as $result){
                    if($result->id_competidor == $resultado->ID_CAD_COMPETIDOR){
                        if($mostraNome == 'completo'){
                            if($nomes == ''){
                                $nomes .= ' - <b>'.$result->competidor.'</b>';
                            }else{
                                $nomes .= ' & <b>'.$result->competidor.'</b>';
                            }
                        }else{
                            if($mostraNome == 'primeiro'){
                                if($nomes == ''){
                                    $nomes .= ' - <b>'.$result->primeiro_nome.'</b>';
                                }else{
                                    $nomes .= ' & <b>'.$result->primeiro_nome.'</b>';
                                }
                            }else{
                                if($mostraNome == 'apelido'){
                                    if($nomes == ''){
                                        $nomes .= ' - <b>'.$result->apelido.'</b>';
                                    }else{
                                        $nomes .= ' & <b>'.$result->apelido.'</b>';
                                    }
                                }
                            }
                        }
                    }else{
                        if($mostraNome == 'completo'){
                            if($nomes == ''){
                                $nomes .= ' - '.$result->competidor;
                            }else{
                                $nomes .= ' & '.$result->competidor;
                            }
                        }else{
                            if($mostraNome == 'primeiro'){
                                if($nomes == ''){
                                    $nomes .= ' - '.$result->primeiro_nome;
                                }else{
                                    $nomes .= ' & '.$result->primeiro_nome;
                                }
                            }else{
                                if($mostraNome == 'apelido'){
                                    if($nomes == ''){
                                        $nomes .= ' - '.$result->apelido;
                                    }else{
                                        $nomes .= ' & '.$result->apelido;
                                    }
                                }
                            }
                        }
                    }
                }

                $dados[] = array(
                    'peso' => number_format(floatval(str_replace(",","",$resultado->peso)), 3, ',', '.').' '.$unidadeMedida->abreviacao,
                    'especie' => $resultado->especie,
                    'modalidade' => $resultado->modalidade,
                    'isca' => $resultado->isca,
                    'competidores' => $resultado->equipe .$nomes,
                    'equipe' => $resultado->id_equipe
                );

                echo json_encode($dados);

            }else{
                echo json_encode(array('data' => []));
            }
        break;

		case 'resultadosEtapaRanking' :
			$idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
			$horario = filter_input(INPUT_POST, 'horario', FILTER_SANITIZE_STRING);
            $unidadeMedida = retornaUnidadeMedidaEtapa($idEtapa);
			if ($resultado = retornaResultadoEtapaRankingHorario($idEtapa, $horario)){
                $i=count($resultado);
                foreach($resultado as $result){
                    echo '<tr>';
                        echo '<td>#'.$i--.'</td>';
                        echo '<td>_____________________________</td>';
                        echo '<td>'.$result->peso.' '.$unidadeMedida->abreviacao.'</td>';
                        echo '<td>'.$result->quantidade.'</td>';

                        $competidores = retornaNomesEquipe($result->id_equipe);
                        $nomes = '';
                        foreach($competidores as $result1){
                                $nomes .= ' - '.$result1->competidor;
                        }

                        echo '<td>'.$result->equipe .$nomes.'</td>';
                    echo '</tr>';
                }
			}else{
				echo '<tr>';
					echo '<td colspan="5">Sem Resultados</td>';
				echo '</tr>';
			}
		break;

		case 'resultadosEtapaRankingSemEntregaTrofeu' :
			$idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
			$horario = filter_input(INPUT_POST, 'horario', FILTER_SANITIZE_STRING);
            $mostraNome = filter_input(INPUT_POST, 'mostraNome', FILTER_SANITIZE_STRING);
            $rankingCompleto = filter_input(INPUT_POST, 'rankingCompleto', FILTER_VALIDATE_BOOLEAN);

            $unidadeMedida = retornaUnidadeMedidaEtapa($idEtapa);

            if($rankingCompleto == true){
                $resultado = retornaResultadoEtapaRankingHorarioTudo($idEtapa, $horario);
            }else{
                $resultado = retornaResultadoEtapaRankingHorario($idEtapa, $horario);
            };

			if ($resultado){
                //$i=0;
                $i=count($resultado);
                foreach($resultado as $result){
                    $competidores = retornaNomesEquipe($result->id_equipe);
                    $nomes = '';
                    foreach($competidores as $result1){
                        if($mostraNome == 'completo'){
                            if($nomes == ''){
                                $nomes .= $result1->competidor;
                            }else{
                                $nomes .= ' & '.$result1->competidor;
                            }
                        }else{
                            if($mostraNome == 'primeiro'){
                                if($nomes == ''){
                                    $nomes .= $result1->primeiro_nome;
                                }else{
                                    $nomes .= ' & '.$result1->primeiro_nome;
                                }
                            }else{
                                if($mostraNome == 'apelido'){

                                    if($nomes == ''){
                                        $nomes .= $result1->apelido;
                                    }else{
                                        $nomes .= ' & '.$result1->apelido;
                                    }
                                }
                            }
                        }
                    }
                    $dados[] = array(
                        'colocacao' => $i--,
                        'peso' => number_format(floatval(str_replace(",","",$result->peso)), 3, ',', '.'). ' ' . $unidadeMedida->abreviacao,
                        'quantidade' => $result->quantidade,
                        'equipe' => $result->equipe . ' - ' .$nomes,
                        'nome_equipe' => $result -> equipe,
                        'id_equipe' => $result->id_equipe,
                        'id_etapa' => $result->id_etapa,
                        'competidores_completo' => array($competidores)
                    );
                }
                echo json_encode(array('data' => $dados));
			}else{
                echo json_encode(array('data' => []));
			}

		break;


		case 'resultadosLancamentosEtapa' :
			$idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
			$horario = filter_input(INPUT_POST, 'horario', FILTER_SANITIZE_STRING);
			$idModalidade = filter_input(INPUT_POST, 'modalidade', FILTER_SANITIZE_STRING);
			$idIsca = filter_input(INPUT_POST, 'isca', FILTER_SANITIZE_STRING);
			$idEquipe = filter_input(INPUT_POST, 'equipe', FILTER_SANITIZE_STRING);

			if ($resultado = retornaLancamentosEtapa($idEtapa, $horario, $idModalidade, $idIsca, $idEquipe)){
                foreach ($resultado as $result){
                    $dados[] = array(
                        'comprovante' => trim($result -> COMPROVANTE),
                        'competidor' => trim($result -> COMPETIDOR),
                        'equipe' => trim($result -> EQUIPE),
                        'especie' => trim($result -> ESPECIE),
                        'modalidade' => trim($result -> MODALIDADE),
                        'isca' => trim($result -> ISCA),
                        'data_hora' => trim($result -> DATA_HORA),
                        'hora' => trim($result -> HORA),
                        'peso' => trim($result -> PESO),
                        'obs' => trim($result -> OBS),
                        'nome_fiscal' => trim($result -> NOME_FISCAL),
                        'excluido' => trim($result -> EXCLUIDO)
                    );
                }
                echo json_encode(array('data' => $dados));
			}else{
                echo json_encode(array('data' => []));
			}
		break;

		case 'resultadosCompetidoresEtapa' :
			$idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);

			if ($resultado = retornaCompetidoresEtapa($idEtapa)){
				foreach($resultado as $result){
					echo '<tr>';
						echo '<td>'.$result->NOME.'</td>';
						echo '<td>'.$result->EQUIPE.'</td>';
						if($result->ID_PULSEIRA==0){
							echo '<td><span class="label label-danger">N/D</span></td>';
						}else{
							echo '<td>'.$result->ID_PULSEIRA.'</td>';
						}
						echo '<td>'.$result->CPF.'</td>';
						echo '<td>'.$result->TAMANHO_CAMISETA.'</td>';
						echo '<td>'.$result->APELIDO.'</td>';
                        echo '<td>'.$result->PATROCINADOR.'</td>';
					echo '</tr>';
				}
			}else{
				echo '<tr>';
					echo '<td colspan="6">Sem Resultados</td>';
				echo '</tr>';
			}
		break;

        case 'resultadosRaiasEtapa' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $etapa = retornaEtapa($idEtapa);

            $qtdBaterias = $etapa -> QTD_BATERIAS;
            $headerBaterias = '';
            for ($i = 1; $i <= $qtdBaterias; $i++) {
                $headerBaterias .= '<th scope="col" align="right">Raia Bat.'.$i.'</th>';
            };


            if ($resultado = retornaCompetidoresAgrupadosEtapa($idEtapa)){
                echo '<thead>';
                echo     '<tr>';
                echo         '<th>Equipe</th>';
                echo         '<th>Competidores</th>';
                echo         '<th>Pulseiras</th>';
                echo         $headerBaterias;
                echo     '</tr>';
                echo '</thead>';
                echo '<tbody >';

                foreach($resultado as $result){
                    echo '<tr>';
                        echo '<td>'.$result -> NOME_EQUIPE.'</td>';
                        echo '<td>'.$result -> APELIDOS_COMPETIDORES.'</td>';
                        if($result -> ID_PULSEIRAS==''){
                            echo '<td><span class="label label-danger">N/D</span></td>';
                        }else{
                            echo '<td>'.$result -> ID_PULSEIRAS.'</td>';
                        }

                        if($etapa -> QTD_BATERIAS != null){
                            $qtdBaterias = $etapa -> QTD_BATERIAS;
                        }else{
                            $qtdBaterias = 0;
                        }

                        if($result -> RAIA_INICIAL == 0){
                            $raia = '';
                        }else{
                            $raia = $result -> RAIA_INICIAL;
                        }



                        echo '<td style="white-space: nowrap" align="right">'.$raia.'</td>';
                        /*funcao para definicao de raias*/
                        for ($i = 1; $i <= $qtdBaterias-1; $i++) {

                            if($etapa -> ID_CAD_TIPO_ORG_RAIA == null){
                                echo '<td style="white-space: nowrap" align="left">Organização de raias não definida na etapa</td>';
                            }else{
                                if($result -> RAIA_INICIAL == 0){
                                    echo '<td style="white-space: nowrap" align="right"></td>';
                                }else{
                                    $tipoOrgRaia = $etapa -> ID_CAD_TIPO_ORG_RAIA;

                                    if($tipoOrgRaia == 1){
                                        if ($raia >= 1 && $raia <= 15) {
                                            $raia = intval($raia) + 15;
                                        }else{
                                            if ($raia >= 16 && $raia <= 23) {
                                                $raia = intval($raia) + 37;
                                            }else{
                                                if ($raia >= 24 && $raia <= 30) {
                                                    $raia = intval($raia) + 22;
                                                }else{
                                                    if ($raia >= 31 && $raias <= 37) {
                                                        $raia = intval($raia) - 22;
                                                    }else{
                                                        if ($raia >= 38 && $raia <= 45) {
                                                            $raia = intval($raia) - 37;
                                                        }else{
                                                            if ($raia >= 46 && $raia <= 60) {
                                                                $raia = intval($raia) - 15;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    /*ORGANIZACAO DE RAIAS TIPO 2*/
                                    if($tipoOrgRaia == 2){
                                        if ($raia >= 1 && $raia <= 15) {
                                            $raia = intval($raia) + 15;
                                        }else{
                                            if ($raia >= 16 && $raia <= 30) {
                                                $raia = intval($raia) + 15;
                                            }else{
                                                if ($raia >= 31 && $raia <= 45) {
                                                    $raia = intval($raia) + 15;
                                                }else{
                                                    if ($raia >= 46 && $raia <= 60) {
                                                        $raia = intval($raia) - 45;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    /*ORGANIZACAO DE RAIAS TIPO 3*/
                                    if($tipoOrgRaia == 3){
                                        //segunda bateria
                                        if(i==0){
                                            if ($raia >= 1 && $raia <= 32) {
                                                $raia = intval($raia) + 10;
                                            }else{
                                                if ($raia >= 33 && $raia <= 42) {
                                                    $raia = intval($raia) - 32;
                                                }
                                            }
                                        }else{
                                            //terceira bateria
                                            if(i==1){
                                                if ($raia >= 1 && $raia <= 31) {
                                                    $raia = intval($raia) + 11;
                                                }else{
                                                    if ($raia >= 32 && $raia <= 42) {
                                                        $raia = intval($raia) - 31;
                                                    }
                                                }
                                            }else{
                                                //quarta bateria
                                                if(i==2){
                                                    if ($raia >= 1 && $raia <= 32) {
                                                        $raia = intval($raia) + 10;
                                                    }else{
                                                        if ($raia >= 33 && $raia <= 42) {
                                                            $raia = intval(raia) - 32;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    echo '<td style="white-space: nowrap" align="right">'.$raia.'</td>';


                                }
                            }

                        }
                    echo '</tr>';
                }

                echo '</tbody>';
            }else{
                echo '<tr>';
                    echo '<td colspan="6">Sem Resultados</td>';
                echo '</tr>';
            }
        break;

        case 'acompanhamentoLive' :
            $ultimaVerificacao = filter_input(INPUT_POST, 'ultimaVerificacao', FILTER_SANITIZE_STRING);
            $idUltimaVerificacao = filter_input(INPUT_POST, 'idUltimaVerificacao', FILTER_SANITIZE_STRING);
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $unidadeMedida = retornaUnidadeMedidaEtapa($idEtapa);
            if ($resultado = retornaResultadosLive($ultimaVerificacao, $idEtapa, 'live', $idUltimaVerificacao)){
                $i = 0;
                $idUltimaVerificacao = -1;
                foreach($resultado as $result){
                    $idUltimaVerificacao = max($result->id, $idUltimaVerificacao);
                    if ($i == 0) {
                        $verificarAposHorario = $result->DATA_HORA;
                        $idEtapaVerificada = $result->ID_ETAPA;
                        $i++;
                    }
                    if($ultimaVerificacao == ''){
                        if($result->EXCLUIDO == 'SIM'){
                            echo '<tr class="linhaLive bg-danger">';
                        }else{
                            echo '<tr class="linhaLive">';
                        }
                    }else{
                        if($result->EXCLUIDO == 'SIM'){
                            echo '<tr class="linhaLive excluido bg-danger">';
                        }else{
                            echo '<tr class="linhaLive invalid">';
                        }
                    }
                    //echo '<td>'.$result->id.'</td>';
                    echo '<td>..'.substr($result->COMPROVANTE,-10).'</td>';
                    //echo '<td>'.$result->ETAPA_NOME.'</td>';
                    echo '<td>'.date("d/m/Y H:i:s", strtotime($result -> DATA_HORA)).'</td>';
                    //echo '<td>'.date("H:i:s", strtotime($result -> DATA_HORA)).'</td>';
                    echo '<td>'.$result->EQUIPE_NOME.'</td>';
                    echo '<td>'.$result->APELIDO.'</td>';
                    echo '<td>'.$result->ESPECIE_NOME.'</td>';
                    echo '<td>'.$result->MODALIDADE_NOME.'</td>';
                    echo '<td>'.$result->ISCA_NOME.'</td>';
                    //echo '<td>'.$result->PONTO_PESAGEM.'</td>';
                    echo '<td>'.number_format($result->PESO, 3, '.', ','). ' '.$unidadeMedida->abreviacao. '</td>';
                    echo '<td>'.$result->OBS.'</td>';
                    echo '</tr>';
                }
                echo "<script>$('#ultimaVerificacao').val('".$verificarAposHorario."')</script>";
                echo "<script>$('#idEtapaAcompanhamentoLive').val('".$idEtapaVerificada."')</script>";
                echo "<script>$('#idUltimaVerificacao').val('".$idUltimaVerificacao."')</script>";

            }
        break;

        case 'colocacaoEquipeLiveNew2':
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $idEquipe = filter_input(INPUT_POST, 'idEquipe', FILTER_SANITIZE_STRING);
            if($resultado = retornaPosicaoEquipe($idEtapa, $idEquipe)){
                echo json_encode($resultado);
            }
        break;

        case 'acompanhamentoLiveNew2' :
            $ultimaVerificacao = filter_input(INPUT_POST, 'ultimaVerificacao', FILTER_SANITIZE_STRING);
            $idUltimaVerificacao = filter_input(INPUT_POST, 'idUltimaVerificacao', FILTER_SANITIZE_STRING);

            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $unidadeMedida = retornaUnidadeMedidaEtapa($idEtapa);
            if ($resultado = retornaResultadosLiveNew2($ultimaVerificacao, $idEtapa, 'live', $idUltimaVerificacao)){
                $i = 0;
                $idUltimaVerificacao = -1;
                foreach($resultado as $result){
                    $idUltimaVerificacao = max($result->id, $idUltimaVerificacao);
                    if ($i == 0) {
                        $verificarAposHorario = $result->DATA_HORA;
                        $idEtapaVerificada = $result->ID_ETAPA;
                        $i++;
                    }
                    if($ultimaVerificacao == ''){
                        if($result->EXCLUIDO == 'SIM'){
                            echo '<tr class="linhaLive pesagem-excluida">';
                        }else{
                            echo '<tr class="linhaLive">';
                        }
                    }else{
                        if($result->EXCLUIDO == 'SIM'){
                            echo '<tr class="linhaLive excluido bg-danger">';
                        }else{
                            echo '<tr class="linhaLive invalid">';
                        }
                    }
                    //echo '<td>'.$result->id.'</td>';
                    //echo '<td>..'.substr($result->COMPROVANTE,-10).'</td>';
                    //echo '<td>'.$result->ETAPA_NOME.'</td>';
                    //echo '<td>'.date("d/m/Y H:i:s", strtotime($result -> DATA_HORA)).'</td>';
                    echo '<td class="equipeID" id="'.$result->ID_CAD_EQUIPE.'">'.$result->ID_CAD_EQUIPE.'</td>';
                    echo '<td>'.date("H:i:s", strtotime($result -> DATA_HORA)).'</td>';
                    echo '<td>'.$result->EQUIPE_NOME.'</td>';
                    echo '<td>'.$result->APELIDO.'</td>';
                    echo '<td>'.$result->ESPECIE_NOME.'</td>';
                    echo '<td>'.$result->MODALIDADE_NOME.'</td>';
                    echo '<td>'.$result->ISCA_NOME.'</td>';
                    //echo '<td>'.$result->PONTO_PESAGEM.'</td>';
                    echo '<td>'.number_format($result->PESO, 3, '.', ','). ''.$unidadeMedida->abreviacao. '</td>';
                    //echo '<td>'.$result->OBS.'</td>';
                    echo '</tr>';
                }
                echo "<script>$('#ultimaVerificacao').val('".$verificarAposHorario."')</script>";
                echo "<script>$('#idEtapaAcompanhamentoLive').val('".$idEtapaVerificada."')</script>";
                echo "<script>$('#idUltimaVerificacao').val('".$idUltimaVerificacao."')</script>";
            }
        break;

        case 'acompanhamentoLiveNarradores' :
            $ultimaVerificacao = filter_input(INPUT_POST, 'ultimaVerificacao', FILTER_SANITIZE_STRING);
            $idUltimaVerificacao = filter_input(INPUT_POST, 'idUltimaVerificacao', FILTER_SANITIZE_STRING);

            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $unidadeMedida = retornaUnidadeMedidaEtapa($idEtapa);
            if ($resultado = retornaResultadosLive($ultimaVerificacao, $idEtapa, 'live', $idUltimaVerificacao)){
                $i = 0;
                $idUltimaVerificacao = -1;
                foreach($resultado as $result){
                    $idUltimaVerificacao = max($result->id, $idUltimaVerificacao);
                    if ($i == 0) {
                        $verificarAposHorario = $result->DATA_HORA;
                        $idEtapaVerificada = $result->ID_ETAPA;
                        $i++;
                    }
                    if($ultimaVerificacao == ''){
                        if($result->EXCLUIDO == 'SIM'){
                            echo '<tr class="linhaLive bg-danger">';
                        }else{
                            echo '<tr class="linhaLive">';
                        }
                    }else{
                        if($result->EXCLUIDO == 'SIM'){
                            echo '<tr class="linhaLive excluido bg-danger">';
                        }else{
                            echo '<tr class="linhaLive invalid">';
                        }
                    }
                    //echo '<td>'.$result->id.'</td>';
                    //echo '<td>..'.substr($result->COMPROVANTE,-10).'</td>';
                    //echo '<td>'.$result->ETAPA_NOME.'</td>';
                    echo '<td>'.date("H:i:s", strtotime($result -> DATA_HORA)).'</td>';
                    //echo '<td>'.date("H:i:s", strtotime($result -> DATA_HORA)).'</td>';
                    echo '<td><a data-toggle="modal" data-target="#myModal" data-id_equipe="'.$result->EQUIPE_ID.'" data-html="true">'.$result->EQUIPE_NOME.'</a></td>';
                    echo '<td>'.$result->APELIDO.'</td>';
                    echo '<td>'.$result->ESPECIE_NOME.'</td>';
                    //echo '<td>'.$result->MODALIDADE_NOME.'</td>';
                    echo '<td>'.$result->ISCA_NOME.'</td>';
                    //echo '<td>'.$result->PONTO_PESAGEM.'</td>';
                    echo '<td>'.number_format($result->PESO, 3, '.', ','). ' '.$unidadeMedida->abreviacao. '</td>';
                    echo '<td>'.$result->OBS.'</td>';
                    echo '</tr>';
                }
                echo "<script>$('#ultimaVerificacao').val('".$verificarAposHorario."')</script>";
                echo "<script>$('#idEtapaAcompanhamentoLive').val('".$idEtapaVerificada."')</script>";
                echo "<script>$('#idUltimaVerificacao').val('".$idUltimaVerificacao."')</script>";
            }
        break;

        case 'acompanhamentoLiveModeracao' :
            $ultimaVerificacao = filter_input(INPUT_POST, 'ultimaVerificacao', FILTER_SANITIZE_STRING);
            $idUltimaVerificacao = filter_input(INPUT_POST, 'idUltimaVerificacao', FILTER_SANITIZE_STRING);
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $unidadeMedida = retornaUnidadeMedidaEtapa($idEtapa);

            if ($resultado = retornaResultadosLive($ultimaVerificacao, $idEtapa, 'mod', $idUltimaVerificacao)){
                $i = 0;
                foreach($resultado as $result){
                    if ($i == 0) {
                        $verificarAposHorario = $result->DATA_HORA;
                        $idEtapaVerificada = $result->ID_ETAPA;
                        $i++;
                    }
                    if($ultimaVerificacao == ''){
                        if($result->EXCLUIDO == 'SIM'){
                            echo '<tr class="linhaLive bg-danger" id="linhaMod'.$result->id.'">';
                        }else{
                            echo '<tr class="linhaLive" id="linhaMod'.$result->id.'">';
                        }
                    }else{
                        if($result->EXCLUIDO == 'SIM'){
                            echo '<tr class="linhaLive excluido bg-danger" id="linhaMod'.$result->id.'">';
                        }else{
                            echo '<tr class="linhaLive invalid" id="linhaMod'.$result->id.'">';
                        }
                    }
                    //echo    '<td>'.$result->id.'</td>';
                    echo    '<td>..'.substr($result->COMPROVANTE,-17).'</td>';
                    //echo    '<td>'.date("d/m/Y H:i:s", strtotime($result -> DATA_HORA)).'</td>';
                    echo    '<td>'.date("H:i:s", strtotime($result -> DATA_HORA)).'</td>';
                    echo    '<td>'.$result->EQUIPE_NOME.'</td>';
                    echo    '<td>'.$result->NOME.'</td>';
                    echo    '<td>'.$result->ESPECIE_NOME.'</td>';
                    echo    '<td>'.$result->MODALIDADE_NOME.'</td>';
                    echo    '<td>'.$result->ISCA_NOME.'</td>';
                    //echo    '<td>'.$result->PONTO_PESAGEM.'</td>';
                    echo    '<td>'.number_format($result->PESO, 3, '.', ','). ' '.$unidadeMedida->abreviacao. '</td>';
                    echo    '<td>'.$result->OBS.'</td>';
                    echo    '<td>';
                    echo        '<div class="btn-group" role="group">';
                    echo            '<button type="button" onClick="moderaMedida(`aprova`, '.$result->id.')" class="btn btn-success btn-xs">Aprovado</button>';
                    echo            '<button type="button" onClick="moderaMedida(`reprova`, '.$result->id.')" class="btn btn-danger btn-xs">Reprovado</button>';
                    echo        '</div>';
                    echo    '</td>';
                    echo '</tr>';
                }
                echo "<script>$('#ultimaVerificacaoModeracao').val('".$verificarAposHorario."')</script>";
                echo "<script>$('#idEtapaModeracao').val('".$idEtapaVerificada."')</script>";
            }
        break;

        case 'informacoesEquipe' :
            $idEquipe = filter_input(INPUT_POST, 'idEquipe', FILTER_SANITIZE_STRING);
            if($resultado = retornaNomesEquipe($idEquipe)){
                echo json_encode($resultado);
            }
        break;

		case 'detalhesModalidade' :
			$idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
			if ($resultado = retornaDetalhesEtapaModalidade($idEtapa)){
				$i = 0;
				foreach($resultado as $result){
					echo '<tr>';
					echo '<td>'.$result->modalidade.'</td>';
					echo '<td>'.$result->total.'</td>';
					echo '</tr>';
				}
			}
        break;

        case 'criaBotoesEquipes':
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            if($resultado = retornaIdsEPeixesEquipes($idEtapa)){
                $i = 0;
                echo json_encode($resultado);
            }
        break;

        case 'detalhesModalidadeLiveNew2' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            if ($resultado = retornaDetalhesEtapaModalidade($idEtapa)){
                $i = 0;
                echo json_encode($resultado);
            }
        break;

        case 'detalhesModalidadeEquipeLiveNew2':
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $idEquipe = filter_input(INPUT_POST, 'idEquipe', FILTER_SANITIZE_STRING);
            if($resultado = retornaDetalhesEquipeModalidade($idEtapa, $idEquipe)){
                echo json_encode($resultado);
            }
        break;

        case 'retornaDetalhesModalidade' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            if ($resultados = retornaDetalhesEtapaModalidade($idEtapa)){

                foreach($resultados as $resultado){
                    $dados[] = array(
                        'modalidade' => $resultado -> modalidade,
                        'quantidade' => $resultado -> total
                    );
                }
                echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }
        break;

		case 'detalhesEspecie' :
			$idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
			if ($resultado = retornaDetalhesEtapaEspecie($idEtapa)){
				$i = 0;
				foreach($resultado as $result){
					echo '<tr>';
					echo '<td>'.$result->especie.'</td>';
					echo '<td>'.$result->total.'</td>';
					echo '</tr>';
				}
			}
		break;

        case 'detalhesEspecieLiveNew2' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            if ($resultado = retornaDetalhesEtapaEspecie($idEtapa)){
                $i = 0;
                echo json_encode($resultado);
            }
        break;

        case 'detalhesEspecieEquipeLiveNew2' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $idEquipe = filter_input(INPUT_POST, 'idEquipe', FILTER_SANITIZE_STRING);
            if ($resultado = retornaDetalhesEquipeEspecie($idEtapa, $idEquipe)){
                $i = 0;
                echo json_encode($resultado);
            }

        break;
        case 'detalhesIsca' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            if ($resultado = retornaDetalhesEtapaIsca($idEtapa)){
                $i = 0;
                foreach($resultado as $result){
                    echo '<tr>';
                    echo '<td>'.$result->isca.'</td>';
                    echo '<td>'.$result->total.'</td>';
                    echo '</tr>';
                }
            }
        break;

        case 'detalhesIscaLiveNew2' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            if ($resultado = retornaDetalhesEtapaIsca($idEtapa)){
                $i = 0;
                echo json_encode($resultado);
                /*
                foreach($resultado as $result){
                    echo '<tr>';
                    echo '<td>'.$result->isca.'</td>';
                    echo '<td>'.$result->total.'</td>';
                    echo '</tr>';
                }
                */
            }
        break;

        case 'detalhesIscaEquipeLiveNew2' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $idEquipe = filter_input(INPUT_POST, 'idEquipe', FILTER_SANITIZE_STRING);
            if ($resultado = retornaDetalhesEquipeIsca($idEtapa, $idEquipe)){
                $i = 0;
                echo json_encode($resultado);
                /*
                foreach($resultado as $result){
                    echo '<tr>';
                    echo '<td>'.$result->isca.'</td>';
                    echo '<td>'.$result->total.'</td>';
                    echo '</tr>';
                }
                */
            }
        break;

        case 'retornaDetalhesEspecie' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            if ($resultados = retornaDetalhesEtapaEspecie($idEtapa)){
                foreach($resultados as $resultado){
                    $dados[] = array(
                        'especie' => $resultado -> especie,
                        'quantidade' => $resultado -> total
                    );
                }
                echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }
        break;




        case 'retornaDetalhesIsca' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            if ($resultados = retornaDetalhesEtapaIsca($idEtapa)){
                foreach($resultados as $resultado){
                    $dados[] = array(
                        'isca' => $resultado -> isca,
                        'quantidade' => $resultado -> total
                    );
                }
                echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }
        break;

		case 'detalhesHora' :
			$idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
			if ($resultado = retornaDetalhesEtapaHora($idEtapa)){
				$i = 0;
				foreach($resultado as $result){
					echo '<tr>';
					echo '<td>'.$result->hora.'</td>';
					echo '<td>'.$result->total.'</td>';
					echo '</tr>';
				}
			}
		break;

        case 'detalhesHoraLiveNew2' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            if ($resultado = retornaDetalhesEtapaHora($idEtapa)){
                $i = 0;
                //echo "<script>console.log('" . json_encode($resultado) . "');</script>";
                echo json_encode($resultado);
                /*
                foreach($resultado as $result){
                    echo '<tr>';
                    echo '<td>'.$result->hora.'</td>';
                    echo '<td>'.$result->total.'</td>';
                    echo '</tr>';
                }
                */
            }
        break;

        case 'detalhesHoraEquipeLiveNew2' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $idEquipe = filter_input(INPUT_POST, 'idEquipe', FILTER_SANITIZE_STRING);
            if ($resultado = retornaDetalhesEquipeHora($idEtapa, $idEquipe)){
                $i = 0;
                //echo "<script>console.log('" . json_encode($resultado) . "');</script>";
                echo json_encode($resultado);
                /*
                foreach($resultado as $result){
                    echo '<tr>';
                    echo '<td>'.$result->hora.'</td>';
                    echo '<td>'.$result->total.'</td>';
                    echo '</tr>';
                }
                */
            }
        break;

        case 'retornaDetalhesHora' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            if ($resultados = retornaDetalhesEtapaHora($idEtapa)){
                foreach($resultados as $resultado){
                    $dados[] = array(
                        'hora' => $resultado -> hora,
                        'quantidade' => $resultado -> total
                    );
                }
                echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }
        break;

        case 'retornaDetalhesEstatisticasEquipes' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $idEquipe = filter_input(INPUT_POST, 'idEquipe', FILTER_SANITIZE_STRING);
            if ($resultados = retornaDetalhesEstatisticasEquipes($idEtapa)){
                foreach($resultados as $resultado){
                    $dados[] = array(
                        'descricao' => $resultado -> DESCRICAO,
                        'total' => $resultado -> TOTAL
                    );
                }
                echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }
        break;

        case 'detalhesQuadrantes' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $unidadeMedida = retornaUnidadeMedidaEtapa($idEtapa);
            if($unidadeMedida){
                $unidadeMedida = $unidadeMedida->abreviacao;
            }else{
                $unidadeMedida ='';
            }

            if ($resultado = retornaDetalhesQuadrantes($idEtapa)){
                $i = 0;
                foreach($resultado as $result){
                    echo '<tr>';
                    echo '<td>'.$result->QUADRANTE.'</td>';
                    echo '<td>'.$result->BATERIA.'</td>';
                    echo '<td>'.$result->QTD_PESAGENS.'</td>';
                    echo '<td>'.number_format(floatval(str_replace(",","",$result->TOTAL_PESAGENS)), 3, ',', '.').' '.$unidadeMedida.'</td>';
                    echo '</tr>';
                }
            }
        break;


        case 'detalhesEstatisticasEquipes' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);

            if ($resultado = retornaDetalhesEstatisticasEquipes($idEtapa)){
                $i = 0;
                foreach($resultado as $result){
                    echo '<tr>';
                    echo '<td>'.$result->DESCRICAO.'</td>';
                    echo '<td>'.$result->TOTAL.'</td>';
                    echo '</tr>';
                }
            }
        break;

		case 'listaEtapasParaComposicaoEquipes' :
			$idCampeonato = filter_input(INPUT_POST, 'idCampeonato', FILTER_SANITIZE_STRING);
			echo '<table class="table table-striped table-hover" id="tableListaEtapas" name="tableListaEtapas">';
			echo '<th><span class="glyphicon glyphicon-arrow-left" data-toggle="modal" role="button" aria-hidden="true" onclick="carregaCampeonatos()"></span> Selecione a etapa</th>';
			echo '<tbody>';
			if ($etapas = retornaEtapas('', $idCampeonato)){
				foreach ($etapas as $result){
					echo '<tr class="odd gradeX">';
					echo '<td class="col-md-10" style="cursor:pointer" onclick="carregaEquipes('.$result -> id .', '.$idCampeonato.', 0);return false;">'. $result -> DESCRICAO . ' <sup>'.date("d/m/Y", strtotime($result -> DATA_ETAPA)).'</sup></td>';
					echo '</tr>';
				}
			}else{
				echo '<tr class="odd gradeX">';
				echo '<td class="col-md-10" style="cursor:pointer" onclick="">Nenhuma etapa cadastrada para este campeonato</td>';
				echo '</tr>';
			}
			echo '</tbody>';
			echo '</table>';
		break;

		case 'alteraPerfilUsuario' :
			$idUsuario = filter_input(INPUT_POST, 'emailPerfilCodigo', FILTER_SANITIZE_STRING);
			$senhaUsuario = filter_input(INPUT_POST, 'senhaPerfilUsuario', FILTER_SANITIZE_STRING);
			if(updatePerfilUsuario($idUsuario, $senhaUsuario)){
				echo $idUsuario;
			}else{
				echo "Problema ao atualizar o perfil do usuário, contate o suporte";
			}
		break;

		case 'listaEtapasParaComposicao' :
			$idCampeonato = filter_input(INPUT_POST, 'idCampeonato', FILTER_SANITIZE_STRING);
			echo '<table class="table table-striped table-hover" id="tableListaEtapas" name="tableListaEtapas">';
			echo '<th><span class="glyphicon glyphicon-arrow-left" data-toggle="modal" role="button" aria-hidden="true" onclick="carregaCampeonatos()"></span> Selecione a etapa</th>';
			echo '<tbody>';
			if ($etapas = retornaEtapas('', $idCampeonato)){
				foreach ($etapas as $result){
					echo '<tr class="odd gradeX">';
					echo '<td class="col-md-10" style="cursor:pointer" onclick="abreComposicaoEtapasSeleciona('.$result -> id .');return false;">'. $result -> DESCRICAO . ' <sup>'.date("d/m/Y", strtotime($result -> DATA_ETAPA)).'</sup></td>';
                    echo '</tr>';
				}
			}else{
				echo '<tr class="odd gradeX">';
				echo '<td class="col-md-10" style="cursor:pointer" onclick="">Nenhuma etapa cadastrada para este campeonato</td>';
				echo '</tr>';
			}
			echo '</tbody>';
			echo '</table>';
		break;

        case 'listaRaiasParaComposicao' :
            $idCampeonato = filter_input(INPUT_POST, 'idCampeonato', FILTER_SANITIZE_STRING);
            echo '<table class="table table-striped table-hover" id="tableListaEtapas" name="tableListaEtapas">';
            echo '<th><span class="glyphicon glyphicon-arrow-left" data-toggle="modal" role="button" aria-hidden="true" onclick="carregaCampeonatos()"></span> Selecione a etapa</th>';
            echo '<tbody>';
            if ($etapas = retornaEtapas('', $idCampeonato)){
                foreach ($etapas as $result){
                    echo '<tr class="odd gradeX">';
                    echo '<td class="col-md-10" style="cursor:pointer" onclick="abreComposicaoRaiasSeleciona('.$result -> id .');return false;">'. $result -> DESCRICAO . ' <sup>'.date("d/m/Y", strtotime($result -> DATA_ETAPA)).'</sup></td>';
                    echo '</tr>';
                }
            }else{
                echo '<tr class="odd gradeX">';
                echo '<td class="col-md-10" style="cursor:pointer" onclick="">Nenhuma etapa cadastrada para este campeonato</td>';
                echo '</tr>';
            }
            echo '</tbody>';
            echo '</table>';
        break;

		case 'listaCampeonatosParaComposicao' :
			echo '<table class="table table-striped table-hover" id="tableListaEtapas" name="tableListaEtapas">';
			echo '<th>Selecione o campeonato</th>';
			echo '<tbody>';
			if ($campeonatos = retornaCampeonatos('', 'A')){
				foreach ($campeonatos as $result){
					echo '<tr class="odd gradeX">';
					echo '<td class="col-md-10" style="cursor:pointer" onclick="carregaEtapas('.$result -> id .');return false;">'. $result -> descricao . '</td>';
					echo '</tr>';
				}
			}else{
				echo '<tr class="odd gradeX">';
				echo '<td class="col-md-10" style="cursor:pointer" onclick="">Nenhuma etapa cadastrada para este campeonato</td>';
				echo '</tr>';
			}
			echo '</tbody>';
			echo '</table>';
		break;

		case 'listaEquipesParaComposicao' :
			$idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
			$idCampeonato = filter_input(INPUT_POST, 'idCampeonato', FILTER_SANITIZE_STRING);
            $idPulseira = filter_input(INPUT_POST, 'idPulseira', FILTER_SANITIZE_STRING);
            $nomeCompetidor = filter_input(INPUT_POST, 'nomeCompetidor', FILTER_SANITIZE_STRING);
            echo '<div class="form-group">
                    <div class="row">
                        <div class="col-lg-6">
                        <b>Filtrar Competidor</b>
                            <div class="input-group">
                                <input type="text" name="filtraNomeCompetidor" id="filtraNomeCompetidor" class="form-control" value="'.$nomeCompetidor .'">
                                <span class="input-group-addon" onclick="carregaEquipes('.$idEtapa.', '.$idCampeonato.', 1, \'\');return false;""><span class="glyphicon glyphicon-search"></span></span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                        <b>Filtrar Pulseira</b>
                            <div class="input-group">
                                <input type="text" name="filtraPulseira" id="filtraPulseira" class="form-control" value="'.$idPulseira .'">
                                <span class="input-group-addon" onclick="carregaEquipes('.$idEtapa.', '.$idCampeonato.', 1, \'\');return false;""><span class="glyphicon glyphicon-search"></span></span>
                            </div>
                        </div>
                    </div>
                </div>';
			echo '<table class="table table-striped table-hover" id="tableListaEtapas" name="tableListaEtapas">';
			echo '<tr><th colspan="4" align="left"><span class="glyphicon glyphicon-arrow-left" data-toggle="modal" role="button"  onclick="carregaEtapas('.$idCampeonato.')"></span> Selecione a equipe</th></tr>';
			echo '<tbody>';
            $i = 1;

			if ($equipes = retornaEquipes('', '', $idEtapa, $idPulseira, $nomeCompetidor)){
				foreach ($equipes as $result){
					//$quantidade = retornaNomesEquipe($result -> id);
					$quantidade = retornaQtdNaEquipe($result -> id);
                    $zerados = retornaIdentificacaoEquipe($result -> id);

                    if($result -> PATROCINADOR == ''){
                        $patrocinador = '<span class="btn btn-xs" id="btnPatrocinador'.$result -> id.'" onclick="informaPatrocinador('. $result -> id .'); event.stopPropagation();" ><span class="glyphicon glyphicon-piggy-bank" aria-hidden="true"></span> Patrocinador</span> ';
                    }else{
                        $patrocinador = '<span class="btn btn-xs btn-primary" id="btnPatrocinador'.$result -> id.'" onclick="informaPatrocinador('. $result -> id .', \''.$result -> PATROCINADOR.'\'); event.stopPropagation();"><span class="glyphicon glyphicon-piggy-bank" aria-hidden="true"></span> '.$result -> PATROCINADOR.'</span> ';
                    }

					if($quantidade -> PARTICIPANTES == 0){
						$contador = '<span class="pull-right label label-danger">'.$quantidade -> PARTICIPANTES.'</span>';
					}else{
						$contador = '<span class="pull-right label label-primary">'.$quantidade -> PARTICIPANTES.'</span>';
					};
					if($zerados->zerados > 0){
						$semCodPulseira = '<span class="pull-right glyphicon glyphicon-qrcode " style="color:#ff0000"></span>';
					}else{
						$semCodPulseira = '';
					};
					echo '<tr class="odd gradeX">';
					echo '<td class="col-md-8" style="cursor:pointer" onclick="abreComposicaoEquipesSeleciona('.$idEtapa.', '.$result -> id .');return false;">'. $i++ . ' - ' . $result -> equipe .'</td>';
                    echo '<td class="col-md-2">'.$patrocinador.'</td>';
                    echo '<td class="col-md-1" style="padding-top: 10px;padding-right: 1px;padding-bottom: 1px;padding-left: 1px;">'.$semCodPulseira.'</td>';
                    echo '<td class="col-md-1" style="padding-top: 10px;padding-right: 1px;padding-bottom: 1px;padding-left: 1px;">'.$contador.'</td>';
					echo '</tr>';
				}
			}else{
				echo '<tr class="odd gradeX">';
				echo '<td class="col-md-10" style="cursor:pointer" onclick="">Nenhuma equipe cadastrada para esta etapa</td>';
				echo '</tr>';
			}
			echo '</tbody>';
			echo '</table>';
		break;

		case 'carregaEquipesLancamentosResultado' :
			$equipeNome = '';
			$idCampeonato = 0;
			$idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);

			if ($resultado = retornaEquipes($equipeNome, $idCampeonato, $idEtapa, 0, '')){
				echo '<option value="0">Todos</option>';
				foreach($resultado as $result){
					echo '<option value="'.$result->id.'">'.$result->equipe.'</option>';
				}
			}else{
				echo '<option value="-1">Nenhuma equipe cadastrada</option>';

			}
		break;

		case 'carregaEquipes' :
			$equipeNome = filter_input(INPUT_POST, 'filtroEquipeNome', FILTER_SANITIZE_STRING);
			$idCampeonato = filter_input(INPUT_POST, 'filtroEquipeCampeonato', FILTER_SANITIZE_STRING);
			$idEtapa = filter_input(INPUT_POST, 'filtroEquipeEtapa', FILTER_SANITIZE_STRING);

			if ($resultado = retornaEquipes($equipeNome, $idCampeonato, $idEtapa, 0, '')){
				foreach ($resultado as $result){
					$dados[] = array(
						'id_equipe' => trim($result -> id),
						'campeonato' => trim($result -> campeonato),
						'etapa' => trim($result -> etapa),
						'equipe' => trim($result -> equipe),
						'acoes' => '<div class="btn-group btn-group-xs" role="group" aria-label="...">'.
										'<button type="button" class="btn btn-warning" onclick="abreModalEditaEquipe('.$result -> id.')"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button>'.
										'<button type="button" class="btn btn-danger" onclick="naoImplementado()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>'.
									'</div>'
					);
				}
				echo json_encode(array('data' => $dados));
			}else{
                echo json_encode(array('data' => []));
			}
		break;

		case 'carregaCompetidores' :
			$competidoresNome = filter_input(INPUT_POST, 'filtroCompetidorNome', FILTER_SANITIZE_STRING);
            $competidoresApelido = filter_input(INPUT_POST, 'filtroCompetidorApelido', FILTER_SANITIZE_STRING);
			$competidoresCPF = filter_input(INPUT_POST, 'filtroCompetidorCPF', FILTER_SANITIZE_STRING);
			if ($resultado = retornaCompetidores($competidoresNome, $competidoresApelido, $competidoresCPF)){
				foreach ($resultado as $result){
					$dados[] = array(
						'id_competidor' => trim($result -> ID),
						'nome_competidor' => trim($result -> NOME),
						'tam_camiseta' => trim($result -> TAMANHO_CAMISETA),
						'acoes' => '<div class="btn-group btn-group-xs" role="group" aria-label="...">'.
										'<button type="button" class="btn btn-warning" onclick="abreModalEditaCompetidor('.$result -> ID.')"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></button>'.
										'<button type="button" class="btn btn-danger" onclick="naoImplementado()"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>'.
									'</div>'
					);
				}
				echo json_encode(array('data' => $dados));

			}else{
				echo json_encode(array('data' => []));
			}
		break;

        case 'retornaDadosEquipe' :
            $idEquipe = filter_input(INPUT_POST, 'idEquipe', FILTER_SANITIZE_STRING);

            if ($resultado = retornaDadosEquipe($idEquipe)){
                foreach ($resultado as $result){
                    $dados[] = array(
                        'id_competidor' => trim($result -> ID_COMPETIDOR),
                        'nome' => trim($result -> NOME),
                        'apelido' => trim($result -> APELIDO),
                        'idade' => trim($result -> IDADE),
                        'cidade' => trim($result -> CIDADE),
                        'uf' => trim($result -> UF),
                        'data_nascimento' => trim($result -> DATA_NASCIMENTO),
                        'historia' => trim($result -> HISTORIA),
                        'curiosidades' => trim($result -> CURIOSIDADES),
                        'patrocinador' => trim($result -> PATROCINADOR)
                    );
                }
                echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }
        break;

        case 'carregaContatosAtualizados' :
            $dataDe = filter_input(INPUT_POST, 'dataDe', FILTER_SANITIZE_STRING);
            $dataAte = filter_input(INPUT_POST, 'dataAte', FILTER_SANITIZE_STRING);

            if ($resultado = retornaQtdContatosAtualizados($dataDe, $dataAte)){
                foreach ($resultado as $result){
                    $dados[] = array(
                        'total' => trim($result -> TOTAL),
                        'data' => trim(date("d/m/Y", strtotime($result -> DATA_MODIFICACAO))),
                        'uf' => trim($result -> UF)
                    );
                }
                echo json_encode(array('data' => $dados));
            }else{
                echo json_encode(array('data' => []));
            }
		break;

		case 'login' :
		//Ajax de login
		$login = filter_input(INPUT_POST, 'login', FILTER_SANITIZE_STRING);
		$senha = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING);
		$lembrar = filter_input(INPUT_POST, 'lembrar', FILTER_SANITIZE_NUMBER_INT);

		if (login($login, $senha)) {
			if (confirmado($login)) {
				// Cria a session login e senha corretos
				$_SESSION['logado_nome'] = pegalogin($login) -> nome;
				$_SESSION['logado_id'] = pegalogin($login) -> id;
				echo "correto";
			}else{
				echo "desativado";
			}
		} else {

			$dados = pegalogin($login);
			if ($dados) {
				echo "pass";
			}  else {
				echo "user";
			}
		}
		break;

		case 'insereEspecie' :
			$nomeEspecie = filter_input(INPUT_POST, 'newCadNomeEspecie', FILTER_SANITIZE_STRING);
			$prioridadeEspecie = filter_input(INPUT_POST, 'newCadPrioridadeEspecie', FILTER_SANITIZE_STRING);
			if(insertEspecie($nomeEspecie, $prioridade)){
				echo "ok";
			}else{
				echo "erro";
			}
		break;

		case 'insereModalidade' :
			$nomeModalidade = filter_input(INPUT_POST, 'newCadNomeModalidade', FILTER_SANITIZE_STRING);
			if(insertModalidade($nomeModalidade)){
				echo "ok";
			}else{
				echo "erro";
			}
		break;

		case 'insereIsca' :
			$nomeIsca = filter_input(INPUT_POST, 'newCadNomeIsca', FILTER_SANITIZE_STRING);
			if(insertIsca($nomeIsca)){
				echo "ok";
			}else{
				echo "erro";
			}
		break;

        case 'insereColetor' :
            $nomeColetor= filter_input(INPUT_POST, 'newCadNomeColetor', FILTER_SANITIZE_STRING);
            $serialColetor= filter_input(INPUT_POST, 'newCadSerialColetor', FILTER_SANITIZE_STRING);
            if(insertColetor($nomeColetor, $serialColetor)){
                echo "ok";
            }else{
                echo "erro";
            }
        break;

        case 'insereFiscal' :
            $nomeFiscal= filter_input(INPUT_POST, 'newCadNomeFiscal', FILTER_SANITIZE_STRING);
            $cpfFiscal= filter_input(INPUT_POST, 'newCadCPFFiscal', FILTER_SANITIZE_STRING);
            if(insertFiscal($nomeFiscal, $cpfFiscal)){
                echo "ok";
            }else{
                echo "erro";
            }
        break;

		case 'insereCompetidor' :
			$nomeCompetidor = filter_input(INPUT_POST, 'newCadNomeCompetidor', FILTER_SANITIZE_STRING);
			$tamanhoCompetidor = filter_input(INPUT_POST, 'newCadTamanhoCompetidor', FILTER_SANITIZE_STRING);
			$apelidoCompetidor = filter_input(INPUT_POST, 'newCadApelidoCompetidor', FILTER_SANITIZE_STRING);
			$CPFCompetidor = filter_input(INPUT_POST, 'newCadCPFCompetidor', FILTER_SANITIZE_STRING);
            $endereco = filter_input(INPUT_POST, 'newCadEnderecoCompetidor', FILTER_SANITIZE_STRING);
            $num = filter_input(INPUT_POST, 'newCadNumeroEndCompetidor', FILTER_SANITIZE_STRING);
            $complemento = filter_input(INPUT_POST, 'newCadComplementoCompetidor', FILTER_SANITIZE_STRING);
            $bairro = filter_input(INPUT_POST, 'newCadBairroCompetidor', FILTER_SANITIZE_STRING);
            $cidade = filter_input(INPUT_POST, 'newCadCidadeCompetidor', FILTER_SANITIZE_STRING);
            $uf = filter_input(INPUT_POST, 'newCadUFCompetidor', FILTER_SANITIZE_STRING);
            $pais = 'Brasil';
            $cep = filter_input(INPUT_POST, 'newCadCEPCompetidor', FILTER_SANITIZE_STRING);

            $celular = filter_input(INPUT_POST, 'newCadTelefoneCompetidor', FILTER_SANITIZE_STRING);
            $email = filter_input(INPUT_POST, 'newCadEmailCompetidor', FILTER_SANITIZE_STRING);
            $sexo = filter_input(INPUT_POST, 'newCadSexoCompetidor', FILTER_SANITIZE_STRING);
            $data_nascimento = filter_input(INPUT_POST, 'newCadNascimentoCompetidor', FILTER_SANITIZE_STRING);
            // reverse data_nascimento
            $data_nascimento = implode("-",array_reverse(explode("/",$data_nascimento)));

            $modelagem = filter_input(INPUT_POST, 'newCadModelagemCompetidor', FILTER_SANITIZE_STRING);

			if(insertCompetidor($nomeCompetidor, $tamanhoCompetidor, $apelidoCompetidor, $CPFCompetidor, $endereco, $num, $complemento, $bairro, $cidade, $uf, $pais, $cep, $celular, $email, $sexo, $data_nascimento, $modelagem)){
				echo "ok";
			}else{
				echo "erro";
			}
		break;

		case 'insereEtapa' :
			$nomeEtapa = filter_input(INPUT_POST, 'newCadNomeEtapa', FILTER_SANITIZE_STRING);
			$nomeEtapaApp = filter_input(INPUT_POST, 'newCadNomeEtapaApp', FILTER_SANITIZE_STRING);
			$newCadCampeonatoEtapa = filter_input(INPUT_POST, 'newCadCampeonatoEtapa', FILTER_SANITIZE_STRING);
			$dataEtapa = filter_input(INPUT_POST, 'historicoDataRecontato', FILTER_SANITIZE_STRING);
			$imagemEtapa = filter_input(INPUT_POST, 'newCadImagemEtapa', FILTER_SANITIZE_STRING);
			$peso_minimo = filter_input(INPUT_POST, 'newCadPesoMinimo', FILTER_SANITIZE_STRING);
			$unidadeMedida = filter_input(INPUT_POST, 'newCadUnidadeMedidaEtapa', FILTER_SANITIZE_STRING);
            $moderacao = filter_input(INPUT_POST, 'newCadModeracaoEtapa', FILTER_SANITIZE_STRING);
			$filedata = base64_decode($imagemEtapa);
			$fileName = 'temp.jpg';
			file_put_contents($fileName, $filedata);

			$im = imagecreatefromjpeg($fileName);

			if($im && imagefilter($im, IMG_FILTER_GRAYSCALE) && imagefilter($im, IMG_FILTER_CONTRAST, -100))
			{
				imagejpeg($im, 'temp-pb.jpg');
			}
			else
			{
				echo 'erro-img';
			}

			$imagedata = file_get_contents("temp-pb.jpg");
			$imagemEtapa_pb = base64_encode($imagedata);

			imagedestroy($im);

			if(insertEtapa($nomeEtapa, $nomeEtapaApp, $newCadCampeonatoEtapa, $dataEtapa, $imagemEtapa, $imagemEtapa_pb, $peso_minimo, $unidadeMedida, $moderacao)){
				echo "ok";
			}else{
				echo "erro";
			}
		break;


		case 'insereCampeonato' :
			$nomeCampeonato = filter_input(INPUT_POST, 'newCadNomeCampeonado', FILTER_SANITIZE_STRING);
			$nomeCampeonatoApp = filter_input(INPUT_POST, 'newCadNomeCampeonadoApp', FILTER_SANITIZE_STRING);
			$imagemCampeonato = filter_input(INPUT_POST, 'newCadImagemCampeonado', FILTER_SANITIZE_STRING);

			$filedata = base64_decode($imagemCampeonato);
			$fileName = 'temp.jpg';
			file_put_contents($fileName, $filedata);

			$im = imagecreatefromjpeg($fileName);

			if($im && imagefilter($im, IMG_FILTER_GRAYSCALE) && imagefilter($im, IMG_FILTER_CONTRAST, -100))
			{
				imagejpeg($im, 'temp-pb.jpg');
			}
			else
			{
				echo 'erro-img';
			}

			$imagedata = file_get_contents("temp-pb.jpg");
			$imagemCampeonato_pb = base64_encode($imagedata);

			imagedestroy($im);

			if(insertCampeonato($nomeCampeonato,$nomeCampeonatoApp, $imagemCampeonato, $imagemCampeonato_pb)){
				echo "ok";
			}else{
				echo "erro";
			}
		break;

		case 'editaCampeonato' :
			$nomeCampeonato = filter_input(INPUT_POST, 'editCadNomeCampeonato', FILTER_SANITIZE_STRING);
			$nomeCampeonatoApp = filter_input(INPUT_POST, 'editCadNomeCampeonatoApp', FILTER_SANITIZE_STRING);
			$imagemCampeonato = filter_input(INPUT_POST, 'editCadImagemCampeonado', FILTER_SANITIZE_STRING);
			$idCampeonato = filter_input(INPUT_POST, 'editCadIdCampeonato', FILTER_SANITIZE_STRING);

			$filedata = base64_decode($imagemCampeonato);
			$fileName = 'temp.jpg';
			file_put_contents($fileName, $filedata);

			$im = imagecreatefromjpeg($fileName);

			//if($im && imagefilter($im, IMG_FILTER_GRAYSCALE) && imagefilter($im, IMG_FILTER_CONTRAST, -100))
			if($im && imagefilter($im, IMG_FILTER_GRAYSCALE))
			{
				imagejpeg($im, 'temp-pb.jpg');
			}
			else
			{
				echo 'erro-img';
			}

			$imagedata = file_get_contents("temp-pb.jpg");
			$imagemCampeonato_pb = base64_encode($imagedata);

			imagedestroy($im);

			if(updateCampeonato($nomeCampeonato, $nomeCampeonatoApp, $imagemCampeonato, $imagemCampeonato_pb, $idCampeonato)){
				echo "ok";
			}else{
				echo "erro";
			}
		break;

		case 'editaEspecie' :
			$idEspecie = filter_input(INPUT_POST, 'editCadIdEspecie', FILTER_SANITIZE_STRING);
			$nomeEspecie = filter_input(INPUT_POST, 'editCadNomeEspecie', FILTER_SANITIZE_STRING);
			$prioridadeEspecie = filter_input(INPUT_POST, 'editCadPrioridadeEspecie', FILTER_SANITIZE_STRING);
			if(updateEspecie($nomeEspecie, $idEspecie, $prioridadeEspecie)){
				echo "ok";
			}else{
				echo "erro";
			}
		break;

		case 'editaEquipe' :
			$idEquipe = filter_input(INPUT_POST, 'editCadIdEquipe', FILTER_SANITIZE_STRING);
			$nomeEquipe = filter_input(INPUT_POST, 'editCadNomeEquipe', FILTER_SANITIZE_STRING);
			$idEtapa = filter_input(INPUT_POST, 'editCadEtapaEquipe', FILTER_SANITIZE_STRING);

			if(updateEquipe($idEquipe, $nomeEquipe, $idEtapa)){
				echo "ok";
			}else{
				echo "erro";
			}
		break;

		case 'insereEquipe' :
			$nomeEquipe = filter_input(INPUT_POST, 'newCadNomeEquipe', FILTER_SANITIZE_STRING);
			$idEtapa = filter_input(INPUT_POST, 'newCadEtapaEquipe', FILTER_SANITIZE_STRING);
			if(insertEquipe($nomeEquipe, $idEtapa)){
				echo "ok";
			}else{
				echo "erro";
			}
		break;

		case 'editaModalidade' :
			$idModalidade = filter_input(INPUT_POST, 'editCadIdModalidade', FILTER_SANITIZE_STRING);
			$nomeModalidade = filter_input(INPUT_POST, 'editCadNomeModalidade', FILTER_SANITIZE_STRING);
			if(updateModalidade($nomeModalidade, $idModalidade)){
				echo "ok";
			}else{
				echo "erro";
			}
		break;

		case 'editaIsca' :
			$idIsca = filter_input(INPUT_POST, 'editCadIdIsca', FILTER_SANITIZE_STRING);
			$nomeIsca = filter_input(INPUT_POST, 'editCadNomeIsca', FILTER_SANITIZE_STRING);
			if(updateIsca($nomeIsca, $idIsca)){
				echo "ok";
			}else{
				echo "erro";
			}
		break;

        case 'editaColetor' :
            $idColetor= filter_input(INPUT_POST, 'editCadIdColetor', FILTER_SANITIZE_STRING);
            $nomeColetor = filter_input(INPUT_POST, 'editCadNomeColetor', FILTER_SANITIZE_STRING);
            $serialColetor = filter_input(INPUT_POST, 'editCadSerialColetor', FILTER_SANITIZE_STRING);
            if(updateColetor($nomeColetor, $serialColetor, $idColetor)){
                echo "ok";
            }else{
                echo "erro";
            }
        break;

        case 'editaFiscal' :
            $idFiscal= filter_input(INPUT_POST, 'editCadIdFiscal', FILTER_SANITIZE_STRING);
            $nomeFiscal = filter_input(INPUT_POST, 'editCadNomeFiscal', FILTER_SANITIZE_STRING);
            $cpfFiscal = filter_input(INPUT_POST, 'editCadCPFFiscal', FILTER_SANITIZE_STRING);
            if(updateFiscal($nomeFiscal, $cpfFiscal, $idFiscal)){
                echo "ok";
            }else{
                echo "erro";
            }
        break;


        case 'moderacaoMedidas' :
            $idPesagem= filter_input(INPUT_POST, 'idPesagem', FILTER_SANITIZE_STRING);
            $idUsuario = $_SESSION['logado_id'];
            $status = filter_input(INPUT_POST, 'statusModeracao', FILTER_SANITIZE_STRING);
            $obs = filter_input(INPUT_POST, 'obs', FILTER_SANITIZE_STRING);
            if(updatePesagensModeracao($idPesagem, $idUsuario, $status, $obs)){
                echo "ok";
            }else{
                echo "erro";
            }
        break;

		case 'editaCompetidor' :
			$idCompetidor = filter_input(INPUT_POST, 'editCadIdCompetidor', FILTER_SANITIZE_STRING);
			$nomeCompetidor = filter_input(INPUT_POST, 'editCadNomeCompetidor', FILTER_SANITIZE_STRING);
			$tamanhoCompetidor = filter_input(INPUT_POST, 'editCadTamanhoCompetidor', FILTER_SANITIZE_STRING);
			$apelidoCompetidor = filter_input(INPUT_POST, 'editCadApelidoCompetidor', FILTER_SANITIZE_STRING);
			$cpfCompetidor = filter_input(INPUT_POST, 'editCadCPFCompetidor', FILTER_SANITIZE_STRING);
            $endereco = filter_input(INPUT_POST, 'editCadEnderecoCompetidor', FILTER_SANITIZE_STRING);
            $num = filter_input(INPUT_POST, 'editCadNumeroEndCompetidor', FILTER_SANITIZE_STRING);
            $complemento = filter_input(INPUT_POST, 'editCadComplementoCompetidor', FILTER_SANITIZE_STRING);
            $bairro = filter_input(INPUT_POST, 'editCadBairroCompetidor', FILTER_SANITIZE_STRING);
            $cidade = filter_input(INPUT_POST, 'editCadCidadeCompetidor', FILTER_SANITIZE_STRING);
            $uf = filter_input(INPUT_POST, 'editCadUFCompetidor', FILTER_SANITIZE_STRING);
            $pais = 'Brasil';
            $cep = filter_input(INPUT_POST, 'editCadCEPCompetidor', FILTER_SANITIZE_STRING);
            $id_usuario_modificacao = $_SESSION['logado_id'];

            $celular = filter_input(INPUT_POST, 'editCadTelefoneCompetidor', FILTER_SANITIZE_STRING);
            $email = filter_input(INPUT_POST, 'editCadEmailCompetidor', FILTER_SANITIZE_STRING);
            $sexo = filter_input(INPUT_POST, 'editCadSexoCompetidor', FILTER_SANITIZE_STRING);
            $data_nascimento = filter_input(INPUT_POST, 'editCadNascimentoCompetidor', FILTER_SANITIZE_STRING);
            // reverse data_nascimento
            $data_nascimento = implode("-",array_reverse(explode("/",$data_nascimento)));
            
            $modelagem = filter_input(INPUT_POST, 'editCadModelagemCompetidor', FILTER_SANITIZE_STRING);

			if(updateCompetidor($idCompetidor, $nomeCompetidor, $tamanhoCompetidor, $apelidoCompetidor, $cpfCompetidor, $endereco, $num, $complemento, $bairro, $cidade, $uf, $pais, $cep, $id_usuario_modificacao, $celular, $email, $sexo, $data_nascimento, $modelagem)){
				echo "ok";
			}else{
				echo "erro";
			}
		break;

		case 'editaEtapa' :
			$idEtapa = filter_input(INPUT_POST, 'editCadIdEtapa', FILTER_SANITIZE_STRING);
			$nomeEtapa = filter_input(INPUT_POST, 'editCadNomeEtapa', FILTER_SANITIZE_STRING);
			$nomeEtapaApp = filter_input(INPUT_POST, 'editCadNomeEtapaApp', FILTER_SANITIZE_STRING);
			$idCadCampeonato = filter_input(INPUT_POST, 'editCadCampeonatoEtapa', FILTER_SANITIZE_STRING);
			$dataEtapa = filter_input(INPUT_POST, 'editDataEtapa', FILTER_SANITIZE_STRING);
			$peso_minimo = filter_input(INPUT_POST, 'editCadPesoMinimo', FILTER_SANITIZE_STRING);
			$unidadeMedida = filter_input(INPUT_POST, 'editCadUnidadeMedidaEtapa', FILTER_SANITIZE_STRING);
            $moderacao = filter_input(INPUT_POST, 'editCadModeracaoEtapa', FILTER_SANITIZE_STRING);
			$imagemEtapa = filter_input(INPUT_POST, 'editCadImagemEtapa', FILTER_SANITIZE_STRING);
            $qtdBaterias = filter_input(INPUT_POST, 'editCadQtdBaterias', FILTER_SANITIZE_STRING);
            $datasEtapa = filter_input(INPUT_POST, 'editCadDatasEtapa', FILTER_SANITIZE_STRING);
            $peixeAlvo = filter_input(INPUT_POST, 'editPeixeAlvoEtapa', FILTER_SANITIZE_STRING);
            $tipoClassificacao = filter_input(INPUT_POST, 'editCadTipoClassificacaoEtapa', FILTER_SANITIZE_STRING);
            $uf_realizacao = filter_input(INPUT_POST, 'editCadUFEtapa', FILTER_SANITIZE_STRING);

            //die($peixeAlvo);

			$filedata = base64_decode($imagemEtapa);
			$fileName = 'temp.jpg';
			file_put_contents($fileName, $filedata);

			$im = imagecreatefromjpeg($fileName);

			if($im && imagefilter($im, IMG_FILTER_GRAYSCALE)){
				imagejpeg($im, 'temp-pb.jpg');
			}else{
				echo 'erro-img';
			}

			$imagedata = file_get_contents("temp-pb.jpg");
			$imagemEtapa_pb = base64_encode($imagedata);

			imagedestroy($im);

            if(updatePeixesAlvo($idEtapa, $peixeAlvo)){
                if(updateEtapa($nomeEtapa, $nomeEtapaApp, $idCadCampeonato, $dataEtapa, $idEtapa, $imagemEtapa, $imagemEtapa_pb, $peso_minimo, $unidadeMedida, $moderacao, $qtdBaterias, $datasEtapa, $tipoClassificacao, $uf_realizacao)){
                    echo "ok";
                }else{
                    echo 'erro';
                }
            }else{
                echo 'erro';
            }

		break;

        case 'publicRetornaEquipes' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $mostraNome = 'apelido_sozinho';
            if ($equipes = retornaEquipes('', 0, $idEtapa, 0, '')){
                foreach ($equipes as $resultEquipe){
                    $competidores = retornaNomesEquipe($resultEquipe -> id);
                    $nomes = '';

                    foreach($competidores as $result){
                        switch ($mostraNome) {
                            case 'apelido_sozinho':
                                $nomes .= ' - '.($result -> apelido);
                                break;
                            case 'apelido':
                                $nomes .= ' - <b>'.($result -> apelido).'</b> ('.$result -> primeiro_nome.')';
                                break;
                            case 'completo':
                                $nomes .= ' - '.($result -> competidor);
                                break;
                            case 'primeiro':
                                $nomes .= ' - '.($result -> primeiro_nome);
                                break;
                        }
                    }

                    $dados[] = array(
                        'id_equipe' => $resultEquipe -> id,
                        'equipe' => $resultEquipe -> equipe,
                        'competidores' => $nomes
                    );
                }

                echo json_encode(array('data' => $dados));

            }else{
                echo json_encode(array('data' => []));
            }
        break;

        case 'publicRetornaRanking' :
            $idEtapa = filter_input(INPUT_POST, 'idEtapa', FILTER_SANITIZE_STRING);
            $mostraNome = 'apelido_sozinho';


            if ($resultado = retornaResultadoEtapaMaiorPeixe($idEtapa, '', 0)){

                $competidores = retornaNomesEquipe($resultado->id_equipe);
                $nomes = '';
                foreach($competidores as $result){
                    if($result->id_competidor == $resultado -> ID_CAD_COMPETIDOR){
                        switch ($mostraNome) {
                            case 'apelido_sozinho':
                                $nomes .= ' - <b>'.($result -> apelido).'</b>';
                                break;
                            case 'apelido':
                                $nomes .= ' - <b>'.$result -> apelido.' ('.$result -> primeiro_nome.')</b>';
                                break;
                            case 'completo':
                                $nomes .= ' - <b>'.$result -> competidor.'</b>';
                                break;
                            case 'primeiro':
                                $nomes .= ' - <b>'.$result -> primeiro_nome.'</b>';
                                break;
                        }
                    }else{
                        switch ($mostraNome) {
                            case 'apelido_sozinho':
                                $nomes .= ' - '.($result -> apelido);
                                break;
                            case 'apelido':
                                $nomes .= ' - '.$result -> apelido.' ('.$result -> primeiro_nome .')';
                                break;
                            case 'completo':
                                $nomes .= ' - '.$result -> competidor;
                                break;
                            case 'primeiro':
                                $nomes .= ' - '.$result -> primeiro_nome;
                                break;
                        }
                    }
                }

                $dados[] = array(
                    'posicao' => 'MP',
                    'id_equipe' => $resultado -> id_equipe,
                    'equipe' => $resultado -> equipe,
                    'competidores' => $nomes,
                    'peso' => $resultado -> peso
                );
            }


            if ($equipes = retornaRankingPublico($idEtapa)){
                foreach ($equipes as $resultEquipe){
                    $competidores = retornaNomesEquipe($resultEquipe -> id_equipe);
                    $nomes = '';

                    foreach($competidores as $result){
                        switch ($mostraNome) {
                            case 'apelido_sozinho':
                                $nomes .= ' - '.($result -> apelido);
                                break;
                            case 'apelido':
                                $nomes .= ' - <b>'.($result -> apelido).'</b> ('.$result -> primeiro_nome.')';
                                break;
                            case 'completo':
                                $nomes .= ' - '.($result -> competidor);
                                break;
                            case 'primeiro':
                                $nomes .= ' - '.($result -> primeiro_nome);
                                break;
                        }
                    }

                    $dados[] = array(
                        'posicao' => $resultEquipe -> posicao,
                        'id_equipe' => $resultEquipe -> id_equipe,
                        'equipe' => $resultEquipe -> equipe,
                        'competidores' => $nomes,
                        'peso' => $resultEquipe -> peso
                    );
                }

                echo json_encode(array('data' => $dados));

            }else{
                echo json_encode(array('data' => []));
            }
        break;


		default :
			echo "erroGeral";
			break;
		}
		ob_end_flush();
	?>
