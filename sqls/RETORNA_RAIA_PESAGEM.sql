--**************************************************************************
--     Table: 
--    Author: 
--      Date: 
--
-- Description:
--
-- 
-- 
--**************************************************************************

CREATE DEFINER=`brasileiroempe02`@`%` FUNCTION `brasileiroempe02`.`RETORNA_RAIA_PESAGEM`(`ID_PESAGEM` int ) RETURNS int(11)
BEGIN
	DECLARE ID_CAD_EQUIPE int;
	DECLARE RAIA_INICIAL int;
	DECLARE QTD_BATERIAS INTEGER;
	DECLARE ID_CAD_TIPO_ORG_RAIA INTEGER;
	DECLARE exit_loop BOOLEAN; 
	DECLARE exit_count INTEGER; 
	DECLARE RAIA INTEGER;
	DECLARE BATERIA INTEGER;
	
	SET exit_loop = FALSE;
	SET exit_count = 0;
	SELECT
		( SELECT c.RAIA_INICIAL FROM composicao_etapas c WHERE c.ID_CAD_EQUIPE = p.ID_CAD_EQUIPE ) RAIA_INICIAL ,
		( SELECT count( b.id ) FROM cad_etapas_baterias b WHERE b.id_cad_etapa = p.ID_ETAPA ) QTD_BATERIAS,
		( SELECT e.ID_CAD_TIPO_ORG_RAIA FROM cad_etapas e WHERE e.ID = p.ID_ETAPA ) ID_CAD_TIPO_ORG_RAIA,
		( SELECT RETORNA_BATERIA_PESAGEM ( p.ID_ETAPA, p.DATA_HORA ) ) BATERIA
	INTO RAIA_INICIAL, QTD_BATERIAS, ID_CAD_TIPO_ORG_RAIA, BATERIA
	FROM
		pesagens p 
	WHERE
		p.ID = ID_PESAGEM;
	
		LOOP_VARRE_BATERIAS: LOOP
			SET exit_count = exit_count + 1;
			IF (exit_count > 10) THEN
				LEAVE LOOP_VARRE_BATERIAS;
			END IF;
			IF(ID_CAD_TIPO_ORG_RAIA = 1) THEN
				IF(BATERIA = 1) THEN
					RETURN RAIA_INICIAL;
				END IF;
				
				IF (BATERIA = 0) THEN
						SET exit_loop = true;
						RETURN 0;
					END IF;
				
				IF(BATERIA > 1) THEN
					IF(RAIA_INICIAL BETWEEN 1 AND 15) THEN
						SET RAIA = RAIA+15;
					END IF;
					
					IF(RAIA_INICIAL BETWEEN 16 AND 23) THEN
						SET RAIA = RAIA+37;
					END IF;
					
					IF(RAIA_INICIAL BETWEEN 24 AND 30) THEN
						SET RAIA = RAIA+22;
					END IF;
					
					IF(RAIA_INICIAL BETWEEN 31 AND 37) THEN
						SET RAIA = RAIA-22;
					END IF;
					
					IF(RAIA_INICIAL BETWEEN 38 AND 45) THEN
						SET RAIA = RAIA-37;
					END IF;
					
					IF(RAIA_INICIAL BETWEEN 46 AND 60) THEN
						SET RAIA = RAIA-15;
					END IF;
					
					SET BATERIA = BATERIA-1;
					
					IF (QTD_BATERIAS = 0) THEN
						SET exit_loop = true;
					END IF;
				END IF;
			END IF;
			

			IF(ID_CAD_TIPO_ORG_RAIA = 2) THEN
				IF(BATERIA = 1) THEN
					RETURN RAIA_INICIAL;
				END IF;
				
				IF (BATERIA = 0) THEN
						SET exit_loop = true;
						RETURN 0;
					END IF;
				
				IF(BATERIA > 1) THEN
					IF(RAIA_INICIAL BETWEEN 1 AND 45) THEN
						SET RAIA = RAIA+15;
					END IF;

					IF(RAIA_INICIAL BETWEEN 46 AND 60) THEN
						SET RAIA = RAIA-45;
					END IF;
					
					SET BATERIA = BATERIA-1;
					
					IF (QTD_BATERIAS = 0) THEN
						SET exit_loop = true;
					END IF;
				END IF;
			END IF;
		
		
			IF exit_loop THEN
				 LEAVE LOOP_VARRE_BATERIAS;
			END IF;
		END LOOP LOOP_VARRE_BATERIAS;		
		
		RETURN RAIA;
		
		

END;
