--**************************************************************************
--     Table: 
--    Author: 
--      Date: 
--
-- Description:
--
-- 
-- 
--**************************************************************************

CREATE DEFINER=`brasileiroempe02`@`%` FUNCTION `brasileiroempe02`.`RETORNA_QUADRANTE_PESAGEM`(`RAIA` integer, `ID_CAD_ETAPA` integer) RETURNS int(11)
BEGIN
	DECLARE TOTAL_EQUIPES INTEGER;
	DECLARE TOTAL_BATERIAS INTEGER;
	DECLARE TOTAL_POR_QUADRANTE INTEGER;
	
	SELECT
		COUNT( ce.ID_CAD_EQUIPE ) 
	INTO
		TOTAL_EQUIPES
	FROM
		composicao_etapas ce 
	WHERE
		ce.ID_CAD_ETAPA = ID_CAD_ETAPA;
		
	SELECT
		COUNT( b.id )
	INTO
		TOTAL_BATERIAS
	FROM
		cad_etapas_baterias b 
	WHERE
		b.ID_CAD_ETAPA = ID_CAD_ETAPA;
		
	SET TOTAL_POR_QUADRANTE	= TOTAL_EQUIPES / TOTAL_BATERIAS;

	RETURN CEIL(RAIA / TOTAL_POR_QUADRANTE);
END
