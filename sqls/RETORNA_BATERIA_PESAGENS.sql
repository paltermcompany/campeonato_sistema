--**************************************************************************
--     Table: 
--    Author: 
--      Date: 
--
-- Description:
--
-- 
-- 
--**************************************************************************

CREATE DEFINER=`brasileiroempe02`@`%` FUNCTION `brasileiroempe02`.`RETORNA_BATERIA_PESAGEM`(`ID_CAD_ETAPA` int,`HORARIO` time) RETURNS varchar(100) CHARSET latin1
BEGIN
	DECLARE HORA_30_30 time;
	DECLARE bateria INTEGER;
	
	set HORA_30_30 = DATE_FORMAT(SEC_TO_TIME(FLOOR((TIME_TO_SEC(HORARIO)+900)/1800)*1800), '%H:%i:%s') ;
	
	SELECT
		t.bateria INTO bateria 
	FROM
		(
		SELECT
			b.*,
			@rownum := @rownum + 1 bateria 
		FROM
			cad_etapas_baterias b,
			( SELECT @rownum := 0 ) r 
		WHERE
			b.id_cad_etapa = ID_CAD_ETAPA 
		ORDER BY
			b.horario_inicio 
			) t 
	WHERE
		DATE_FORMAT( SEC_TO_TIME( FLOOR( ( TIME_TO_SEC( HORARIO) + 900 ) / 1800 ) * 1800 ), '%H:%i:%s' ) BETWEEN DATE_FORMAT( t.horario_inicio, '%H:%i:%s' ) 
		AND DATE_FORMAT( t.horario_termino, '%H:%i:%s' );

	
	RETURN COALESCE(bateria,0);
END;

 