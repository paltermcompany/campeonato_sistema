--**************************************************************************
--     Table: 
--    Author: 
--      Date: 
--
-- Description:
--
-- 
-- 
--**************************************************************************

CREATE DEFINER=`brasileiroempe02`@`%` PROCEDURE `brasileiroempe02`.`RETORNA_MP_ETAPA`(IN `P_ID_ETAPA` integer,OUT `ID_EQUIPE` integer,OUT `NOME_EQUIPE` varchar(100),OUT `NOME_COMPETIDORES` varchar(100),OUT `TOTAL` DOUBLE, OUT `UNIDADE_MEDIDA` varchar(100))
BEGIN
	DECLARE v_id_mp INTEGER;
	DECLARE v_medida DOUBLE;
	DECLARE v_empates INTEGER;
	DECLARE v_id_equipe INTEGER;
	DECLARE v_medida_maior DOUBLE;
	DECLARE v_medida_maior_id INTEGER;
	DECLARE v_medida_mp_para_empate DOUBLE;
	DECLARE v_qtd_loops INTEGER;
	DECLARE v_tentativas_desempate INTEGER;
	DECLARE exit_loop BOOLEAN; 
	DECLARE exit_loop1 BOOLEAN; 
	DECLARE cursor_empates CURSOR FOR
		SELECT
				pesagens.ID_CAD_EQUIPE 
		FROM
				pesagens
		WHERE
				pesagens.EXCLUIDO = 'NAO'
				AND pesagens.ID_ETAPA = P_ID_ETAPA
		ORDER BY
				pesagens.peso DESC
				LIMIT v_empates;

	##NAO ESTA SENDO USADO, PQ ESTAVA DANDO `EXIT` QUANDO LOCALIZAVA ALGUM RETORNO NULL EM QUALQUER SELECT DA PROCEDURE
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET exit_loop1 = TRUE;

	##PEGA MAIOR MEDIDA VALIDA NA ETAPA
	SELECT
			pesagens.ID,
			pesagens.PESO INTO v_id_mp, v_medida
	FROM
			pesagens
	WHERE
			pesagens.EXCLUIDO = 'NAO'
			AND pesagens.ID_ETAPA = P_ID_ETAPA
	ORDER BY
			pesagens.peso DESC
			LIMIT 1;
			
	##VERIFICA SE EXISTE ALGUM EMPATE COM A MAIOR MEDIDA		
	SELECT
			COUNT( DISTINCT ID ) TOTAL_EMPATES INTO v_empates
	FROM
			pesagens
	WHERE
			pesagens.EXCLUIDO = 'NAO'
			AND
			pesagens.PESO = v_medida
			AND
			ID_ETAPA = P_ID_ETAPA;	
			
	IF (v_empates = 1) THEN
		##SE NAO EXISTE EMPATE, PEGA OS DADOS DA MEDIDA
		SELECT
				pesagens.peso TOTAL,
				composicao_equipes.ID_EQUIPE,
				cad_equipes.DESCRICAO NOME_EQUIPE,
				cad_unidades_medida.abreviacao UNIDADE_MEDIDA,
				GROUP_CONCAT( cad_competidores.APELIDO ORDER BY cad_competidores.APELIDO SEPARATOR ' & ' ) NOME_COMPETIDORES
				INTO TOTAL, ID_EQUIPE, NOME_EQUIPE, UNIDADE_MEDIDA, NOME_COMPETIDORES 
		FROM
				pesagens
				INNER JOIN composicao_equipes ON ( composicao_equipes.id_equipe = pesagens.ID_CAD_EQUIPE )
				INNER JOIN cad_equipes ON ( cad_equipes.ID = composicao_equipes.id_equipe )
				INNER JOIN cad_competidores ON ( cad_competidores.ID = composicao_equipes.id_competidor )
				INNER JOIN cad_etapas ON (cad_etapas.ID = pesagens.ID_ETAPA)
				INNER JOIN cad_unidades_medida ON (cad_unidades_medida.id = cad_etapas.id_cad_unidade_medida)
		WHERE
				pesagens.id = v_id_mp and composicao_equipes.SUPLENTE = 'N';
	ELSE
		##SE EXISTE EMPATE, EXECUTA ROTINA
		SET v_medida_mp_para_empate = v_medida;
		SET v_medida_maior = 0;
		SET v_medida_maior_id = 0;
		SET v_tentativas_desempate = 1;
		SET v_qtd_loops = v_empates * 4; ##4 tentativas de desempate
		SET exit_loop = FALSE;
		
		OPEN cursor_empates;
		empates_loop: LOOP
			##TENTATIVAS DESEMPATE INICIA EM 1+1 PARA FAZER O CONTROLE DO OFFSET, INICIANDO EM 2
			SET v_tentativas_desempate = v_tentativas_desempate+1;
			
			##LOOP QUE PEGA TODOS AS EQUIPES QUE TIVERAM O MESMO EMPATE
			FETCH  cursor_empates INTO v_id_equipe;

				SET v_qtd_loops = v_qtd_loops - 1;
				IF (v_tentativas_desempate = 5) OR (v_qtd_loops = 0) THEN
					SET exit_loop = true;
				END IF;
			
				SET v_medida = NULL;
				SET v_id_mp = NULL;
				
				##SELECT PARA IR PEGANDO O MAIOR PESO, 
				SELECT
						pesagens.peso, pesagens.ID INTO v_medida, v_id_mp
				FROM
						pesagens
				WHERE
						pesagens.ID_CAD_EQUIPE = v_id_equipe AND pesagens.ID_ETAPA = P_ID_ETAPA
				ORDER BY pesagens.PESO DESC LIMIT 1 OFFSET v_tentativas_desempate;##2,3,4,5 ;
				
				IF(v_medida > 0) THEN
					IF (v_medida > v_medida_maior) THEN
						SET v_medida_maior = v_medida;
						SET v_medida_maior_id = v_id_mp;
					END IF;
				END IF;

				IF exit_loop THEN
					 CLOSE cursor_empates;
					 LEAVE empates_loop;
				END IF;
		END LOOP empates_loop;
		##QUANDO LOCALIZA O DESEMPATE, PEGA OS DADOS DA MEDIDA
		SELECT
				composicao_equipes.ID_EQUIPE,
				cad_equipes.DESCRICAO NOME_EQUIPE,
				cad_unidades_medida.abreviacao UNIDADE_MEDIDA,
				GROUP_CONCAT( cad_competidores.APELIDO ORDER BY cad_competidores.APELIDO SEPARATOR ' & ' ) NOME_COMPETIDORES
				INTO ID_EQUIPE, NOME_EQUIPE, UNIDADE_MEDIDA, NOME_COMPETIDORES 
		FROM
				pesagens
				INNER JOIN composicao_equipes ON ( composicao_equipes.id_equipe = pesagens.ID_CAD_EQUIPE )
				INNER JOIN cad_equipes ON ( cad_equipes.ID = composicao_equipes.id_equipe )
				INNER JOIN cad_competidores ON ( cad_competidores.ID = composicao_equipes.id_competidor )
				INNER JOIN cad_etapas ON (cad_etapas.ID = pesagens.ID_ETAPA)
				INNER JOIN cad_unidades_medida ON (cad_unidades_medida.id = cad_etapas.id_cad_unidade_medida)
		WHERE
				pesagens.id = v_medida_maior_id;

		SET TOTAL = v_medida_mp_para_empate;
	END IF;

END;
