@echo off
chcp 65001
cls
type banner.msg
echo ----------------------------------------------
echo Iniciando sistema de campeonato
copy .\gc\assets\js\config.js.prod .\gc\assets\js\config.js /Y
timeout 1 /nobreak > nil
echo * Chat Interno
timeout 1 /nobreak > nil
cd chat
start /MIN chat.bat
cd ..
echo * Sincronizador GC
timeout 1 /nobreak > nil
cd gc
start /MIN servidor.bat
echo * Renderizador GC
timeout 2 /nobreak > nil
cd ChromeNdi\Build\x64\Release
start ChromeNdi.exe /MIN
cd ../../../../..
echo ---------------------------------------------
echo Campeonato Iniciado!
echo http://localhost:3000           Para abrir o chat 
echo http://localhost/gc             Para abrir o GC   
echo http://localhost/gc/touch.html  Para abrir o controle do GC
pause
