<?php
require '../inc/redirect.php';
include '../functions/conexao.php';
require '../functions/crud.php';
require '../functions/login.php';

if (isset($_GET['logout']) && $_GET['logout'] == 'true') {
	session_destroy();
	header("Location: login.php");
 }

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="author" content="Douglas Lessing">
	<meta charset="UTF-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Campeonatos</title>

    <link href="../bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
	<link href="../dist/css/bootstrap-multiselect.css" type="text/css" rel="stylesheet" />
    <link href="../bower_components/font-awesome/css/all.css" rel="stylesheet">
	<link href="../dist/css/sb-admin-2.css" rel="stylesheet">


    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <script src="../dist/js/sb-admin-2.js"></script>
	<script src="../js/custom.js" type="text/javascript"></script>
	<script src="../dist/js/bootstrap-multiselect.js" type="text/javascript"></script>

	<!-- blueimp Gallery styles -->
	<!--<link rel="stylesheet" href="//blueimp.github.io/Gallery/css/blueimp-gallery.min.css">-->
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<!--<link rel="stylesheet" href="../bower_components/fileupload/css/jquery.fileupload.css">
	<link rel="stylesheet" href="../bower_components/fileupload/css/jquery.fileupload-ui.css">-->
	<!-- CSS adjustments for browsers with JavaScript disabled -->
	<!--<noscript><link rel="stylesheet" href="css/jquery.fileupload-noscript.css"></noscript>
	<noscript><link rel="stylesheet" href="css/jquery.fileupload-ui-noscript.css"></noscript>	-->

	<link rel="shortcut icon" type="image/ico" href="../favicon.ico"/>

</head>
