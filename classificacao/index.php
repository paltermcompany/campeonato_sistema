<!DOCTYPE html>
<html>
<head>
	<link href="https://fonts.googleapis.com/css?family=Roboto:500" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="style.css" media="all" />
	<link rel="stylesheet" type="text/css" href="flex.css" media="all" />
	<title>Ranking</title>
</head>
<body class="cl-center-center">
	<div class="corpo rw-h-spread">

		<?php
		$k=0;
		for ($i=0; $i < 2; $i++) { 						
		?>
		<div class="coluna cl-center-center">
			<?php
			for ($j=1; $j <= 5; $j++) { 
				$k++;
			?>
			<div class="linha rw-center-center">				
				<div class="linha-back"></div>
				<div class="posicao cl-center-center"><?php echo $k;?>º</div>
				<div class="raia cl-center-center">Raia 01</div>
				<div class="perfil cl-center-center">
					<div class="perfil-background rw-h-spread">
						<div class="equipe"><img src="equipe.png" /></div>
						<div class="cl-center-center">Marcel & Hiro</div>
					</div>
				</div>
				<div class="peso rw-center-center">9,820 <span>KG</span></div>								
			</div>
			<?php }?>
		</div>		
		<?php }?>
	</div>
</body>
</html>