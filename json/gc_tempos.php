<?php
/*
    JSON DE MODALIDADES DA ETAPA PARA GC
*/
    header("Access-Control-Allow-Origin: *");
    include '../functions/conexao.php';
    $pdo = conecta();
    $array = [];
    try {

        $consultar = $pdo -> prepare("  SELECT
                                            count( t.ID ) QTD_PESAGENS,
                                            sum( t.PESO ) TOTAL_PESAGENS,
                                            t.QUADRANTE,
                                            t.BATERIA
                                        FROM
                                            (
                                        SELECT
                                            p.ID,
                                            p.COMPROVANTE,
                                            p.ID_CAD_COMPETIDOR,
                                            p.DATA_HORA,
                                            p.PESO,
                                            ( SELECT RETORNA_BATERIA_PESAGEM ( p.id_etapa, p.DATA_HORA ) ) BATERIA,
                                            ( SELECT RETORNA_RAIA_PESAGEM ( p.ID ) ) RAIA,
                                            ( SELECT RETORNA_QUADRANTE_PESAGEM ( RAIA, p.id_etapa ) ) QUADRANTE
                                        FROM
                                            pesagens p
                                            INNER JOIN cad_etapas e ON ( e.ID = p.ID_ETAPA )
                                            INNER JOIN cad_etapas_datas ed ON ( ed.id_cad_etapa = e.ID )
                                        WHERE
                                            ed.data_etapa = CURRENT_DATE
                                            AND p.EXCLUIDO = 'NAO'
                                        ORDER BY
                                            p.ID,
                                            BATERIA
                                            ) t
                                        GROUP BY
                                            t.BATERIA,
                                            t.QUADRANTE");

        $consultar -> execute();
        if ($consultar -> rowCount() > 0) {
            $i = 1;
            while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
                $array[] = array(
                    'DESCRICAO' => 'TEMPO '.$linha -> BATERIA,
                    'TEMPO' => $linha -> BATERIA,
                    'QTD_PESAGENS' => $linha -> QTD_PESAGENS,
                    'TOTAL_PESAGENS' => $linha -> TOTAL_PESAGENS,
                    'QUADRANTE' => $linha -> QUADRANTE
                );
            }

            if(isset($array)){
                if(defined('RETORNAR')) { return $array; }
                echo json_encode(array("TEMPOS"=>$array) );
            }
        }
    } catch(PDOException $e) {
        echo $e -> getMessage();
    }
