<?php
    header("Access-Control-Allow-Origin: *");

    include '../functions/conexao.php';
    require '../functions/crud.php';

    if($etapa = retornaEtapaHoje(null)){
        $ID_CAD_ETAPA = $etapa->ID;
    }

    if(isset($ID_CAD_ETAPA)){
        $pdo = conecta();
        try {

            $consultar = $pdo -> prepare("  SELECT
                                                t.ID_CAD_EQUIPE,
                                                t.NOME_EQUIPE,
                                                t.ID_COMPETIDORES,
                                                ( SELECT GROUP_CONCAT( c.APELIDO SEPARATOR ' & ' ) FROM cad_competidores c WHERE FIND_IN_SET( c.ID, t.ID_COMPETIDORES ) ) NOME_COMPETIDORES
                                            FROM
                                                (
                                            SELECT
                                                cet.ID_CAD_EQUIPE,
                                                cad_equipes.DESCRICAO NOME_EQUIPE,
                                                GROUP_CONCAT( DISTINCT ceq.id_competidor ) ID_COMPETIDORES
                                            FROM
                                                composicao_etapas cet
                                                INNER JOIN composicao_equipes ceq ON ceq.id_equipe = cet.ID_CAD_EQUIPE
                                                inner join cad_equipes on cad_equipes.ID = cet.ID_CAD_EQUIPE
                                            WHERE
                                                cet.ID_CAD_ETAPA = :ID_CAD_ETAPA
                                                AND
                                                ceq.SUPLENTE = 'N'
                                            GROUP BY
                                                cet.ID_CAD_EQUIPE
                                                ) t
                                            ORDER BY
                                                t.NOME_EQUIPE");

            $consultar -> bindValue(':ID_CAD_ETAPA', $ID_CAD_ETAPA, PDO::PARAM_STR);

            $consultar -> execute();
            if ($consultar -> rowCount() > 0) {
                $i = 1;
                while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
                    $competidores = $linha -> NOME_COMPETIDORES;
                    /*para verificar se eh dupla ou individual*/
                    if(strpos($competidores, " & ") > 0){
                        $competidor1 = substr($competidores, 0, strpos($competidores, " & "));
                        $competidor2 = substr($competidores, strpos($competidores, " & ") +3, strlen($competidores));
                    }else{
                        $competidor1 = $competidores;
                        $competidor2 = '';
                    }

                    $array[] = array(
                        'id_equipe' => $linha -> ID_CAD_EQUIPE,
                        'nome_equipe' => $linha -> NOME_EQUIPE,
                        'nome_competidores' => $linha -> NOME_COMPETIDORES,
                        'competidor1' => $competidor1,
                        'competidor2' => $competidor2
                    );
                }

                if(isset($array)){
                    if(defined('RETORNAR')) { return $array; }
                    echo json_encode(array("resultados"=>$array) );
                }
            }
        } catch(PDOException $e) {
            echo $e -> getMessage();
        }
    }
?>
