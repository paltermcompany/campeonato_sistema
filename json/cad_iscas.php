<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-type: application/json');

	include '../functions/conexao.php';
	$pdo = conecta();

	try {
		$consultar = $pdo -> prepare("SELECT id, descricao FROM cad_iscas");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
				$array[] = array(
					'id' => $linha -> id,
					'descricao' => $linha -> descricao
				);
			}
			if(isset($array)){
				echo json_encode(array("iscas"=>$array));
			}
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}

?>
