<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-type: application/json');
	
	include '../functions/conexao.php';
	$serialColetor=$_GET['serialColetor'];

	if(isset($serialColetor)){
		$pdo = conecta();

		try {
			$consultar = $pdo -> prepare("	SELECT DISTINCT
												cad_campeonatos.id,
												cad_campeonatos.descricao,
												cad_campeonatos.descricao_app,
												cad_campeonatos.imagem,
												cad_campeonatos.imagem_pb
											FROM
												cad_campeonatos
												INNER JOIN cad_etapas ON ( cad_etapas.id_cad_campeonato = cad_campeonatos.id )
												INNER JOIN composicao_etapa_coletor ON ( composicao_etapa_coletor.id_cad_etapa = cad_etapas.ID )
												INNER JOIN cad_coletores ON ( composicao_etapa_coletor.id_cad_coletor = cad_coletores.id )
												INNER JOIN cad_etapas_datas ON (cad_etapas_datas.id_cad_etapa = cad_etapas.ID)
											WHERE
												cad_etapas_datas.data_etapa >= CURRENT_DATE
												AND cad_coletores.serial = :serialColetor");

			$consultar -> bindValue(':serialColetor', $serialColetor, PDO::PARAM_STR);
			$consultar -> execute();
			if ($consultar -> rowCount() > 0) {
				while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
					$array[] = array(
						'id' => $linha -> id,
						'descricao' => $linha -> descricao,
						'descricao_app' => $linha -> descricao_app,
						'imagem' => $linha -> imagem,
						'imagem_pb' => $linha -> imagem_pb
					);
				}
				if(isset($array)){
					echo json_encode(array("campeonatos"=>$array));
				}
			}
		} catch(PDOException $e) {
			echo $e -> getMessage();
		}
	}

?>
