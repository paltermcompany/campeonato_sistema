<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-type: application/json');

	include '../functions/conexao.php';
	$serialColetor=$_GET['serialColetor'];

    if(isset($serialColetor)){
		$pdo = conecta();
		try {
			$consultar = $pdo -> prepare("	SELECT DISTINCT
												composicao_etapas.id,
												composicao_etapas.id_cad_etapa,
												composicao_etapas.id_cad_equipe
											FROM
												composicao_etapas
												INNER JOIN cad_etapas ON ( cad_etapas.id = composicao_etapas.id_cad_etapa )
												INNER JOIN composicao_etapa_coletor ON ( composicao_etapa_coletor.id_cad_etapa = cad_etapas.ID )
												INNER JOIN cad_coletores ON ( composicao_etapa_coletor.id_cad_coletor = cad_coletores.id )
												INNER JOIN cad_etapas_datas ON (cad_etapas_datas.id_cad_etapa = cad_etapas.ID)
											WHERE
												cad_etapas_datas.data_etapa >= CURRENT_DATE
												AND cad_coletores.serial = :serialColetor");

			$consultar -> bindValue(':serialColetor', $serialColetor, PDO::PARAM_STR);
			$consultar -> execute();
			if ($consultar -> rowCount() > 0) {
				while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
					$array[] = array(
						'id' => $linha -> id,
						'id_cad_etapa' => $linha -> id_cad_etapa,
						'id_cad_equipe' => $linha -> id_cad_equipe
					);
				}
				if(isset($array)){
					echo json_encode(array("etapas"=>$array));
				}
			}
		} catch(PDOException $e) {
			echo $e -> getMessage();
		}
	}

?>
