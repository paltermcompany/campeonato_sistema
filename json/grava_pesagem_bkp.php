<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

	header('Access-Control-Allow-Origin: *');
	include '../functions/conexao.php';
	$pdo = conecta();

	$serial_instrumento = $_GET['serial_instrumento'];
	$app_version = intval($_GET['app_version'] ?? "1");


	//grava_pesagem.php?id_etapa=93&id_cad_competidor=1&id_cad_equipe=3&id_cad_especie=2&data_hora=2019-06-04+12%3A02%3A22&id_cad_modalidade=2&peso=0.3&excluido=NAO&obs=&comprovante=397318188435165&serial_instrumento=V102174T03981&nome_fiscal=TESTE+FISCAL+1&id_cad_isca=294:13&id_cad_modalidade=1&peso=1&excluido=teste&obs=teste&comprovante=0000-00&serial_instrumento=123132154-888&nome_fiscal=Douglas Fiscal

	//atualiza 3 maiores pesagens do competidor para esta etapa
	function setTop3($id_etapa, $id_cad_competidor) {
		global $pdo;
		//zera todas
		$consultar = $pdo -> prepare("UPDATE pesagens SET TOP3 = 0 WHERE ID_ETAPA=:ID_ETAPA AND ID_CAD_COMPETIDOR=:ID_CAD_COMPETIDOR");
		$consultar -> bindValue(':ID_ETAPA', $id_etapa, PDO::PARAM_STR);
		$consultar -> bindValue(':ID_CAD_COMPETIDOR', $id_cad_competidor, PDO::PARAM_STR);
    $consultar -> execute();
		//busca 3 maiores
		$consultar = $pdo -> prepare("SELECT ID FROM pesagens WHERE ID_ETAPA=:ID_ETAPA AND ID_CAD_COMPETIDOR=:ID_CAD_COMPETIDOR AND EXCLUIDO = 'NAO' ORDER BY PESO DESC LIMIT 0,3");
		$consultar -> bindValue(':ID_ETAPA', $id_etapa, PDO::PARAM_STR);
		$consultar -> bindValue(':ID_CAD_COMPETIDOR', $id_cad_competidor, PDO::PARAM_STR);
    $consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			$top3 = [];
			while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
				$top3[] = $linha -> ID;
			}
			if(count($top3)) {
				$top3 = implode(",", $top3);
				$query = "UPDATE pesagens SET TOP3 = 1 WHERE ID IN($top3)";
				$atualizar = $pdo -> query($query);
			}
		}
	}

	try{
		$id_etapa = $_GET['id_etapa'];
		$id_cad_competidor = $_GET['id_cad_competidor'];
		$id_cad_equipe = $_GET['id_cad_equipe'];
		$id_cad_especie = $_GET['id_cad_especie'];
		$data_hora = $_GET['data_hora'];
		$id_cad_modalidade = $_GET['id_cad_modalidade'];
		$peso = $_GET['peso'];
		$excluido = $_GET['excluido'];
		$obs = $_GET['obs'];
		$comprovante = $_GET['comprovante'];
		$nome_fiscal = $_GET['nome_fiscal'];
		$id_cad_isca = $_GET['id_cad_isca'];


		//busca peso mínimo e tipo de classificação da etapa atual
		$pesoMinimo = false;
		$tipoClassificacao = false;
		$consultar = $pdo -> prepare("SELECT ID_CAD_TIPO_CLASSIFICACAO, peso_minimo FROM cad_etapas WHERE ID=:ID_ETAPA");
		$consultar -> bindValue(':ID_ETAPA', $id_etapa, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			$linha = $consultar -> fetch(PDO::FETCH_OBJ);
			$pesoMinimo = floatval($linha -> peso_minimo);
			$tipoClassificacao = $linha -> ID_CAD_TIPO_CLASSIFICACAO;
		}

		//verificar se ja existe um comprovante igual para essa etapa, se sim, fazer update na pesagem ja lancada, se nao, fazer insert
		$consultar = $pdo -> prepare("SELECT id FROM pesagens WHERE ID_ETAPA=:ID_ETAPA AND COMPROVANTE=:COMPROVANTE AND SERIAL_INSTRUMENTO_MED=:SERIAL_INSTRUMENTO_MED");
		$consultar -> bindValue(':ID_ETAPA', $id_etapa, PDO::PARAM_STR);
		$consultar -> bindValue(':COMPROVANTE', $comprovante, PDO::PARAM_STR);
		$consultar -> bindValue(':SERIAL_INSTRUMENTO_MED', $serial_instrumento, PDO::PARAM_STR);
    $consultar -> execute();

    if ($consultar -> rowCount() > 0) {
			//estamos cancelando uma pesagem
     	$cadastrar = $pdo -> prepare("UPDATE pesagens SET excluido = :EXCLUIDO, obs = :OBS, enviado_site_brasileiro = NULL WHERE ID_ETAPA=:ID_ETAPA AND COMPROVANTE=:COMPROVANTE");
			$cadastrar -> bindValue(':EXCLUIDO', $excluido, PDO::PARAM_STR);
			$cadastrar -> bindValue(':OBS', $obs, PDO::PARAM_STR);
			$cadastrar -> bindValue(':ID_ETAPA', $id_etapa, PDO::PARAM_STR);
			$cadastrar -> bindValue(':COMPROVANTE', $comprovante, PDO::PARAM_STR);
			$salvou = $cadastrar -> execute();

			//se motivo do cancelamento foi erro do fiscal, precisamos liberar outra pesagem que eventualmente esteja cancelada por excesso de capturas
			$comprovantePesagemLiberada = 0;
			if(in_array($obs,['PESAGEM / MEDIDA INCORRETA', 'COMPETIDOR INCORRETO', 'ESPÉCIE INCORRETA']) && $tipoClassificacao == "2") {
				$consultar = $pdo -> prepare("SELECT COMPROVANTE FROM pesagens WHERE ID_ETAPA=:ID_ETAPA AND ID_CAD_COMPETIDOR=:ID_CAD_COMPETIDOR AND EXCLUIDO = 'SIM' AND OBS = 'EXCEDEU LIMITE DE CAPTURAS' order by ID desc limit 0,1");
				$consultar -> bindValue(':ID_ETAPA', $id_etapa, PDO::PARAM_STR);
				$consultar -> bindValue(':ID_CAD_COMPETIDOR', $id_cad_competidor, PDO::PARAM_STR);
				$consultar -> execute();
				if ($consultar -> rowCount() > 0) {
					$linha = $consultar -> fetch(PDO::FETCH_OBJ);
					$comprovantePesagemLiberada = $linha -> COMPROVANTE;
					$cadastrar = $pdo -> prepare("UPDATE pesagens SET excluido = 'NAO', obs = '', enviado_site_brasileiro = NULL WHERE ID_ETAPA=:ID_ETAPA AND COMPROVANTE=:COMPROVANTE");
					$cadastrar -> bindValue(':ID_ETAPA', $id_etapa, PDO::PARAM_STR);
					$cadastrar -> bindValue(':COMPROVANTE', $comprovantePesagemLiberada, PDO::PARAM_STR);
					$salvou = $cadastrar -> execute();
				}
			}

			//atualiza as 3 maiores pesagens do competidor
			setTop3($id_etapa, $id_cad_competidor);

			//aqui vamos diferenciar chamadas do app v2 pq esse retorno tava errado.
			//vamos manter a resposta original por compatibilidade ao app v1
			if($app_version > 1) {

				//retorno do novo app pwa
				header('Content-type: application/json');
				if($salvou) {
					die(json_encode([ "sucesso" => true, "excluido" => $excluido, "obs" => $obs, "liberar" => $comprovantePesagemLiberada ]));
				} else {
					die(json_encode([ "sucesso" => false, 'info' => $cadastrar->errorInfo() ]));
				}

			} else {
				
				//Retorno antigo do app android nativo
				if ($salvou) {
					echo 'ok';
				} else {
					return false;
				}

			}
    } else {
			//estamos cadastrando uma nova pesagem
			if(floatval($peso) < $pesoMinimo) {
				if($excluido == 'NAO') {
					$excluido = 'SIM';
					$obs = 'EXEMPLAR ABAIXO DO PESO MÍNIMO';	
				}
			}

			if($tipoClassificacao == "2") {
				$consultar = $pdo -> prepare("SELECT ID FROM pesagens WHERE ID_ETAPA=:ID_ETAPA AND ID_CAD_COMPETIDOR=:ID_CAD_COMPETIDOR AND (EXCLUIDO = 'NAO' OR EXCLUIDO = 'SIM' AND OBS NOT IN('PESAGEM / MEDIDA INCORRETA', 'COMPETIDOR INCORRETO', 'ESPÉCIE INCORRETA', 'REGISTRO DUPLICADO'))");
				$consultar -> bindValue(':ID_ETAPA', $id_etapa, PDO::PARAM_STR);
				$consultar -> bindValue(':ID_CAD_COMPETIDOR', $id_cad_competidor, PDO::PARAM_STR);
				$consultar -> execute();
				if ($consultar -> rowCount() >= 6) {
					if($excluido == 'NAO') {
						$excluido = 'SIM';
						$obs = 'EXCEDEU LIMITE DE CAPTURAS';
					}
				}
			}

			$cadastrar = $pdo -> prepare("INSERT INTO pesagens (id_etapa, id_cad_competidor, id_cad_equipe, id_cad_especie, data_hora, id_cad_modalidade, peso, excluido, obs, comprovante, SERIAL_INSTRUMENTO_MED, nome_fiscal, id_cad_isca) VALUES (:ID_ETAPA, :ID_CAD_COMPETIDOR, :ID_CAD_EQUIPE, :ID_CAD_ESPECIE, :DATA_HORA, :ID_CAD_MODALIDADE, :PESO, :EXCLUIDO, :OBS, :COMPROVANTE, :SERIAL_INSTRUMENTO_MED, :NOME_FISCAL, :ID_CAD_ISCA)");

			$cadastrar -> bindValue(':ID_ETAPA', $id_etapa, PDO::PARAM_STR);
			$cadastrar -> bindValue(':ID_CAD_COMPETIDOR', $id_cad_competidor, PDO::PARAM_STR);
			$cadastrar -> bindValue(':ID_CAD_EQUIPE', $id_cad_equipe, PDO::PARAM_STR);
			$cadastrar -> bindValue(':ID_CAD_ESPECIE', $id_cad_especie, PDO::PARAM_STR);
			$cadastrar -> bindValue(':DATA_HORA', $data_hora, PDO::PARAM_STR);
			$cadastrar -> bindValue(':ID_CAD_MODALIDADE', $id_cad_modalidade, PDO::PARAM_STR);
			$cadastrar -> bindValue(':PESO', $peso, PDO::PARAM_STR);
			$cadastrar -> bindValue(':EXCLUIDO', $excluido, PDO::PARAM_STR);
			$cadastrar -> bindValue(':OBS', $obs, PDO::PARAM_STR);
			$cadastrar -> bindValue(':COMPROVANTE', $comprovante, PDO::PARAM_STR);
			$cadastrar -> bindValue(':SERIAL_INSTRUMENTO_MED', $serial_instrumento, PDO::PARAM_STR);
			$cadastrar -> bindValue(':NOME_FISCAL', $nome_fiscal, PDO::PARAM_STR);
			$cadastrar -> bindValue(':ID_CAD_ISCA', $id_cad_isca, PDO::PARAM_STR);
			$cadastrar -> execute();

			setTop3($id_etapa, $id_cad_competidor);

			if($app_version > 1) {

				header('Content-type: application/json');
				if ($cadastrar -> rowCount() > 0) {
					die(json_encode([ "sucesso" => true, "excluido" => $excluido, "obs" => $obs ]));
				} else {
					die(json_encode([ "sucesso" => false ]));
				}

			} else {

				if ($cadastrar -> rowCount() > 0) {
					echo 'ok';
				} else {
					return false;
				}
	
			}
    }
	} catch(PDOException $e) {

		if($app_version > 1) {

			header('Content-type: application/json');
			die(json_encode([ "sucesso" => false, "erro" => $e -> getMessage() ]));

		} else {

			echo $e -> getMessage();

		}



  }

?>

