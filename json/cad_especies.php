<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-type: application/json');

	include '../functions/conexao.php';
	$pdo = conecta();

	try {
		$consultar = $pdo -> prepare("SELECT id, descricao, prioridade FROM cad_especies");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
				$array[] = array(
					'id' => $linha -> id,
					'descricao' => $linha -> descricao,
					'prioridade' => $linha -> prioridade
				);
			}
			if(isset($array)){
				echo json_encode(array("especies"=>$array));
			}
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}

?>
