<?php
/*
    JSON DE ESPECIES DA ETAPA PARA GC
*/
    header("Access-Control-Allow-Origin: *");

    include '../functions/conexao.php';
    $pdo = conecta();
    $array = [];
    try {
        
        $consultar = $pdo -> prepare("  (SELECT
                                            ( SELECT RETORNA_BATERIA_PESAGEM ( p.id_etapa, p.DATA_HORA ) ) BATERIA
                                        FROM
                                            pesagens p
                                            INNER JOIN cad_etapas e ON ( e.ID = p.ID_ETAPA )
                                            INNER JOIN cad_etapas_datas ed ON ( ed.id_cad_etapa = e.ID )
                                            INNER JOIN cad_especies es on (p.ID_CAD_ESPECIE = es.ID)
                                        WHERE
                                            ed.data_etapa = CURRENT_DATE
                                            AND p.EXCLUIDO = 'NAO'
                                        GROUP BY
                                            BATERIA
                                        ORDER BY 
                                            BATERIA)");

        $consultar -> execute();
        

        if ($consultar -> rowCount() > 0) {
            $i = 1;
            /*
            foreach($resultado as $result){
                echo $result['DESCRICAO']."\n";
            }
            */

            while($linha = $consultar -> fetch(PDO::FETCH_OBJ)){

                $array[] = array(
                    'TEMPO ' .$linha -> BATERIA,
                );

            }
            
            if(isset($array)){
                if(defined('RETORNAR')) { return $array; }
                echo json_encode(array("TEMPOS"=>$array) );
            }
        }
    } catch(PDOException $e) {
        echo $e -> getMessage();
    }
