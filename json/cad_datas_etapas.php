<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-type: application/json');

    include '../functions/conexao.php';
    $serialColetor=$_GET['serialColetor'];

    if(isset($serialColetor)){
        $pdo = conecta();
        try {
            $consultar1 = $pdo -> prepare("  SELECT DISTINCT
                                                cad_etapas_datas.id_cad_etapa
                                            FROM
                                                cad_etapas
                                                INNER JOIN composicao_etapa_coletor ON ( composicao_etapa_coletor.id_cad_etapa = cad_etapas.ID )
                                                INNER JOIN cad_coletores ON ( composicao_etapa_coletor.id_cad_coletor = cad_coletores.id )
                                                INNER JOIN cad_etapas_datas ON (cad_etapas_datas.id_cad_etapa = cad_etapas.ID)
                                            WHERE
                                                cad_etapas_datas.data_etapa >= CURRENT_DATE
                                                AND cad_coletores.serial = :serialColetor");

            $consultar1 -> bindValue(':serialColetor', $serialColetor, PDO::PARAM_STR);
            $consultar1 -> execute();
            if ($consultar1 -> rowCount() > 0) {

                while ($linha1 = $consultar1 -> fetch(PDO::FETCH_OBJ)) {

                    $consultar2 = $pdo -> prepare("  SELECT
                                                        cad_etapas_datas.id,
                                                        cad_etapas_datas.id_cad_etapa,
                                                        cad_etapas_datas.data_etapa
                                                    FROM
                                                        cad_etapas_datas
                                                    WHERE
                                                        cad_etapas_datas.id_cad_etapa = :id_cad_etapa");

                    $consultar2 -> bindValue(':id_cad_etapa', $linha1 -> id_cad_etapa, PDO::PARAM_STR);
                    $consultar2 -> execute();
                    if ($consultar2 -> rowCount() > 0) {
                        while ($linha2 = $consultar2 -> fetch(PDO::FETCH_OBJ)) {
                            $array[] = array(
                                'id' => $linha2 -> id,
                                'id_cad_etapa' => $linha2 -> id_cad_etapa,
                                'data_etapa' => $linha2 -> data_etapa
                            );
                        }

                    }
                }

                if(isset($array)){
                    echo json_encode(array("cad_etapas_datas"=>$array));
                }

            }
        } catch(PDOException $e) {
            echo $e -> getMessage();
        }
    }

?>
