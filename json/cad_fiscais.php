<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-type: application/json');

    include '../functions/conexao.php';
    $serialColetor=$_GET['serialColetor'];

    if(isset($serialColetor)){
        $pdo = conecta();
        try {
            $consultar = $pdo -> prepare("SELECT DISTINCT
                                            cad_fiscais.id,
                                            cad_fiscais.nome,
                                            cad_fiscais.cpf
                                        FROM
                                            cad_fiscais
                                            INNER JOIN composicao_etapa_coletor ON ( composicao_etapa_coletor.id_cad_fiscal = cad_fiscais.id )
                                            INNER JOIN cad_coletores ON ( composicao_etapa_coletor.id_cad_coletor = cad_coletores.id )
                                        WHERE
                                            cad_fiscais.ativo = 'S'
                                            AND cad_coletores.serial = :serialColetor");

            $consultar -> bindValue(':serialColetor', $serialColetor, PDO::PARAM_STR);
            $consultar -> execute();
            if ($consultar -> rowCount() > 0) {
                while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
                    $array[] = array(
                        'id' => $linha -> id,
                        'nome' => $linha -> nome,
                        'cpf' => $linha -> cpf
                    );
                }
                if(isset($array)){
                    echo json_encode(array("fiscais"=>$array));
                }
            }
        } catch(PDOException $e) {
            echo $e -> getMessage();
        }
    }

?>
