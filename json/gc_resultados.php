<?php
/*
    GC PARA RETORNAR RANKING + MP DA ETAPA SENDO REALIZADA NO DIA ATUAL
*/
    header("Access-Control-Allow-Origin: *");

    include '../functions/conexao.php';
    require '../functions/crud.php';

    if($etapa = retornaEtapaHoje(null)){
        $ID_CAD_ETAPA = $etapa->ID;
    }

    if(isset($ID_CAD_ETAPA)){
        $pdo = conecta();

        //busca tipo de classificação da etapa
        //1: considera todas as pesagens do competidor (regional)
        //2: considera somente 3 maiores pesagens (open)
        $tipoClassificacao = 1;
        $consultar = $pdo -> prepare("SELECT ID_CAD_TIPO_CLASSIFICACAO FROM cad_etapas where ID=:ID_CAD_ETAPA");
        $consultar -> bindValue(':ID_CAD_ETAPA', $ID_CAD_ETAPA, PDO::PARAM_STR);
        $consultar -> execute();
        if ($consultar -> rowCount() > 0) {
            $linha = $consultar -> fetch(PDO::FETCH_OBJ);
            $tipoClassificacao = $linha -> ID_CAD_TIPO_CLASSIFICACAO;
        }

        try {
            /*VERIFICA QUAL A MAIOR PESAGEM VALIDA NA ETAPA*/
            $consultar = $pdo -> prepare("CALL RETORNA_MP_ETAPA(:ID_CAD_ETAPA, @id_equipe, @nome_equipe, @nome_competidores, @total, @unidade_medida);");
            $consultar -> bindValue(':ID_CAD_ETAPA', $ID_CAD_ETAPA, PDO::PARAM_STR);
            $consultar -> execute();
            if ($consultar -> rowCount() > 0) {
                $consultar = $pdo -> prepare("SELECT @id_equipe ID_EQUIPE, @nome_equipe NOME_EQUIPE, @nome_competidores NOME_COMPETIDORES, @total TOTAL, @unidade_medida UNIDADE_MEDIDA");
                $consultar -> execute();

                $linha = $consultar -> fetch(PDO::FETCH_OBJ);
                $array[] = array(
                    'posicao' => 'mp',
                    'id_equipe' => $linha -> ID_EQUIPE,
                    'nome_equipe' => $linha -> NOME_EQUIPE,
                    'nome_competidores' => $linha -> NOME_COMPETIDORES,
                    'total' => number_format($linha -> TOTAL, 3, ',', '.') . ' ' . $linha -> UNIDADE_MEDIDA,
                    'classificado_final' => 'N'
                );
            }
            /*ALTEREI O SQL PARA PEGAR O NOME DE TODOS MEMBROS DA EQUIPE, MESMO QUANDO SAO OS MESMO NOMES*/
            /*PARA MELHORAR O DESEMPENHO FUTURAMENTE, DEIXAR O SELECT DO NOME DAS EQUIPES POR FORA DO SQL DOS RESULTADOS*/
            $limite = $tipoClassificacao == 1 ? '' : 'AND pesagens.TOP3 = 1';
            $query = "  SELECT t.QUANTIDADE, t.TOTAL, t.ID_EQUIPE, t.ID_ETAPA, t.NOME_EQUIPE, t.UNIDADE_MEDIDA, t.ID_COMPETIDORES,
            (SELECT  GROUP_CONCAT( cad_competidores.APELIDO ORDER BY cad_competidores.APELIDO  SEPARATOR ' & ' ) FROM cad_competidores WHERE FIND_IN_SET(cad_competidores.ID, ID_COMPETIDORES)) NOME_COMPETIDORES

            FROM
            (
            SELECT
                    count( DISTINCT pesagens.ID ) QUANTIDADE,
                    /*SOMA DO PESO DIVIDO PELA QUANTIDADE DE COMPETIDORES NA EQUIPE*/
                    sum( pesagens.PESO ) / count( DISTINCT composicao_equipes.id_competidor ) TOTAL,
                    composicao_equipes.ID_EQUIPE,
                    pesagens.ID_ETAPA,
                    cad_equipes.DESCRICAO NOME_EQUIPE,
                    cad_unidades_medida.abreviacao UNIDADE_MEDIDA,
                    GROUP_CONCAT( DISTINCT composicao_equipes.id_competidor ORDER BY composicao_equipes.id_competidor ) ID_COMPETIDORES

            FROM
                    pesagens
                    INNER JOIN composicao_equipes ON ( composicao_equipes.id_equipe = pesagens.ID_CAD_EQUIPE )
                    INNER JOIN cad_equipes ON ( cad_equipes.ID = composicao_equipes.id_equipe )
                    INNER JOIN cad_competidores ON ( cad_competidores.ID = composicao_equipes.id_competidor )
                    INNER JOIN cad_etapas ON (cad_etapas.ID = pesagens.ID_ETAPA)
                    INNER JOIN cad_unidades_medida ON (cad_unidades_medida.id = cad_etapas.id_cad_unidade_medida)
            WHERE
                    pesagens.EXCLUIDO = 'NAO'
                    AND pesagens.ID_ETAPA = :ID_CAD_ETAPA
                    ".$limite."
                    AND composicao_equipes.SUPLENTE = 'N'
            GROUP BY
                    composicao_equipes.ID_EQUIPE,
                    NOME_EQUIPE
            ORDER BY
                    TOTAL DESC
                    /*LIMIT 11*/
            ) t";
            $consultar = $pdo -> prepare($query);

            $consultar -> bindValue(':ID_CAD_ETAPA', $ID_CAD_ETAPA, PDO::PARAM_STR);
            $consultar -> execute();
            if ($consultar -> rowCount() > 0) {
                $i = 1;
                while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
                    $array[] = array(
                        'posicao' => $i++,
                        'id_equipe' => $linha -> ID_EQUIPE,
                        'nome_equipe' => $linha -> NOME_EQUIPE,
                        'nome_competidores' => $linha -> NOME_COMPETIDORES,
                        'total' => number_format($linha -> TOTAL, 3, ',', '.') . ' ' . $linha -> UNIDADE_MEDIDA,
                        'classificado_final' => 'N'
                    );
                }

                if(isset($array)){
                    echo json_encode(array("resultados"=>$array) );
                }
            }
        } catch(PDOException $e) {
            echo $e -> getMessage();
        }
    }
?>
