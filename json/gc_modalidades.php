<?php
/*
    JSON DE MODALIDADES DA ETAPA PARA GC
*/
    header("Access-Control-Allow-Origin: *");

    include '../functions/conexao.php';
    $pdo = conecta();
    $array = [];
    try {

        $consultar = $pdo -> prepare("  ( SELECT
                                            cad_modalidades.DESCRICAO MODALIDADE,
                                            count( pesagens.ID ) TOTAL,
                                            cad_modalidades.COR_GC_HEX
                                            FROM
                                                cad_modalidades
                                                INNER JOIN pesagens ON ( pesagens.ID_CAD_MODALIDADE = cad_modalidades.ID )
                                                INNER JOIN cad_etapas ON ( cad_etapas.ID = pesagens.ID_ETAPA )
                                                INNER JOIN cad_etapas_datas ON ( cad_etapas_datas.id_cad_etapa = cad_etapas.ID )
                                            WHERE
                                                cad_etapas_datas.data_etapa = CURRENT_DATE
                                                AND pesagens.EXCLUIDO = 'NAO'
                                            GROUP BY
                                                cad_modalidades.DESCRICAO
                                            ORDER BY
                                                TOTAL DESC, MODALIDADE
                                                LIMIT 5
                                                ) UNION ALL
                                                (
                                            SELECT
                                                'OUTROS' MODALIDADES,
                                                SUM( TOTAL ) TOTAL,
                                                '#808080' COR_GC_HEX
                                            FROM
                                                (
                                            SELECT
                                                cad_modalidades.DESCRICAO MODALIDADE,
                                                count( pesagens.ID ) TOTAL
                                            FROM
                                                cad_modalidades
                                                INNER JOIN pesagens ON ( pesagens.ID_CAD_MODALIDADE = cad_modalidades.ID )
                                                INNER JOIN cad_etapas ON ( cad_etapas.ID = pesagens.ID_ETAPA )
                                                INNER JOIN cad_etapas_datas ON ( cad_etapas_datas.id_cad_etapa = cad_etapas.ID )
                                            WHERE
                                                cad_etapas_datas.data_etapa = CURRENT_DATE
                                                AND pesagens.EXCLUIDO = 'NAO'
                                            GROUP BY
                                                cad_modalidades.DESCRICAO
                                            ORDER BY
                                                TOTAL DESC
                                                LIMIT 100 OFFSET 5
                                                ) AS T
                                            GROUP BY
                                                MODALIDADES
                                                )");

        $consultar -> execute();
        if ($consultar -> rowCount() > 0) {
            $i = 1;
            while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
                $array[] = array(
                    'MODALIDADE' => $linha -> MODALIDADE,
                    'TOTAL' => $linha -> TOTAL,
                    'COR_GC_HEX' => $linha -> COR_GC_HEX
                );
            }

            if(isset($array)){
                if(defined('RETORNAR')) { return $array; }
                echo json_encode(array("MODALIDADES"=>$array) );
            }
        }
    } catch(PDOException $e) {
        echo $e -> getMessage();
    }
