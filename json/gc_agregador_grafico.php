<?php

    define('RETORNAR', true);

    /*
    cores grafico
    $coresGrafico = array("#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA");
    */
    echo json_encode([
        'TEMPOS'      => include('gc_tempos_etapa.php'),
        'ESPECIES'    => include('gc_especies_tempo.php'),
        'ISCAS'       => include('gc_iscas_tempo.php'),
        'MODALIDADES' => include('gc_modalidade_tempo.php'),
        //'ETAPA_HOJE'  => include('gc_etapa_hoje.php'),
        //'EQUIPES_ETAPA' => include('gc_equipes_etapa.php'),
    ]);

?>
