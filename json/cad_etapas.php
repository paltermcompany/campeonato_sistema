<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-type: application/json');

	include '../functions/conexao.php';
	$serialColetor=$_GET['serialColetor'];

	if(isset($serialColetor)){
		$pdo = conecta();
		try {
			$consultar = $pdo -> prepare("	SELECT DISTINCT
												cad_etapas.id,
												cad_etapas.id_cad_campeonato,
												cad_etapas.descricao,
												cad_etapas.descricao_app,
												cad_etapas.data_etapa,
												cad_etapas.imagem,
												cad_etapas.imagem_pb,
												cad_etapas.peso_minimo,
												cad_etapas.id_cad_tipo_classificacao,
												composicao_etapa_coletor.id_cad_fiscal,
												cad_etapas.id_cad_unidade_medida
											FROM
												cad_etapas
												INNER JOIN composicao_etapa_coletor ON ( composicao_etapa_coletor.id_cad_etapa = cad_etapas.ID )
												INNER JOIN cad_coletores ON ( composicao_etapa_coletor.id_cad_coletor = cad_coletores.id )
												INNER JOIN cad_etapas_datas ON (cad_etapas_datas.id_cad_etapa = cad_etapas.ID)
											WHERE
												cad_etapas_datas.data_etapa >= CURRENT_DATE
												AND cad_coletores.serial = :serialColetor");

			$consultar -> bindValue(':serialColetor', $serialColetor, PDO::PARAM_STR);
			$consultar -> execute();
			if ($consultar -> rowCount() > 0) {
				while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
					$array[] = array(
						'id' => $linha -> id,
						'id_cad_campeonato' => $linha -> id_cad_campeonato,
						'descricao' => $linha -> descricao,
						'descricao_app' => $linha -> descricao_app,
						'data_etapa' => $linha -> data_etapa,
						'imagem' => $linha -> imagem,
						'imagem_pb' => $linha -> imagem_pb,
						'peso_minimo' => $linha -> peso_minimo,
						'id_cad_fiscal' => $linha -> id_cad_fiscal,
						'id_cad_unidade_medida' => $linha -> id_cad_unidade_medida,
						'id_cad_tipo_classificacao' => $linha -> id_cad_tipo_classificacao
					);
				}
				if(isset($array)){
					echo json_encode(array("etapas"=>$array));
				}
			}
		} catch(PDOException $e) {
			echo $e -> getMessage();
		}
	}
?>
