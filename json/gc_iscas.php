<?php
/*
    JSON DE ISCAS DA ETAPA PARA GC
*/
    header("Access-Control-Allow-Origin: *");

    include '../functions/conexao.php';
    $pdo = conecta();
    $array = [];
    try {

        $consultar = $pdo -> prepare("  ( SELECT
                                            cad_iscas.DESCRICAO ISCA,
                                            count( pesagens.ID ) TOTAL,
                                            cad_iscas.COR_GC_HEX
                                            FROM
                                                cad_iscas
                                                INNER JOIN pesagens ON ( pesagens.ID_CAD_ISCA = cad_iscas.ID )
                                                INNER JOIN cad_etapas ON ( cad_etapas.ID = pesagens.ID_ETAPA )
                                                INNER JOIN cad_etapas_datas ON ( cad_etapas_datas.id_cad_etapa = cad_etapas.ID )
                                            WHERE
                                                cad_etapas_datas.data_etapa = CURRENT_DATE
                                                AND pesagens.EXCLUIDO = 'NAO'
                                            GROUP BY
                                                cad_iscas.DESCRICAO
                                            ORDER BY
                                                TOTAL DESC, ISCA
                                                LIMIT 5
                                                ) UNION ALL
                                                (
                                            SELECT
                                                'OUTROS' ISCAS,
                                                SUM( TOTAL ) TOTAL,
                                                '#808080' COR_GC_HEX
                                            FROM
                                                (
                                            SELECT
                                                cad_iscas.DESCRICAO ISCA,
                                                count( pesagens.ID ) TOTAL
                                            FROM
                                                cad_iscas
                                                INNER JOIN pesagens ON ( pesagens.ID_CAD_ISCA = cad_iscas.ID )
                                                INNER JOIN cad_etapas ON ( cad_etapas.ID = pesagens.ID_ETAPA )
                                                INNER JOIN cad_etapas_datas ON ( cad_etapas_datas.id_cad_etapa = cad_etapas.ID )
                                            WHERE
                                                cad_etapas_datas.data_etapa = CURRENT_DATE
                                                AND pesagens.EXCLUIDO = 'NAO'
                                            GROUP BY
                                                cad_iscas.DESCRICAO
                                            ORDER BY
                                                TOTAL DESC
                                                LIMIT 100 OFFSET 5
                                                ) AS T
                                            GROUP BY
                                                ISCAS
                                                )");

        $consultar -> execute();
        if ($consultar -> rowCount() > 0) {
            $i = 1;
            while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
                $array[] = array(
                    'ISCA' => $linha -> ISCA,
                    'TOTAL' => $linha -> TOTAL,
                    'COR_GC_HEX' => $linha -> COR_GC_HEX
                );
            }

            if(isset($array)){
                if(defined('RETORNAR')) { return $array; }
                echo json_encode(array("ISCAS"=>$array) );
            }
        }
    } catch(PDOException $e) {
        echo $e -> getMessage();
    }
