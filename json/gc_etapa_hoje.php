<?php
/*
    GC PARA RETORNAR DATAS DA ETAPA SENDO REALIZADA NO DIA ATUAL
*/
    header("Access-Control-Allow-Origin: *");

    include '../functions/conexao.php';
    $pdo = conecta();
    try {

        $consultar = $pdo -> prepare("  SELECT
                                            ce.ID,
                                            ce.DESCRICAO,
                                            (
                                        SELECT
                                            COUNT( ID )
                                        FROM
                                            cad_etapas
                                        WHERE
                                            cad_etapas.ID_CAD_CAMPEONATO = ce.ID_CAD_CAMPEONATO
                                            AND cad_etapas.ID_CAD_UF_REALIZACAO = ce.ID_CAD_UF_REALIZACAO
                                            AND cad_etapas.DATA_ETAPA <= ce.DATA_ETAPA
                                            ) ETAPA,
                                            uf.SIGLA UF
                                        FROM
                                            cad_etapas ce
                                            INNER JOIN cad_etapas_datas de ON ( ce.ID = de.id_cad_etapa )
                                            INNER JOIN cad_estados uf ON ( ce.ID_CAD_UF_REALIZACAO = uf.ID )
                                        WHERE
                                            de.data_etapa = CURRENT_DATE");

        $consultar -> execute();
        if ($consultar -> rowCount() > 0) {
            $i = 1;
            while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
                $array[] = array(
                    'ID' => $linha -> ID,
                    'DESCRICAO' => $linha -> DESCRICAO,
                    'ETAPA' => $linha -> ETAPA,
                    'UF' => $linha -> UF
                );
            }

            if(isset($array)){
                if(defined('RETORNAR')) { return $array; }
                echo json_encode(array("ETAPA_HOJE"=>$array) );
            }
        }
    } catch(PDOException $e) {
        echo $e -> getMessage();
    }

?>
