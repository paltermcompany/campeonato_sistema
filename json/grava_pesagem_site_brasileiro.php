<html>
<head>
    <meta http-equiv="refresh" content="300">
</head>
<body>
<?php
	set_time_limit(0);
	ini_set('implicit_flush', 1);
	ob_implicit_flush(1);
    echo '<pre>Ultima atualizacao: '.date('d/m/Y H:i:s').'<br>';
    /*
    CAMINHO PARA INSERIR PESAGEM:
    http://campeonato.devco.com.br/api/pesagem/f8f3b9abeb362fbcadaa0b3e1e7a9edf?id_etapa=3&pesagem_id=147&id_pedido=5&peso=5.9871
    */
    if(defined('REMOTO')) {
        echo 'Executando em modo remoto<br>';
        define("SERVIDOR", "192.168.3.90");
        define("USER", "brasileiroempe02");
        define("PASSWORD", "iddqd1");
        define("DB", "brasileiroempe02");

        function conecta(){
            $dsn = "mysql:host=".SERVIDOR.";dbname=".DB.";charset=utf8";

            try{
                $conn = new PDO($dsn, USER, PASSWORD);
                $conn->exec("set names utf8");
                $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

                return $conn;
            }catch(PDOException $error){
                echo "Erro!: <br>" . utf8_encode($error->getMessage()) . "<br>";

                die("Não foi possível conectar ao banco de dados.");
            }
        }
    } else {
        require '../functions/conexao.php';
        echo 'Executando em modo local<br>';
    }

    $pdo =conecta();
try {
    /*PESAGENS NAO ENVIADAS PARA O SITE DO CBP*/
    $consultar = $pdo->prepare(
        "SELECT
				cad_etapas.id_etapa_integracao_brasileiro ID_CAD_ETAPA_CBP,
				cad_equipes.ID_CAD_PEDIDO_CBP,
				pesagens.ID,
				pesagens.ID_ETAPA,
				pesagens.COMPROVANTE,
				pesagens.ID_CAD_EQUIPE,
				pesagens.ID_CAD_COMPETIDOR,
				pesagens.PONTO_PESAGEM,
				pesagens.ID_CAD_ESPECIE,
				pesagens.DATA_HORA,
				pesagens.ID_CAD_MODALIDADE,
				pesagens.PESO,
				pesagens.EXCLUIDO,
				pesagens.OBS,
				pesagens.SERIAL_INSTRUMENTO_MED,
				pesagens.nome_fiscal,
				pesagens.enviado_site_brasileiro,
				pesagens.ID_CAD_ISCA,
				pesagens.MODERADO_EM,
				pesagens.MODERADO_ID_CAD_USUARIO
			FROM
				pesagens
				INNER JOIN cad_equipes ON ( cad_equipes.ID = pesagens.ID_CAD_EQUIPE )
				INNER JOIN cad_etapas ON ( cad_etapas.ID = pesagens.ID_ETAPA )
			WHERE
				pesagens.enviado_site_brasileiro IS NULL
				AND cad_etapas.id_etapa_integracao_brasileiro > 0
				AND pesagens.EXCLUIDO = 'NAO'
				AND DATE( pesagens.DATA_HORA ) <= CURRENT_DATE
				ORDER BY pesagens.ID ASC
		"
    );

    $consultar->execute();
    /*if ($consultar->rowCount() > 0) {
            die("Error:<br>".$conexao->error);
    }*/
    if ($consultar->rowCount() > 0) {
        while ($linha = $consultar->fetch(PDO::FETCH_OBJ)) {
             echo '<BR>'.$linha->ID.': ';
			$ch = curl_init();
			//$url = "https://brasileiroempesqueiros.com.br/api/pesagem/f8f3b9abeb362fbcadaa0b3e1e7a9edf?id_etapa=".$linha->ID_CAD_ETAPA_CBP."&pesagem_id=".$linha->ID."&id_pedido=".$linha->ID_CAD_PEDIDO_CBP."&peso=".$linha->PESO.'&id_competidor='.$linha->ID_CAD_COMPETIDOR;
            $url = "http://127.0.0.1/campeonato/api/pesagem/f8f3b9abeb362fbcadaa0b3e1e7a9edf?id_etapa=".$linha->ID_CAD_ETAPA_CBP."&pesagem_id=".$linha->ID."&id_pedido=".$linha->ID_CAD_PEDIDO_CBP."&peso=".$linha->PESO.'&id_competidor='.$linha->ID_CAD_COMPETIDOR;

			unset($linha->ID_CAD_ETAPA_CBP);
			unset($linha->ID_CAD_PEDIDO_CBP);
			$data_string = http_build_query(['pesagem' => $linha]);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

            $result=curl_exec($ch);

            if(is_numeric($result)) {

                try {
                      $cadastrar = $pdo->prepare("UPDATE pesagens SET enviado_site_brasileiro = NOW() WHERE ID = :ID");
                      $cadastrar->bindValue(':ID', $linha->ID, PDO::PARAM_STR);

                      $cadastrar->execute();

                    if ($cadastrar->rowCount() > 0) {
                        echo 'ok';
                    } else {
                        echo 'ERRO';
                    }

                } catch(PDOException $e2) {
                       echo $e2->getMessage();
                }

            }else{
                echo '<b>erro:</b><br>'.$result;
			}
			flush();
			ob_flush();
        }
    }
} catch(PDOException $e) {
       echo $e->getMessage();
}


?>
