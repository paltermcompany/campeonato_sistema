<?php
	header('Access-Control-Allow-Origin: *');
	header('Content-type: application/json');

    include '../functions/conexao.php';
    $serialColetor=$_GET['serialColetor'];

    if(isset($serialColetor)){
        $pdo = conecta();
        try {
            $consultar = $pdo -> prepare("  SELECT DISTINCT
                                                composicao_peixes_alvo.id,
                                                composicao_peixes_alvo.id_cad_especie,
                                                composicao_peixes_alvo.id_cad_etapa
                                            FROM
                                                composicao_peixes_alvo
                                                INNER JOIN cad_etapas ON ( cad_etapas.ID = composicao_peixes_alvo.id_cad_etapa )
                                                INNER JOIN cad_etapas_datas ON ( cad_etapas_datas.id_cad_etapa = cad_etapas.ID )
                                                INNER JOIN composicao_etapa_coletor ON ( composicao_etapa_coletor.id_cad_etapa = cad_etapas.ID )
                                                INNER JOIN cad_coletores ON ( cad_coletores.id = composicao_etapa_coletor.id_cad_coletor )
                                            WHERE
                                                cad_etapas_datas.data_etapa >= CURRENT_DATE
                                                AND cad_coletores.serial =  :serialColetor
                                            ORDER BY
                                                composicao_peixes_alvo.id_cad_etapa,
                                                composicao_peixes_alvo.id_cad_especie");

            $consultar -> bindValue(':serialColetor', $serialColetor, PDO::PARAM_STR);
            $consultar -> execute();
            if ($consultar -> rowCount() > 0) {
                while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
                    $array[] = array(
                        'id' => $linha -> id,
                        'id_cad_especie' => $linha -> id_cad_especie,
                        'id_cad_etapa' => $linha -> id_cad_etapa
                    );
                }
                if(isset($array)){
                    echo json_encode(array("composicao_peixes_alvo"=>$array));
                }
            }
        } catch(PDOException $e) {
            echo $e -> getMessage();
        }
    }

?>
