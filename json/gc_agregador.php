<?php

    define('RETORNAR', true);
    echo json_encode([
        'ESPECIES'    => include('gc_especies.php'),
        'ISCAS'       => include('gc_iscas.php'),
        'MODALIDADES' => include('gc_modalidades.php'),
        'TEMPOS'      => include('gc_tempos.php'),
        //'ETAPA_HOJE'  => include('gc_etapa_hoje.php'),
        //'EQUIPES_ETAPA' => include('gc_equipes_etapa.php'),
    ]);

?>
