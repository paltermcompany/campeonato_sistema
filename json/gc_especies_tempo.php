<?php
/*
    JSON DE ESPECIES DA ETAPA PARA GC
*/
    header("Access-Control-Allow-Origin: *");

    include '../functions/conexao.php';
    $pdo = conecta();
    $array = [];
    try {
        
        $consultar = $pdo -> prepare("  (SELECT
                                            count(p.ID) QTD_PESAGENS, 
                                            sum(p.PESO) PESO_TOTAL,
                                            es.ID,
                                            es.DESCRICAO,
                                            ( SELECT RETORNA_BATERIA_PESAGEM ( p.id_etapa, p.DATA_HORA ) ) BATERIA,
                                            es.COR_GC_HEX
                                        FROM
                                            pesagens p
                                            INNER JOIN cad_etapas e ON ( e.ID = p.ID_ETAPA )
                                            INNER JOIN cad_etapas_datas ed ON ( ed.id_cad_etapa = e.ID )
                                            INNER JOIN cad_especies es on (p.ID_CAD_ESPECIE = es.ID)
                                        WHERE
                                            ed.data_etapa = CURRENT_DATE
                                            AND p.EXCLUIDO = 'NAO'
                                        GROUP BY
                                            es.DESCRICAO,
                                            BATERIA
                                        ORDER BY 
                                            DESCRICAO,
                                            BATERIA,
                                            QTD_PESAGENS DESC)");

        $consultarNomes = $pdo -> prepare("  (SELECT DISTINCT
                                                es.ID,
                                                es.DESCRICAO,
                                                es.COR_GC_HEX,
                                                count(p.ID) QTD_PESAGENS,
                                                (SELECT
                                                    count(p.ID) TOTAL_PESAGENS
                                                FROM
                                                    pesagens p
                                                    INNER JOIN cad_etapas e ON ( e.ID = p.ID_ETAPA )
                                                    INNER JOIN cad_etapas_datas ed ON ( ed.id_cad_etapa = e.ID )
                                                    INNER JOIN cad_especies es on (p.ID_CAD_ESPECIE = es.ID)
                                                WHERE
                                                    ed.data_etapa = CURRENT_DATE
                                                    AND p.EXCLUIDO = 'NAO') TOTAL
                                                FROM
                                                        pesagens p
                                                        INNER JOIN cad_etapas e ON ( e.ID = p.ID_ETAPA )
                                                        INNER JOIN cad_etapas_datas ed ON ( ed.id_cad_etapa = e.ID )
                                                        INNER JOIN cad_especies es on (p.ID_CAD_ESPECIE = es.ID)
                                                WHERE
                                                        ed.data_etapa = CURRENT_DATE
                                                        AND p.EXCLUIDO = 'NAO'
                                                GROUP BY
                                                    es.DESCRICAO
                                                ORDER BY
                                            QTD_PESAGENS DESC)");
        $consultarNomes -> execute();
        $consultar -> execute();
        

        if ($consultarNomes -> rowCount() > 0) {
            $i = 1;
            $posicaoCor = 0;
            // CORES PARA AS BARRAS DO GC
            $coresGrafico = array("#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA");
            /*
            foreach($resultado as $result){
                echo $result['DESCRICAO']."\n";
            }
            */
            while($linhaNomes = $consultarNomes -> fetch(PDO::FETCH_OBJ)){

                $array[] = array(
                    'ID' => $linhaNomes -> ID,
                    'DESCRICAO' => $linhaNomes -> DESCRICAO,
                    'QTDS_TEMPO' => array(0, 0, 0, 0, 0),
                    'PESOS_TEMPO' => array(0, 0, 0, 0, 0),
                    //'COR_GC_HEX' => $linhaNomes -> COR_GC_HEX
                    'COR_GC_HEX' => $coresGrafico[$posicaoCor]
                );

                $posicaoCor++;

            }

            while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {

                for($j = 0; $j < count($array); $j++){

                    if($array[$j]['ID'] == $linha->ID){
                        $array[$j]['QTDS_TEMPO'][$linha->BATERIA] = (int)$linha->QTD_PESAGENS;
                        $array[$j]['PESOS_TEMPO'][$linha->BATERIA] = (double)$linha->PESO_TOTAL;
                    }

                }
            
            }
            /*
            while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {

                $teste = $array['DESCRICAO'];
                echo $teste;

                //$proximaLinha = $consultar ->fetch(PDO::FETCH_OBJ);

                //if($linha->ID == $proximaLinha->ID){
                //if($consultar -> fetch(PDO::FETCH_OBJ) -> ID == $linha -> ID){
                
                //$array[][]['QTDS_TEMPO'][$linha->BATERIA] = $linha->QTD_PESAGENS;

                //}
                //}
            }
            */
            
            if(isset($array)){
                if(defined('RETORNAR')) { return $array; }
                echo json_encode(array("ESPECIES"=>$array) );
            }
        }
    } catch(PDOException $e) {
        echo $e -> getMessage();
    }
