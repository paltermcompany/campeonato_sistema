<?php
    include '../../functions/conexao.php';
    set_time_limit(0);

    $pdo0 = conectaCBP();
    $pdo1 =conecta();
    $pdo2 =conecta();
    $pdo3 =conecta();
    $equipe = 0;
    $anterior = '';
    try {
        $consultar0 = $pdo0 -> prepare("SELECT
                                            s.id,
                                            s.id_pedido,
                                            s.nome,
                                            s.cpf,
                                            s.email,
                                            s.celular,
                                            s.sexo,
                                            s.nome_impresso,
                                            s.modelagem,
                                            s.tamanho,
                                            e.nome_completo etapa,
                                            e.id id_etapa
                                        FROM
                                            cp_cliente_pedidos_suplentes s
                                            INNER JOIN cp_cliente_pedidos p ON ( p.id = s.id_pedido )
                                            INNER JOIN cp_campeonato_classificatorios_etapa e ON ( e.id = p.id_etapa )
                                        WHERE
                                            e.data_campeonato > CURRENT_DATE");
        $consultar0 -> execute();

        if ($consultar0 -> rowCount() > 0) {
            while ($linha = $consultar0 -> fetch(PDO::FETCH_OBJ)) {
                echo "<br>id pedido:" . $linha -> id_pedido . '<br>';
                echo "nome suplente:" . $linha -> nome . '<br>';
                echo "etapa:" . $linha -> etapa . '<br>';

                /*suplente*/
                $suplente = $pdo1 -> prepare("SELECT
                                                    c.ID,
                                                    c.NOME,
                                                    c.TAMANHO_CAMISETA,
                                                    c.APELIDO,
                                                    c.DATA_MODIFICACAO,
                                                    c.DATA_NASCIMENTO,
                                                    c.EMAIL,
                                                    c.CELULAR,
                                                    c.SEXO
                                                FROM
                                                    cad_competidores c
                                                WHERE
                                                    c.CPF = :CPF_COMPETIDOR");
                $suplente -> bindValue(':CPF_COMPETIDOR', $linha -> cpf, PDO::PARAM_STR);
                $suplente -> execute();

                $params = [
                        ':NOME' => mb_strtoupper($linha -> nome),
                        ':TAMANHO_CAMISETA' => mb_strtoupper($linha -> tamanho),
                        ':MODELAGEM_CAMISETA' => ($linha -> modelagem),
                        ':APELIDO' => mb_strtoupper($linha -> nome_impresso),
                        ':EMAIL' => mb_strtoupper($linha -> email),
                        ':CELULAR' => $linha -> celular,
                        ':SEXO' => mb_strtoupper($linha -> sexo),
                        ':CPF' => $linha -> cpf
                    ];

                if ($suplente -> rowCount() > 0) {

                    $idSuplente = $suplente -> fetch(PDO::FETCH_OBJ);
                    $idSuplente = $idSuplente -> ID;

                    /*$sql = 'UPDATE cad_competidores
                            SET
                                NOME = :NOME,
                                TAMANHO_CAMISETA = :TAMANHO_CAMISETA,
                                MODELAGEM_CAMISETA = :MODELAGEM_CAMISETA,
                                APELIDO = :APELIDO,
                                DATA_MODIFICACAO = CURRENT_TIMESTAMP,
                                EMAIL = :EMAIL,
                                CELULAR = :CELULAR,
                                SEXO = :SEXO
                            WHERE
                                CPF = :CPF';

                    $suplente = $pdo1 -> prepare($sql);
                    $suplente->execute(($params));*/
                    echo 'atualizado competidor '.mb_strtoupper($linha -> nome).'<br>';



                }else{
                    $sql = 'INSERT INTO cad_competidores ( nome, cpf, tamanho_camiseta, modelagem_camiseta, apelido, email, celular, sexo, data_cadastro )
                            VALUES
                            (
                                :NOME,
                                :CPF,
                                :TAMANHO_CAMISETA,
                                :MODELAGEM_CAMISETA,
                                :APELIDO,
                                :EMAIL,
                                :CELULAR,
                                :SEXO,
                                CURRENT_TIMESTAMP
                            )';
                    $suplente = $pdo1 -> prepare($sql);
                    $suplente->execute(($params));
                    $idSuplente = $pdo1 -> lastInsertId();
                    echo 'inserido competidor '.mb_strtoupper($linha -> nome).'<br>';

                }
                //dump_pdo( $sql, $params );

                $geral = $pdo3 -> prepare(' SELECT
                                                GROUP_CONCAT( CPF ORDER BY CPF SEPARATOR "|" ) as CPFs,
                                                eq.DESCRICAO
                                            FROM
                                                cad_equipes eq
                                                LEFT JOIN cad_etapas et ON et.id = eq.ID_CAD_ETAPA
                                                LEFT JOIN composicao_equipes ce ON ce.id_equipe = eq.id
                                                LEFT JOIN cad_competidores cp ON cp.id = ce.id_competidor
                                            WHERE
                                                et.ID_CAD_CAMPEONATO = 3
                                                AND et.id_etapa_integracao_brasileiro = :ID_ETAPA_CBP
                                                AND ( CPF = :CPF)
                                            GROUP BY
                                                eq.id');

                $geral -> bindValue(':ID_ETAPA_CBP', $linha -> id_etapa, PDO::PARAM_STR);
                $geral -> bindValue(':CPF', ($linha -> cpf), PDO::PARAM_STR);
                $geral->execute();

                if($geral -> rowCount() > 0) {
                    $jatem = $geral -> fetch(PDO::FETCH_OBJ);
                    echo 'Já esta inserido na tabela como equipe '.$jatem->DESCRICAO.' cpfs '.$jatem->CPFs.'<br>';
                } else {
                    echo '<b>Não tem equipe inserida</b><br>';

                    $geral = $pdo3 -> prepare(' SELECT
                                                    eq.ID
                                                FROM
                                                    cad_equipes eq
                                                WHERE
                                                    eq.ID_CAD_PEDIDO_CBP = :ID_CAD_PEDIDO_CBP');

                    $geral -> bindValue(':ID_CAD_PEDIDO_CBP', $linha -> id_pedido, PDO::PARAM_STR);
                    $geral -> execute();

                    if($geral -> rowCount() > 0) {

                        $linha2 = $geral -> fetch(PDO::FETCH_OBJ);

                        $geral = $pdo3 -> prepare(' INSERT INTO composicao_equipes ( id_competidor, id_equipe, suplente )
                                                    VALUES
                                                (:ID_CAD_COMPETIDOR,
                                                :ID_CAD_EQUIPE,
                                                "S") ');

                        $geral -> bindValue(':ID_CAD_COMPETIDOR', $idSuplente, PDO::PARAM_STR);
                        $geral -> bindValue(':ID_CAD_EQUIPE', $linha2 -> ID, PDO::PARAM_STR);

                        $geral -> execute();

                        echo 'inserida composicao de equipe para competidor suplente '.$idSuplente.'<br>';
                    } else {
                        echo 'Nao encontrada equipe para esse suplente, verificar codigo do pedido '.$linha -> id_pedido;
                    }

                }

            }


        }
    } catch(PDOException $e) {
        echo $e -> getMessage();
    }

    function dump_pdo($query, $params){
       echo '<br>';
       var_dump(
           str_replace(array_keys($params), $params, $query)
       );
    }

    function mask($val, $mask){
        $maskared = '';
        $k = 0;
        for($i = 0; $i<=strlen($mask)-1; $i++){
            if($mask[$i] == '#'){
                if(isset($val[$k]))
                    $maskared .= $val[$k++];
            }else{
                if(isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }



?>
