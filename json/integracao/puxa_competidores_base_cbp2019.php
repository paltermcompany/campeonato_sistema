<?php
    include '../../functions/conexao.php';
    set_time_limit(0);
    $fazerUpdate = true;
    $inserirEquipes = false;
    $pdo0 = conectaCBP();
    $pdo1 =conecta();
    $pdo2 =conecta();
    $pdo3 =conecta();
    $equipe = 0;
    $anterior = '';
    try {
        $consultar0 = $pdo0 -> prepare("  SELECT
                                            p.id pedido,
                                            p.id_etapa,
                                            campeonato.titulo campeonato,
                                            etapa.nome etapa,
                                            p.id_cliente,
                                            c1.nome nome1,
                                            c1.cpf cpf1,
                                            c1.nome_impresso apelido1,
                                            c1.tamanho tamanho_camiseta1,
                                            c1.modelagem modelagem_camiseta1,
                                            c1.sexo sexo1,
                                            c1.email email1,
                                            c1.celular celular1,
                                            p.id_parceiro,
                                            c2.nome nome2,
                                            c2.cpf cpf2,
                                            c2.nome_impresso apelido2,
                                            c2.tamanho tamanho_camiseta2,
                                            c2.modelagem modelagem_camiseta2,
                                            c2.sexo sexo2,
                                            c2.email email2,
                                            c2.celular celular2
                                        FROM
                                            cp_cliente_pedidos p
                                            INNER JOIN cp_cliente c1 ON ( c1.id = p.id_cliente )
                                            INNER JOIN cp_cliente c2 ON ( c2.id = p.id_parceiro )
                                            INNER JOIN cp_campeonato_classificatorios_etapa etapa ON ( etapa.id = p.id_etapa )
                                            INNER JOIN cp_campeonato_classificatorios campeonato ON ( campeonato.id = etapa.id_campeonato_classificatorio )
                                        WHERE

                                            p.`status` = 'pago' and p.`data_excluido` is null and p.id_etapa in (22,23)");
                                            //p.data_alterado >= '2019-01-19 15:35' and
        $consultar0 -> execute();

        if ($consultar0 -> rowCount() > 0) {
            while ($linha = $consultar0 -> fetch(PDO::FETCH_OBJ)) {
                echo "<br>id etapa:" . $linha -> id_etapa . '<br>';
                echo "campeonato:" . $linha -> campeonato . '<br>';
                echo "etapa:" . $linha -> etapa . '<br>';
                echo "competidores:" . $linha -> nome1 . ' E ' . $linha -> nome2 . '<br>';
                //continue;
                /*
                    ID_ETAPA
                        03  -   RS 1    -   001 a 060
                        04  -   RS 2    -   061 a 120

                        15  -   SC 1    -   121 a 180
                        11  -   SC 2    -   181 a 240

                        19  -   PR 1    -   241 a 300
                        16  -   PR 2    -   301 a 360

                        13  -   MG 1    -   361 a 420
                        18  -   MG 2    -   420 a 480

                        10  -   GO 1    -   481 a 540
                        09  -   GO 2    -   541 a 600

                        17  -   SP 1    -   601 a 660
                        21  -   SP 2    -   661 a 720
                        22  -   SP 3    -   721 a 780
                        23  -   SP 4    -   781 a 840

                */
                if($linha -> id_etapa == 3){
                    if($anterior != $linha -> id_etapa){
                        $anterior = $linha -> id_etapa;
                        $equipe = 0;
                    }
                }

                if($linha -> id_etapa == 4){
                    if($anterior != $linha -> id_etapa){
                        $anterior = $linha -> id_etapa;
                        $equipe = 60;
                    }
                }

                if($linha -> id_etapa == 15){
                    if($anterior != $linha -> id_etapa){
                        $anterior = $linha -> id_etapa;
                        $equipe = 120;
                    }
                }

                if($linha -> id_etapa == 11){
                    if($anterior != $linha -> id_etapa){
                        $anterior = $linha -> id_etapa;
                        $equipe = 180;
                    }
                }

                if($linha -> id_etapa == 19){
                    if($anterior != $linha -> id_etapa){
                        $anterior = $linha -> id_etapa;
                        $equipe = 240;
                    }
                }

                if($linha -> id_etapa == 16){
                    if($anterior != $linha -> id_etapa){
                        $anterior = $linha -> id_etapa;
                        $equipe = 300;
                    }
                }

                if($linha -> id_etapa == 13){
                    if($anterior != $linha -> id_etapa){
                        $anterior = $linha -> id_etapa;
                        $equipe = 360;
                    }
                }

                if($linha -> id_etapa == 18){
                    if($anterior != $linha -> id_etapa){
                        $anterior = $linha -> id_etapa;
                        $equipe = 420;
                    }
                }

                if($linha -> id_etapa == 10){
                    if($anterior != $linha -> id_etapa){
                        $anterior = $linha -> id_etapa;
                        $equipe = 480;
                    }
                }

                if($linha -> id_etapa == 9){
                    if($anterior != $linha -> id_etapa){
                        $anterior = $linha -> id_etapa;
                        $equipe = 540;
                    }
                }

                if($linha -> id_etapa == 17){
                    if($anterior != $linha -> id_etapa){
                        $anterior = $linha -> id_etapa;
                        $equipe = 600;
                    }
                }

                if($linha -> id_etapa == 21){
                    if($anterior != $linha -> id_etapa){
                        $anterior = $linha -> id_etapa;
                        $equipe = 660;
                    }
                }

                if($linha -> id_etapa == 22){
                    if($anterior != $linha -> id_etapa){
                        $anterior = $linha -> id_etapa;
                        $equipe = 720;
                    }
                }

                if($linha -> id_etapa == 23){
                    if($anterior != $linha -> id_etapa){
                        $anterior = $linha -> id_etapa;
                        $equipe = 780;
                    }
                }

                /*competidor 1*/
                $competidor1 = $pdo1 -> prepare("SELECT
                                                    c.ID,
                                                    c.NOME,
                                                    c.TAMANHO_CAMISETA,
                                                    c.APELIDO,
                                                    c.DATA_MODIFICACAO,
                                                    c.DATA_NASCIMENTO,
                                                    c.EMAIL,
                                                    c.CELULAR,
                                                    c.SEXO
                                                FROM
                                                    cad_competidores c
                                                WHERE
                                                    c.CPF = :CPF_COMPETIDOR");
                $competidor1 -> bindValue(':CPF_COMPETIDOR', mask($linha -> cpf1,'###.###.###-##'), PDO::PARAM_STR);
                $competidor1 -> execute();

                $params = [
                        ':NOME' => mb_strtoupper($linha -> nome1),
                        ':TAMANHO_CAMISETA' => mb_strtoupper($linha -> tamanho_camiseta1),
                        ':MODELAGEM_CAMISETA' => ($linha -> modelagem_camiseta1),
                        ':APELIDO' => mb_strtoupper($linha -> apelido1),
                        ':EMAIL' => mb_strtoupper($linha -> email1),
                        ':CELULAR' => $linha -> celular1,
                        ':SEXO' => mb_strtoupper($linha -> sexo1),
                        ':CPF' => mask($linha -> cpf1,'###.###.###-##')
                    ];

                if ($competidor1 -> rowCount() > 0) {
                    if($fazerUpdate){
                        $id1 = $competidor1 -> fetch(PDO::FETCH_OBJ);
                        $id1 = $id1 -> ID;

                        $sql = 'UPDATE cad_competidores
                                SET
                                    NOME = :NOME,
                                    TAMANHO_CAMISETA = :TAMANHO_CAMISETA,
                                    MODELAGEM_CAMISETA = :MODELAGEM_CAMISETA,
                                    APELIDO = :APELIDO,
                                    DATA_MODIFICACAO = CURRENT_TIMESTAMP,
                                    EMAIL = :EMAIL,
                                    CELULAR = :CELULAR,
                                    SEXO = :SEXO
                                WHERE
                                    CPF = :CPF';

                        $competidor1 = $pdo1 -> prepare($sql);
                        $competidor1->execute(($params));
                        echo 'atualizado competidor '.mb_strtoupper($linha -> nome1).'<br>';
                    }


                }else{
                    $sql = 'INSERT INTO cad_competidores ( nome, cpf, tamanho_camiseta, modelagem_camiseta, apelido, email, celular, sexo, data_cadastro )
                            VALUES
                            (
                                :NOME,
                                :CPF,
                                :TAMANHO_CAMISETA,
                                :MODELAGEM_CAMISETA,
                                :APELIDO,
                                :EMAIL,
                                :CELULAR,
                                :SEXO,
                                CURRENT_TIMESTAMP
                            )';
                    /*$competidor1 = $pdo1 -> prepare($sql);
                    $competidor1->execute(($params));
                    $id1 = $pdo1 -> lastInsertId();*/
                    echo 'inserido competidor '.mb_strtoupper($linha -> nome1).'<br>';

                }
                //dump_pdo( $sql, $params );


                /*competidor 2*/
                $competidor2 = $pdo2 -> prepare("SELECT
                                                    c.ID,
                                                    c.NOME,
                                                    c.TAMANHO_CAMISETA,
                                                    c.APELIDO,
                                                    c.DATA_MODIFICACAO,
                                                    c.DATA_NASCIMENTO,
                                                    c.EMAIL,
                                                    c.CELULAR,
                                                    c.SEXO
                                                FROM
                                                    cad_competidores c
                                                WHERE
                                                    c.CPF = :CPF_COMPETIDOR");
                $competidor2 -> bindValue(':CPF_COMPETIDOR', mask($linha -> cpf2,'###.###.###-##'), PDO::PARAM_STR);
                $competidor2 -> execute();

                $params = [
                        ':NOME' => mb_strtoupper($linha -> nome2),
                        ':TAMANHO_CAMISETA' => mb_strtoupper($linha -> tamanho_camiseta2),
                        ':MODELAGEM_CAMISETA' => ($linha -> modelagem_camiseta2),
                        ':APELIDO' => mb_strtoupper($linha -> apelido2),
                        ':EMAIL' => mb_strtoupper($linha -> email2),
                        ':CELULAR' => $linha -> celular2,
                        ':SEXO' => mb_strtoupper($linha -> sexo2),
                        ':CPF' => mask($linha -> cpf2,'###.###.###-##')
                    ];

                if ($competidor2 -> rowCount() > 0) {
                    if($fazerUpdate){
                        $id2 = $competidor2 -> fetch(PDO::FETCH_OBJ);
                        $id2 = $id2 -> ID;

                        $sql = 'UPDATE cad_competidores
                                SET
                                    NOME = :NOME,
                                    TAMANHO_CAMISETA = :TAMANHO_CAMISETA,
                                    MODELAGEM_CAMISETA = :MODELAGEM_CAMISETA,
                                    APELIDO = :APELIDO,
                                    DATA_MODIFICACAO = CURRENT_TIMESTAMP,
                                    EMAIL = :EMAIL,
                                    CELULAR = :CELULAR,
                                    SEXO = :SEXO

                                WHERE
                                    CPF = :CPF';

                        //dump_pdo( $sql, $params );

                        $competidor2 = $pdo2 -> prepare($sql);
                        $competidor2->execute(($params));
                        echo 'atualizado competidor '.mb_strtoupper($linha -> nome2).'<br>';
                    }
                }else{
                    $sql = 'INSERT INTO cad_competidores ( nome, cpf, tamanho_camiseta, modelagem_camiseta, apelido, email, celular, sexo, data_cadastro )
                            VALUES
                            (
                                :NOME,
                                :CPF,
                                :TAMANHO_CAMISETA,
                                :MODELAGEM_CAMISETA,
                                :APELIDO,
                                :EMAIL,
                                :CELULAR,
                                :SEXO,
                                CURRENT_TIMESTAMP
                            )';

                    /*$competidor2 = $pdo2 -> prepare($sql);
                    $competidor2->execute(($params));
                    $id2 = $pdo2 -> lastInsertId();*/
                    echo 'inserior competidor '.mb_strtoupper($linha -> nome2).'<br>';
                }

                /*if($inserirEquipes){
                    $geral = $pdo3 -> prepare(' SELECT
                                                    GROUP_CONCAT( CPF ORDER BY CPF SEPARATOR "|" ) as CPFs,
                                                    eq.DESCRICAO
                                                FROM
                                                    cad_equipes eq
                                                    LEFT JOIN cad_etapas et ON et.id = eq.ID_CAD_ETAPA
                                                    LEFT JOIN composicao_equipes ce ON ce.id_equipe = eq.id
                                                    LEFT JOIN cad_competidores cp ON cp.id = ce.id_competidor
                                                WHERE
                                                    et.ID_CAD_CAMPEONATO = 3
                                                    AND et.id_etapa_integracao_brasileiro = :ID_ETAPA_CBP
                                                    AND ( CPF = :CPF1 OR CPF = :CPF2 )
                                                GROUP BY
                                                    eq.id');
                    $geral -> bindValue(':ID_ETAPA_CBP', $linha -> id_etapa, PDO::PARAM_STR);
                    $geral -> bindValue(':CPF1', mask($linha->cpf1,'###.###.###-##'), PDO::PARAM_STR);
                    $geral -> bindValue(':CPF2', mask($linha->cpf2,'###.###.###-##'), PDO::PARAM_STR);
                    $geral->execute();

                    if($geral -> rowCount() > 0) {
                        $jatem = $geral -> fetch(PDO::FETCH_OBJ);
                        echo 'Já esta inserido na tabela como equipe '.$jatem->DESCRICAO.' cpfs '.$jatem->CPFs.'<br>';
                    } else {
                        echo '<b>Não tem equipe isnerida</b><br>';
                    }

                    $geral = $pdo3 -> prepare(' SELECT
                                                    id ID
                                                FROM
                                                    cad_etapas
                                                WHERE
                                                    ID_CAD_CAMPEONATO = 3
                                                    AND id_etapa_integracao_brasileiro = :ID_ETAPA_CBP');
                    $geral -> bindValue(':ID_ETAPA_CBP', $linha -> id_etapa, PDO::PARAM_STR);
                    $geral->execute();


                    if ( $inserirEquipes && $geral -> rowCount() > 0) {
                        $equipe++;
                        $idEtapa = $geral -> fetch(PDO::FETCH_OBJ);
                        $idEtapa = $idEtapa -> ID;

                        $geral = $pdo3 -> prepare(' INSERT INTO cad_equipes ( DESCRICAO, ID_CAD_ETAPA, ID_CAD_PEDIDO_CBP )
                                                    VALUES
                                                        (LPAD( :DESCRICAO_EQUIPE, 3, 0),
                                                        :ID_CAD_ETAPA,
                                                        :ID_CAD_PEDIDO) ');

                        $geral -> bindValue(':DESCRICAO_EQUIPE', $equipe, PDO::PARAM_STR);
                        $geral -> bindValue(':ID_CAD_ETAPA', $idEtapa, PDO::PARAM_STR);
                        $geral -> bindValue(':ID_CAD_PEDIDO', $linha -> pedido, PDO::PARAM_STR);
                        $geral -> execute();
                        $idEquipe = $pdo3 -> lastInsertId();

                        echo '<b>inserida equipe '.mb_strtoupper($equipe).'</b><br>';


                        $geral = $pdo3 -> prepare(' INSERT INTO composicao_equipes ( id_competidor, id_equipe )
                                                    VALUES
                                                        (:ID_CAD_COMPETIDOR,
                                                        :ID_CAD_EQUIPE) ');

                        $geral -> bindValue(':ID_CAD_COMPETIDOR', $id1, PDO::PARAM_STR);
                        $geral -> bindValue(':ID_CAD_EQUIPE', $idEquipe, PDO::PARAM_STR);
                        $geral -> execute();

                        echo 'inserida composicao de equipe competidor1 '.$id1.'<br>';

                        $geral = $pdo3 -> prepare(' INSERT INTO composicao_equipes ( id_competidor, id_equipe )
                                                    VALUES
                                                        (:ID_CAD_COMPETIDOR,
                                                        :ID_CAD_EQUIPE) ');

                        $geral -> bindValue(':ID_CAD_COMPETIDOR', $id2, PDO::PARAM_STR);
                        $geral -> bindValue(':ID_CAD_EQUIPE', $idEquipe, PDO::PARAM_STR);
                        $geral -> execute();

                        echo 'inserida composicao de equipe competidor2 '.$id2.'<br>';

                    }
                }*/





                /*$array[] = array(
                    'id_cliente' => $linha -> id_cliente,
                    'nome1' => $linha -> nome1,
                    'cpf1' => mask($linha -> cpf1,'###.###.###-##'),
                    'apelido1' => $linha -> apelido1,
                    'tamanho_camiseta1' => $linha -> tamanho_camiseta1,
                    'sexo1' => $linha -> sexo1,
                    'email1' => $linha -> email1,
                    'celular1' => $linha -> celular1,
                    'id_parceiro' => $linha -> id_parceiro,
                    'nome2' => $linha -> nome2,
                    'cpf2' => mask($linha -> cpf2,'###.###.###-##'),
                    'apelido2' => $linha -> apelido2,
                    'tamanho_camiseta2' => $linha -> tamanho_camiseta2,
                    'sexo2' => $linha -> sexo2,
                    'email2' => $linha -> email2,
                    'celular2' => $linha -> celular2,
                    'id_etapa' => $linha -> id_etapa,
                    'campeonato' => $linha -> campeonato,
                    'etapa' => $linha -> etapa
                );*/
            }


        }
    } catch(PDOException $e) {
        echo $e -> getMessage();
    }

    function dump_pdo($query, $params){
       echo '<br>';
       var_dump(
           str_replace(array_keys($params), $params, $query)
       );
    }

    function mask($val, $mask){
        $maskared = '';
        $k = 0;
        for($i = 0; $i<=strlen($mask)-1; $i++){
            if($mask[$i] == '#'){
                if(isset($val[$k]))
                    $maskared .= $val[$k++];
            }else{
                if(isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }



?>
