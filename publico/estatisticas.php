<?php
    header('Content-Type: text/html; charset=utf-8');
    include '../functions/conexao.php';
    require '../functions/crud.php';
    $etapa = retornaEtapaHoje(null);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="author" content="Douglas Lessing">
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Campeonatos FishTV</title>
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="shortcut icon" type="image/ico" href="../favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <meta http-equiv="refresh" content="120">

</head>

<body>


<div class="container">
    <div class="row header">
        <?php
            if($etapa = retornaEtapaHoje(null)){
                echo '<div class="col-xs-4 text-center logo-etapa" onclick="voltaHome()"><h3><img src="data:image/png;base64,'. $etapa -> imagem.' " alt="Logo Etapa"></h3></div>';
                echo '<div class="col-xs-8 nome-etapa"><h3>'.$etapa -> DESCRICAO.'</h3></div>';
            }else{
                echo '<div class="panel panel-default">';
                echo '<div class="panel-heading">';
                echo '<div class="text-center" onclick="voltaHome()"><img src="../img/logo.png" alt="Logo Etapa"></div><br>';
                echo '<h3 class="panel-title text-center">Nenhuma etapa sendo realizada hoje</h3>';
                echo '</div>';
                echo '</div>';
            }
        ?>
    </div>
    <?php
        echo '<div class="margem-header"></div>';
    ?>
    <?php
    if($etapa != null){
    ?>
    <div class="graficos-etapa">
        <h3 class="text-center">Toque nos gráficos para ver as informações</h3>
        <input type="hidden" id="idEtapa" name="etapa" value=<?php echo $etapa -> ID ?>>
        <canvas id="graficoDuplasComPeixe" height=250></canvas>
        <canvas id="graficoModalidade" height=250></canvas>
        <canvas id="graficoEspecies" height=250></canvas>
        <canvas id="graficoIscas" height=250></canvas>
        <canvas id="graficoTempos" height=250></canvas>
    </div>
    <?php
    }else{
        echo '<h3 class="text-center" onclick="voltaHome()">Voltar</h3>';
    }
    ?>
</div>

</body>
<script type="text/javascript" src="../bower_components/chartjs/Chart.min.js"></script>
<script type="text/javascript" src="js/exibeGraficos.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="../js/custom_publico.js"></script>
</html>