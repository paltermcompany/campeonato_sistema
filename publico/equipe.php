<?php
    header('Content-Type: text/html; charset=utf-8');
    include '../functions/conexao.php';
    require '../functions/crud.php';
    $etapa = retornaEtapaHoje(null);
    $idEquipe = $_GET['idEquipe'];
    $idCompetidores = retornaCompetidoresEtapa($idEquipe);
    $equipe = retornaEquipe($idEquipe);
    $competidores = '';

    if ($resultado = retornaNomesEquipe($idEquipe)){
        foreach($resultado as $result){
            $competidores .= ' - ' . $result->apelido;

        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="author" content="Douglas Lessing">
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Campeonatos FishTV</title>
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="shortcut icon" type="image/ico" href="../favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/equipe.css">
    <meta http-equiv="refresh" content="120">


</head>

<body>


<div class="container">
    <div class="row header">
        <?php
            if($etapa = retornaEtapaHoje(null)){
                echo '<div class="col-xs-4 text-center logo-etapa" onclick="voltaHome()"><h3><img src="data:image/png;base64,'. $etapa -> imagem.' " alt="Logo Etapa"></h3></div>';
                echo '<div class="col-xs-8 nome-etapa"><h3>'.$etapa -> DESCRICAO.'</h3></div>';
                echo '<input type="hidden" id="idEtapa" name="etapa" value="'. $etapa -> ID .'">';
            }else{
                echo '<div class="panel panel-default">';
                echo '<div class="panel-heading">';
                echo '<div class="text-center" onclick="voltaHome()"><img src="../img/logo.png" alt="Logo Etapa"></div><br>';
                echo '<h3 class="panel-title text-center">Nenhuma etapa sendo realizada hoje</h3>';
                echo '</div>';
                echo '</div>';
            }
        ?>
    </div>

    <?php
        echo '<div class="margem-header"></div>';
    ?>

    <ul class="list-group">
        <li class="list-group-item text-center"><h4><?php echo $equipe -> DESCRICAO .  $competidores; ?></h4></li>
        <?php
            if($posicaoAtualRanking = retornaPosicaoRankingPublico($etapa -> ID, $idEquipe)){
                echo '<li class="list-group-item text-center"><b>Posição no ranking:</b> <br>'.$posicaoAtualRanking -> posicao.'º</li>';
                echo '<input type="hidden" id="idEquipe" name="equipe" value="'. $idEquipe .'">';
            }else{
                echo '<li class="list-group-item text-center"><b>Posição no ranking:</b> <br>Nenhum exemplar capturado</li>';
            }

            if($mpEquipe = retornaResultadoEtapaMaiorPeixe($etapa -> ID, '', $idEquipe)){
                echo '<li class="list-group-item text-center"><b>Maior exemplar da equipe:</b> <br>'. $mpEquipe -> especie .' - '.$mpEquipe -> peso .' Kg</li>';
            }else{
                echo '<li class="list-group-item text-center"><b>Maior exemplar da equipe:</b> <br>Nenhum examplar capturado</li>';
            }
            if($posicaoAtualRanking){
                echo '<li class="list-group-item text-center"><b>Quantidade de pesagens:</b> <br>'. $posicaoAtualRanking -> peso.' Kg ('.$posicaoAtualRanking -> quantidade.' exemplares)</li>';
            }else{
                echo '<li class="list-group-item text-center"><b>Quantidade de pesagens:</b> <br>Nenhum exemplar capturado</li>';
            }
        ?>
    </ul>
    <div class="col-xs-12">
        <button class="btn btn-lg btn-block botao-padrao" type="button" id="botaoEstatisticas">
            <h4>Estatísticas</h4>
        </button>
    </div>
    <div class="graficos col-xs-12 escondido">
        <h3>Toque nos gráficos ver as informações</h3>
        <canvas id="graficoModalidadeEquipe" height=250></canvas>
        <canvas id="graficoEspeciesEquipe" height=250> </canvas>
        <canvas id="graficoIscasEquipe" height=250></canvas>
        <canvas id="graficoPesagensTempoEquipe" height=250></canvas>
    </div>
    <br>

    <div id="table-wrapper" class="col-xs-12">
        <h3>Pesagens</h3>
        <div id="table-scroll-equipe">
            <table class="table table-striped" id="tableRanking" name="tableRanking">
                <thead>
                    <tr>
                        <th><span class="text">Compr.</span></th>
                        <th><span class="text">Espécie</span></th>
                        <th ><span class="text">Hora</span></th>
                        <th ><span class="text">Peso</span></th>
                    </tr>
                </thead>
                <tbody>

                <?php
                    $ranking = '';


                    if ($resultado = retornaResultadosEquipe($idEquipe)){
                        foreach($resultado as $result){
                            $ranking.= '<tr>';
                            if($result -> OBS == ''){
                                $ranking.= '<td width="10%" align="right">...'.$result -> COMPROVANTE .'</td>';
                                $ranking.= '<td width="30%" align="left">'.$result -> ESPECIE .'</td>';
                                $ranking.= '<td width="10%" align="right">'.$result -> HORA .'</td>';
                                $ranking.= '<td width="20%" height="50px" align="right" >'.$result -> PESO .' Kg</td>';
                            }else{
                                $ranking.= '<td colspan="4" style="background: rgb(255, 179, 179);" align="left">Registro Excluido ('.$result -> OBS.')</td>';
                            }
                            $ranking.= '</tr>';
                        }
                    }else{
                        $ranking.= '<tr>';
                            $ranking.= '<td colspan="4">Sem Resultados</td>';
                        $ranking.= '</tr>';
                    }
                    echo $ranking;
                ?>

                </tbody>
            </table>
        </div>
    </div>

    
</div>

</body>
<script type="text/javascript" src="../bower_components/chartjs/Chart.min.js"></script>
<script type="text/javascript" src="js/exibeGraficosEquipe.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="../js/custom_publico.js"></script>
</html>
