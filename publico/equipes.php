<?php
    header('Content-Type: text/html; charset=utf-8');
    include '../functions/conexao.php';
    require '../functions/crud.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="author" content="Douglas Lessing">
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Campeonatos FishTV</title>
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="shortcut icon" type="image/ico" href="../favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>

<div class="container">
    <div class="row header">
        <?php
            if($etapa = retornaEtapaHoje(null)){
                echo '<div class="col-xs-4 text-center logo-etapa" onclick="voltaHome()"><h3><img src="data:image/png;base64,'. $etapa -> imagem.' " alt="Logo Etapa"></h3></div>';
                echo '<div class="col-xs-8 nome-etapa"><h3>'.$etapa -> DESCRICAO.'</h3></div>';
            }else{
                echo '<div class="panel panel-default">';
                echo '<div class="panel-heading">';
                echo '<div class="text-center" onclick="voltaHome()"><img src="../img/logo.png" alt="Logo Etapa"></div><br>';
                echo '<h3 class="panel-title text-center">Nenhuma etapa sendo realizada hoje</h3>';
                echo '</div>';
                echo '</div>';
            }
        ?>
    </div>

    <?php
        echo '<div class="margem-header"></div>';
    ?>

    <?php
    if($etapa != null){
    ?>
    <div id="table-wrapper">
        <div id="table-scroll" class="listaEquipes">

        </div>
    </div>
    <?php
    }else{
        echo '<h3 class="text-center" onclick="voltaHome()">Voltar</h3>';
    }
    ?>
</div>

</body>
<script type="text/javascript" src="../js/custom_publico.js"></script>
<script>
$( document ).ready(function() {
    retornaEquipes(<?php echo $etapa -> ID;?>);

    function retornaEquipes(idEtapa){
        $('.listaEquipes').html('<center><img src="fish.gif" width="100%"><br><h3>CARREGANDO<h3></center>');
        $.post('../ajax/controller.php',{
            acao:"publicRetornaEquipes",
            idEtapa:idEtapa
        },function(retorno){
            $tabela = '';
            $tabela += '<table class="table table-striped" id="tableEquipes" name="tableEquipes">';
            $tabela += '<thead>';
            $tabela += '<tr>';
            $tabela += '<th><span class="text">Equipe</span></th>';
            $tabela += '</tr>';
            $tabela += '</thead>';
            $tabela += '<tbody>';

            $.each(JSON.parse(retorno), function (index, value) {
                $.each(this, function (index, value) {
                    //console.log(this.equipe + this.competidores);
                    $tabela += '<tr onClick="abreEquipe('+this.id_equipe+')"><td width="100%" height="50px" align="left" ><h5>'+this.equipe+this.competidores+'</h5></td></tr>';
                });

            $tabela += '</tbody></table>';
            });

            $('.listaEquipes').html($tabela);
        });
    }
});
</script>
</html>
