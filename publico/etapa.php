<?php
    header('Content-Type: text/html; charset=utf-8');
    include '../functions/conexao.php';
    require '../functions/crud.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="author" content="Douglas Lessing">
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Campeonatos FishTV</title>
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/custom_publico.js"></script>
    <link rel="shortcut icon" type="image/ico" href="../favicon.ico"/>
    <link rel="stylesheet" href="css/etapa.css">
</head>
<body>
    <div class="container">
        <div class="row">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php
                        if($etapa = retornaEtapaHoje(null)){
                            echo '<div class="text-center logo-etapa"><img src="data:image/png;base64,'. $etapa -> imagem.' " alt="Logo Etapa" /></div>';
                            echo '<div class="trapezio"><div class="parte-cima"></div><div class="parte-baixo"></div></div>';
                            echo '<h1 class="text-center nome-etapa"><b>'.$etapa -> DESCRICAO.'</b></h1>';
                        }else{
                            echo '<div class="text-center"><img src="../img/logo.png" alt="Logo Etapa"></div><br>';
                            echo '<h3 class="panel-title text-center">Nenhuma etapa sendo realizada hoje</h3>';
                        }
                    ?>


                </div>
                <div class="panel-body">
                    <form role="form" name="form_login" id="form_login" method="post" action="" >
                        <fieldset>
                            <button class="btn btn-lg btn-block botao-padrao" type="button" onClick="abreRanking()">
                                <h4>Ranking Geral</h4>
                            </button>
                            <button class="btn btn-lg btn-block botao-padrao" type="button" onClick="abreEquipes()">
                                <h4>Equipes</h4>
                            </button>
                            <button class="btn btn-lg btn-block botao-padrao" type="button" onClick="abreHistorico()">
                                <h4>Histórico</h4>
                            </button>
                            <button class="btn btn-lg btn-block botao-padrao" type="button" onClick="abreEstatisticas()">
                                <h4>Estatísticas</h4>
                            </button>
                        </fieldset>
                    </form>
                </div>
            </div>

        </div>
    </div>
</body>

</html>
