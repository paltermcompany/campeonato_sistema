function criaGraficos(item, qtdItem, nomeDoElemento, tipoGrafico, nomeDoGrafico){
        
    var elementoGrafico = $(nomeDoElemento);

    var dadosGrafico = {
        datasets:[{
            backgroundColor: ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"],              
            data: qtdItem
        }],

        labels: item
    };

    var comecaNoZero

    if(tipoGrafico == 'horizontalBar'){
        comecaNoZero={
                xAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
        }
    }

    var opcoes = {
        title:{
            display: true,
            text: nomeDoGrafico,
            fontSize: 15
        },
        tooltips:{
            callbacks:{
                label: function(tooltipItem, data){

                    var dataset = data.datasets[tooltipItem.datasetIndex];
                    var total = 0;
                    var valorAtual = dataset.data[tooltipItem.index];

                    for(var i = 0; i < dataset.data.length; i++){
                        total = total + parseInt(dataset.data[i], 10);
                    }

                    var porcentagem = Math.floor(((valorAtual / total) * 100) + 0.5);

                    return data.labels[tooltipItem.index]+ ': ' + valorAtual + ' - ' +porcentagem + '%';

                }
            }
        },
        legend:{
            display: false
        },
        scales: comecaNoZero
    };

    new Chart(elementoGrafico, {
        type: tipoGrafico,
        data: dadosGrafico,
        options: opcoes
    });

}