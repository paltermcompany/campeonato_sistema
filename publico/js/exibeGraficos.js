var idEtapa = $('#idEtapa').val();
var idEquipe = $('#idEquipe').val();

$.post('../ajax/controller.php', {
    acao: 'detalhesModalidadeLiveNew2',
    idEtapa: idEtapa
}, function(retorno){

    var modalidade = [];
    var qtdModalidade = [];

    for(var i in retorno){
        modalidade.push(retorno[i].modalidade);
        qtdModalidade.push(retorno[i].total);
    }

    criaGraficos(modalidade, qtdModalidade, '#graficoModalidade', 'pie', 'Modalidades');
}, "json");

$.post('../ajax/controller.php', {
    acao: 'detalhesEspecieLiveNew2',
    idEtapa: idEtapa
}, function(retorno){
    
    var especie = [];
    var qtdEspecie = [];

    for(var i in retorno){
        especie.push(retorno[i].especie);
        qtdEspecie.push(retorno[i].total);
    }

    criaGraficos(especie, qtdEspecie, '#graficoEspecies', 'pie', 'Espécies');

}, "json");

$.post('../ajax/controller.php', {
    acao: 'detalhesIscaLiveNew2',
    idEtapa: idEtapa
}, function(retorno){

    var isca = [];
    var qtdIscas = [];

    for(var i in retorno){
        isca.push(retorno[i].isca);
        qtdIscas.push(retorno[i].total);
    }

    criaGraficos(isca, qtdIscas, '#graficoIscas', 'pie', "Iscas");

}, "json");

$.post('../ajax/controller.php', {
    acao: 'detalhesHoraLiveNew2',
    idEtapa: idEtapa
}, function(retorno){
    var intervaloHora = [];
    var qtdPeixes = [];

    for(var i in retorno){
        intervaloHora.push(retorno[i].hora);
        qtdPeixes.push(retorno[i].total);
    }

    criaGraficos(intervaloHora, qtdPeixes, '#graficoTempos', 'horizontalBar', 'Pesagens divididas em intervalos de tempo');

}, "json")

$.post('../ajax/controller.php', {
    acao:"retornaDetalhesEstatisticasEquipes",
    idEtapa: idEtapa
}, function(retorno){

    var elementoGrafico = $('#graficoDuplasComPeixe');
    var dadosGrafico;

    var equipesTotal;
    var equipesComPesagens;
    var equipesSemPesagens;

    $.each(JSON.parse(retorno), function(index, obj){
        
        $.each(this, function(k, v){

            if(k == 0){
                equipesTotal = v.total;
            }else if(k == 1){
                equipesComPesagens = v.total;
            }else if(k == 2){
                equipesSemPesagens = equipesTotal - equipesComPesagens;
            }
        });

    });


    dadosGrafico = new Chart(elementoGrafico, {
        type: "doughnut",
        data:{
            datasets:[{
                backgroundColor:[
                    "#0074D9",
                    "#2ECC40",
                    "#FF4136"
                ],
                data:[
                    equipesTotal
                ]
            },
            {
                backgroundColor:[
                    "#0074D9",
                    "#2ECC40",
                    "#FF4136"
                ],
                data:[
                    0,
                    equipesComPesagens,
                    equipesSemPesagens
                ]
                
            }],
            labels:[
                "Total de Equipes",
                "Já pegaram peixe",
                "Não pegaram peixe"
            ],
            
        },
        options:{
            title:{
                display:true,
                text: 'Equipes',
                fontSize: 15
            },
            legend:{
                display: false
            }
        }
    })

});