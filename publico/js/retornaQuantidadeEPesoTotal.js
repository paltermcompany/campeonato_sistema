function retornaQuantidadeEPesoTotal(idEtapa){
    var pesoTotal = $('#pesoTotal');
    var qtdTotal = $('#qtdTotal');

    $.post('../ajax/controller.php', {
        acao: 'resultadosEtapa',
        idEtapa: idEtapa
    }, function(retorno){
        var json = JSON.parse(retorno);
        pesoTotal.text(json.data[0].total_peso);
        qtdTotal.text(json.data[0].total_quantidade);
    });
}



