var idEtapa = $('#idEtapa').val();
var idEquipe = $('#idEquipe').val();

var botao = $('#botaoEstatisticas');
var graficos = $('.graficos');


botao.on('click', exibeGraficos);

function exibeGraficos(){
    graficos.toggleClass('active');
    graficos.toggleClass('escondido');
    graficos.toggleClass('exibido');
}

$.post('../ajax/controller.php', {
    acao: 'detalhesModalidadeEquipeLiveNew2',
    idEtapa: idEtapa,
    idEquipe: idEquipe
}, function(retorno){

    var modalidade = [];
    var qtdModalidade = [];

    for(var i in retorno){
        modalidade.push(retorno[i].modalidade);
        qtdModalidade.push(retorno[i].total);
    }

    criaGraficos(modalidade, qtdModalidade, '#graficoModalidadeEquipe', 'pie', 'Modalidades');
}, "json");

$.post('../ajax/controller.php', {
    acao: 'detalhesEspecieEquipeLiveNew2',
    idEtapa: idEtapa,
    idEquipe: idEquipe
}, function(retorno){
    
    var especie = [];
    var qtdEspecie = [];

    for(var i in retorno){
        especie.push(retorno[i].especie);
        qtdEspecie.push(retorno[i].total);
    }

    criaGraficos(especie, qtdEspecie, '#graficoEspeciesEquipe', 'pie', 'Espécies');

}, "json");

$.post('../ajax/controller.php', {
    acao: 'detalhesIscaEquipeLiveNew2',
    idEtapa: idEtapa,
    idEquipe: idEquipe
}, function(retorno){

    var isca = [];
    var qtdIscas = [];

    for(var i in retorno){
        isca.push(retorno[i].isca);
        qtdIscas.push(retorno[i].total);
    }

    criaGraficos(isca, qtdIscas, '#graficoIscasEquipe', 'pie', "Iscas");

}, "json");

$.post('../ajax/controller.php', {
    acao: 'detalhesHoraEquipeLiveNew2',
    idEtapa: idEtapa,
    idEquipe: idEquipe
}, function(retorno){
    var intervaloHora = [];
    var qtdPeixes = [];

    for(var i in retorno){
        intervaloHora.push(retorno[i].hora);
        qtdPeixes.push(retorno[i].total);
    }

    criaGraficos(intervaloHora, qtdPeixes, '#graficoPesagensTempoEquipe', 'horizontalBar', 'Pesagens divididas em intervalos de tempo');

}, "json")