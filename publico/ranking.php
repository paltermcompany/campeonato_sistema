<?php
    header('Content-Type: text/html; charset=utf-8');
    include '../functions/conexao.php';
    require '../functions/crud.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="author" content="Douglas Lessing">
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Campeonatos FishTV</title>
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="shortcut icon" type="image/ico" href="../favicon.ico"/>
    <meta http-equiv="refresh" content="120">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/ranking.css">
    <link href="../bower_components/font-awesome/css/all.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="../dist/css/adminlte.min.css">

</head>
<body>

<div class="container">
    <div class="row header">
        <?php
            if($etapa = retornaEtapaHoje(null)){
                echo '<div class="col-xs-4 text-center logo-etapa" onclick="voltaHome()"><h3><img src="data:image/png;base64,'. $etapa -> imagem.' " alt="Logo Etapa"></h3></div>';
                echo '<div class="col-xs-8 nome-etapa"><h3>'.$etapa -> DESCRICAO.'</h3></div>';
            }else{
                echo '<div class="panel panel-default">';
                echo '<div class="panel-heading">';
                echo '<div class="text-center" onclick="voltaHome()"><img src="../img/logo.png" alt="Logo Etapa"></div><br>';
                echo '<h3 class="panel-title text-center">Nenhuma etapa sendo realizada hoje</h3>';
                echo '</div>';
                echo '</div>';
            }
        ?>
    </div>

    <?php
        echo '<div class="margem-header"></div>';
    ?>

    <div class="col-xs-12 info-etapa">
        <div class="col-xs-6">
            <div class="small-box bg-info" id="cardQtdTotal">
                <div class="inner">
                    <h3 id="qtdTotal">0</h3>
                    <p>Qtd Total</p>
                </div>
                <div class="icon">
                    <i class="fas fa-calculator"></i>
                </div>
            </div>
        </div>

        <div class="col-xs-6">
            <div class="small-box bg-info" id="cardPesoTotal">
                <div class="inner">
                    <h3 id="pesoTotal">0</h3>
                    <p>Peso Total</p>
                </div>
                <div class="icon">
                    <i class="fas fa-balance-scale"></i>
                </div>
            </div>
        </div>
    </div>

    <?php
    if($etapa != null){
    ?>
    <div class="listaRanking col-xs-12">



    </div>
    <?php
    }else{
        echo '<h3 class="text-center" onclick="voltaHome()">Voltar</h3>';
    }
    ?>

</body>
<script type="text/javascript" src="../js/custom_publico.js"></script>
<script type="text/javascript" src="js/retornaQuantidadeEPesoTotal.js"></script>
<script>
$( document ).ready(function() {
    retornaRanking(<?php echo $etapa -> ID;?>);
    retornaQuantidadeEPesoTotal(<?php echo $etapa -> ID;?>);

    function retornaRanking(idEtapa){
        //$('.listaRanking').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando..</center>');
        $('.listaRanking').html('<center><img width="100%" src="fish.gif"><br><h3>CARREGANDO<h3></center>');

        $.post('../ajax/controller.php',{
            acao:"publicRetornaRanking",
            idEtapa:idEtapa
        },function(retorno){
            $tabela = '';
            /*
            $tabela += '<ul class="list-group">';
                $tabela += '<li class="list-group-item text-center"><b>Quantidade Total:</b> <br>xxx exemplares</li>';
                $tabela += '<li class="list-group-item text-center"><b>Peso Total</b> <br>xxxx Kg</li>';
            $tabela += '</ul>';
            */
            $tabela += '<div id="table-wrapper">';
            $tabela += '<div id="table-scroll" >';
            $tabela += '<table class="table table-striped" id="tableRanking" name="tableRanking">';
            $tabela += '<thead>';
            $tabela += '<tr>';
            $tabela += '<th><span class="text">#</span></th>';
            $tabela += '<th><span class="text">Equipe</span></th>';
            $tabela += '<th><span class="text">Peso Total</span></th>';
            $tabela += '</tr>';
            $tabela += '</thead>';
            $tabela += '<tbody>';

            $.each(JSON.parse(retorno), function (index, value) {
                $.each(this, function (index, value) {
                    //console.log(this.equipe + this.competidores);
                    $tabela += '<tr onClick="abreEquipe('+this.id_equipe+')">';
                    $tabela += '<td width="5%" align="center"><h4><span class="label label-primary">'+this.posicao+'</h4></span></td>';
                    $tabela += '<td width="70%" height="50px" align="left" ><h5>'+this.equipe+this.competidores+'</h5></td>';
                    $tabela += '<td width="25%" style="white-space: nowrap; text-overflow: ellipsis;" align="right"><h5>'+this.peso+' Kg</h5></td>';
                    $tabela += '</tr>';
                });


            });
            $tabela += '</tbody></table></div></div>';
            $('.listaRanking').html($tabela);
        });
    }
});
</script>
</html>
