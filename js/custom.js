$(document).ready(function() {

	$('.upcase').keyup(function(){
		$(this).val($(this).val().toUpperCase());
	});

	var conteudo = $('.modal-body');
	var titulo = $('.modal-title');

	$('form[name="form_atualizaPerfilUsuario"]').submit(function() {

		var forma = $(this);
		var botao = $(this).find(':button');
		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=alteraPerfilUsuario&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				console.log('retorno - ' + retorno);
				botao.attr('disabled', false).html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar alterações');
				alert('Alterações efetuadas com sucesso');
				$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
				$('#page-wrapper').load('user-profile.php?idUsuario='+retorno);

			}
		});
		return false;
	});


	$('form[name="form_login"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');

		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=login&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				console.log(retorno);
				botao.attr('disabled', false).html('Logar');
				if (retorno === 'user') {
					msg('Usuário Inválido', 'erro');
				}
				if (retorno === 'correto') {
					$(location).attr('href', '../pages/index.php');
					msg('Logado Com Sucesso', 'sucesso');
				}
				if (retorno === 'pass') {
					msg('Senha Inválida', 'erro');
				}
				if(retorno === 'desativado'){
					msg('Usuário desativado', 'alerta');
				}
			}
		});
		return false;
	});

	$('form[name="form_newCadEspecie"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');

		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=insereEspecie&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				console.log(retorno);
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Cadastro Realizado', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraEspecies" ).submit();
				}
			}
		});
		return false;
	});

	$('form[name="form_newCadModalidade"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');

		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=insereModalidade&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				//console.log(retorno);
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Cadastro Realizado', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraModalidades" ).submit();
				}
			}
		});
		return false;
	});

	$('form[name="form_newCadIsca"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');

		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=insereIsca&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Cadastro Realizado', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraIscas" ).submit();
				}
			}
		});
		return false;
	});

	$('form[name="form_newCadColetor"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');

		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=insereColetor&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Cadastro Realizado', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraColetores" ).click();
				}
			}
		});
		return false;
	});

	$('form[name="form_newCadFiscal"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');

		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=insereFiscal&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Cadastro Realizado', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraFiscais" ).click();
				}
			}
		});
		return false;
	});

	$('form[name="form_newCadCompetidor"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');

		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=insereCompetidor&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);
				if (retorno === 'ok') {
					msg('Cadastro Realizado', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraCompetidores" ).click();
				}else{
					if ((retorno.search("Duplicate entry") > 0) && (retorno.search("CPF") > 0)){
						$msg = 'Dados informados (CPF) já estão cadastrados no sistema!';
					}else{
						$msg = retorno;
					}
					$('#alerta').html('<div class="alert alert-danger"><strong>Atenção!</strong><br>'+$msg+'</div>');
				}
			}
		});
		return false;
	});

	$('form[name="form_newCadEtapa"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');

		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=insereEtapa&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Cadastro Realizado', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraEtapas" ).submit();
				}
			}
		});
		return false;
	});

	$('form[name="form_newCadEquipe"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');
		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=insereEquipe&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				console.log(retorno);
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Cadastro Realizado', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraEquipes" ).click();
				}
			}
		});
		return false;
	});

	$('form[name="form_editCadEspecie"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');

		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=editaEspecie&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				console.log(retorno);
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Alterado com sucesso', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraEspecies" ).submit();
				}
			}
		});
		return false;
	});

	$('form[name="form_editCadEquipe"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');

		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=editaEquipe&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				console.log(retorno);
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Alterado com sucesso', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraEquipes" ).submit();
				}
			}
		});
		return false;
	});

	$('form[name="form_editCadModalidade"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');

		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=editaModalidade&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				console.log(retorno);
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Alterado com sucesso', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraModalidades" ).submit();
				}
			}
		});
		return false;
	});

	$('form[name="form_editCadIsca"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');

		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=editaIsca&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				console.log(retorno);
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Alterado com sucesso', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraIscas" ).submit();
				}
			}
		});
		return false;
	});

	$('form[name="form_editCadColetor"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');

		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=editaColetor&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				console.log(retorno);
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Alterado com sucesso', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraColetores" ).click();
				}
			}
		});
		return false;
	});

	$('form[name="form_editCadFiscal"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');

		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=editaFiscal&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				console.log(retorno);
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Alterado com sucesso', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraFiscais" ).click();
				}
			}
		});
		return false;
	});

	$('form[name="form_editCadCompetidor"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');

		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=editaCompetidor&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				console.log(retorno);
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Alterado com sucesso', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraCompetidores" ).click();
				}else{
					if ((retorno.search("Duplicate entry") > 0) && (retorno.search("CPF") > 0)){
						$msg = 'Dados informados (CPF) já estão cadastrados no sistema!';
					}else{
						$msg = retorno;
					}
					$('#alerta').html('<div class="alert alert-danger"><strong>Atenção!</strong><br>'+$msg+'</div>');
				}
			}
		});
		return false;
	});

	$('form[name="form_editCadEtapa"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find('.btn-salva');
		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=editaEtapa&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				console.log(retorno);
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Alterado com sucesso', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraEtapas" ).submit();
				}
			}
		});
		return false;
	});

	$('form[name="form_editCadCampeonato"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');
		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=editaCampeonato&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				console.log(retorno);
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Alterado com sucesso', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraCampeonatos" ).submit();
				}
			}
		});
		return false;
	});


	$('form[name="form_newCadCampeonado"]').submit(function() {
		var forma = $(this);
		var botao = $(this).find(':button');

		$.ajax({
			url : "../ajax/controller.php",
			type : "POST",
			data : "acao=insereCampeonato&" + forma.serialize(),
			beforeSend : function() {
				botao.html('Aguarde').attr('disabled', true);
			},
			success : function(retorno) {
				console.log(retorno);
				botao.html('<i class="glyphicon glyphicon-floppy-disk"></i> Salvar').attr('disabled', false);;
				if (retorno === 'ok') {
					msg('Cadastro Realizado', 'sucesso');
					$('#buscaProjetos').modal('hide');
					$( "#buttonFiltraCampeonatos" ).submit();
				}
			}
		});
		return false;
	});

	//Funções Gerais
	function msg(msg, type) {
		var retorno = $('.retornar');
		var tipo = (type == 'sucesso') ? 'success' : (type == 'alerta') ? 'warning' : (type == 'erro') ? 'danger' : (type == 'info') ? 'info' : alert('INFORME O TIPO DA MENSAGEM');

		retorno.empty().fadeOut('fast', function() {
			return $(this).html('</br><div class="alert alert-' + tipo + '" role="alert">' + msg + '</div>').fadeIn('slow');
		});
	}


});

function placeholderPrincipal(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
}

function startCountdown(){
	if((g_iCount - 1) >= 0){
		g_iCount = g_iCount - 1;

		$('#numberCountdown').html(' atualiza em: ' + g_iCount);
		setTimeout('startCountdown()',1000);
		if(g_iCount==0){
			checkPrevias();
			console.log('checkando prévias');
		}
	}
}

function voltaHome(){
	placeholderPrincipal();
	$('#page-wrapper').load('dashboard.php');

	/*if (typeof g_iCount == "undefined") {
		g_iCount = 30;
		startCountdown();
	}else{
		g_iCount = 30;
	}*/
}

function abreModeracao(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('moderacao.php');
}

function abreCadastroCampeonatos(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('cad-campeonatos.php');
}

function abreCadastroModalidades(){
	$('#page-wrapper').html('<center><i class="fas fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('cad-modalidades.php');
}

function abreCadastroIscas(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('cad-iscas.php');
}

function abreCadastroFiscaisProva(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('cad-fiscais.php');
}

function abreCadastroColetores(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('cad-coletores.php');
}

function abreCadastroEspecies(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('cad-especies.php');
}

function abreCadastroCompetidores(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('cad-competidores.php');
}

function abreCadastroEtapas(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('cad-etapas.php');
}

function abreComposicaoEquipes(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('composicao-equipes.php');
}

function abreComposicaoEtapas(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('composicao-etapas.php');
}

function abreComposicaoRaias(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('composicao-raias.php');
}

function abreComposicaoRaiasSeleciona($idEtapa){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('organizacao-raias.php?idEtapa='+$idEtapa);
}

function abreComposicaoEtapasSeleciona($idEtapa){
	$('#infoListaEtapas').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#infoListaEtapas').load('composicao-etapas-seleciona.php?idEtapa='+$idEtapa);
}

function abreComposicaoEquipesSeleciona($idEtapa, $idEquipe){
	$('#infoListaEquipes').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#infoListaEquipes').load('composicao-equipes-seleciona.php?idEtapa='+$idEtapa+'&idEquipe='+$idEquipe);
}

function abreCadastroEquipes(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('cad-equipes.php');
}

function abreResultadoEtapas(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('resultados.php');
}

function abreResultadoCampeonato(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('resultados-campeonatos.php');
}

function abreRelacaoRaias(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('raias.php');
}


function abreLancamentosEtapas(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('lancamentos.php');
}

function abreCompetidoresEtapas(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('competidores.php');
}

function abreDetalhesEtapas(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('resumo-etapa.php');
}

function abreCadastrosAtualizados(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('contatos-atualizados.php');
}

function abreBoasVindas(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('boas-vindas.php');
}

function abreEtiquetaCamiseta(){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('etiqueta-camiseta.php');
}

function abrePerfilUsuario($idUsuario){
	$('#page-wrapper').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
	$('#page-wrapper').load('user-profile.php?idUsuario='+$idUsuario);
}

function imprimiDetalhesEtapa($idEtapa){
	window.open("print-detalhes.php?idEtapa="+$idEtapa);
}

function imprimiResultadoEtapas($idEtapa, $horario, $rankingcompleto, $mostranome){
	window.open("print-resultados.php?idEtapa="+$idEtapa+"&horario="+$horario+"&rankingcompleto="+$rankingcompleto+"&mostranome="+$mostranome);
}

function imprimiContatosAtualizadosUF($dataDe, $dataAte){
	window.open("print-contatos-atualizados.php?dataDe="+$dataDe+"&dataAte="+$dataAte);
}

function imprimiCompetidoresEtapa($idEtapa){
	window.open("print-competidores.php?idEtapa="+$idEtapa);
}

function imprimiRaiasEtapa($idEtapa){
	window.open("print-raias.php?idEtapa="+$idEtapa);
}

function imprimiBoasVindas($idEtapa){
	window.open("print-boas-vindas.php?idEtapa="+$idEtapa);
}

function imprimiEtiquetasCamisetas($idEtapa, $tipoEtiqueta){
	if($tipoEtiqueta == 'frente'){
		window.open("print-etiqueta-camiseta.php?idEtapa="+$idEtapa);
	}else{
		window.open("print-etiqueta-camiseta-costas.php?idEtapa="+$idEtapa);
	}
}

function abreModalNovaEspecie(){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Cadastro de Espécies");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('new-cad-especie.php');
};

function abreModalNovaModalidade(){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Cadastro de Modalidade");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('new-cad-modalidade.php');
};

function abreModalNovaIsca(){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Cadastro de Iscas");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('new-cad-isca.php');
};

function abreModalNovoColetor(){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Cadastro de Coletores");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('new-cad-coletor.php');
};

function abreModalNovoFiscal(){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Cadastro de Fiscais");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('new-cad-fiscal.php');
};

function abreModalNovaEquipe(){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Cadastro de Equipes");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('new-cad-equipe.php');
};

function abreModalNovoCompetidor(){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Cadastro de Competidores");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('new-cad-competidor.php');
};

function abreModalNovoCampeonato(){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Cadastro de Campeonatos");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('new-cad-campeonato.php');
};

function abreModalNovaEtapa(){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Cadastro de Etapas");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('new-cad-etapa.php');
};

function abreModalEditaEspecie($idEspecie){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Editar Espécies");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('edit-cad-especies.php?idEspecie='+$idEspecie);
};

function abreModalEditaEquipe($idEquipe){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Editar Equipe");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('edit-cad-equipe.php?idEquipe='+$idEquipe);
};

function abreModalEditaModalidade($idModalidade){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Editar Modalidades");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('edit-cad-modalidade.php?idModalidade='+$idModalidade);
};

function abreModalEditaIsca($idIsca){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Editar Iscas");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('edit-cad-isca.php?idIsca='+$idIsca);
};

function abreModalEditaColetor($idColetor){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Editar Coletor");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('edit-cad-coletor.php?idColetor='+$idColetor);
};

function abreModalEditaFiscal($idFiscal){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Editar Fiscal");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('edit-cad-fiscal.php?idFiscal='+$idFiscal);
};

function abreModalEditaCampeonato($idCampeonato){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Editar Campeonato");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('edit-cad-campeonato.php?idCampeonato='+$idCampeonato);
};

function abreModalEditaEtapa($idEtapa){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Editar Etapa");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('edit-cad-etapa.php?idEtapa='+$idEtapa);
};

function abreModalEditaCompetidor($idCompetidor){
	$('#buscaProjetos').modal({
		backdrop : 'static'
	});
	$('.modal-title').html("Editar Competidor");
	$('.modal-body').html("Carregando...");
	$('.modal-body').load('edit-cad-competidor.php?idCompetidor='+$idCompetidor);
};


function naoImplementado(){
	alert('Ops, funcão ainda não implementada..')
}






//mascaras
//cnpj
function MascaraCNPJ(cnpj){
	if(mascaraInteiro(cnpj)==false){
		event.returnValue = false;
	}
	return formataCampo(cnpj, '00.000.000/0000-00', event);
}

//inteiro
function mascaraInteiro(){
	if (event.keyCode < 48 || event.keyCode > 57){
		event.returnValue = false;
		return false;
	}
	return true;
}

//telefone
function MascaraTelefone(tel){
	if(mascaraInteiro(tel)==false){
		event.returnValue = false;
	}
	return formataCampo(tel, '(00) 0000-0000', event);
}

//cep
function MascaraCep(cep){
	if(mascaraInteiro(cep)==false){
		event.returnValue = false;
	}
	return formataCampo(cep, '00000-000', event);
}

//formata de forma generica os campos
function formataCampo(campo, Mascara, evento) {
	var boleanoMascara;

	var Digitato = evento.keyCode;
	exp = /\-|\.|\/|\(|\)| /g
	campoSoNumeros = campo.value.toString().replace( exp, "" );

	var posicaoCampo = 0;
	var NovoValorCampo="";
	var TamanhoMascara = campoSoNumeros.length;;

	if (Digitato != 8) { // backspace
		for(i=0; i<= TamanhoMascara; i++) {
			boleanoMascara  = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".")
													|| (Mascara.charAt(i) == "/"))
			boleanoMascara  = boleanoMascara || ((Mascara.charAt(i) == "(")
													|| (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " "))
			if (boleanoMascara) {
					NovoValorCampo += Mascara.charAt(i);
					  TamanhoMascara++;
			}else {
					NovoValorCampo += campoSoNumeros.charAt(posicaoCampo);
					posicaoCampo++;
			  }
		  }
		campo.value = NovoValorCampo;
		  return true;
	}else {
		return true;
	}
}
