$(document).ready(function() {

    $('.upcase').keyup(function(){
        $(this).val($(this).val().toUpperCase());
    });


    //Funções Gerais
    function msg(msg, type) {
        var retorno = $('.retornar');
        var tipo = (type == 'sucesso') ? 'success' : (type == 'alerta') ? 'warning' : (type == 'erro') ? 'danger' : (type == 'info') ? 'info' : alert('INFORME O TIPO DA MENSAGEM');

        retorno.empty().fadeOut('fast', function() {
            return $(this).html('</br><div class="alert alert-' + tipo + '" role="alert">' + msg + '</div>').fadeIn('slow');
        });
    }


});
function abreRanking(){
    location.href = "ranking.php";
}

function abreEquipes(){
    location.href = "equipes.php";
}

function abreEquipe(idEquipe){
    location.href = "equipe.php?idEquipe="+idEquipe;
}

function abreHistorico(idEtapa){
    location.href = "historico.php";
}

function abreEstatisticas(){
    location.href = "estatisticas.php"
}

function voltaHome(){
    location.href = "etapa.php";
}

