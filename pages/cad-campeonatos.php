<?php
	session_start();
	include '../functions/conexao.php';
	require '../functions/crud.php';
?>

<script type="text/javascript" src="../js/custom.js"></script>

<script>
	var tableCadCampeonatos;

	function filtraCampeonatos(){
		tableCadCampeonatos.ajax.reload();
	}

	$(function() {

		tableCadCampeonatos = $('#tableCadCampeonatos').DataTable({
			"ajax": {
				url: "../ajax/controller.php",
				type: "POST",
				data : function(d){
					d.acao = "carregaCampeonatos",
					d.filtroCampeonatoNome = $('#filtroCampeonatoNome').val()
				},
			    beforeSend : function() {
                    $('#buttonFiltraCampeonatos').html('Aguarde').attr('disabled', true);
                },
                complete : function() {
                    $('#buttonFiltraCampeonatos').html('Filtrar').attr('disabled', false);
                }
			},
			"columns": [
				{"data": "id_campeonato"},
				{"data": "nome_campeonato"},
				{
                    "orderable": false,
                    "defaultContent": '<div class="btn-group btn-group-xs"><button type="button" class="btn btn-warning btnAlteraCampeonato"><span class="glyphicon glyphicon-edit"></span> </button><button type="button" class="btn btn-danger" onclick="naoImplementado()"><span class="glyphicon glyphicon-remove"></span></button></div>'
                },

			],
			"language": {
				"url": "../bower_components/datatables/Portuguese-Brasil.json"
			},
			"paging": true,
			"lengthChange": true,
			"searching": false,
			"destroy": true,
			"ordering": true,
			"order": [[ 1, "asc" ]],
			"info": true,
			"autoWidth": false
		} );

		$('#tableCadCampeonatos tbody').on( 'click', 'button', '.btnAlteraCampeonato', function () {
	        var data = tableCadCampeonatos.row( $(this).parents('tr') ).data();
	        abreModalEditaCampeonato(data.id_campeonato);
	    } );
	});
</script>

<div id="propria">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Cadastro de Campeonatos  <button onClick="abreModalNovoCampeonato()" type="button" class="close glyphicon glyphicon-plus text-muted" ><span aria-hidden="true"></span></button></h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<form name="form_filtraCampeonatos" id="form_filtraCampeonatos" method="post" >
				<div class="well form-group">
					<h4>Filtros</h4>
					<div class="row">
						<div class="col-lg-11">
							<label>Descrição</label>
							<input type="text" id="filtroCampeonatoNome" name="filtroCampeonatoNome" class="form-control upcase">
						</div>

						<div class="col-lg-1 pull-right">
						<label></label>
							<div class="input-group">
								<button class="btn btn-primary" name="buttonFiltraCampeonatos" id="buttonFiltraCampeonatos" style="background-color:#005081" type="button" onclick="filtraCampeonatos()"><b>Filtrar</b></button>
							</div>
						</div>
					</div>

				</div>
			</form>
			<div class="row" >
				<div class="col-lg-12">
					<table id="tableCadCampeonatos" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Descrição</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody id="resultadoFiltroCampeonatos">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

