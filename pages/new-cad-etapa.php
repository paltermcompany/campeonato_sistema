<?php

	include '../functions/conexao.php';
	require '../functions/crud.php';
?>

<script>
	function alteraUnidadeMedida(idTipoUnidadeMedida){
		if(idTipoUnidadeMedida==1){
			$('#newCadDescricaoUnidadeMedida').html('Kg');
		}else{
			if(idTipoUnidadeMedida==2){
				$('#newCadDescricaoUnidadeMedida').html('Metros');
			}
		}
	}
	alteraUnidadeMedida($( '#newCadUnidadeMedidaEtapa option:selected' ).val());
</script>

	<script type="text/javascript" src="../js/custom.js"></script>
	<div class="panel-body">
		<form role='form' method='post' action='' name='form_newCadEtapa'>
			<fieldset>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label>Nome Etapa</label>
							<input id="newCadNomeEtapa" name="newCadNomeEtapa" class="form-control upcase" placeholder="Nome Etapa" required>
						</div>
						<div class="form-group">
							<label>Nome Etapa (Impressão App)</label>
							<input id="newCadNomeEtapaApp" name="newCadNomeEtapaApp" class="form-control upcase" placeholder="Nome Etapa para App" required>
						</div>
						<div class="form-group">
							<label>Campeonato</label>
							<select name="newCadCampeonatoEtapa" id="newCadCampeonatoEtapa" class="form-control">
								<?php
									$campeonatos = retornaCampeonatos('', 'A');
									foreach ($campeonatos as $result){
										echo '<option value="'.$result->id.'">'.$result->descricao.'</option>';
									}
								 ?>
							</select>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-lg-6">
									<label>Unidade Medida</label>
									<select name="newCadUnidadeMedidaEtapa" id="newCadUnidadeMedidaEtapa" class="form-control" onchange="alteraUnidadeMedida($( '#newCadUnidadeMedidaEtapa option:selected' ).val())">
										<?php
											$unidadesMedida = retornaUnidadesMedida('');
											foreach ($unidadesMedida as $result){
												echo '<option value="'.$result->id.'">'.$result->descricao.'</option>';
											}
										 ?>
									</select>
								</div>
								<div class="col-lg-6">
									<label>Moderação Medidas</label>
										<select name="newCadModeracaoEtapa" id="newCadModeracaoEtapa" class="form-control">
											<option value="N">Não usar sistema de moderação</option>;
											<option value="S">Usar sistema de moderação</option>;
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label>Exemplar Mínimo</label>
							<label><sub id="newCadDescricaoUnidadeMedida"></sub></label>
							<input id="newCadPesoMinimo" name="newCadPesoMinimo" class="form-control" placeholder="Exemplar Mínimo" required type="number">
						</div>
						<div class="form-group">
							<label for="historicoDataRecontato">Data Realização</label>
							<div class="input-group date time form_datetime" data-link-field="historicoDataRecontato">
								<input type="text" name="historicoDataRecontato2" id="historicoDataRecontato2" class="form-control" type="text" readonly required>
								<span class="input-group-addon"><span class="glyphicon glyphicon-remove" ></span></span>
								<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
							</div>
							<input type="hidden" name="historicoDataRecontato" id="historicoDataRecontato" />
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Imagem</label><br>
								Selecione uma imagem: <input id="fileEtapa" type="file" name="img">
								<div id="imagemEtapa">

								</div>
								<input type="hidden" id="newCadImagemEtapa" value="" name="newCadImagemEtapa">

							</div>
						</div>
					</div>
				</div>
			</fieldset>
			<div class="span6 pull-right">
				<button id="botaoInsereEtapa" name="botaoInsereEtapa" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
			</div>
		</form>
	</div>



	<script type='text/javascript'>

		$('.form_datetime').datetimepicker({
			weekStart: 0,
			todayBtn:  1,
			autoclose: 1,
			format: 'dd/mm/yyyy',
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0,
			language: 'pt-BR',
			showMeridian: 1
		});

		$('#fileEtapa').change(function(){
			var files = document.getElementById('fileEtapa').files;
			if (files.length > 0) {
				getBase64(files[0]);
			}
		});

		function getBase64(fileEtapa) {
			var reader = new FileReader();
			var base64_str;
			reader.readAsDataURL(fileEtapa);
			reader.onload = function () {
				base64_str = reader.result;
				base64_str = base64_str.replace('data:image/jpeg;base64,', "");
				$('#imagemEtapa').html('<img src="data:image/jpeg;base64, '+base64_str+'" alt="Logo Etapa" />');
				$('#newCadImagemEtapa').val(base64_str);

			};
			reader.onerror = function (error) {
				console.log('Error: ', error);
			};
		}
	</script>
