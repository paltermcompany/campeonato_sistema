<?php
    session_start();
    header('Content-Type: text/html; charset=utf-8');
    include '../functions/conexao.php';
    require '../functions/crud.php';
    $idEtapa = $_REQUEST['idEtapa'];
    $etapa = retornaEtapa($idEtapa);


    $raias = '';
    if ($competidores = retornaCompetidoresAgrupadosEtapa($idEtapa)){
        foreach($competidores as $competidor){

            if($competidor->RAIA_INICIAL == 0){
                $raia = '';
            }else{
                $raia = $competidor->RAIA_INICIAL;
            }

            $raias.= '<tr style="white-space: nowrap">';
                $raias.= '<td style="white-space: nowrap" align="left">'.($competidor -> NOME_EQUIPE).'</td>';
                $raias.= '<td style="white-space: nowrap" align="left">'.($competidor -> APELIDOS_COMPETIDORES).'</td>';
                $raias.= '<td align="right">'.$competidor -> ID_PULSEIRAS.'</td>';

                if($etapa -> QTD_BATERIAS != null){
                    $qtdBaterias = $etapa -> QTD_BATERIAS;
                }else{
                    $qtdBaterias = 0;
                }

                $raias.= '<td style="white-space: nowrap" align="right">'.$raia.'</td>';
                /*funcao para definicao de raias*/
                for ($i = 1; $i <= $qtdBaterias-1; $i++) {

                    if($etapa -> ID_CAD_TIPO_ORG_RAIA == null){
                        $raias.= '<td style="white-space: nowrap" align="left">Organização de raias não definida na etapa</td>';
                    }else{
                        if($competidor->RAIA_INICIAL == 0){
                            $raias.= '<td style="white-space: nowrap" align="right">0</td>';
                        }else{

                            $tipoOrgRaia = $etapa -> ID_CAD_TIPO_ORG_RAIA;

                            if($tipoOrgRaia == 1){
                                if ($raia >= 1 && $raia <= 15) {
                                    $raia = intval($raia) + 15;
                                }else{
                                    if ($raia >= 16 && $raia <= 23) {
                                        $raia = intval($raia) + 37;
                                    }else{
                                        if ($raia >= 24 && $raia <= 30) {
                                            $raia = intval($raia) + 22;
                                        }else{
                                            if ($raia >= 31 && $raia <= 37) {
                                                $raia = intval($raia) - 22;
                                            }else{
                                                if ($raia >= 38 && $raia <= 45) {
                                                    $raia = intval($raia) - 37;
                                                }else{
                                                    if ($raia >= 46 && $raia <= 60) {
                                                        $raia = intval($raia) - 15;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            /*ORGANIZACAO DE RAIAS TIPO 2*/
                            if($tipoOrgRaia == 2){
                                if ($raia >= 1 && $raia <= 15) {
                                    $raia = intval($raia) + 15;
                                }else{
                                    if ($raia >= 16 && $raia <= 30) {
                                        $raia = intval($raia) + 15;
                                    }else{
                                        if ($raia >= 31 && $raia <= 45) {
                                            $raia = intval($raia) + 15;
                                        }else{
                                            if ($raia >= 46 && $raia <= 60) {
                                                $raia = intval($raia) - 45;
                                            }
                                        }
                                    }
                                }
                            }

                            /*ORGANIZACAO DE RAIAS TIPO 3*/
                            if($tipoOrgRaia == 3){
                                //segunda bateria
                                if(i==0){
                                    if ($raia >= 1 && $raia <= 32) {
                                        $raia = intval($raia) + 10;
                                    }else{
                                        if ($raia >= 33 && $raia <= 42) {
                                            $raia = intval($raia) - 32;
                                        }
                                    }
                                }else{
                                    //terceira bateria
                                    if(i==1){
                                        if ($raia >= 1 && $raia <= 31) {
                                            $raia = intval($raia) + 11;
                                        }else{
                                            if ($raia >= 32 && $raia <= 42) {
                                                $raia = intval($raia) - 31;
                                            }
                                        }
                                    }else{
                                        //quarta bateria
                                        if(i==2){
                                            if ($raia >= 1 && $raia <= 32) {
                                                $raia = intval($raia) + 10;
                                            }else{
                                                if ($raia >= 33 && $raia <= 42) {
                                                    $raia = intval(raia) - 32;
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            $raias.= '<td style="white-space: nowrap" align="right">'.$raia.'</td>';
                        }
                    }
                }

            $raias.= '</tr>';
        }
    }else{
        $raias.= '<tr>';
            $raias.= '<td colspan="'.(3 + $etapa -> QTD_BATERIAS).'">Sem Resultados</td>';
        $raias.= '</tr>';
    }



$qtdBaterias = $etapa -> QTD_BATERIAS;
$headerBaterias = '';
for ($i = 1; $i <= $qtdBaterias; $i++) {
    $headerBaterias .= '<th scope="col" align="right">Raia Bat.'.$i.'</th>';
}

$html = '
    <table width="100%" class="bpmTopic">
        <thead>
            <tr>
                <th width="20%" align="left">Equipe</th>
                <th width="25%" align="left">Apelidos</th>
                <th width="10%" align="right">Pulseiras</th>
                '.$headerBaterias.'

            </tr>
        </thead>
        <tbody id="resultadoListarCompetidoresImpressao">

            '.$raias.'

        </tbody>
    </table>
';
    $etapa = retornaEtapa($idEtapa);
    $nomeEtapa = $etapa->DESCRICAO;
    $campeonato = retornaCampeonato($etapa->ID_CAD_CAMPEONATO);
    $nomeCampeonato = ($campeonato->descricao);

    $header = '
        <table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;">
            <tr>
                <td width="20%" align="left"><img src="../img/logo.png" width="126px" /></td>
                <td width="60%" align="center" ><h2>Relação de Raias e Equipes</h2></td>
                <td width="20%" align="left"></td>
            </tr>
            <tr>
                <td colspan="3" width="100%" align="center" ><h4>'.($nomeCampeonato).' - '.($nomeEtapa).'</h4></td>

            </tr>
        </table>
    ';
$footer = '<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: justify; padding-top: 3mm; ">Este documento pode conter informação confidencial ou privilegiada, sendo seu sigilo protegido. Se você não for o destinatário ou a pessoa autorizada a receber este documento, não pode usar, copiar ou divulgar as informações nele contidos ou tomar qualquer ação baseada nessas informações. Se você recebeu este documento por engano, por favor, destrua-o imediatamente. Agradecemos sua cooperação.<br><div style="font-size: 9pt; text-align: center; padding-top: 3mm; ">Página {PAGENO} de {nb}</div></div>';


//==============================================================
//==============================================================
//==============================================================


include("../imprimir/mpdf.php");

function tirarAcentos($string){
    return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
}

$mpdf=new mPDF('utf-8','A4-L','','','15','15','28','30');

/*

$this->mpdf->mPDF('utf-8','A4','','','15','15','28','18');
When 15=margin-left, 15=margin-right, 28=margin-top, 18=margin-bottom*/

$mpdf->SetHTMLHeader($header);
$mpdf->SetHTMLFooter($footer);
$mpdf->SetDisplayMode('fullpage');

// LOAD a stylesheet
$stylesheet = file_get_contents('../imprimir/examples/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);    // The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,2);

$mpdf->Output('mpdf.pdf','I');
exit;
//==============================================================
//==============================================================
//==============================================================


?>
