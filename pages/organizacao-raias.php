<?php
    session_start();
    include '../functions/conexao.php';
    require '../functions/crud.php';
    header('Content-Type: text/html; charset=utf-8');
    $idEtapa = $_REQUEST['idEtapa'];
    $etapa = retornaEtapa($idEtapa);
    $competidores = retornaCompetidoresAgrupadosEtapa($idEtapa);
?>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Composição Raias</h1>
        </div>
    </div>

<div class="panel panel-default col-lg-12">
    <table id="tableOrgRaias" name="tableOrgRaias" class="table table-hover table-striped">
        <thead>
            <tr>
                <th scope="col">Equipe</th>
                <th scope="col">Apelidos</th>
                <th scope="col">Pulseiras</th>
                <?php
                    $qtdBaterias = $etapa -> QTD_BATERIAS;
                    for ($i = 1; $i <= $qtdBaterias; $i++) {
                        echo '<th scope="col">Raia Bat.'.$i.'</th>';
                    }
                ?>

            </tr>
        </thead>
        <tbody>
            <?php
                if($competidores){
                    foreach ($competidores as $competidor) {
                        if($competidor->RAIA_INICIAL == 0){
                            $semRaia = 'danger';
                        }else{
                            $semRaia = 'success';
                        }

            ?>
                <tr class="<?php echo $semRaia;?>" data="<?php echo $competidor -> ID_COMP_ETAPA;?>">
                    <th scope="row"><?php echo $competidor -> NOME_EQUIPE;?></th>
                    <td><?php echo $competidor -> NOMES_COMPETIDORES;?></td>
                    <td><?php echo $competidor -> ID_PULSEIRAS;?></td>
                    <?php
                    $qtdBaterias = $etapa -> QTD_BATERIAS;
                    for ($i = 1; $i <= $qtdBaterias; $i++) {
                        if($i == 1){
                            echo    '<td id="raia'.$competidor -> ID_COMP_ETAPA .'-'. $competidor -> RAIA_INICIAL .'">
                                        <input class="inputRaiaInicial" type="text" style="width:15%;" value="'.$competidor->RAIA_INICIAL .'"/>
                                        <span class="btn btn-xs btn-danger" onclick="apagaRaiaInicial('.$competidor -> ID_COMP_ETAPA .'); event.stopPropagation();">
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                        </span>
                                    </td>';
                        }else{

                            //echo '<script>alert('.$etapa -> ID_CAD_TIPO_ORG_RAIA.')</script>';
                            if($etapa -> ID_CAD_TIPO_ORG_RAIA == null){
                                echo '<td>Organização de raias não definida na etapa</td>';
                            }else{
                                if($competidor->RAIA_INICIAL == 0){
                                    echo '<td>0</td>';
                                }else{
                                    echo '<td id="raia'.$competidor -> ID_COMP_ETAPA .'-'. $competidor -> RAIA_INICIAL .'"></td>';
                                }
                            }
                        }
                    }
                    ?>
                </tr>
            <?php
                    }
                }
            ?>
        </tbody>
    </table>
</div>

<script>
    function raias($tds, raia){
        qtdBaterias = <?php echo $qtdBaterias; ?>;
        tipoOrgRaia = <?php echo $etapa -> ID_CAD_TIPO_ORG_RAIA; ?>;
        var i;
        for (i = 0; i < qtdBaterias-1; i++) {
            /*ORGANIZACAO DE RAIAS TIPO 1*/
            if(tipoOrgRaia == 1){
                if (raia >= 1 && raia <= 15) {
                    $tds.eq(3+i).text(parseInt(raia) + 15) ;
                    raia = parseInt(raia) + 15;
                }else{
                    if (raia >= 16 && raia <= 23) {
                        $tds.eq(3+i).text(parseInt(raia) + 37) ;
                        raia = parseInt(raia) + 37;
                    }else{
                        if (raia >= 24 && raia <= 30) {
                            $tds.eq(3+i).text(parseInt(raia) + 22) ;
                            raia = parseInt(raia) + 22;
                        }else{
                            if (raia >= 31 && raia <= 37) {
                                $tds.eq(3+i).text(parseInt(raia) -22) ;
                                raia = parseInt(raia) - 22;
                            }else{
                                if (raia >= 38 && raia <= 45) {
                                    $tds.eq(3+i).text(parseInt(raia) - 37) ;
                                    raia = parseInt(raia) - 37;
                                }else{
                                    if (raia >= 46 && raia <= 60) {
                                        $tds.eq(3+i).text(parseInt(raia) - 15) ;
                                        raia = parseInt(raia) - 15;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            /*ORGANIZACAO DE RAIAS TIPO 2*/
            if(tipoOrgRaia == 2){
                if (raia >= 1 && raia <= 15) {
                    $tds.eq(3+i).text(parseInt(raia) + 15) ;
                    raia = parseInt(raia) + 15;
                }else{
                    if (raia >= 16 && raia <= 30) {
                        $tds.eq(3+i).text(parseInt(raia) + 15) ;
                        raia = parseInt(raia) + 15;
                    }else{
                        if (raia >= 31 && raia <= 45) {
                            $tds.eq(3+i).text(parseInt(raia) + 15) ;
                            raia = parseInt(raia) + 15;
                        }else{
                            if (raia >= 46 && raia <= 60) {
                                $tds.eq(3+i).text(parseInt(raia) -45) ;
                                raia = parseInt(raia) - 45;
                            }
                        }
                    }
                }
            }

            /*ORGANIZACAO DE RAIAS TIPO 3*/
            if(tipoOrgRaia == 3){
                //segunda bateria
                if(i==0){
                    if (raia >= 1 && raia <= 32) {
                        $tds.eq(3+i).text(parseInt(raia) + 10) ;
                        raia = parseInt(raia) + 10;
                    }else{
                        if (raia >= 33 && raia <= 42) {
                            $tds.eq(3+i).text(parseInt(raia) - 32) ;
                            raia = parseInt(raia) - 32;
                        }
                    }
                }else{
                    //terceira bateria
                    if(i==1){
                        if (raia >= 1 && raia <= 31) {
                            $tds.eq(3+i).text(parseInt(raia) + 11) ;
                            raia = parseInt(raia) + 11;
                        }else{
                            if (raia >= 32 && raia <= 42) {
                                $tds.eq(3+i).text(parseInt(raia) - 31) ;
                                raia = parseInt(raia) - 31;
                            }
                        }
                    }else{
                        //quarta bateria
                        if(i==2){
                            if (raia >= 1 && raia <= 32) {
                                $tds.eq(3+i).text(parseInt(raia) + 10) ;
                                raia = parseInt(raia) + 10;
                            }else{
                                if (raia >= 33 && raia <= 42) {
                                    $tds.eq(3+i).text(parseInt(raia) - 32) ;
                                    raia = parseInt(raia) - 32;
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    $(document).ready(function(){
        $('#tableOrgRaias').find('tr').each(function (i, el) {
            var $input = $(this).find('input');
            raia = $input.val();

            var $tds = $(this).find('td');
            if($.isNumeric(raia)){
                raias($tds, raia)
            }
        });

        $( ".inputRaiaInicial" ).blur(function() {
            //console.log($(this).parent());
            raia = $(this).val();
            var $tr = $(this).parent().parent();
            var $tds = $(this).parent().parent().find('td');
            var idCompEtapa = $tr.attr('data');

            if($.isNumeric(raia)){
                raias($tds, raia)
                if ((raia != '')&&(raia != null)&&($.isNumeric(raia))&&(raia>0)) {
                    $.post('../ajax/controller.php',{
                        acao:"insertNumeroRaiaComposicaoEquipe",
                        idComposicao:idCompEtapa,
                        idRaiaInicial:raia
                    },function(retorno){
                        if(retorno=='ok'){
                            $tr.removeClass( 'danger' );
                            $tr.addClass( 'success' );
                        }else{
                            alert('Erro ao inserir o numero de raia, informe ao suporte!!\n' + retorno);
                        }
                    });
                };

            }
        });

    });

    function apagaRaiaInicial(idCompEtapa){

        var $tr = $('*[data="'+idCompEtapa+'"]');
        var $tds = $tr.find('td');
        var $input = $tr.find('input');

        if($.isNumeric(idCompEtapa)){

            $.post('../ajax/controller.php',{
                acao:"insertNumeroRaiaComposicaoEquipe",
                idComposicao:idCompEtapa,
                idRaiaInicial:null
            },function(retorno){
                if(retorno=='ok'){
                    $tr.removeClass( 'success' );
                    $tr.addClass( 'danger' );
                    $input.val('0');
                    qtdBaterias = <?php echo $qtdBaterias; ?>;

                    for (i = 0; i < qtdBaterias-1; i++) {
                        $tds.eq(3+i).text(0) ;
                    }


                }else{
                    alert('Erro ao inserir o numero de raia, informe ao suporte!!\n' + retorno);
                }
            });


        }
    }



</script>
