<?php
	session_start();
	include '../functions/conexao.php';
	require '../functions/crud.php';

	header('Content-Type: text/html; charset=utf-8');
	$idEtapa = $_REQUEST['idEtapa'];
	$idEquipe = $_REQUEST['idEquipe'];
	$nomeEquipe = retornaNomeDaEquipe($idEquipe);
?>

<style>

	.listagemCompetidores {
	  	overflow: auto;
	  	height:500px;
	}

	ol li a:active {
		font-weight: bold;
	}

	/* Orange */
	.btn-outline-warning {
	  border-color: #ff9800;
	  color: orange;
	}

	.btn-outline-warning:hover {
	  background: #ff9800;
	  color: white;
	}

</style>


<div class="panel-body">
<h4 id="tituloEquipe"><b><?php echo $nomeEquipe->DESCRICAO; ?></b></h4>
	<div class="row">
		<div class="col-lg-6 listagemCompetidores">
			<b>Competidores nessa equipe:</b>
			<div class="list-group">
				<?php retornaEquipesComposicaoAdicionadas($idEtapa, $idEquipe) ?>
			</div>

			<b>Historia da equipe:</b> <span id="btnSalvaHistoria" class="btn btn-xs btn-danger hidden" onclick="salvaHistoria(<?php echo $idEquipe;?>); event.stopPropagation();"><span class="glyphicon glyphicon-ok"></span> Salvar</span>
			<textarea id="textHistoriaEquipe" class="form-control" rows="4" onkeyup="$('#btnSalvaHistoria').removeClass('hidden');"><?php echo $nomeEquipe -> HISTORIA ?></textarea>
			<br>
			<b>Curiosidades da equipe:</b> <span id="btnSalvaCuriosidades" class="btn btn-xs btn-danger hidden" onclick="salvaCuriosidades(<?php echo $idEquipe;?>); event.stopPropagation();"><span class="glyphicon glyphicon-ok"></span> Salvar</span>
			<textarea id="textCuriosidadesEquipe" class="form-control" rows="4" onkeyup="$('#btnSalvaCuriosidades').removeClass('hidden');"><?php echo $nomeEquipe -> CURIOSIDADES?></textarea>

		</div>

		<div class="col-lg-6 listagemCompetidores">
			Competidores disponíveis:
			<div class="form-group">
				<div class="input-group">
					<input type="text" class="form-control" id="nomeCompetidorBusca" placeholder="Buscar competidores...">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button" onclick="buscarCompetidor()" >Buscar!</button>
					</span>
				</div>
			</div>
			<div id="listDestino" class="list-group">
				<?php retornaEquipesComposicaoCompetidores($idEtapa) ?>
			</div>
		</div>
	</div>


</div>
<script>
	$(document).ready(function() {
		podeAlterar();
	});

	function salvaHistoria(idEquipe){
		if($podeAlterar){
			$.post('../ajax/controller.php',{
				acao:"salvaHistoriaEquipe",
				idEquipe:idEquipe,
				historia:$("#textHistoriaEquipe").val()
			},function(retorno){
				if(retorno=='ok'){
					$('#btnSalvaHistoria').addClass('hidden');
				}else{
					alert('Erro ao salvar a historia da equipe, informe ao suporte!');
				}
			});
		}else{
			alert('Etapa já aconteceu, não pode mais ser alterada');
		}
	}

	function salvaCuriosidades(idEquipe){
		if($podeAlterar){
			$.post('../ajax/controller.php',{
				acao:"salvaCuriosidadesEquipe",
				idEquipe:idEquipe,
				curiosidades:$("#textCuriosidadesEquipe").val()
			},function(retorno){
				if(retorno=='ok'){
					$('#btnSalvaCuriosidades').addClass('hidden');
				}else{
					alert('Erro ao salvar as curiosidades da equipe, informe ao suporte!');
				}
			});
		}else{
			alert('Etapa já aconteceu, não pode mais ser alterada');
		}
	}

	function podeAlterar(){
		$.post('../ajax/controller.php',{
			acao:"pegaDataEtapa2",
			idEtapa:<?php echo $idEtapa;?>
		},function(retorno){
			var today = new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate());
			var etapa = new Date(new Date(retorno).getFullYear(),new Date(retorno).getMonth() , new Date(retorno).getDate());
			if(etapa < today){
				$podeAlterar =  false;
				console.log($podeAlterar);
			}else{
				$podeAlterar = true;
				console.log($podeAlterar);
			}
		});
	}

	function excluirComposicaoEquipe(idComposicaoEquipe){
		if($podeAlterar){
			if (confirm("Deseja excluir esse competidor da equipe selecionada?") == true){
				$.post('../ajax/controller.php',{
					acao:"excluiEquipeComposicao",
					idComposicaoEquipe:idComposicaoEquipe
				},function(retorno){
					if(retorno=='ok'){
						abreComposicaoEquipesSeleciona(<?php echo $idEtapa;?>, <?php echo $idEquipe;?>);
					}else{
						alert('Erro ao excluir, informe ao suporte!');
					}
				});
			};
		}else{
			alert('Etapa já aconteceu, não pode mais ser alterada');
		}
	}

	function insereComposicaoEquipe(idCompetidor){
		if($podeAlterar){
			if (confirm("Deseja adicionar esse competidor à equipe selecionada?") == true){
				$.post('../ajax/controller.php',{
				acao:"insertEquipeComposicao",
					idEquipe:<?php echo $idEquipe;?>,
					idCompetidor:idCompetidor
				},function(retorno){
					if(retorno=='ok'){
						abreComposicaoEquipesSeleciona(<?php echo $idEtapa;?>, <?php echo $idEquipe;?>);
					}else{
						alert('Erro ao inserir, informe ao suporte!');
					}
				});
			};
		}else{
			alert('Etapa já aconteceu, não pode mais ser alterada');
		}
	}

	function informarQRCode(idComposicao){
		var idPulseira = prompt("Informe o número de identificação", "");
		if ((idPulseira != '')&&(idPulseira != null)&&($.isNumeric(idPulseira))) {
			$.post('../ajax/controller.php',{
				acao:"insertEquipeComposicaoIdentificacao",
				idComposicao:idComposicao,
				idPulseira:idPulseira
			},function(retorno){
				if(retorno=='ok'){
					abreComposicaoEquipesSeleciona(<?php echo $idEtapa;?>, <?php echo $idEquipe;?>);
				}else{
					if(retorno=='duplicado'){
						alert('Essa pulseira já está cadastrada, não pode ser utilizada novamente!');
					}else{
						alert('Erro ao inserir a codigo de identificação, informe ao suporte!');
					}
				}
			});
		};
	}

	function alteraApelido(idCompetidor, apelidoAtual){
		var novoApelido = prompt("Informe o novo apelido do competidor", apelidoAtual);
		if ((novoApelido != '')&&(novoApelido != null)) {
			$.post('../ajax/controller.php',{
				acao:"updateApelidoCompetidor",
				idCompetidor:idCompetidor,
				novoApelido: novoApelido.toUpperCase(),
				anteriorApelido : apelidoAtual
			},function(retorno){
				if(retorno=='ok'){
					$('#apelidoCompet'+idCompetidor).text(novoApelido.toUpperCase());
				}else{
					alert('Erro ao atualizar o apelido do competidor, informe ao suporte!');
				}
			});
		};
	}

	function marcaSuplente(idComposicao, marcacao){
		if($podeAlterar){
			$.post('../ajax/controller.php',{
				acao:"marcaEquipeComposicaoSuplente",
				idComposicao:idComposicao,
				marcacao:marcacao
			},function(retorno){
				if(retorno=='ok'){
					abreComposicaoEquipesSeleciona(<?php echo $idEtapa;?>, <?php echo $idEquipe;?>);
				}else{
					alert('Erro ao inserir a codigo de identificação, informe ao suporte!');
				}
			});
		}else{
			alert('Etapa já aconteceu, não pode mais ser alterada');
		}
	}

	function buscarCompetidor(){
		$('#listDestino').html('');
		$.post('../ajax/controller.php',{
			acao:"retornaBuscaEquipesComposicaoCompetidores",
			idEtapa:<?php echo $idEtapa;?>,
			nome:$("#nomeCompetidorBusca").val()
			},function(retorno){
				$('#listDestino').html(retorno);
			}
		);
	}

	$("#nomeCompetidorBusca").on('keyup', function (e) {
		if (e.keyCode == 13) {
			buscarCompetidor();
		}
	});

</script>
