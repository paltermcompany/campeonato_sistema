<?php
    session_start();
    header('Content-Type: text/html; charset=utf-8');
    include("../imprimir/mpdf.php");
    include '../functions/conexao.php';
    require '../functions/crud.php';
    $idEtapa = $_REQUEST['idEtapa'];
    $etapa = retornaEtapa($idEtapa);

    function add_custom_fonts_to_mpdf($mpdf, $fonts_list) {
        // Logic from line 1146 mpdf.pdf - $this->available_unifonts = array()...
        foreach ($fonts_list as $f => $fs) {
            // add to fontdata array
            $mpdf->fontdata[$f] = $fs;

            // add to available fonts array
            if (isset($fs['R']) && $fs['R']) { $mpdf->available_unifonts[] = $f; }
            if (isset($fs['B']) && $fs['B']) { $mpdf->available_unifonts[] = $f.'B'; }
            if (isset($fs['I']) && $fs['I']) { $mpdf->available_unifonts[] = $f.'I'; }
            if (isset($fs['BI']) && $fs['BI']) { $mpdf->available_unifonts[] = $f.'BI'; }
        }
        $mpdf->default_available_fonts = $mpdf->available_unifonts;
    }

    //$mpdf=new mPDF('utf-8',array(310,90),0,'',15,15,20,16,9,9,'P');
    $mpdf=new mPDF('utf-8',array(200,60),0,'',-10,-10,10,-15,0,0,'P');
    /*
    $mpdf = new mPDF('',    // mode - default ''
     '',    // format - A4, for example, default ''
     0,     // font size - default 0
     '',    // default font family
     15,    // margin_left
     15,    // margin right
     16,     // margin top
     16,    // margin bottom
     9,     // margin header
     9,     // margin footer
     'L');  // L - landscape, P - portrait
     */

    $custom_fontdata = array(
        'sourcesanspro-regular' => array(
            'R' => "montserrat-black.ttf"
            // use 'R' to support CSS font-weight: normal
            // use 'B', 'I', 'BI' and etc. to support CSS font-weight: bold, font-style: italic, and both...
        )
    );

    add_custom_fonts_to_mpdf($mpdf, $custom_fontdata);

    $mpdf->SetDisplayMode('fullpage');

/*$stylesheet = file_get_contents('../imprimir/examples/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);    // The parameter 1 tells that this is css/style only and no body/html/text
*/
    $mpdf->shrink_tables_to_fit = 1;

    $raias = '';
    if ($competidores = retornaCompetidoresEtapa($idEtapa)){
        foreach($competidores as $competidor){

            if(strlen(trim($competidor->APELIDO)) == 9){
                $fonteApelido = "font-size: 55pt;";
            }else{
                if(strlen(trim($competidor->APELIDO)) == 11){
                    $fonteApelido = "font-size: 50pt;";
                }else{
                    if(strlen(trim($competidor->APELIDO)) == 12){
                        $fonteApelido = "font-size: 45pt;";
                    }else{
                        if(strlen(trim($competidor->APELIDO)) == 13){
                            $fonteApelido = "font-size: 38pt;";
                        }else{
                            if(strlen(trim($competidor->APELIDO)) == 10){
                                $fonteApelido = "font-size: 52pt;";
                            }else{
                                if(strlen(trim($competidor->APELIDO)) == 16){
                                    $fonteApelido = "font-size: 30pt;";
                                }else{
                                    if(strlen(trim($competidor->APELIDO)) > 13){
                                        $fonteApelido = "font-size: 38pt;";
                                    }else{
                                        $fonteApelido = "font-size: 60pt;";
                                    }
                                }
                            }
                        }
                    }
                }
            }


            $raias = '  <div align="center" style="/*border:1px solid red*/">
                            <font face="sourcesanspro-regular" style="font-size: 80pt;">'.$competidor->EQUIPE.'</font>
                            <font face="sourcesanspro-regular" style="'.$fonteApelido.'">&nbsp;'.trim($competidor->APELIDO)./*strlen(trim($competidor->APELIDO)).*/'</font>
                        </div>';

            /*$raias = ' <html><body style="text-align:center;"> <table  width="100%" style="width=100%; border: 1px solid black;overflow: wrap">
                            <tr>
                                <td align="right" style="border: 1px solid black;">
                                    <font face="sourcesanspro-regular" style="font-size: 165pt;">'.$competidor->EQUIPE.'</font>
                                </td>
                                <td align="left" style="border: 1px solid black;">
                                    <font face="sourcesanspro-regular" style="font-size: 110pt;">' .trim($competidor->APELIDO).'</font>
                                </td>
                            </tr>
                        </table></body></html>';*/

            $html = $raias;

            $mpdf->AddPage();
            $mpdf->WriteHTML($html,2);
        }
    }


$mpdf->Output('mpdf.pdf','I');
exit;



?>
