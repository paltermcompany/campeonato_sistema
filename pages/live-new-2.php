<?php
    session_start();
    header('Content-Type: text/html; charset=utf-8');
    include '../functions/conexao.php';
    require '../functions/crud.php';
?>
<html>
<body>
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/font-awesome/css/all.css" rel="stylesheet" type="text/css">
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../dist/css/adminlte.min.css">
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="../js/custom.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/custom.js"></script>
    <script type="text/javascript" src="../bower_components/chartjs/Chart.min.js"></script>
    <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, maximum-scale=10, minimum-scale=1.0">
<script>
    var tableResultadosGeral;
    var tableResultadosMaiorPeixe;
    var tableResultadosRankingFinal;
    var linhaRanking;

	$( document ).ready(function() {

        var botaoMobile = document.getElementById('botao-menu');
        botaoMobile.addEventListener('click', abrirMenuMobile);

        var menuMobile = document.querySelector('#menu');

        menuMobile.addEventListener('click', abreOpcao);

        atualiza();
    });

    function abrirMenuMobile(){

        var menu = document.getElementById('menu');
        var display = window.getComputedStyle(menu).display;
        alternaIconesBotaoMenu(this);
        if(display == 'block'){
            escondeElemento(menu);
        }else{
            mostraElemento(menu);
        }
    }

    function alternaIconesBotaoMenu(elemento){

        if(elemento.classList.contains('fa-bars')){
            elemento.classList.remove('fa-bars');
            elemento.classList.add('fa-times');
        }else{
            elemento.classList.remove('fa-times');
            elemento.classList.add('fa-bars');
        }
    }

    function abreOpcao(){

        var opcao = event.target.textContent;
        var graficos = document.querySelectorAll('.graficos');
        var pesagens = document.querySelector('.pesagens');
        var equipes = document.querySelector('.equipes');
        var ranking = document.querySelector('.ranking');
        var menu = document.getElementById('menu');
        var tituloPagina = document.getElementById('titulo-pagina');
        var botaoMenu = document.getElementById('botao-menu');

        if(opcao == 'Ranking'){
            mostraElemento(ranking);
            escondeElemento(pesagens);
            escondeElemento(equipes);
            escondeElementos(graficos);
            alteraTextoElemento(tituloPagina, opcao);
        }else if(opcao == 'Pesagens'){
            escondeElemento(ranking);
            mostraElemento(pesagens);
            escondeElemento(equipes);
            escondeElementos(graficos);
            alteraTextoElemento(tituloPagina, opcao);
        }else if(opcao == 'Gráficos'){
            escondeElemento(ranking);
            escondeElemento(pesagens);
            escondeElemento(equipes);
            mostraElementos(graficos);
            alteraTextoElemento(tituloPagina, opcao);
        }else if(opcao == 'Equipes'){
            escondeElemento(ranking);
            escondeElemento(pesagens);
            mostraElemento(equipes);
            escondeElementos(graficos);
            alteraTextoElemento(tituloPagina, opcao);
        }else if(opcao == 'Atualizar Dados'){
            window.location.reload();
        }

        alternaIconesBotaoMenu(botaoMenu);
        escondeElemento(menu);

    }

    function alteraTextoElemento(elemento, text){
        elemento.textContent = text;
    }

    function escondeElemento(elemento){
        elemento.style.display = 'none';
    }

    function mostraElemento(elemento){
        elemento.style.display = 'block';
    }

    function escondeElementos(elementos){
        elementos.forEach(function(elemento){
            escondeElemento(elemento);
        });
    }

    function mostraElementos(elementos){
        elementos.forEach(function(elemento){
            mostraElemento(elemento);
        });
    }

    function atualiza() {
            idEtapa = $( '#idEtapaAcompanhamentoLive').val();

            $.post('../ajax/controller.php',{
                acao:"unidadeMedidaEtapa",
                idEtapa:idEtapa
            },function(retorno){
                retorno = JSON.parse(retorno);
                $.each( retorno, function( key, value ) {
                    document.cookie = "cookieDescricaoUnidadeMedida="+value.descricao;
                    document.cookie = "cookieAbreviacaoUnidadeMedida="+value.abreviacao;
                });
            });

            $.post('../ajax/controller.php',{
                acao:"resultadosEtapaLiveNew2",
                idEtapa:idEtapa
            },function(retorno){

                var boxQtd;
                var boxPeso;

                if(window.screen.width > 520){
                    boxQtd = '<div class="small-box bg-green"><div class="inner"><p class="dado-smallbox">'+retorno[0].total_quantidade+'</p><p class="label-smallbox">Quantidade Total</p></div><div class="icon"><i class="fa fa-calculator"></i></div></div>';
                    boxPeso = '<div class="small-box bg-green"><div class="inner"><p class="dado-smallbox">'+retorno[0].total_peso+'</p><p class="label-smallbox">Peso Total</p></div><div class="icon"><i class="fa  fa-balance-scale"></i></div></div>';
                    $('#boxqtdpeixes').append(boxQtd);
                    $('#boxpesopeixes').append(boxPeso);
                }else{
                    var infosEtapa;
                    infosEtapa = $('<div class="infos-etapa-mobile"></div>');
                    boxQtd = '<div class="peso-total-mobile col-xs-12"><p><i class="fas fa-fish"></i> '+retorno[0].total_quantidade+'</p></div>';
                    boxPeso= '<div class="quantidade-total-mobile col-xs-12"><p><i class="fas fa-balance-scale"></i> '+retorno[0].total_peso+'</p></div>';
                    infosEtapa.append(boxQtd);
                    infosEtapa.append(boxPeso);
                    $('footer').append(infosEtapa);
                }

            }, "json");

            $.post('../ajax/controller.php', {
                acao:"resultadosEtapaMaiorPeixeSemEntregaTrofeuLiveNew2",
                idEtapa:idEtapa,
                mostraNome:"apelido"
            },function(retorno){

                if(window.screen.width > 520){
                    var smallBox = '<div class="small-box bg-red" style="cursor:pointer"><div class="inner"><p class="pesoMP">'+retorno[0].peso+' - '+retorno[0].especie+'</p> <p class="competidoresMP">'+retorno[0].competidores+'</p> <p class="iscamodalidadeMP">'+retorno[0].isca+' - '+retorno[0].modalidade+'</p></div><div class="icon"><i class="fas fa-fish">MP</i></div></div>';
                    $('#boxmaiorpeixe').append(smallBox);
                    $('#boxmaiorpeixe').append('<td class="equipeID" style="display:none">'+retorno[0].equipe+'<td>');
                    $('#boxmaiorpeixe').on('click', exibeModalInfoCompetidores);
                }else{
                    $('footer').append('<div class="mp-mobile-mp" value='+retorno[0].equipe+'><i class="fas fa-fish"><BR>MP</i><p>'+retorno[0].peso+' - '+retorno[0].especie+'</p><p>'+retorno[0].competidores+'</p><p>'+retorno[0].isca+' - '+retorno[0].modalidade+'</p></div>');
                    $('.mp-mobile-mp').append('<td class="equipeID" style="display:none">'+retorno[0].equipe+'<td>');
                    $('.mp-mobile-mp').on('click', exibeModalInfoCompetidores);
                }
            }, "json");

            $.post('../ajax/controller.php', {
                acao:"retornaDetalhesEstatisticasEquipes",
                idEtapa: idEtapa
            }, function(retorno){

                var elementoGrafico = $('#graficoDuplasComPeixe');
                var dadosGrafico;

                var equipesTotal;
                var equipesComPesagens;
                var equipesSemPesagens;

                $.each(JSON.parse(retorno), function(index, obj){

                    $.each(this, function(k, v){

                        if(k == 0){
                            equipesTotal = v.total;
                        }else if(k == 1){
                            equipesComPesagens = v.total;
                        }else if(k == 2){
                            equipesSemPesagens = equipesTotal - equipesComPesagens;
                        }
                    });

                });


                dadosGrafico = new Chart(elementoGrafico, {
                    type: "doughnut",
                    data:{
                        datasets:[{
                            backgroundColor:[
                                "#0074D9",
                                "#2ECC40",
                                "#FF4136"
                            ],
                            data:[
                                equipesTotal
                            ]
                        },
                        {
                            backgroundColor:[
                                "#0074D9",
                                "#2ECC40",
                                "#FF4136"
                            ],
                            data:[
                                0,
                                equipesComPesagens,
                                equipesSemPesagens
                            ]

                        }],
                        labels:[
                            "Total de Equipes",
                            "Já pegaram peixe",
                            "Não pegaram peixe"
                        ],

                    },
                    options:{
                        title:{
                            display:true,
                            text: 'Equipes',
                            fontSize: 15
                        },
                        legend:{
                            display: false
                        }
                    }
                })

            });

            tableResultadosGeral = $('#tableResultadosGeral').DataTable( {
                "ajax": {
                        url: "../ajax/controller.php",
                        type: "POST",
                        data : function(d){
                            d.acao = "resultadosEtapa",
                            d.idEtapa = idEtapa,
                            d.horario = ""
                        }
                    },
                "columns": [
                    {
                        "className": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {"data": "total_quantidade"},
                    {"data": "total_peso"}
                ],
                "language": {
                    "url": "../bower_components/datatables/Portuguese-Brasil.json"
                },
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "destroy": true,
                "ordering": false,
                "info": false,
                "autoWidth": false
            } );

            tableResultadosMaiorPeixe = $('#tableResultadosMaiorPeixe').DataTable( {
                "ajax": {
                        url: "../ajax/controller.php",
                        type: "POST",
                        data : function(d){
                            d.acao = "resultadosEtapaMaiorPeixeSemEntregaTrofeu",
                            d.idEtapa = idEtapa,
                            d.horario = "",
                            d.mostraNome = "apelido";
                        }
                    },
                "columns": [
                    {
                        "className": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {"data": "peso"},
                    {"data": "especie"},
                    {"data": "isca"},
                    {"data": "modalidade"},
                    {"data": "competidores"}
                ],
                "language": {
                    "url": "../bower_components/datatables/Portuguese-Brasil.json"
                },
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "destroy": true,
                "ordering": false,
                "info": false,
                "autoWidth": false
            } );

            tableResultadosRankingFinal = $('#tableResultadosRankingFinal').DataTable( {
                "createdRow": function(row, data, dataIndex){
                        $(row).addClass('linhaRanking'),
                        $(row).on('click', exibeModalInfoCompetidores)
                    },
                "ajax": {
                        url: "../ajax/controller.php",
                        type: "POST",
                        data : function(d){
                            d.acao = "resultadosEtapaRankingSemEntregaTrofeu",
                            d.rankingCompleto = false,
                            d.idEtapa = idEtapa,
                            d.horario = "",
                            d.mostraNome = "apelido";
                        }
                    },
                "columns": [
                    {
                        "className": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {"data": "colocacao"},
                    {"data": "peso"},
                    {"data": "quantidade"},
                    {"data": "equipe"},
                    {
                        "data": 'id_equipe',
                        "className":'equipeID'
                    }
                ],
                "language": {
                    "url": "../bower_components/datatables/Portuguese-Brasil.json"
                },
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "destroy": true,
                "ordering": false,
                "info": false,
                "autoWidth": false
            } );

            filtraEtapasResultadoLive();
            filtraDetalhes();
            g_iCount = 30;
            if(window.screen.width >= 520){
                startCountdown();
            }
	};

	function startCountdown(){
            if((g_iCount - 1) >= 0){
                g_iCount = g_iCount - 1;

                $('#numberCountdown').html(' atualiza em: ' + g_iCount);
                setTimeout('startCountdown()',1000);
                if(g_iCount==0){
                    if(modalAberto == false){
                        checkPrevias();
                        console.log('checkando prévias');
                        window.location.reload();
                        //atualiza();
                    }
                }
            }
	}

	$primeiraVez = true;
	var g_iCount = new Number();
    var modalAberto = false;

	function filtraEtapasResultadoLive(){
            idEtapa = $('#idEtapaAcompanhamentoLive').val();

            if(idEtapa!=''){
                $.post('../ajax/controller.php',{
                    acao:"acompanhamentoLiveNew2",
                    idEtapa:idEtapa,
                    ultimaVerificacao: $('#ultimaVerificacao').val(),
                    idUltimaVerificacao: $('#idUltimaVerificacao').val()
                },function(retorno){
                    if(retorno!=''){
                        $( ".linhaLive" ).removeClass( 'invalid' );
                        $( ".linhaLive" ).removeClass( 'excluido' );
                    }
                    $('#acompanhamentoLive').prepend(retorno);
                    if($primeiraVez){
                        console.log('primeira vez');
                        checkPrevias();
                        $primeiraVez = false;
                    }
                });
            }

            $('.linhaLive').unbind('click');
            $('.linhaLive').on('click', exibeModalInfoCompetidores);
    }

    function montaInformacoesCompetidorModal(competidor, dataNascimento, idade, cidade, uf){
        var html, apelidosHTML, dataNascimentoHTML, cidadeHTML;

        html = document.createElement('div');
        apelidosHTML = document.createElement('h3');
        dataNascimentoHTML = document.createElement('h4');
        cidadeHTML = document.createElement('h5');

        html.classList.add('col-lg-6');
        html.classList.add('col-xs-6')

        apelidosHTML.textContent = competidor;

        dataNascimentoHTML.textContent = '';

        if(dataNascimento != null){
            dataNascimentoHTML.textContent = dataNascimento;
        }

        if(idade != null){
            dataNascimentoHTML.textContent = dataNascimentoHTML.textContent.concat(' - ' +idade+ ' anos');
        }

        if(cidade != null && cidade != ''){
            cidadeHTML.textContent = cidade;
        }

        if(uf != null && uf != ''){
            cidadeHTML.textContent = cidadeHTML.textContent.concat(' - ' +uf);
        }

        html.appendChild(apelidosHTML);
        html.appendChild(dataNascimentoHTML);
        html.appendChild(cidadeHTML);

        return html

    }

    function exibeModalInfoCompetidores(){

        modalAberto = true;

        var modalInfoCompetidores = $('.modalinfocompetidores');
        var colocacaoEquipe = $('.colocacao-modal');
        var infosBody = $('.infos-body');
        var idEquipe = $(this).find('.equipeID').text();
        var idEtapa = $('#idEtapaAcompanhamentoLive').val();
        var capturouPeixe = false;
        var colocacaoEquipe;
        var nomesCompetidores;

        if(idEquipe == ''){
            idEquipe = $(this).val();
        }


        $.post('../ajax/controller.php',{
            acao:"colocacaoEquipeLiveNew2",
            idEtapa:idEtapa,
            idEquipe:idEquipe
        },function(retorno){
            colocacaoEquipe.empty();
            colocacaoEquipe.append('<p class="posicao-modal">'+retorno.posicao+'º</p><p class="capturas-modal"><i class="fas fa-fish"></i> '+retorno.quantidade+'</p><p class="peso-modal"><i class="fas fa-balance-scale-right"> '+retorno.peso+' Kg</i></p>');

        }, "json").fail(function(){
            colocacaoEquipe.empty();
        });

        $.post('../ajax/controller.php',{
            acao:"informacoesEquipe",
            idEquipe:idEquipe
        },function(retorno){



            infosBody.empty();

            var jsonParse = JSON.parse(retorno);

            infosBody.append('<h1>'+jsonParse[0].apelido+' & '+jsonParse[1].apelido+'</h1>');
            infosBody.append('<p>'+jsonParse[0].DESCRICAO+'</p>');

            infosBody.append(montaInformacoesCompetidorModal(jsonParse[0].competidor, jsonParse[0].DATA_NASCIMENTO, jsonParse[0].IDADE, jsonParse[0].CIDADE, jsonParse[0].UF));
            infosBody.append(montaInformacoesCompetidorModal(jsonParse[1].competidor, jsonParse[1].DATA_NASCIMENTO, jsonParse[1].IDADE, jsonParse[1].CIDADE, jsonParse[1].UF));

            if(jsonParse[0].HISTORIA != null){
                infosBody.append('<div class="col-lg-12 col-xs-12"><h5><b>História: </b>'+jsonParse[0].HISTORIA+'</h5></div>');
            }

            if(jsonParse[0].CURIOSIDADES != null){
                infosBody.append('<div class="col-lg-12 col-xs-12"><h5><b>Curiosidades: </b>'+jsonParse[0].CURIOSIDADES+'</h5></div>');
            }

            infosBody.append('<div class="col-lg-12 col-xs-12"><hr></div>');

        });

        $.post('../ajax/controller.php', {
            acao: 'detalhesModalidadeEquipeLiveNew2',
            idEtapa: idEtapa,
            idEquipe: idEquipe
        }, function(retorno){

            var modalidade = [];
            var qtdModalidade = [];

            infosBody.append('<div class="col-lg-3">'+
                                '<canvas id="graficoModalidadeEquipe" height="400" width="400"></canvas>'+
                            '</div>');

            for(var i in retorno){
                modalidade.push(retorno[i].modalidade);
                qtdModalidade.push(retorno[i].total);
            }

            criaGraficos(modalidade, qtdModalidade, '#graficoModalidadeEquipe', 'pie', 'Modalidades');
        }, "json");

        $.post('../ajax/controller.php', {
            acao: 'detalhesEspecieEquipeLiveNew2',
            idEtapa: idEtapa,
            idEquipe: idEquipe
        }, function(retorno){

            var especie = [];
            var qtdEspecie = [];

            infosBody.append('<div class="col-lg-3">'+
                                '<canvas id="graficoEspeciesEquipe" height="400" width="400"></canvas>'+
                            '</div>');

            for(var i in retorno){
                especie.push(retorno[i].especie);
                qtdEspecie.push(retorno[i].total);
            }

            criaGraficos(especie, qtdEspecie, '#graficoEspeciesEquipe', 'pie', 'Espécies');

        }, "json");

        $.post('../ajax/controller.php', {
            acao: 'detalhesIscaEquipeLiveNew2',
            idEtapa: idEtapa,
            idEquipe: idEquipe
        }, function(retorno){

            var isca = [];
            var qtdIscas = [];

            infosBody.append('<div class="col-lg-3">'+
                                '<canvas id="graficoIscasEquipe" height="400" width="400"></canvas>'+
                            '</div>');

            for(var i in retorno){
                isca.push(retorno[i].isca);
                qtdIscas.push(retorno[i].total);
            }

            criaGraficos(isca, qtdIscas, '#graficoIscasEquipe', 'pie', "Iscas");

        }, "json");

        $.post('../ajax/controller.php', {
            acao: 'detalhesHoraEquipeLiveNew2',
            idEtapa: idEtapa,
            idEquipe: idEquipe
        }, function(retorno){
            var intervaloHora = [];
            var qtdPeixes = [];

            infosBody.append('<div class="col-lg-3">'+
                                '<canvas id="graficoPesagensTempoEquipe" height="400" width="400"></canvas>'+
                            '</div>');

            for(var i in retorno){
                intervaloHora.push(retorno[i].hora);
                qtdPeixes.push(retorno[i].total);
            }

            criaGraficos(intervaloHora, qtdPeixes, '#graficoPesagensTempoEquipe', 'horizontalBar', 'Pesagens divididas em intervalos de tempo');

        }, "json").fail(function(){

            infosBody.append('<div class="col-lg-12"><h1>Essa equipe ainda não capturou nenhum peixe</h1></div>');

        });

        $('.modalinfocompetidores').show();

        $('.fechamodalinfocompetidores').on('click', function(){
            modalInfoCompetidores.hide();
            infosBody.empty();
            modalAberto = false;
            if(g_iCount == 0){
                window.location.reload();
            }
        });
    }

    function criaGraficos(item, qtdItem, nomeDoElemento, tipoGrafico, nomeDoGrafico){

        var elementoGrafico = $(nomeDoElemento);

        var dadosGrafico = {
            datasets:[{
                backgroundColor: ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"],
                data: qtdItem
            }],

            labels: item
        };

        var comecaNoZero

        if(tipoGrafico == 'horizontalBar'){
            comecaNoZero={
                    xAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
            }
        }

        var opcoes = {
            title:{
                display: true,
                text: nomeDoGrafico,
                fontSize: 15
            },
            tooltips:{
                callbacks:{
                    label: function(tooltipItem, data){

                        var dataset = data.datasets[tooltipItem.datasetIndex];
                        var total = 0;
                        var valorAtual = dataset.data[tooltipItem.index];

                        for(var i = 0; i < dataset.data.length; i++){
                            total = total + parseInt(dataset.data[i], 10);
                        }

                        var porcentagem = Math.floor(((valorAtual / total) * 100) + 0.5);

                        return data.labels[tooltipItem.index]+ ': ' + valorAtual + ' - ' +porcentagem + '%';

                    }
                }
            },
            legend:{
                display: false
            },
            scales: comecaNoZero
        };

        new Chart(elementoGrafico, {
            type: tipoGrafico,
            data: dadosGrafico,
            options: opcoes
        });

    }

	function filtraDetalhes(){
            idEtapa = $('#idEtapaAcompanhamentoLive').val();
            if(idEtapa!=''){
                /*
                $.post('../ajax/controller.php',{
                    acao:"detalhesModalidade",
                    idEtapa:idEtapa
                },function(retorno){
                    $('#resultadoDetalhesModalidade').html(retorno);
                });
                */
                $.post('../ajax/controller.php',{
                    acao:"detalhesModalidadeLiveNew2",
                    idEtapa:idEtapa
                },function(retorno){
                    var modalidade = [];
                    var qtdModalidade = [];

                    for(var i in retorno){
                        modalidade.push(retorno[i].modalidade);
                        qtdModalidade.push(retorno[i].total);
                    }

                    criaGraficos(modalidade, qtdModalidade, '#graficoModalidade', 'pie', 'Modalidades');
                }, "json");
                /*
                $.post('../ajax/controller.php',{
                    acao:"detalhesEspecie",
                    idEtapa:idEtapa
                },function(retorno){
                    $('#resultadoDetalhesEspecie').html(retorno);
                });
                */
                $.post('../ajax/controller.php',{
                    acao:"detalhesEspecieLiveNew2",
                    idEtapa:idEtapa
                },function(retorno){

                    var especie = [];
                    var qtdEspecie = [];

                    for(var i in retorno){
                        especie.push(retorno[i].especie);
                        qtdEspecie.push(retorno[i].total);
                    }

                    criaGraficos(especie, qtdEspecie, '#graficoEspecies', 'pie', 'Espécies');
                }, "json");
                /*
                $.post('../ajax/controller.php',{
                    acao:"detalhesIsca",
                    idEtapa:idEtapa
                },function(retorno){
                    $('#resultadoDetalhesIsca').html(retorno);
                });
                */
                $.post('../ajax/controller.php',{
                    acao:"detalhesIscaLiveNew2",
                    idEtapa:idEtapa
                },function(retorno){

                    var iscas = [];
                    var qtdIscas = [];

                    for(var i in retorno){
                        iscas.push(retorno[i].isca);
                        qtdIscas.push(retorno[i].total);
                    }

                    criaGraficos(iscas, qtdIscas, '#graficoIscas', 'pie', 'Iscas');

                }, "json");
                /*
                $.post('../ajax/controller.php',{
                    acao:"detalhesHora",
                    idEtapa:idEtapa
                },function(retorno){

                    $('#resultadoDetalhesHora').html(retorno);
                });
                */
                $.post('../ajax/controller.php',{
                    acao:"detalhesHoraLiveNew2",
                    idEtapa:idEtapa
                },function(retorno){
                    var intervaloHora = [];
                    var qtdPeixes = [];

                    for(var i in retorno){
                        intervaloHora.push(retorno[i].hora);
                        qtdPeixes.push(retorno[i].total);
                    }

                    criaGraficos(intervaloHora, qtdPeixes, '#graficoHorario', 'horizontalBar', 'Pesagens divididas em intervalos de tempo');

                }, "json");

                $.post('../ajax/controller.php',{
                    acao:"criaBotoesEquipes",
                    idEtapa:idEtapa
                },function(retorno){

                    var tdsEquipes = [];

                    for(var i in retorno){

                        if(retorno[i].contador != null){
                            corBotao = 'btn-success';
                        }else{
                            corBotao = 'btn-primary';
                        }

                        tdsEquipes.push($('<button class="btn-sm ' +corBotao+ '" value="'+retorno[i].ID+'">'+retorno[i].DESCRICAO+'</button>'));
                        $('#botoes-equipes').append(tdsEquipes[i]);
                        tdsEquipes[i].on('click', exibeModalInfoCompetidores);
                    }

                }, "json");
            }
	}

	function exibeAcomLive(){
            $( "#previas" ).addClass( 'hidden' );
            $( "#live" ).removeClass( 'hidden' );
            $( "#detalhes" ).addClass( 'hidden' );
	}

	function exibeAcomPrevias(){
            $( "#previas" ).removeClass( 'hidden' );
            $( "#live" ).addClass( 'hidden' );
            $( "#detalhes" ).addClass( 'hidden' );
	}

	function exibeAcomDetalhes(){
            $( "#previas" ).addClass( 'hidden' );
            $( "#live" ).addClass( 'hidden' );
            $( "#detalhes" ).removeClass( 'hidden' );
	}

	function filtraEtapasResultadoDashboard(){
            tableResultadosGeral.ajax.reload();
            tableResultadosMaiorPeixe.ajax.reload();
            tableResultadosRankingFinal.ajax.reload();

		/*idEtapa = $( '#idEtapaAcompanhamentoLive').val();
		if(idEtapa!=''){


			$('#resultadoPreviaEtapa').html('');
			$.post('../ajax/controller.php',{
				acao:"resultadosEtapa",
				idEtapa:idEtapa
			},function(retorno){
				$('#resultadoPreviaEtapa').html('<center><i class="fa fa-5x fa-refresh fa-spin"></i><br>Carregando...</center>');
				$('#resultadoPreviaEtapa').html(retorno);
			});*/

			/*$('#resultadoPreviaMaiorPeixeEtapa').html('');
			$.post('../ajax/controller.php',{
				acao:"resultadosEtapaMaiorPeixeSemEntregaTrofeu",
				idEtapa:idEtapa
			},function(retorno){
				$('#resultadoPreviaMaiorPeixeEtapa').html('<center><i class="fa fa-5x fa-refresh fa-spin"></i><br>Carregando...</center>');
				$('#resultadoPreviaMaiorPeixeEtapa').html(retorno);
			});*/

			/*$('#resultadoPreviaRankingColocacoes').html('');
			$.post('../ajax/controller.php',{
				acao:"resultadosEtapaRankingSemEntregaTrofeu",
				idEtapa:idEtapa,
				horario:''
			},function(retorno){
				$('#resultadoPreviaRankingColocacoes').html('<center><i class="fa fa-5x fa-refresh fa-spin"></i><br>Carregando...</center>');
				$('#resultadoPreviaRankingColocacoes').html(retorno);
			});
		}*/
	}

	var refreshId = setInterval(function(){ checkAcompanhamentoLive(); }, 5000);

	function checkAcompanhamentoLive() {
            filtraEtapasResultadoLive();

	}

	function checkPrevias() {
            filtraEtapasResultadoDashboard();
            //filtraDetalhes();
            //alert('30');
            g_iCount = 30;
	}

</script>


<style>
@-webkit-keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
@-moz-keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
@-o-keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
@keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
.invalid {
  -webkit-animation: invalid 1s infinite; /* Safari 4+ */
  -moz-animation:    invalid 1s infinite; /* Fx 5+ */
  -o-animation:      invalid 1s infinite; /* Opera 12+ */
  animation:         invalid 1s infinite; /* IE 10+ */
}

@-webkit-keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
@-moz-keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
@-o-keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
@keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
.excluido {
  -webkit-animation: excluido 1s infinite; /* Safari 4+ */
  -moz-animation:    excluido 1s infinite; /* Fx 5+ */
  -o-animation:      excluido 1s infinite; /* Opera 12+ */
  animation:         excluido 1s infinite; /* IE 10+ */
}

td {
    padding: 1em;
}

header{
    background-color: darkblue;
    overflow: auto;
    color: white;
}

.dado-smallbox{
    font-weight: bold;
    font-size: 20px !important;
}

.label-smallbox{
    font-weight: bold;
}

.pesoMP{
    font-weight: bold;
    font-size: 15px !important;
}

.competidoresMP{
    font-size: 10px !important;
}

.iscamodalidadeMP{
    font-size: 10px !important;
    margin: 0;
}

#tableResultadosMaiorPeixe{
    margin: 0;
}

#tableResultadosGeral{
    margin: 0;
}

header #tableResultadosGeral tbody td{
    background-color: red;
    color: white;
}

header #tableResultadosGeral thead th{
    border: 1px solid transparent;
    background-color: red;
    font-size: 11px;
}

header #tableResultadosGeral tbody td{
    border: 1px solid transparent;
    background-color: red;
    font-size: 11px;
}

header #tableResultadosMaiorPeixe td, header #tableResultadosMaiorPeixe th{
    background-color: green;
    color: white;
    border-color: transparent;
    font-size: 11px;
}

header #ao-vivo{
    background-color: red;
    color: white;
    padding: 5px;
    border-radius: 5px;
    font-size: 9px;
    margin-left: 5px;
    white-space: nowrap;
}

table td{
    font-size: 13px;
}
table th{
    font-size: 15px;
}

thead th{
    color: white;
    background-color: darkblue;
}

.titulo-tabela{
    background-color: darkblue;
}

#tableExportaCLientes2{
    border-top: 1px solid white;
}

#tableExportaClientes2 td{
    font-size: 11px;
}

#tableExportaClientes2 thead tr:not(:first-child) th{
    background-color: #df6500;
}

#tableExportaClientes2 tbody tr:nth-child(odd):not(.bg-danger){
    background-color: #fff1e6;
}

#tableExportaClientes2 tbody tr:nth-child(even):not(.bg-danger){
    background-color: #ffe2ca;
}

#tableExportaClientes2 tbody tr:nth-child(odd):not(.bg-danger):hover{
    background-color: #ffbd87;
}

#tableExportaClientes2 tbody tr:nth-child(even):not(.bg-danger):hover{
    background-color: #ffbd87;
}

.pesagem-excluida{
    opacity: 0.3;
}

#tableResultadosRankingFinal tbody tr:nth-child(odd){
    background-color: #d2e2d2;
}

#tableResultadosRankingFinal tbody tr:nth-child(even){
    background-color: #f2f5f2;
}

#tableResultadosRankingFinal tbody tr:nth-child(odd):hover{
    background-color: #83a683;
}

#tableResultadosRankingFinal tbody tr:nth-child(even):hover{
    background-color: #83a683;
}

#tableResultadosRankingFinal thead tr:not(:first-child) th{
    background-color: #009200;
}

#tableResultadosRankingFinal td{
    font-size: 12px;
}

.header-detalhes th{
    background-color: red;
}

.unidademedida{
    font-size: 8px;
}

#previas{
    padding-right: 0;
    padding-left: 0;
}

.linhaLive, #resultadoPreviaRankingColocacoes tr{
    cursor: pointer;
}

.modalinfocompetidores{
    position: fixed;
    background-color: white;
    display: none;
    -webkit-box-shadow: 0px 0px 54px -4px rgba(0,0,0,0.69);
    -moz-box-shadow: 0px 0px 54px -4px rgba(0,0,0,0.69);
    box-shadow: 0px 0px 54px -4px rgba(0,0,0,0.69);
    height: 90%;
    width: 70%;
    left: 15%;
    bottom: 5%;
    z-index: 99999;
    overflow-y:scroll;
}

.fechamodalinfocompetidores{
    position:absolute;
    right:50;
}

.fechamodalinfocompetidores i{
    position:fixed;
    cursor: pointer;
    font-size: 50px;
}

.equipeID{
    display:none;
}

.colocacao-modal{
    position: fixed;
    display: flex;
    flex-direction: column;
    z-index:99999;
}

.colocacao-modal p{
    margin:0;
    padding: 3px 1px;
}

.colocacao-modal .posicao-modal{
    font-size: 40px;
    margin:0;
    background-color: green;
    color: white;
}

.colocacao-modal .capturas-modal{
    display:block;
    background-color: blue;
    color: white;
    font-size: 15px;
}

.colocacao-modal .peso-modal{
    display:block;
    background-color: red;
    color:white;
    font-size:15px;
}

.peso-total-mobile{
    background-color:green;
    color:white;
}

.quantidade-total-mobile{
    background-color:olive;
    color:white;
}

.infos-etapa-mobile{
    display: flex;
    clear:left;
}

.mp-mobile-mp{
    background-color:#BA0B0B;
    display: flex;
    color:white;
}

.mp-mobile-mp i{
    background-color:#970808;
    color:white;
}

#menu{
    display:none;
}

#botao-menu{
    display:none;
}

#titulo-pagina{
    display:none;
}

@media (max-width: 520px){
    header{
        position: fixed;
        top:0;
        z-index:99999;
        -webkit-box-shadow: 0px 17px 22px 0px rgba(0,0,0,0.31);
        -moz-box-shadow: 0px 17px 22px 0px rgba(0,0,0,0.31);
        box-shadow: 0px 17px 22px 0px rgba(0,0,0,0.31);
    }
    header h3{
        font-size:20px;
    }

    footer{
        position:fixed;
        bottom:0;
        margin:0;
        -webkit-box-shadow: -1px -22px 65px -6px rgba(0,0,0,0.53);
        -moz-box-shadow: -1px -22px 65px -6px rgba(0,0,0,0.53);
        box-shadow: -1px -22px 65px -6px rgba(0,0,0,0.53);
    }

    #botao-menu{
        position: relative;
        top:20px;
        right:0;
        display:block;
        font-size:25px;
        bottom:0;
    }

    #menu ul{
        padding:0;
    }

    #menu li{
        list-style: none;
        text-align:center;
        font-size: 20px;
        font-weight: bold;
        padding:10px;
    }

    .equipes, .graficos, .pesagens{
        display:none;
    }

    .ranking{
        display:block;
    }

    #titulo-pagina{
        display: block;
    }

    .modalinfocompetidores{
        overflow: scroll;
        left:1px;
        width:99%;
        -webkit-box-shadow: 0px 0px 133px 77px rgba(0,0,0,0.99);
        -moz-box-shadow: 0px 0px 133px 77px rgba(0,0,0,0.99);
        box-shadow: 0px 0px 133px 77px rgba(0,0,0,0.99);
    }

    .fechamodalinfocompetidores{
        position:fixed;
        font-size:40px;
        right:10px;
        z-index: 99999;
    }

    .colocacao-modal{
        flex-direction: row;
        z-index: 99999;
    }

    .colocacao-modal p{
        margin:0;
        padding: 3px 5px;
    }

    .colocacao-modal .posicao-modal{
        font-size: 25px;
        margin:0;
        background-color: green;
        color: white;
    }

    .colocacao-modal .capturas-modal{
        display:block;
        background-color: blue;
        color: white;
        font-size: 20px;
    }

    .colocacao-modal .peso-modal{
        display:block;
        background-color: red;
        color:white;
        font-size:20px;
    }

    .body{
        margin: 100px 0;
    }

    #tableExportaClientes2 td, #tableExportaClientes2 th{
        width: 10px;
    }

    .mp-mobile-mp i{
        background
    }

    .infos-body{
        padding-top: 20px;
    }

    .modalinfocompetidores h1{
        font-size:20px;
    }

    .modalinfocompetidores h3{
        font-size:15px;
    }

    #atualiza-dados{
        background-color: yellow;
        color:black;
    }
}
</style>

<header class="class-fixed">
    <div class="col-lg-3 col-sm-3 col-xs-10">
        <?php
            if($etapa = retornaEtapaHoje(null)){
                //echo '<h1 class="header-aovivo">Ao vivo <small id="etapa" align=center>'.$etapa->DESCRICAO.'</small></h1>';
                echo '<h3 class="header-aovivo">'.$etapa->DESCRICAO.'<small id="ao-vivo" align=center>Ao vivo</small></h3>';
                echo '<input type="hidden" name="idEtapaAcompanhamentoLive" id="idEtapaAcompanhamentoLive" value="'.$etapa->ID.'">';
            }else{
                echo '<h3 class="page-header">Acompanhamento ao vivo <small id="etapa" align=center>Nenhuma etapa sendo realizada hoje</small></h3>';
                echo '<input type="hidden" name="idEtapaAcompanhamentoLive" id="idEtapaAcompanhamentoLive" value="">';
            }
        ?>
        <small id='numberCountdown' align=center></small>
    </div>

    <div class="col-xs-2 row">
        <i class="fas fa-bars" id="botao-menu"></i>
    </div>

    <div class="col-xs-12" id="menu">
        <ul>
            <li id="atualiza-dados">Atualizar Dados</li>
            <li>Ranking</li>
            <li>Pesagens</li>
            <li>Gráficos</li>
            <li>Equipes</li>
        </ul>
    </div>

    <div class="col-lg-2 col-sm-2 col-xs-12">
        <div id="boxqtdpeixes"></div>
    </div>
    <div class="col-lg-2 col-sm-2 col-xs-12">
        <div id="boxpesopeixes"></div>
    </div>
    <div class="col-lg-3 col-sm-3 col-xs-12">
        <div id="boxmaiorpeixe"></div>
    </div>

</header>

<div class="col-xs-12">
    <h3 id="titulo-pagina" align=center>Ranking</h3>
</div>

<div class="modalinfocompetidores" align=center>
    <div class="colocacao-modal"></div>
    <div class="fechamodalinfocompetidores">
        <i class="fas fa-times-circle"></i>
    </div>
    <div class=infos-body>
    </div>
</div>

<div class="col-lg-12 body">
    <!--
	<ul class="nav nav-tabs">
            <li><a href="#" onclick="exibeAcomLive()">ACOMPANHAMENTO AO VIVO</a></li>
            <li><a href="#" onclick="exibeAcomPrevias()">PRÉVIA DOS RESULTADOS</a></li>
            <li><a href="#" onclick="exibeAcomDetalhes()">DETALHES PROVA</a></li>
	</ul>
    -->

    <div id="detalhes" class="col-lg-8 col-sm-12">
        <!--
        <div class="row">
            <div class="col-lg-12">
                <?php
                    if($etapa = retornaEtapaHoje(null)){
                        echo '<h3 class="page-header">Detalhes da prova <small id="etapa" align=center>'.$etapa->DESCRICAO.'</small></h3>';
                        echo '<input type="hidden" name="idEtapaAcompanhamentoDetalhe" id="idEtapaAcompanhamentoDetalhe" value="'.$etapa->ID.'">';
                    }else{
                        echo '<h3 class="page-header">Acompanhamento ao vivo <small id="etapa" align=center>Nenhuma etapa sendo realizada hoje</small></h3>';
                        echo '<input type="hidden" name="idEtapaAcompanhamentoDetalhe" id="idEtapaAcompanhamentoDetalhe" value="">';
                    }
                ?>
            </div>
        </div>
        -->

        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="row" >
                        <div class="col-lg-3 col-sm-3 col-xs-12 graficos">
                            <canvas id="graficoModalidade" height="400" width="400"></canvas>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-xs-12 graficos">
                            <canvas id="graficoEspecies" height="400" width="400"></canvas>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-xs-12 graficos">
                            <canvas id="graficoIscas" height="400" width="400"></canvas>
                        </div>
                        <div class="col-lg-3 col-sm-3 col-xs-12 graficos">
                            <canvas id="graficoDuplasComPeixe" height="400" width="400"></canvas>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5 col-sm-5 col-xs-12 graficos">
                            <canvas id="graficoHorario" height="200"></canvas>
                        </div>
                        <div class="col-lg-7 col-sm-7 col-xs-12 equipes">
                            <div id="botoes-equipes" align="center"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

	<div id="previas" class="col-lg-4 ranking">
            <div class="row">
                <div class="">
                    <div class="form-group">
                        <table id= "tableResultadosRankingFinal" class="table table-hover table-condensed" >
                            <thead>
                                <tr>
                                    <th colspan="5" class="titulo-tabela">Ranking<small id='numberCountdown' align=center></th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th>Col.</th>
                                    <th><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?></th>
                                    <th>Qtd</th>
                                    <th>Equipe/Competidores</th>
                                </tr>
                            </thead>
                            <tbody id="resultadoPreviaRankingColocacoes">
                                <!--RESULTADO RANKING-->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
	</div>

    <div id="horario" class="col-lg-4">

    </div>

    <div id="live" class="col-lg-12 pesagens">
            <div class="row">
                <div>
                    <!--
                    <?php
                        if($etapa = retornaEtapaHoje(null)){
                            //echo '<h1 class="header-aovivo">Ao vivo <small id="etapa" align=center>'.$etapa->DESCRICAO.'</small></h1>';
                            echo '<h3 class="header-aovivo">'.$etapa->DESCRICAO.'<small id="etapa" align=center>Ao vivo</small></h3>';
                            echo '<input type="hidden" name="idEtapaAcompanhamentoLive" id="idEtapaAcompanhamentoLive" value="'.$etapa->ID.'">';
                        }else{
                            echo '<h3 class="page-header">Acompanhamento ao vivo <small id="etapa" align=center>Nenhuma etapa sendo realizada hoje</small></h3>';
                            echo '<input type="hidden" name="idEtapaAcompanhamentoLive" id="idEtapaAcompanhamentoLive" value="">';
                        }
                    ?>
                    -->
                </div>
            </div>


            <div  class="row">
                <div class="">
                    <input type="hidden" name="ultimaVerificacao" id="ultimaVerificacao" value="">
                    <input type="hidden" name="idUltimaVerificacao" id="idUltimaVerificacao" value="">
                    <div class="form-group">
                        <div class="row">
                            <div class="">
                                <div class="table-responsive table-overflow" id="tableExportaClientes2">
                                    <table class="table table-hover table-condensed" >
                                        <thead>
                                            <tr>
                                                <th colspan="9" class="titulo-tabela">Últimas pesagens</th>
                                            </tr>
                                            <tr>
                                                <!--<th>#</th>-->
                                                <!--<th>Comprovante</th>-->
                                                <!--<th>Etapa</th>-->
                                                <!--<th>Data/Hora</th>-->
                                                <th>Hora</th>
                                                <th>Equipe</th>
                                                <th>Competidor</th>
                                                <th>Espécie</th>
                                                <th>Modalidade</th>
                                                <th>Iscas</th>
                                                <!--<th>Ponto Pesagem</th>-->
                                                <th><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?></th>
                                                <!--<th>Obs</th>-->
                                            </tr>
                                        </thead>
                                        <tbody id="acompanhamentoLive">
                                            <!--RESULTADO GERAL-->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

</div>

<footer align=center>

</footer>

	<link rel="stylesheet" href="../bower_components/jquery-ui/jquery-ui.css">
	<script src="../bower_components/jquery-ui/jquery-ui.js"></script>
	<script src="../bower_components/datatables/jquery.dataTables.min.js"></script>
	<script src="../bower_components/datatables/dataTables.bootstrap.min.js"></script>
</body>

</html>
