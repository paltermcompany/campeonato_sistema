<?php
session_start();
include '../functions/conexao.php';
require '../functions/crud.php';
?>

<script type="text/javascript" src="../js/custom.js"></script>

<script>
	var tableCadEquipes;

	function filtraEquipes(){
		tableCadEquipes.ajax.reload();
	}

	$(document).ready(function() {
		tableCadEquipes = $('#tableFiltroEquipes').DataTable({
			"ajax": {
				url: "../ajax/controller.php",
				type: "POST",
				data : function(d){
					d.acao = "carregaEquipes",
					d.filtroEquipeNome = $( '#filtroEquipeNome' ).val(),
					d.filtroEquipeCampeonato = $( '#filtroEquipeCampeonato' ).val(),
					d.filtroEquipeEtapa = $( '#filtroEquipeEtapa' ).val()
				},
			    beforeSend : function() {
                    $('#buttonFiltraEquipes').html('Aguarde').attr('disabled', true);
                },
                complete : function() {
                    $('#buttonFiltraEquipes').html('Filtrar').attr('disabled', false);
                }
			},
			"columns": [
				{"data": "id_equipe"},
				{"data": "campeonato"},
				{"data": "etapa"},
				{"data": "equipe"},
				{"data": "acoes"}
			],
			"language": {
				"url": "../bower_components/datatables/Portuguese-Brasil.json"
			},
			"paging": true,
			"lengthChange": true,
			"searching": false,
			"destroy": true,
			"ordering": true,
			"order": [[ 1, "asc" ]],
			"info": true,
			"autoWidth": false
		} );
	});

	function filtraEtapasCombo(idCampeonato){
		$('#filtroEquipeEtapa').html('');
		$.post('../ajax/controller.php',{
			acao:"listaEtapas",
			idCampeonato:idCampeonato
		},function(retorno){
			$('#filtroEquipeEtapa').html('<option value="-1">Todos</option>');
			$('#filtroEquipeEtapa').append(retorno);
		});
	}

</script>

<div id="propria">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Cadastro de Equipes  <button onClick="abreModalNovaEquipe()" type="button" class="close glyphicon glyphicon-plus text-muted" ><span aria-hidden="true"></span></button></h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<form name="form_filtraEquipes" id="form_filtraEquipes" >
				<div class="well form-group">
					<h4>Filtros</h4>
					<div class="row">
						<div class="col-lg-6">
							<label>Equipe</label>
							<input type="text" id="filtroEquipeNome" name="filtroEquipeNome" class="form-control upcase">
						</div>

						<div class="col-lg-3">
							<label>Campeonato</label>
							<select id="filtroEquipeCampeonato" name="filtroEquipeCampeonato" class="form-control" onchange="filtraEtapasCombo($( '#filtroEquipeCampeonato option:selected' ).val())">
								<option value="-1">Todos</option>
									<?php
										$campeonatos = retornaCampeonatos('', '');
										foreach ($campeonatos as $result) {
											echo '<option value="' . $result->id . '">' . $result->descricao . '</option>';
										}
									?>
							</select>
						</div>

						<div class="col-lg-2">
							<label>Etapa</label>
							<select id="filtroEquipeEtapa" name="filtroEquipeEtapa" class="form-control">
								<option value="-1">Todos</option>
								<?php
									$etapas = retornaEtapas('', 0);
									foreach ($etapas as $result) {
										echo '<option value="' . $result->id . '">' . $result->DESCRICAO . '</option>';
									}
								?>
							</select>
						</div>

						<div class="col-lg-1 pull-right">
						<label></label>
							<div class="input-group">
								<button class="btn btn-primary" name="buttonFiltraEquipes" id="buttonFiltraEquipes" style="background-color:#005081" type="button" onclick="filtraEquipes()"><b>Filtrar</b></button>
							</div>
						</div>
					</div>

				</div>
			</form>
			<div class="row" >
				<div class="col-lg-12" id="resultadoFiltroEquipes">
					<table id="tableFiltroEquipes" class="table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Campeonato</th>
								<th>Etapa</th>
								<th>Equipe</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody id="resultadoLancamentosPorEtapa">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

