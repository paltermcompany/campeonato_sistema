//JQUERY CODE..
//acts like document.onload..
$(function(){
	function pegaRanking(handleData){
		$.ajax({
			type: "POST",
			url : "dados.php",
			dataType: "json",
			async:false, 
			success : function (data) {
				handleData(data);
			}
		});
	}
	
	var compareCells = function(a, b) {
        var b = $.text([b]);
        var a = $.text([a]);

        if (isNumber(a) && isNumber(b)) {
            return parseFloat(b) - parseFloat(a);
        } else {
            return a.localeCompare(b);
        }
    };

	
	var animatedSort = function() {
        var table = $(this);

        //What column are we ordering on?
        var sortIndex = table.find(".anim\\:sort").index();

        //Old table order
        var idIndex = table.find(".anim\\:id").index();
        var startList = table.find('td').filter(function() {
          return $(this).index() === idIndex;
        });

        //Sort the list
        table.find('td').filter(function() {
            return $(this).index() === sortIndex;
        }).sortElements(compareCells, function() { // parentNode is the row we want to move
            return this.parentNode;
        });

        //New table order
        var idIndex = table.find(".anim\\:id").index();
        var endList = table.find('td').filter(function() {
            return $(this).index() === idIndex;
        });
      };


	var portfolio;
	
    //gets the given number with given decimals..
    var display = function(number, decimals){
        number = "" + round(number, decimals);
        var dot = number.indexOf('.');
        if(dot < 0){
            number += '.';
            dot = number.length-1; 
        }
        for(var i=number.length-1-dot; i < decimals; i++){
            number += "0";
        }
        return number;
    };
	
	//rough rounding func..
    var round = function(number, decimals){
        var factor = 1;
        for(var i=0; i<decimals; i++){
            factor *= 10;
        }
        number *= factor;
        number = Math.round(number);
        return number/factor;
    };

    var updatePortfolio = function(){
		pegaRanking(function(output){
			portfolio = output;
		});
    };

	pegaRanking(function(output){
		portfolio = output;

		var sortingFuncs = {
			number: function(i, j){
				return i - j;
			},
			string: function(i, j){
				return (i > j) ? 1 : (i == j) ? 0 : -1;
			}
		};
		
		//initially sort on p and l decending..
		var colSortedOn = 4;
		var ascending = false;
		var sortingFunc = sortingFuncs.number;
		
		//it is currently updating..
		var updating = false;
		
		var generateTable = function(){
			pegaRanking(function(output){
				portfolio = output;
			});
			
			var table = $('#stocks .template').clone();
			table.attr('class', 'actualTable');
			var tBody = $(table[0].tBodies[0]);
			

			
			
			
		
			/*//var table = $(this);

			//What column are we ordering on?
			

			//Old table order
			var idIndex = table.find(".anim\\:id").index();
			var startList = table.find('td').filter(function() {
			  return $(this).index() === idIndex;
			});

			//Sort the list
			table.find('td').filter(function() {
				return $(this).index() === colSortedOn;
			}).sortElements(compareCells, function() { // parentNode is the row we want to move
				return this.parentNode;
			});

			//New table order
			var idIndex = table.find(".anim\\:id").index();
			var endList = table.find('td').filter(function() {
				return $(this).index() === idIndex;
			});
      		*/
			
			
			//sort the output..
			portfolio.sort(function(i, j){
				i = i[colSortedOn];
				j = j[colSortedOn];
				return (ascending) ? sortingFunc(i,j) : 0-sortingFunc(i,j);
			});

			$.each(portfolio, function(i, position){
				if(i < 9){
					position[0] = '0'+ (i+1);
				}else{
					position[0] = ''+ (i+1);
				}
				
				var row = $('<tr />');
				$.each(position, function(j, value){
					
					row.append($('<td />', {
						text: (typeof value == 'number') ? display(value, 2) : value,
						css: {
							textAlign: (typeof value == 'number') ? 'right' : 'left',
						}
					}));
				});
				tBody.append(row);
			});
			
			//attach click events..
			table.find('.fe-sortable').each(function(i, header){
				header = $(header);
				header.click(function(event){
					event.preventDefault();
					if(updating){
						return;
					}
					updating = true;
					if(colSortedOn == i){
						ascending = !ascending;
					} else {
						ascending = true;
					}
					colSortedOn = i;
					sortingFunc = (header.hasClass('fe-string')) ? sortingFuncs.string : sortingFuncs.number;
					
					//animatedSort();
					
					updateTable();
				});
			});
			
			return table;
		};
		
		//Updates the status with the current time..
		var updateStatusTime = function(){
			var date = new Date();
			var dateStr = (date.getHours()<10?'0':'') + date.getHours() + ":" + (date.getMinutes()<10?'0':'') + date.getMinutes() + ":" 
							+ (date.getSeconds()<10?'0':'') + date.getSeconds() + ", " + date.getDate() + '\/'
							+ (date.getMonth()+1) + '\/' + date.getFullYear();
			$('#stocks .status').html("atualizado em: " + dateStr);
		};
		
		//set up the table initially..
		var table = generateTable().show();
		//add image showing sort..
		$(table[0].tHead.rows[0].cells[4]).append('&darr;');
		$('#stocks .tableHolder').append(table);
		updateStatusTime();

		//Updates the table..
		var updateTable = function(updateStatus){
			var newTable = generateTable();
			table.rankingTableUpdate(newTable, {
				//duration: [1000,200,700,200,1000],
				duration: [1000,0,500,0,500],
				onComplete: function(){
					updating = false;
					if(updateStatus){
					   updateStatusTime();
					}
				},
				animationSettings: {
					up: {
						left: 10,
						backgroundColor: '#CCFFCC'
					},
					down: {
						left: -5,
						backgroundColor: '#FFCCCC'
					},
					fresh: {
						left: 0,
						backgroundColor: '#ffffcc'
					},
					drop: {
						left: 0,
						backgroundColor: '#FFCCCC'
					}
				}
			});
			table = newTable;
			$(table[0].tHead.rows[0].cells[colSortedOn]).append((ascending) ? '&uarr;' : '&darr;');
		}
		
		//reference to the infinite loop..
		var loop = null;
		//$('#stocks .triggerUpdates').click(function(event){
		$( document ).ready(function() {
			if(!loop){
				var loopFunc = function(){
					if(!updating){
						updating = true;
				
						$('#stocks .status').html("Atualizando..");
						//set the columns to be updating..
						$('#stocks .actualTable .livePrice').removeClass('anim:constant');
						$('#stocks .actualTable .profitLoss').removeClass('anim:constant');
			
						updatePortfolio();
						updateTable(true);
						//set the columns to be updating..
						$('#stocks .actualTable .livePrice').addClass('anim:constant');
						$('#stocks .actualTable .profitLoss').addClass('anim:constant');
					}
				}
				loopFunc();
				loop = setInterval(loopFunc, 10000);
			} else {
				clearInterval(loop);
				loop = null;
			}
			event.preventDefault();
		});		
		

	});
	

	
});
