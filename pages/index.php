<?php
	session_start();
	include_once '../inc/topo.php';
?>


<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;background-color:Gainsboro;height:80px;">
            <div class="navbar-header">
				<a class="navbar-brand-centered" href="index.php"><img src="../img/logos/default.png" style="height:60px;"></a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
                        <i class="fa fa-user fa-fw"></i> <?php echo $_SESSION['logado_nome']; ?> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
							<a href="javascript:void(0)" onClick="abrePerfilUsuario(<?php echo $_SESSION['logado_id']; ?>)"><i class="fa fa-user fa-fw"></i> Perfil do usuário</a>
						</li>
                        <li class="divider"></li>
                        <li>
							<a href="?logout=true"><i class="fa fa-sign-out fa-fw"></i> Sair do sistema</a>
						</li>
                    </ul>
                </li>
            </ul>


            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="javascript:void(0)" onClick="voltaHome()" class="menuprincipal"><i class="fa fa-home fa-fw"></i> Tela Inicial</a>
                        </li>

                        <li>
                            <a href="javascript:void(0)"><i class="fa fa-plus-circle fa-fw"></i> Cadastros<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="javascript:void(0)" onClick="abreCadastroCampeonatos()">Campeonatos</a>
                                </li>
								<li>
                                    <a href="javascript:void(0)" onClick="abreCadastroEtapas()">Etapas</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" onClick="abreCadastroCompetidores()">Competidores</a>
                                </li>
								<li>
                                    <a href="javascript:void(0)" onClick="abreCadastroEquipes()">Equipes</a>
                                </li>
								<li>
                                    <a href="javascript:void(0)" onClick="abreCadastroEspecies()">Espécies</a>
                                </li>
								<li>
                                    <a href="javascript:void(0)" onClick="abreCadastroModalidades()">Modalidades</a>
                                </li>
								<li>
                                    <a href="javascript:void(0)" onClick="abreCadastroIscas()">Iscas</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" onClick="abreCadastroColetores()">Coletores</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" onClick="abreCadastroFiscaisProva()">Fiscais de Prova</a>
                                </li>
                            </ul>
                        </li>

						<li>
                            <a href="javascript:void(0)"><i class="fa fa-users fa-fw"></i> Composições<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="javascript:void(0)" onClick="abreComposicaoEquipes()">Equipes</a>
                                </li>
								<li>
                                    <a href="javascript:void(0)" onClick="abreComposicaoEtapas()">Etapas</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" onClick="abreComposicaoRaias()">Raias</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="javascript:void(0)"><i class="fa fa-trophy fa-fw"></i> Relatórios<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="javascript:void(0)" onClick="abreResultadoEtapas()">Resultados Etapa</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" onClick="abreResultadoCampeonato()">Resultados Campeonato</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" onClick="abreRelacaoRaias()">Relação de Raias</a>
                                </li>
				                <li>
                                    <a href="javascript:void(0)" onClick="abreLancamentosEtapas()">Lançamentos Etapa</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" onClick="abreCompetidoresEtapas()">Competidores Etapa</a>
                                </li>
				                <li>
                                    <a href="javascript:void(0)" onClick="abreDetalhesEtapas()">Detalhes Etapa</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" onClick="abreCadastrosAtualizados()">Cadastros Atualizados</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" onClick="abreEtiquetaCamiseta()">Etiquetas Camisetas</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" onClick="abreBoasVindas()">Carta Boas Vindas</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0)" onClick="abreModeracao()" ><i class="fa fa-heartbeat fa-fw"></i> Moderação de Medidas</a>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
        <div id="page-wrapper">
			<script>voltaHome();</script>

        </div>
    </div>
	<!-- Modal -->
	<div class="modal fade" id="buscaProjetos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button id="closebtn" type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Cadastro</h4>
				</div>
				<div class="modal-body">

				</div>
			</div>
		</div>
	</div>


    <div class="modal" id="modalAux" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="titleModalAux">Second Modal title</h4>
                </div>
                <div class="container">

                </div>
                <div class="modal-body" id="bodyModalAux">
                    Conteudo do modal
                </div>
                <div class="modal-footer" id="footerModalAux">

                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

	<link href="../bower_components/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.css" rel="stylesheet" media="screen">



    <link href="https://cdn.datatables.net/rowgroup/1.0.2/css/rowGroup.dataTables.min.css" rel="stylesheet" media="screen">
	<script type="text/javascript" src="../bower_components/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>


	<script type="text/javascript" src="../bower_components/bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.pt-BR.js" charset="UTF-8"></script>
	<script type="text/javascript" src="../bower_components/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js" charset="UTF-8"></script>
	<link rel="stylesheet" href="../bower_components/jquery-ui/jquery-ui.css">
	<script src="../bower_components/jquery-ui/jquery-ui.js"></script>
	<script src="../bower_components/datatables/jquery.dataTables.min.js"></script>
        <script src="../bower_components/datatables/dataTables.rowGroup.min.js"></script>
	<script src="../bower_components/datatables/dataTables.bootstrap.min.js"></script>

 <script type="text/javascript" src="../bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js" charset="UTF-8"></script>


  <link href="../bower_components/bootstrap-datepicker/bootstrap-datepicker3.min.css" rel="stylesheet" media="screen">

</body>

</html>
