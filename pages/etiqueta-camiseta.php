<?php
    session_start();
    include '../functions/conexao.php';
    require '../functions/crud.php';
    header('Content-Type: text/html; charset=utf-8');
?>

<script type="text/javascript" src="../js/custom.js"></script>


<script>
    function imprimirRaias(){
        window.open('print-raias.php','_blank');
        return false;
    };
    filtraEtapasComboResultados($( '#filtroEquipeCampeonatoResultado2 option:selected' ).val())


    function filtraEtapasComboResultados(idCampeonato){
        $('#filtroEtiquetaCamisetaEtapaResultado2').html('');
        $.post('../ajax/controller.php',{
            acao:"listaEtapas",
            idCampeonato:idCampeonato
        },function(retorno){
            $('#filtroEtiquetaCamisetaEtapaResultado2').append(retorno);
        });
    }



</script>

<style>

.barraBotoes {
  float: right;
  margin-right:5px;
  font-size: 21px;
  font-weight: bold;
  line-height: 1;
  color: #000;
  text-shadow: 0 1px 0 #fff;
  filter: alpha(opacity=20);
  opacity: .2;
  -webkit-appearance: none;
  padding: 0;
  cursor: pointer;
  background: transparent;
  border: 0;

</style>

<div id="propria">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Etiquetas para Camisetas <button onClick="imprimiEtiquetasCamisetas($( '#filtroEtiquetaCamisetaEtapaResultado2 option:selected' ).val(),$('input[name=\'etiquetaCamiseta\']:checked').val())" type="button" class="barraBotoes glyphicon glyphicon-print text-muted" ></button></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form name="form_filtraResultadosCompetidoresEtapa" id="form_filtraResultadosCompetidoresEtapa" method="post" >
                <div class="well form-group">
                    <h4>Filtros</h4>
                    <div class="row">
                        <div class="col-lg-3">
                            <label>Campeonato</label>
                            <select id="filtroEquipeCampeonatoResultado2" name="filtroEquipeCampeonatoResultado2" class="form-control" onchange="filtraEtapasComboResultados($( '#filtroEquipeCampeonatoResultado2 option:selected' ).val())">
                                <?php
                                    $campeonatos = retornaCampeonatos('', '');
                                    foreach ($campeonatos as $result){
                                        echo '<option value="'.$result -> id.'">'.$result -> descricao.'</option>';
                                    }
                                 ?>
                            </select>
                        </div>

                        <div class="col-lg-2">
                            <label>Etapa</label>
                            <select id="filtroEtiquetaCamisetaEtapaResultado2" name="filtroEtiquetaCamisetaEtapaResultado2" class="form-control">
                                <?php
                                    $etapas = retornaEtapas('', 0);
                                    foreach ($etapas as $result){
                                        echo '<option value="'.$result -> id.'">'.$result -> DESCRICAO.'</option>';
                                    }
                                 ?>
                            </select>
                        </div>

                        <div class="col-lg-2">
                            <div class="radio" >
                                <label><input name="etiquetaCamiseta" id="checkEtiquetaFrente" type="radio" value='frente' checked>Etiqueta Frente</label>
                                <label><input name="etiquetaCamiseta" id="checkEtiquetaCostas" type="radio" value='costas'>Etiqueta Costas</label>
                            </div>
                        </div>


                    </div>

                </div>
            </form>


        </div>
    </div>
</div>
