<?php
	include '../functions/conexao.php';
	require '../functions/crud.php';
	header('Content-Type: text/html; charset=utf-8');
?>
    <script type="text/javascript" src="../js/custom.js"></script>
	<div class="panel-body">
	<div id='alerta'></div>
		<form role='form' method='post' action='' name='form_newCadCompetidor'>
			<fieldset>
				<div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Nome Competidor</label>
                            <input id="newCadNomeCompetidor" name="newCadNomeCompetidor" class="form-control upcase" placeholder="Nome Competidor" required>
                        </div>
                    </div>
                </div>

				<div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>CPF</label>
                            <input id="newCadCPFCompetidor" name="newCadCPFCompetidor" class="form-control" placeholder="CPF" required>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Apelido</label>
                            <input id="newCadApelidoCompetidor" name="newCadApelidoCompetidor" class="form-control upcase" placeholder="Apelido" required>
                        </div>
                    </div>
				</div>

                <div class="row">
                    <div class="col-lg-2">
                        <label>Tam. Camiseta</label>
                        <select id="newCadTamanhoCompetidor" name="newCadTamanhoCompetidor" class="form-control">
                            <option value="">Selecione</option>
                            <optgroup label="Adulto">
                                <?php
                                foreach( ['PP','P','M','G','GG','EX','G3','G4','G5','G6','G7'] as $tamanho ) {
                                    echo '<option value="'.$tamanho.'">'.$tamanho.'</option>';                                    
                                }
                                ?>
                            </optgroup>
                            <optgroup label="Infantil">
                                <?php
                                foreach( ['04','06','08','10','12'] as $tamanho ) {
                                    echo '<option value="'.$tamanho.'">'.$tamanho.'</option>';                                    
                                }
                                ?>
                            </optgroup>
                        </select>
                    </div>

                    <!-- Modelagem camiseta -->
                    <input type="hidden" name="newCadModelagemCompetidor" value="Normal">
                    <!--
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Modelagem Camiseta</label>
                            <select id="newCadModelagemCompetidor" name="newCadModelagemCompetidor" class="form-control">
                                <option value="Normal">Normal</option>
                                <option value="Baby Look">Baby Look</option>
                            </select>
                        </div>
                    </div>
                    -->
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="newCadNascimentoCompetidor">Nascimento</label>
                            <div class="input-group date time form_datetime" data-link-field="newCadNascimentoCompetidor">
                                <input type="text" name="newCadNascimentoCompetidor2" id="newCadNascimentoCompetidor2" class="form-control" type="text" readonly required >
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove" ></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                            </div>
                            <input type="hidden" name="newCadNascimentoCompetidor" id="newCadNascimentoCompetidor" value="">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Telefone:</label>
                            <input id="newCadTelefoneCompetidor" name="newCadTelefoneCompetidor" class="form-control upcase" placeholder="Telefone" required>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>E-mail</label>
                            <input id="newCadEmailCompetidor" name="newCadEmailCompetidor" class="form-control upcase" placeholder="E-mail" required>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <label>Sexo</label>
                        <select id="newCadSexoCompetidor" name="newCadSexoCompetidor" class="form-control">
                            <option value="M">Masculino</option>
                            <option value="F">Feminino</option>
                        </select>
                    </div>
                </div>

                <div class="well well-sm">
    				<div class="row">
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label>Logradouro</label>
                                <input id="newCadEnderecoCompetidor" name="newCadEnderecoCompetidor" class="form-control upcase" placeholder="Endereço" required>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Número</label>
                                <input id="newCadNumeroEndCompetidor" name="newCadNumeroEndCompetidor" class="form-control" placeholder="Número" required>
                            </div>
                        </div>
    				</div>



                    <div class="row">
                        <div class="col-lg-10">
                            <div class="form-group">
                                <label>Complemento</label>
                                <input id="newCadComplementoCompetidor" name="newCadComplementoCompetidor" class="form-control upcase" placeholder="Complemento">
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>CEP</label>
                                <input id="newCadCEPCompetidor" name="newCadCEPCompetidor" class="form-control upcase" placeholder="CEP" required>
                            </div>
                        </div>
    				</div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Bairro</label>
                                <input id="newCadBairroCompetidor" name="newCadBairroCompetidor" class="form-control upcase" placeholder="Bairro" required>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Cidade</label>
                                <input id="newCadCidadeCompetidor" name="newCadCidadeCompetidor" class="form-control upcase" placeholder="Cidade" required>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>UF</label>
                                <select id="newCadUFCompetidor" name="newCadUFCompetidor" class="form-control" required>
                                    <?php
                                        if ($estados = retornaUFs()){
                                            foreach($estados as $estado){
                                                echo '<option value="'.$estado->sigla.'">'.$estado->descricao.' ('.$estado->sigla.')</option>';
                                            }
                                        }else{
                                            echo '<option value="nenhumEstado">Nenhum estado encontrado</option>';
                                        }
                                    ?>

                                </select>
                            </div>
                        </div>
    				</div>
                </div>
			</fieldset>
			<div class="span6 pull-right">
				<button id="botaoInsereCompetidor" name="botaoInsereCompetidor" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
			</div>
		</form>
	</div>
	<script>
            $('#newCadCPFCompetidor').mask('000.000.000-00', {reverse: true});
	</script>
