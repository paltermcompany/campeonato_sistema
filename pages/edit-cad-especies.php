<?php
	include '../functions/conexao.php';
	require '../functions/crud.php';
	header('Content-Type: text/html; charset=utf-8');
	$idEspecie = $_REQUEST['idEspecie'];
	$especie = retornaEspecie($idEspecie);
?>
	<script type="text/javascript" src="../js/custom.js"></script>
	<div class="panel-body">
		<form role='form' method='post' action='' name='form_editCadEspecie'>
			<fieldset>
				<input type="hidden" name="editCadIdEspecie" id="editCadIdEspecie" value="<?php echo $especie -> id;?>">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label>Nome Espécie</label>
							<input id="editCadNomeEspecie" name="editCadNomeEspecie" class="form-control upcase" placeholder="Nome Espécie" required value="<?php echo $especie->descricao;?>">
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label>Prioridade</label>
							<input id="editCadPrioridadeEspecie" name="editCadPrioridadeEspecie" type="number" class="form-control" placeholder="Nome Espécie" required value="<?php echo $especie->prioridade;?>">
						</div>
					</div>
				</div>
			</fieldset>
			<div class="span6 pull-right">
				<button id="botaoEditaEspecie" name="botaoEditaEspecie" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
			</div>
		</form>
	</div>
