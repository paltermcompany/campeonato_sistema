<?php
	session_start();
	include '../functions/conexao.php';
	require '../functions/crud.php';
?>
<div id="propria">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Composição Equipes</h1>
		</div>
	</div>


	<style>
	.boxRolagem {
	max-height: 600px;
	overflow: auto;
	}
	</style>

	<div class="panel panel-default col-lg-12">

			<div class="col-lg-4 boxRolagem" id="listaCampeonatosEquipes">
				<?php
				if ($etapas = retornaCampeonatos('', 'A')){

					$tabela =		'<table class="table table-striped table-hover" id="tableListaEtapas" name="tableListaEtapas">'.
										'<th>Selecione o Campeonato</th>'.
										'<tbody>';

					foreach ($etapas as $result){
						$tabela=$tabela .	'<tr class="odd gradeX">'.
												'<td class="col-md-10" style="cursor:pointer" onclick="carregaEtapas('.$result -> id .');return false;">'. $result -> descricao . '</td>'.
											'</tr>';
					}
					$tabela= $tabela.	'</tbody>'.
									'</table>';
					echo $tabela;
				}else{
					echo 'Nenhum campeonato encontrado';
				}
				?>
			</div>
			<div class="col-lg-8" name="infoListaEquipes" id="infoListaEquipes">

			</div>
	</div>
</div>

<script>


	function informaPatrocinador(idEquipe, $patrocinador){
		var patrocinador = prompt("Informe o nome do patrocinador", $patrocinador);
		if ((patrocinador != '')&&(patrocinador != null)) {
			$.post('../ajax/controller.php',{
			acao:"informaPatrocinadorEquipe",
			idEquipe:idEquipe,
			patrocinador:patrocinador.toUpperCase()
			},function(retorno){
				if(retorno=='ok'){
					$('#btnPatrocinador'+idEquipe).html('<span class="glyphicon glyphicon-piggy-bank" aria-hidden="true"></span> ' + patrocinador.toUpperCase());
					$('#btnPatrocinador'+idEquipe).addClass("btn-primary");
				}else{
					alert('Erro ao inserir a codigo de identificação, informe ao suporte!' + retorno);
				}
			});
		};
	}


	function carregaEtapas(idCampeonato){
		$('#listaCampeonatosEquipes').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando..</center>');
		$.post('../ajax/controller.php',{
			acao:"listaEtapasParaComposicaoEquipes",
			idCampeonato:idCampeonato
		},function(retorno){
			$('#listaCampeonatosEquipes').html(retorno);
			$('#infoListaEquipes').html('');
		});
	}

	function carregaCampeonatos(){
		$('#listaCampeonatosEquipes').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando..</center>');
		$.post('../ajax/controller.php',{
			acao:"listaCampeonatosParaComposicao"
		},function(retorno){
			$('#listaCampeonatosEquipes').html(retorno);
			$('#infoListaEquipes').html('');
		});
	}

	function carregaEquipes(idEtapa, idCampeonato, idPulseira, nomeCompetidor){
		idPulseira = $('#filtraPulseira').val();
		nomeCompetidor = $('#filtraNomeCompetidor').val();
		$('#listaCampeonatosEquipes').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando..</center>');
		$.post('../ajax/controller.php',{
			acao:"listaEquipesParaComposicao",
			idEtapa:idEtapa,
			idCampeonato:idCampeonato,
			idPulseira:idPulseira,
			nomeCompetidor:nomeCompetidor
		},function(retorno){
			$('#listaCampeonatosEquipes').html(retorno);
			$('#infoListaEquipes').html('');
		});
	}

</script>
