<?php
    session_start();
    include '../functions/conexao.php';
    require '../functions/crud.php';
?>

<script type="text/javascript" src="../js/custom.js"></script>

<script>
    var tableCadColetores;

    function filtraColetores(){
        tableCadColetores.ajax.reload();
    }

    $(function() {
        tableCadColetores = $('#tableCadColetores').DataTable({
            "ajax": {
                url: "../ajax/controller.php",
                type: "POST",
                data : function(d){
                    d.acao = "carregaColetores",
                    d.filtroColetorNome = $('#filtroColetorNome').val()
                },
                beforeSend : function() {
                    $('#buttonFiltraColetores').html('Aguarde').attr('disabled', true);
                },
                complete : function() {
                    $('#buttonFiltraColetores').html('Filtrar').attr('disabled', false);
                }
            },
            "columns": [
                {"data": "id_coletor"},
                {"data": "nome_coletor"},
                {
                    "orderable": false,
                    "defaultContent": '<div class="btn-group btn-group-xs"><button type="button" class="btn btn-warning btnAlteraColetor"><span class="glyphicon glyphicon-edit"></span> </button><button type="button" class="btn btn-danger" onclick="naoImplementado()"><span class="glyphicon glyphicon-remove"></span></button></div>'
                },
            ],
            "language": {
                "url": "../bower_components/datatables/Portuguese-Brasil.json"
            },
            "paging": true,
            "lengthChange": true,
            "searching": false,
            "destroy": true,
            "ordering": true,
            "order": [[ 1, "asc" ]],
            "info": true,
            "autoWidth": false
        } );

        $('#tableCadColetores tbody').on( 'click', 'button', '.btnAlteraColetor', function () {
            var data = tableCadColetores.row( $(this).parents('tr') ).data();
            console.log(data);
            abreModalEditaColetor(data.id_coletor);
        } );
    });
</script>

<div id="propria">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cadastro de Coletores  <button onClick="abreModalNovoColetor()" type="button" class="close glyphicon glyphicon-plus text-muted" ><span aria-hidden="true"></span></button></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form name="form_filtraColetores" id="form_filtraColetores" method="post" >
                <div class="well form-group">
                    <h4>Filtros</h4>
                    <div class="row">
                        <div class="col-lg-11">
                            <label>Descrição</label>
                            <input type="text" id="filtroColetorNome" name="filtroColetorNome" class="form-control upcase">
                        </div>

                        <div class="col-lg-1 pull-right">
                        <label></label>
                            <div class="input-group">
                                <button class="btn btn-primary" name="buttonFiltraColetores" id="buttonFiltraColetores" style="background-color:#005081" type="button" onclick="filtraColetores()"><b>Filtrar</b></button>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
            <div class="row" >
                <div class="col-lg-12">
                    <table id="tableCadColetores" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Descrição</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody id="resultadoFiltroColetores">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

