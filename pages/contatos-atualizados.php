<?php
	session_start();
	include '../functions/conexao.php';
	require '../functions/crud.php';
	
?>

<script type="text/javascript" src="../js/custom.js"></script>


<script type="text/javascript" src="../bower_components/bootstrap-daterangepicker-master/moment.js"></script>
<script type="text/javascript" src="../bower_components/bootstrap-daterangepicker-master/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../bower_components/bootstrap-daterangepicker-master/daterangepicker.css" />

<script>
    function imprimirStatusProjetos(){
        //window.open('report-projects-print.php','_blank');
        return false;	
    };
    
    function filtraContatosAtualizados(){
	dataDe = $( '#filtroContatosEfetuadosDataContatoDe' ).val();
        dataAte = $( '#filtroContatosEfetuadosDataContatoAte' ).val();

        $('#tableContatosAtualizados').DataTable({
            "ajax": {
                url: "../ajax/controller.php",
                type: "POST",
                data :{
                    acao: "carregaContatosAtualizados",
                    dataDe:dataDe,
                    dataAte:dataAte
                }
            },
            "columnDefs": [
                { "visible": false, "targets": 0 }
            ],
            "columns": [
                {"data": "data"},
                {"data": "uf"},
                {"data": "total"}
                    
            ],

            "language": {
                    "url": "../bower_components/datatables/Portuguese-Brasil.json"
            },
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "destroy": true,
            "ordering": true,
            "order": [[ 0, "asc" ]],
            "info": false,
            "autoWidth": false,
            "drawCallback": function ( row, data, start, end, display ) {
                var api = this.api();
                var rows = api.rows( {page:'current'} ).nodes();
                var last=null;

                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
                
                total = api
                    .column( 2 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                
                $( api.column( 2 ).footer() ).html(
                     total
                );

                api.column(0, {page:'current'} ).data().each( function ( group, i ) {
                    if ( last !== group ) {
                        $(rows).eq( i ).before(
                            '<tr class="group"><td colspan="3">'+group+'</td></tr>'
                        );

                        last = group;
                    }
                } );
            },
            rowGroup: {
                startRender: null,
                endRender: function ( rows, group ) {
                    var qtdSoma = rows
                            .data()
                            .pluck("total")
                            .reduce(function (a, b) {
                                return a + b * 1;
                            }, 0);

                    return "Total em " + group +': '+qtdSoma;
                },
                dataSrc: "data"
            }
        } );
    }       


</script>

<div id="propria">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Relatório Contatos Atualizados  <span aria-hidden="true"></span></button> <button onClick="imprimiContatosAtualizadosUF($('#filtroContatosEfetuadosDataContatoDe').val(), $('#filtroContatosEfetuadosDataContatoAte').val())" type="button" class="close glyphicon glyphicon-print text-muted" ><span aria-hidden="true"></span></button></h1>
        </div>
    </div>
	
    <div class="row">
        <div class="col-lg-12">
            <form name="form_filtraContatosAtualizados" id="form_filtraContatosAtualizados" method="post" >
                <div class="well form-group">
                    <h4>Filtros</h4>
                    <div class="row">
                        <div class="col-lg-2">
                            <label>Data Contato</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" name="filtroContatosEfetuadosDataContato" id="filtroContatosEfetuadosDataContato" placeholder="Intervalo Contato" value="">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar fa fa-calendar"></span></span>
                                </div>
                        </div>
                        <input type="hidden" class="form-control" id="filtroContatosEfetuadosDataContatoDe" name="filtroContatosEfetuadosDataContatoDe">
                        <input type="hidden" class="form-control" id="filtroContatosEfetuadosDataContatoAte" name="filtroContatosEfetuadosDataContatoAte">

                        <div class="col-lg-1 pull-right">
                            <div class="input-group">
                                <button class="btn btn-primary" name="buttonFiltraLancamentos" id="buttonFiltraLancamentos" style="background-color:#005081" type="button" onclick="filtraContatosAtualizados()"><b>Filtrar</b></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <table id="tableContatosAtualizados" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>Data</th>
                        <th>UF</th>
                        <th>Quantidade</th>
                    </tr>
                </thead>
                <tbody id="resultadoClientesAtualizados">

                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="2" style="text-align:right">Total:</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>	
 

<script type="text/javascript">
    $(function() {
	//data de contato
	$('#filtroContatosEfetuadosDataContato').daterangepicker({
		autoApply: true,
		autoUpdateInput: false,
		locale: {
			"format": "DD/MM/YYYY",
			"separator": " - ",
			"applyLabel": "Aplicar",
			"cancelLabel": "Cancelar",
			"fromLabel": "De",
			"toLabel": "Até",
			"customRangeLabel": "Outro período",
			"daysOfWeek": [
				"D",
				"S",
				"T",
				"Q",
				"Q",
				"S",
				"S"
			],
			"monthNames": [
				"Janeiro",
				"Fevereiro",
				"Março",
				"Abril",
				"Maio",
				"Junho",
				"Julho",
				"Agosto",
				"Setembro",
				"Outubro",
				"Novembro",
				"Dezembro"
			],
			"firstDay": 1
		},
		ranges: {
           'Hoje': [moment(), moment()],
           'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
           'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
           'Este Mês': [moment().startOf('month'), moment().endOf('month')],
           'Último Mês': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },		
		opens: "left"
	}, function(start, end, label) {
		$('#filtroContatosEfetuadosDataContatoDe').val(start.format('YYYY-MM-DD') );
		$('#filtroContatosEfetuadosDataContatoAte').val(end.format('YYYY-MM-DD') );
	});
	
        $('input[name="filtroContatosEfetuadosDataContato"]').on('apply.daterangepicker', function(ev, picker) {
             $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));	  
         });

         $('input[name="filtroContatosEfetuadosDataContato"]').on('cancel.daterangepicker', function(ev, picker) {
             $(this).val('');
         });

        $("#filtroContatosEfetuadosDataContato").change(function(){
               if($(this).val() == ''){
                       $('#filtroContatosEfetuadosDataContatoDe').val('');
                       $('#filtroContatosEfetuadosDataContatoAte').val('');
               }
        });  
    });
   

  
</script>
