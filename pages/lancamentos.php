<?php
	session_start();
	include '../functions/conexao.php';
	require '../functions/crud.php';
	header('Content-Type: text/html; charset=utf-8');
?>

<script type="text/javascript" src="../js/custom.js"></script>
<script type="text/javascript" src="../bower_components/bootstrap-daterangepicker-master/moment.js"></script>
<script type="text/javascript" src="../bower_components/bootstrap-daterangepicker-master/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../bower_components/bootstrap-daterangepicker-master/daterangepicker.css" />

<script>


	function imprimirStatusProjetos(){
		//window.open('report-projects-print.php','_blank');
		return false;
	};
	filtraEtapasComboResultados($( '#filtroEquipeCampeonatoResultado option:selected' ).val())

	function filtraEtapasComboResultados(idCampeonato){
		$('#filtroEquipeEtapaResultado').html('');
		$.post('../ajax/controller.php',{
			acao:"listaEtapas",
			idCampeonato:idCampeonato
		},function(retorno){
			$('#filtroEquipeEtapaResultado').append(retorno);
			pegaDataEtapa($( '#filtroEquipeEtapaResultado option:selected' ).val());

		});
	}

	function pegaDataEtapa(idEtapa){
		$.post('../ajax/controller.php',{
			acao:"pegaDataEtapa",
			idEtapa:idEtapa
		},function(retorno){
			$('#dataNews2Cad2').val();
			$('#dataNewsCad2').val(retorno);
			formata(retorno);
			pegaEquipesEtapa(idEtapa);
		});
	}

	function pegaEquipesEtapa(idEtapa){
		$('#filtroEquipeEtapaLancamentos').val();
		$.post('../ajax/controller.php',{
			acao:"carregaEquipesLancamentosResultado",
			idEtapa:idEtapa
		},function(retorno){
			$('#filtroEquipeEtapaLancamentos').html(retorno);
		});
	}

	function filtraEtapasLancamentos(){
		idEtapa = $( '#filtroEquipeEtapaResultado option:selected' ).val();
		modalidade = $( '#filtroModalidadeLancamentos option:selected' ).val();
		isca = $( '#filtroIscaLancamentos option:selected' ).val();
		equipe = $( '#filtroEquipeEtapaLancamentos option:selected' ).val();

		checarhorario = $('#dataNews2Cad2').val();
		if(checarhorario != ''){
			horario = $('#dataNewsCad2').val();
		}else{
			horario = '';
		}


		$('#tableLancamentosEtapa').DataTable({
			"ajax": {
				url: "../ajax/controller.php",
				type: "POST",
				data :{
					acao: "resultadosLancamentosEtapa",
					idEtapa:idEtapa,
					horario:horario,
					modalidade:modalidade,
					isca:isca,
					equipe:equipe
				}
			},

			"columns": [
				{"data": "comprovante"},
				{"data": "competidor"},
				{"data": "equipe"},
				{"data": "nome_fiscal"},
				{"data": "especie"},
				{"data": "modalidade"},
				{"data": "isca"},
				{"data": "data_hora"},
				{"data": "peso"},
				{"data": "obs"}
			],

			"language": {
				"url": "../bower_components/datatables/Portuguese-Brasil.json"
			},
			"paging": true,
			"lengthChange": true,
			"searching": false,
			"destroy": true,
			"ordering": true,
			"order": [[ 6, "asc" ]],
			"info": true,
			"autoWidth": false,
			"fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
				$('td', nRow).closest('tr').css('background', aData.excluido == 'SIM' ? '#ffb3b3' : '#fff');
				return nRow;
			}
		} );
	}

	formata($('#dataNews2Cad2').val());

	function formata($data){
		$('.form_datetime2').datetimepicker({
			weekStart: 0,
			startDate: $data + ' 00:00:00',
			initialDate: $data,
			todayBtn:  false,
			autoclose: 1,
			format: 'hh:ii',
			minuteStep: 5,
			todayHighlight: false,
			startView: 1,
			minView: 0,
			maxView: 1,
			forceParse: 0,
			language: 'pt-BR',
			daysOfWeekDisabled: [],
			showMeridian: false
		});
		$('.form_datetime2').datetimepicker('update');
		$('.form_datetime2').datetimepicker('setStartDate', $data + ' 00:00:00');
		$('.form_datetime2').datetimepicker('setEndDate', $data + ' 23:59:59');
	};

</script>

<style>

.barraBotoes {
  float: right;
  margin-right:5px;
  font-size: 21px;
  font-weight: bold;
  line-height: 1;
  color: #000;
  text-shadow: 0 1px 0 #fff;
  filter: alpha(opacity=20);
  opacity: .2;
  -webkit-appearance: none;
  padding: 0;
  cursor: pointer;
  background: transparent;
  border: 0;

</style>

<div id="propria">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Lançamentos por Etapa <button onClick="imprimiLancamentosEtapas($('#filtroEquipeEtapaResultado option:selected').val(),$('#dataNewsCad2').val())" type="button" class="barraBotoes glyphicon glyphicon-print text-muted" ></button></h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<form name="form_filtraResultados" id="form_filtraResultados" method="post" >
				<div class="well form-group">
					<h4>Filtros</h4>
					<div class="row">
						<div class="col-lg-3">
							<label>Campeonato</label>
							<select id="filtroEquipeCampeonatoResultado" name="filtroEquipeCampeonatoResultado" class="form-control" onchange="filtraEtapasComboResultados($( '#filtroEquipeCampeonatoResultado option:selected' ).val())">
								<?php
									$campeonatos = retornaCampeonatos('','');
									foreach ($campeonatos as $result){
										echo '<option value="'.$result->id.'">'.$result->descricao.'</option>';
									}
								 ?>
							</select>
						</div>

						<div class="col-lg-3">
							<label>Etapa</label>
							<select id="filtroEquipeEtapaResultado" name="filtroEquipeEtapaResultado" class="form-control" onchange="pegaDataEtapa($( '#filtroEquipeEtapaResultado option:selected' ).val())">
								<?php
									$etapas = retornaEtapas('', 0);
									foreach ($etapas as $result){
										echo '<option value="'.$result->id.'">'.$result->DESCRICAO.'</option>';
									}
								 ?>
							</select>
						</div>

						<div class="col-lg-2">
							<label for="dataNewsCad2">Simular Horário</label>
							<div class="input-group date time form_datetime2" data-link-field="dataNewsCad2">
								<input type="text" name="dataNews2Cad2" id="dataNews2Cad2" class="form-control" type="text" readonly value="">
								<span class="input-group-addon"><span class="glyphicon glyphicon-remove" ></span></span>
								<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
							</div>
							<input type="hidden" name="dataNewsCad2" id="dataNewsCad2" value="<?php echo date('Y-m-d H:i:s', time()); ?>"/>
						</div>

						<div class="col-lg-1 pull-right">
						<label></label>
							<div class="input-group">
								<button class="btn btn-primary" name="buttonFiltraLancamentos" id="buttonFiltraLancamentos" style="background-color:#005081" type="button" onclick="filtraEtapasLancamentos()"><b>Filtrar</b></button>
							</div>
						</div>
					</div>
					<br>
					<div class="row">
						<div class="col-lg-2">
							<label>Modalidade</label>
							<select id="filtroModalidadeLancamentos" name="filtroModalidadeLancamentos" class="form-control">
								<option value="0">Todos</option>;
								<?php
									$modalidades = retornaModalidades('', 0);
									foreach ($modalidades as $result){
										echo '<option value="'.$result->id.'">'.$result->descricao.'</option>';
									}
								 ?>
							</select>
						</div>

						<div class="col-lg-2">
							<label>Isca</label>
							<select id="filtroIscaLancamentos" name="filtroIscaLancamentos" class="form-control">
								<option value="0">Todos</option>;
								<?php
									$iscas = retornaIscas('');
									foreach ($iscas as $result){
										echo '<option value="'.$result->id.'">'.$result->descricao.'</option>';
									}
								 ?>
							</select>
						</div>

						<div class="col-lg-2">
							<label>Equipes</label>
							<select id="filtroEquipeEtapaLancamentos" name="filtroEquipeEtapaLancamentos" class="form-control">

							</select>
						</div>
					</div>

				</div>
			</form>

			<table id="tableLancamentosEtapa" class="table table-bordered table-hover table-striped">
				<thead>
					<tr>
						<th>Comprovante</th>
						<th>Competidor</th>
						<th>Equipe</th>
						<th>Fiscal</th>
						<th>Espécie</th>
						<th>Modalidade</th>
						<th>Isca</th>
						<th>Data/Hora</th>
						<th>Peso</th>
						<th>Obs</th>
					</tr>
				</thead>
				<tbody id="resultadoLancamentosPorEtapa">

				</tbody>
			</table>
		</div>
	</div>
</div>
