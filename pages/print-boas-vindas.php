<?php
    session_start();
    header('Content-Type: text/html; charset=utf-8');
    include("../imprimir/mpdf.php");
    include '../functions/conexao.php';
    require '../functions/crud.php';
    $idEtapa = $_REQUEST['idEtapa'];
    $etapa = retornaEtapa($idEtapa);


    $mpdf=new mPDF('utf-8','A4','','','0','0','0','30');
    /*
    When 15=margin-left, 15=margin-right, 28=margin-top, 18=margin-bottom*/

    $header = '';
    $footer = '<img src="../img/cbp2019/footer.png" width="100%" />';

    $mpdf->SetHTMLHeader($header);
    $mpdf->SetHTMLFooter($footer);
    $mpdf->SetDisplayMode('fullpage');

    // LOAD a stylesheet
    $stylesheet = file_get_contents('../imprimir/examples/mpdfstyletables.css');
    $mpdf->WriteHTML($stylesheet,1);    // The parameter 1 tells that this is css/style only and no body/html/text

    $raias = '';
    if ($competidores = retornaCompetidoresAgrupadosEtapa($idEtapa)){
        foreach($competidores as $competidor){

            if($etapa -> QTD_BATERIAS != null){
                $qtdBaterias = $etapa -> QTD_BATERIAS;
            }else{
                $qtdBaterias = 0;
            }

            if($competidor->RAIA_INICIAL == 0){
                $raia = '';
            }else{
                $raia = $competidor->RAIA_INICIAL;
            }
            $raia1 = $raia;

            /*funcao para definicao de raias*/
            for ($i = 1; $i <= $qtdBaterias-1; $i++) {

                $tipoOrgRaia = $etapa -> ID_CAD_TIPO_ORG_RAIA;

                if($tipoOrgRaia == 1){
                    if ($raia >= 1 && $raia <= 15) {
                        $raia = intval($raia) + 15;
                    }else{
                        if ($raia >= 16 && $raia <= 23) {
                            $raia = intval($raia) + 37;
                        }else{
                            if ($raia >= 24 && $raia <= 30) {
                                $raia = intval($raia) + 22;
                            }else{
                                if ($raia >= 31 && $raia <= 37) {
                                    $raia = intval($raia) - 22;
                                }else{
                                    if ($raia >= 38 && $raia <= 45) {
                                        $raia = intval($raia) - 37;
                                    }else{
                                        if ($raia >= 46 && $raia <= 60) {
                                            $raia = intval($raia) - 15;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                /*ORGANIZACAO DE RAIAS TIPO 2*/
                if($tipoOrgRaia == 2){
                    if ($raia >= 1 && $raia <= 15) {
                        $raia = intval($raia) + 15;
                    }else{
                        if ($raia >= 16 && $raia <= 30) {
                            $raia = intval($raia) + 15;
                        }else{
                            if ($raia >= 31 && $raia <= 45) {
                                $raia = intval($raia) + 15;
                            }else{
                                if ($raia >= 46 && $raia <= 60) {
                                    $raia = intval($raia) - 45;
                                }
                            }
                        }
                    }
                }

                /*ORGANIZACAO DE RAIAS TIPO 3*/
                if($tipoOrgRaia == 3){
                    //segunda bateria
                    if(i==0){
                        if ($raia >= 1 && $raia <= 32) {
                            $raia = intval($raia) + 10;
                        }else{
                            if ($raia >= 33 && $raia <= 42) {
                                $raia = intval($raia) - 32;
                            }
                        }
                    }else{
                        //terceira bateria
                        if(i==1){
                            if ($raia >= 1 && $raia <= 31) {
                                $raia = intval($raia) + 11;
                            }else{
                                if ($raia >= 32 && $raia <= 42) {
                                    $raia = intval($raia) - 31;
                                }
                            }
                        }else{
                            //quarta bateria
                            if(i==2){
                                if ($raia >= 1 && $raia <= 32) {
                                    $raia = intval($raia) + 10;
                                }else{
                                    if ($raia >= 33 && $raia <= 42) {
                                        $raia = intval(raia) - 32;
                                    }
                                }
                            }
                        }
                    }
                }

                if($i == 1){
                    $raia2 = $raia;
                }
                if($i == 2){
                    $raia3 = $raia;
                }
                if($i == 3){
                    $raia4 = $raia;
                }

            }

             $raias = '<img src="../img/cbp2019/header.png" width="100%" />
                        <table style="width:100%;">
                            <tr>
                                <td align="center">
                                    <p width="80%"><font size="6">
                                    <br><br>Olá, <b>'.($competidor->NOMES_COMPETIDORES).'</b>!
                                    <br><br>Bem-vindos ao Campeonato Brasileiro em Pesqueiros!
                                    <br><br>Neste Kit, vocês vão encontrar 02 camisetas, 02 bonés , 02 pulseiras de identificação e 01 regulamento.
                                    <br><br>As raias que a sua dupla irá ficar são:<br><br>

                                    <table style="width:40%;">
                                        <tr>
                                            <td align="center">
                                                <font size="6">Bateria 1</font>
                                            </td>
                                            <td align="center">
                                                <font size="6">9h30 às 11h</font>
                                            </td>
                                            <td align="center">
                                                <font size="6">Raia '.$raia1.'</font>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <font size="6">Bateria 2</font>
                                            </td>
                                            <td align="center">
                                                <font size="6">11h30 às 13h</font>
                                            </td>
                                            <td align="center">
                                                <font size="6">Raia '.$raia2.'</font>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <font size="6">Bateria 3</font>
                                            </td>
                                            <td align="center">
                                                <font size="6">14h às 15h30</font>
                                            </td>
                                            <td align="center">
                                                <font size="6">Raia '.$raia3.'</font>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <font size="6">Bateria 4</font>
                                            </td>
                                            <td align="center">
                                                <font size="6">16h às 17h30</font>
                                            </td>
                                            <td align="center">
                                                <font size="6">Raia '.$raia4.'</font>
                                            </td>
                                        </tr>
                                    </table>
                                    <br><br>
                                    Em caso de dúvidas, vocês podem procurar a organização para esclarecimentos.<br><br>
                                    Boa prova! Esperamos encontrá-los novamente na final!</font></p>
                                </td>
                            </tr>
                        </table>
                        ';

            $html = $raias;

            $mpdf->AddPage();
            $mpdf->WriteHTML($html,2);
        }
    }



/*$qtdBaterias = $etapa -> QTD_BATERIAS;
$headerBaterias = '';
for ($i = 1; $i <= $qtdBaterias; $i++) {
    $headerBaterias .= '<th scope="col" align="right">Raia Bat.'.$i.'</th>';
}*/

/*$html = '
    <table width="100%" class="bpmTopic">
        <thead>
            <tr>
                <th width="20%" align="left">Equipe</th>
                <th width="25%" align="left">Apelidos</th>
                <th width="10%" align="right">Pulseiras</th>
                '.$headerBaterias.'

            </tr>
        </thead>
        <tbody id="resultadoListarCompetidoresImpressao">

            '.$raias.'

        </tbody>
    </table>
';*/

/*$mpdf->WriteHTML($html,2);

$mpdf->AddPage();

$mpdf->WriteHTML($html,2);
*/
$mpdf->Output('mpdf.pdf','I');
exit;
//==============================================================
//==============================================================
//==============================================================


?>
