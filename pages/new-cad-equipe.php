<?php
	include '../functions/conexao.php';
	require '../functions/crud.php';
?>	

<script>
	function filtraEtapasComboNewCad(idCampeonato){
		$('#newCadEtapaEquipe').html('');
		$.post('../ajax/controller.php',{
			acao:"listaEtapas",
			idCampeonato:idCampeonato
		},function(retorno){
			$('#newCadEtapaEquipe').append(retorno);			
		});
	}
	
	filtraEtapasComboNewCad($( '#newCadCampeonatoEquipe option:selected' ).val());
</script>

	<script type="text/javascript" src="../js/custom.js"></script>
	<div class="panel-body">
		<form role='form' method='post' action='' name='form_newCadEquipe'>
			<fieldset>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label>Nome Equipe</label>
							<input id="newCadNomeEquipe" name="newCadNomeEquipe" class="form-control upcase" placeholder="Nome Equipe" required>
						</div>
						<div class="form-group">
							<label>Campeonato</label>
							<select name="newCadCampeonatoEquipe" id="newCadCampeonatoEquipe" class="form-control" onchange="filtraEtapasComboNewCad($( '#newCadCampeonatoEquipe option:selected' ).val())">
								<?php
									$campeonatos = retornaCampeonatos('', 'A');
									foreach ($campeonatos as $result){
										echo '<option value="'.$result->id.'">'.$result->descricao.'</option>';
									}	
								 ?>
							</select>
						</div>
						
						<div class="form-group">
							<label>Etapa</label>
							<select id="newCadEtapaEquipe" name="newCadEtapaEquipe" class="form-control">
								<option value="-2">Selecione um campeonato</option>								
							</select>
						</div>	
						
					</div>
				</div>
			</fieldset>	
			<div class="span6 pull-right">
				<button id="botaoInsereEquipe" name="botaoInsereEquipe" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
			</div>
		</form>
	</div>	