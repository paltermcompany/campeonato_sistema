<?php
    session_start();
    include '../functions/conexao.php';
    require '../functions/crud.php';
    header('Content-Type: text/html; charset=utf-8');

?>

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Composição Raias</h1>
        </div>
    </div>

<div class="panel panel-default col-lg-12">
    <div class="row"><br>
        <div class="col-lg-3" id="listaCampeonatosEtapas">
            <?php
            if ($etapas = retornaCampeonatos('', 'A')){

                $tabela =       '<table class="table table-striped table-hover" id="tableListaEtapas" name="tableListaEtapas">'.
                                    '<th>Selecione o Campeonato</th>'.
                                    '<tbody>';

                foreach ($etapas as $result){
                    $tabela=$tabela .   '<tr class="odd gradeX">'.
                                            '<td class="col-md-10" style="cursor:pointer" onclick="carregaEtapas('.$result -> id .');return false;">'. $result -> descricao . '</td>'.
                                        '</tr>';
                }
                $tabela= $tabela.   '</tbody>'.
                                '</table>';
                echo $tabela;


            }else{
                echo 'Nenhum campeonato encontrado';
            }
            ?>
        </div>
        <div class="col-lg-9" name="infoListaEtapas" id="infoListaEtapas">

        </div>
    </div>
</div>

<script>

    function carregaEtapas(idCampeonato){
        $('#listaCampeonatosEtapas').html('');
        $.post('../ajax/controller.php',{
            acao:"listaRaiasParaComposicao",
            idCampeonato:idCampeonato
        },function(retorno){
            $('#listaCampeonatosEtapas').html(retorno);
        });
    }

    function carregaCampeonatos(){
        $('#listaCampeonatosEtapas').html('');
        $.post('../ajax/controller.php',{
            acao:"listaCampeonatosParaComposicao"
        },function(retorno){
            $('#listaCampeonatosEtapas').html(retorno);
        });
    }

</script>
