<?php
    session_start();
    header('Content-Type: text/html; charset=utf-8');
    include '../functions/conexao.php';
    require '../functions/crud.php';
?>
<html>
    <head>
        <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../bower_components/font-awesome/css/all.css" rel="stylesheet">
        <link href="../dist/css/AdminLTE.css" rel="stylesheet">
        <link rel="stylesheet" href="../bower_components/jquery-ui/jquery-ui.css">
    </head>
<body>
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/custom.js"></script>
<script>

    var tableResultadosRankingFinal;

	$( document ).ready(function() {

        idEtapa = $( '#idEtapaAcompanhamentoLive').val();
        $.post('../ajax/controller.php',{
            acao:"unidadeMedidaEtapa",
            idEtapa:idEtapa
        },function(retorno){
            retorno = JSON.parse(retorno);
            $.each( retorno, function( key, value ) {
                document.cookie = "cookieDescricaoUnidadeMedida="+value.descricao;
                document.cookie = "cookieAbreviacaoUnidadeMedida="+value.abreviacao;
            });
        });

        tableResultadosRankingFinal = $('#tableResultadosRankingFinal').DataTable( {
            "ajax": {
                    url: "../ajax/controller.php",
                    type: "POST",
                    data : function(d){
                        d.acao = "resultadosEtapaRankingSemEntregaTrofeu",
                        d.rankingCompleto = false,
                        d.idEtapa = idEtapa,
                        d.horario = "",
                        d.mostraNome = "apelido";
                    }
                },
            "drawCallback": function(settings) {
               $('[data-toggle="popover"]').popover();
            },
            "columns": [
                {"data" : null, defaultContent: ""},
                {"data": "colocacao"},
                {"data": "peso"},
                {"data": "quantidade"},
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
            ],
            "columnDefs": [ {
                "targets": 4,
                "data": "download_link",
                "render": function ( data, type, row, meta ) {
                    return "<td align='left' width='80%'><a data-toggle='modal' data-target='#myModal' data-id_equipe='"+data.id_equipe+"' data-html='true' class=''>"+data.equipe+"</a></td>";

                }
            } ],
            "language": {
                "url": "../bower_components/datatables/Portuguese-Brasil.json"
            },
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "destroy": true,
            "ordering": false,
            "info": false,
            "autoWidth": false
        } );


        filtraEtapasResultadoLive();
        filtraDetalhes();
        g_iCount = 30;
        startCountdown();



	});

	function startCountdown(){
        if((g_iCount - 1) >= 0){
            g_iCount = g_iCount - 1;

            $('#numberCountdown').html(' atualiza em: ' + g_iCount);
            setTimeout('startCountdown()',1000);
            if(g_iCount==0){
                checkPrevias();
                console.log('checkando prévias');
            }
        }
	}

	$primeiraVez = true;
	var g_iCount = new Number();

	function filtraEtapasResultadoLive(){
        idEtapa = $('#idEtapaAcompanhamentoLive').val();

        if(idEtapa!=''){
            $.post('../ajax/controller.php',{
                acao:"acompanhamentoLiveNarradores",
                idEtapa:idEtapa,
                ultimaVerificacao: $('#ultimaVerificacao').val(),
                idUltimaVerificacao: $('#idUltimaVerificacao').val()
            },function(retorno){
                if(retorno!=''){
                    $( ".linhaLive" ).removeClass( 'invalid' );
                    $( ".linhaLive" ).removeClass( 'excluido' );
                }
                $('#acompanhamentoLive').prepend(retorno);
                if($primeiraVez){
                    console.log('primeira vez');
                    checkPrevias();
                    $primeiraVez = false;
                }
            });
        }
	}

	function filtraDetalhes(){
        idEtapa = $('#idEtapaAcompanhamentoLive').val();
        if(idEtapa!=''){

            $.post('../ajax/controller.php',{
                acao:"retornaDetalhesModalidade",
                idEtapa:idEtapa
            },function(retorno){
                var jsonfile = JSON.parse(retorno);
                var labels = jsonfile.data.map(function(e) {
                   return e.modalidade;
                });
                var data = jsonfile.data.map(function(e) {
                   return e.quantidade;
                });

                var ctx = document.getElementById("chartModalidades");
                var chartModalidades = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: labels,
                        datasets: [{
                            label: '# de Modalidades',
                            data: data,
                            backgroundColor: palette('tol', 7).map(function(hex) {
                                return '#' + hex;
                            }),
                            borderWidth: 1
                        }]
                    },
                    options: {
                        cutoutPercentage: 40,
                        responsive: false,
                        title: {
                            display: true,
                            text: 'Modalidades',
                            fontSize: 20
                        },
                        legend: {
                            display: false,
                        }
                    }
                });
            });


            $.post('../ajax/controller.php',{
                acao:"retornaDetalhesEspecie",
                idEtapa:idEtapa
            },function(retorno){
                var jsonfile = JSON.parse(retorno);
                var labels = jsonfile.data.map(function(e) {
                   return e.especie;
                });
                var data = jsonfile.data.map(function(e) {
                   return e.quantidade;
                });

                var ctx = document.getElementById("chartEspecies");
                var chartEspecies = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: labels,
                        datasets: [{
                            label: '# de Especies',
                            data: data,
                            backgroundColor: palette('tol', 7).map(function(hex) {
                                return '#' + hex;
                            }),
                            borderWidth: 1
                        }]
                    },
                    options: {
                        cutoutPercentage: 40,
                        responsive: false,
                        title: {
                            display: true,
                            text: 'Especies',
                            fontSize: 20
                        },
                        legend: {
                            display: false,
                        }
                    }
                });
            });


            $.post('../ajax/controller.php',{
                acao:"retornaDetalhesIsca",
                idEtapa:idEtapa
            },function(retorno){
                var jsonfile = JSON.parse(retorno);
                var labels = jsonfile.data.map(function(e) {
                   return e.isca;
                });
                var data = jsonfile.data.map(function(e) {
                   return e.quantidade;
                });

                var ctx = document.getElementById("chartIscas");
                var chartIscas = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: labels,
                        datasets: [{
                            label: '# de Iscas',
                            data: data,
                            backgroundColor: palette('tol', 7).map(function(hex) {
                                return '#' + hex;
                            }),
                            borderWidth: 1
                        }]
                    },
                    options: {
                        cutoutPercentage: 40,
                        responsive: false,
                        title: {
                            display: true,
                            text: 'Iscas',
                            fontSize: 20
                        },
                        legend: {
                            display: false,
                        }
                    }
                });
            });

            $.post('../ajax/controller.php',{
                acao:"retornaDetalhesHora",
                idEtapa:idEtapa
            },function(retorno){
                var jsonfile = JSON.parse(retorno);
                var labels = jsonfile.data.map(function(e) {
                   return e.hora;
                });
                var data = jsonfile.data.map(function(e) {
                   return e.quantidade;
                });

                var ctx = document.getElementById("chartHorarios");
                var chartHorarios = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: labels,
                        datasets: [{
                            label: '# de Horarios',
                            data: data,
                            backgroundColor: palette('tol', 7).map(function(hex) {
                                return '#' + hex;
                            }),
                            borderWidth: 1
                        }]
                    },
                    options: {
                        cutoutPercentage: 40,
                        responsive: false,
                        title: {
                            display: true,
                            text: 'Horarios',
                            fontSize: 20
                        },
                        legend: {
                            display: false,
                        }
                    }
                });
            });


            $.post('../ajax/controller.php',{
                acao:"retornaDetalhesEstatisticasEquipes",
                idEtapa : idEtapa
            },function(retorno){
                $.each(JSON.parse(retorno), function(idx, obj) {
                    $i = -1;
                    $total = 0;
                    $comEquipes = 0;
                    $semEquipes = 0;
                    $.each(this, function(k, v) {
                        $i++;
                        if($i == 0){
                            $total = v.total;

                            $("#totalEquipes").text(v.total + ' ' + v.descricao) ;
                            $("#progressTotalEquipes").css("width", '100%');
                        }
                        if($i == 1){
                            $("#totalEquipesComPesagem").text(v.total + ' ' + v.descricao) ;
                            $comEquipes = (v.total * 100) / $total;
                            $("#totalEquipesComPesagem").css("width", $comEquipes + "%");
                        }
                        if($i == 2){
                            $("#totalEquipesSemPesagem").text(v.total + ' ' + v.descricao) ;
                            $semEquipes =  100-$comEquipes;
                            $("#totalEquipesSemPesagem").css("width", $semEquipes + "%");
                        }

                    });

                });
            });

            $.post('../ajax/controller.php',{
                acao:"resultadosEtapa",
                idEtapa : idEtapa,
                horario : ""
            },function(retorno){
                $.each(JSON.parse(retorno), function(idx, obj) {
                    $("#totalPesagensEtapa").text(obj[0].total_peso) ;
                    $("#totalQuantidadeEtapa").text(obj[0].total_quantidade) ;
                });
            });

            $.post('../ajax/controller.php',{
                acao:"resultadosEtapaMaiorPeixeSemEntregaTrofeu",
                idEtapa : idEtapa,
                horario : "",
                mostraNome : "apelido"
            },function(retorno){
                $.each(JSON.parse(retorno), function(idx, obj) {
                    console.log(obj[0].id_equipe);
                    $("#maiorPeixeDescricao").text(obj[0].peso) ;
                    $("#maiorPeixeEquipe").html(obj[0].competidores);





                    /*$("#modalMP").data("info", $listaCompetidores + $listaCuriosidadesHistoria);
                    $("#modalMP").data("title", 'Informações da equipe <b>'+obj[0].nome_equipe+'</b>' );*/
                    $("#modalMP").data("id_equipe", obj[0].id_equipe);
                });
            });

        }



	}


	function filtraEtapasResultadoDashboard(){
        tableResultadosRankingFinal.ajax.reload();
        $('[data-toggle="popover"]').popover();
	}

	var refreshId = setInterval(function(){ checkAcompanhamentoLive(); }, 5000);

	function checkAcompanhamentoLive() {
        filtraEtapasResultadoLive();

	}

	function checkPrevias() {
        filtraEtapasResultadoDashboard();
        filtraDetalhes();
        //alert('30');
        g_iCount = 30;
	}






</script>


<style>
@-webkit-keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
@-moz-keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
@-o-keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
@keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
.invalid {
  -webkit-animation: invalid 1s infinite;
  -moz-animation:    invalid 1s infinite;
  -o-animation:      invalid 1s infinite;
  animation:         invalid 1s infinite;
}

@-webkit-keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
@-moz-keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
@-o-keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
@keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
.excluido {
    -webkit-animation: excluido 1s infinite;
    -moz-animation:    excluido 1s infinite;
    -o-animation:      excluido 1s infinite;
    animation:         excluido 1s infinite;
}

td {
    padding: 1em;
}

.table-condensed{
    font-size: 14px;
}



.barraBotoes {
    float: right;
    margin-right:5px;
    font-size: 21px;
    font-weight: bold;
    line-height: 1;
    color: #000;
    text-shadow: 0 1px 0 #fff;
    filter: alpha(opacity=20);
    opacity: .2;
    -webkit-appearance: none;
    padding: 0;
    cursor: pointer;
    background: transparent;
    border: 0;
}

.progress-sem-margem{
    margin-bottom: 0px;
}



</style>

<script>
    $( document ).ready(function() {
        $('#myModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var title = button.data('title') // Extract info from data-* attributes
            var info = button.data('info')
            var idEquipe = button.data('id_equipe')

            $.post('../ajax/controller.php',{
                acao:"retornaDadosEquipe",
                idEquipe:idEquipe
            },function(retorno){

                retorno = JSON.parse(retorno);
                //console.log(retorno);
                $listaCompetidores = '';
                $listaCuriosidadesHistoria = '';
                $i = 1;
                $.each( retorno, function( key, value ) {
                    $.each( value, function( key2, v ) {
                        if($i == 1){
                            $listaCuriosidadesHistoria += '<b>Curiosidades:</b><br> ' + v.curiosidades;
                            $listaCuriosidadesHistoria += '<br><br>';
                            $listaCuriosidadesHistoria += '<b>Historia:</b><br> ' + v.historia;
                        }

                        $listaCompetidores += '<b>Competidor ' + $i++ + ': </b><br>' + v.apelido;
                        $listaCompetidores += '<br>';
                        $listaCompetidores += '<b>Nome completo:</b><br>' + v.nome;
                        $listaCompetidores += '<br>';
                        $listaCompetidores += '<b>Localização:</b><br>' + v.cidade + ' - ' + v.uf;
                        $listaCompetidores += '<br>';
                        $listaCompetidores += '<b>Aniversario:</b><br>' + v.data_nascimento + ' (' + v.idade + ')';
                        $listaCompetidores += '<br><br>';

                    });

                    modal.find('.modal-body').html($listaCompetidores + $listaCuriosidadesHistoria);
                    modal.find('.modal-title').html('Dados da Equipe:')

                });
            });

          // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
          // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
          var modal = $(this)
          modal.find('.modal-title').html('Carregando...')
          modal.find('.modal-body').html('<center><i class="fas fa-5x fa-sync fa-spin"></i></center>')
        })

    });
</script>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">TITLE</h4>
            </div>
            <div class="modal-body">
                INFO
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12">

	<div id="live">
        <div class="row">
            <div class="col-lg-12">
                <?php
                    if($etapa = retornaEtapaHoje(null)){
                        echo '<h1 class="main-header">'.$etapa->DESCRICAO.'<small id="etapa" align=center> AO VIVO</small> <i class="pull-right"><h5 id="numberCountdown" align=right class="barraBotoes"></h5></i></h1>';
                        echo '<input type="hidden" name="idEtapaAcompanhamentoLive" id="idEtapaAcompanhamentoLive" value="'.$etapa->ID.'">';
                    }else{
                        echo '<h1 class="main-header">Nenhuma etapa sendo realizada hoje<small id="etapa" align=center></small></h1>';
                        echo '<input type="hidden" name="idEtapaAcompanhamentoLive" id="idEtapaAcompanhamentoLive" value="">';
                    }
                ?>
            </div>
        </div>

        <div  class="row">
            <div class="col-lg-6">

                <!--<div class="well">
                    <div class="row">
                        <div class="col-lg-12" >
                            <canvas id="canvas" height="23px" width='100%'></canvas>
                        </div>
                    </div>
                </div>-->
                <input type="hidden" name="ultimaVerificacao" id="ultimaVerificacao" value="">
                <input type="hidden" name="idUltimaVerificacao" id="idUltimaVerificacao" value="">
                <div class="well form-group">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive" id="tableExportaClientes2">
                                <table class="table table-bordered table-hover table-condensed" >
                                    <thead>
                                        <tr>
                                            <th>Hora</th>
                                            <th>Equipe</th>
                                            <th>Competidor</th>
                                            <th>Espécie</th>
                                            <!--<th>Modalidade</th>-->
                                            <th>Isca</th>
                                            <th><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?></th>
                                            <th>Obs</th>
                                        </tr>
                                    </thead>
                                    <tbody id="acompanhamentoLive" >
                                        <!--RESULTADO GERAL-->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="well">
                    <div class="row">
                        <div class="col-lg-3" >
                            <canvas id="chartModalidades" height="200" width="200"></canvas>
                        </div>
                        <div class="col-lg-3">
                            <canvas id="chartEspecies" height="200" width="200"></canvas>
                        </div>
                        <div class="col-lg-3">
                            <canvas id="chartIscas" height="200" width="200"></canvas>
                        </div>
                        <div class="col-lg-3">
                            <canvas id="chartHorarios" height="200" width="200"></canvas>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-xs-4">
                    <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3 id="totalQuantidadeEtapa">calculando...</h3>

                                <p>Quantidade Total</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-calculator"></i>
                            </div>
                            <a href="#" class="small-box-footer">Detalhes <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                    <!-- ./col -->
                    <div class="col-lg-4 col-xs-4">
                    <!-- small box -->
                        <div class="small-box bg-orange">
                            <div class="inner">
                                <h3 id="totalPesagensEtapa">calculando...</h3>
                                <p><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?> Total</p>
                            </div>
                            <div class="icon">
                                <i class="fa fa-balance-scale"></i>
                            </div>
                            <a href="#" class="small-box-footer">Detalhes <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>


                    <div class="col-lg-4 col-xs-4">
                      <!-- small box -->
                        <div class="small-box bg-green ">
                            <div class=" inner">
                                <h3 id="maiorPeixeDescricao">calculando...</h3>
                                <p id="maiorPeixeEquipe">&nbsp;</p>
                            </div>

                            <div class="icon">
                                <i class="fa fa-fish"></i>MP
                            </div>
                            <a href="#" id="modalMP" class="small-box-footer" data-toggle='modal' data-target='#myModal'  data-html='true' >Detalhes <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                </div>

                <h3><b><i class="fa fa-users"></i> Total de equipes</b></h3>
                <div class="progress progress-sem-margem">
                    <div class="progress-bar progress-bar-success" role="progressbar" style="width:0%" id="progressTotalEquipes">
                        <b id="totalEquipes">0 equipes</b>
                    </div>
                </div>


                <div class="progress">
                    <div class="progress-bar progress-bar-warning" role="progressbar" style="width:0%" id="totalEquipesComPesagem">
                        0 equipes com pesagem
                    </div>

                    <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" style="width:0%" id="totalEquipesSemPesagem">
                        0 equipes sem pesagem
                    </div>
                </div>



                <table id= "tableResultadosRankingFinal" class="table table-striped table-bordered table-hover table-condensed" >
                    <thead>
                        <tr>
                            <th colspan="5"><h3><b><i class="fa fa-medal"></i> Ranking</b></h3></th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>Colocação</th>
                            <th><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?></th>
                            <th>Quantidade</th>
                            <th>Equipe/Competidores</th>
                        </tr>
                    </thead>
                    <tbody id="resultadoPreviaRankingColocacoes">
                        <!--RESULTADO RANKING-->
                    </tbody>
                </table>
            </div>
        </div>
	</div>

</div>

<script src="../bower_components/chartjs/Chart.min.js"></script>
<script src="../bower_components/palette/palette.js"></script>
<script src="../bower_components/jquery-ui/jquery-ui.js"></script>
<script src="../bower_components/datatables/jquery.dataTables.min.js"></script>
<script src="../bower_components/datatables/dataTables.bootstrap.min.js"></script>
</body>

</html>
