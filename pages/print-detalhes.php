<?php
	session_start();
	
	header('Content-Type: text/html; charset=utf-8');
	include '../functions/conexao.php';
	require '../functions/crud.php';
	$idEtapa = $_REQUEST['idEtapa'];

	$modalidades = '';
	if ($resultados = retornaDetalhesEtapaModalidade($idEtapa)){
		foreach($resultados as $resultado){
			$modalidades .= '<tr><td style="white-space: nowrap" align="left">'.$resultado->modalidade.'</td>';
			$modalidades .= '<td style="white-space: nowrap" align="right">'.$resultado->total.'</td></tr>';
		}
	}else{
		$modalidades .= '<td style="white-space: nowrap" colspan="2">Sem Resultados</td>';
	}
	
	$especies = '';
	if ($resultados = retornaDetalhesEtapaEspecie($idEtapa)){
		foreach($resultados as $resultado){
			$especies .= '<tr><td style="white-space: nowrap" align="left">'.$resultado->especie.'</td>';
			$especies .= '<td style="white-space: nowrap" align="right">'.$resultado->total.'</td></tr>';
		}
	}else{
		$especies .= '<td style="white-space: nowrap" colspan="2">Sem Resultados</td>';
	}
	
	$iscas = '';
	if ($resultados = retornaDetalhesEtapaIsca($idEtapa)){
		foreach($resultados as $resultado){
			$iscas .= '<tr><td style="white-space: nowrap" align="left">'.$resultado->isca.'</td>';
			$iscas .= '<td style="white-space: nowrap" align="right">'.$resultado->total.'</td></tr>';
		}
	}else{
		$iscas .= '<td style="white-space: nowrap" colspan="2">Sem Resultados</td>';
	}
	
	$horarios = '';
	if ($resultados = retornaDetalhesEtapaHora($idEtapa)){
		foreach($resultados as $resultado){
			$horarios .= '<tr><td style="white-space: nowrap" align="left">'.$resultado->hora.'</td>';
			$horarios .= '<td style="white-space: nowrap" align="right">'.$resultado->total.'</td></tr>';
		}
	}else{
		$horarios .= '<td style="white-space: nowrap" colspan="2">Sem Resultados</td>';
	}


$html = '
	
	<h4><b>Modalidades Utilizadas</b></h4>
	<table width="100%" class="bpmTopic">
		<thead>
			<tr>
				<th width="60%" align="left">Descrição</th>
				<th width="30%" align="right">Total</th>
			</tr>
		</thead>
		<tbody id="detalhesModalidades">
			<tr>
				'.$modalidades.'
			</tr>
		</tbody>
	</table>
	
	<h4><b>Especies Registradas</b></h4>
	<table width="100%" class="bpmTopic">
		<thead>
			<tr>
				<th width="60%" align="left">Descrição</th>
				<th width="30%" align="right">Total</th>
			</tr>
		</thead>
		<tbody id="detalhesEspecies">
			<tr>
				'.$especies.'
			</tr>
		</tbody>
	</table>
	
	<h4><b>Iscas Utilizadas</b></h4>
	<table width="100%" class="bpmTopic">
		<thead>
			<tr>
				<th width="60%" align="left">Descrição</th>
				<th width="30%" align="right">Total</th>
			</tr>
		</thead>
		<tbody id="detalhesIscas">
			<tr>
				'.$iscas.'
			</tr>
		</tbody>
	</table>
	
	<h4><b>Registros por Hora</b></h4>
	<table width="100%" class="bpmTopic">
		<thead>
			<tr>
				<th width="60%" align="left">Descrição</th>
				<th width="30%" align="right">Total</th>
			</tr>
		</thead>
		<tbody id="detalhesHorarios">
			<tr>
				'.$horarios.'
			</tr>
		</tbody>
	</table>

';

	$etapa = retornaEtapa($idEtapa);
	
	$nomeEtapa = $etapa->DESCRICAO;
	
	$campeonato = retornaCampeonato($etapa->ID_CAD_CAMPEONATO);
	
	$nomeCampeonato = $campeonato->descricao;
	

$header = '
	<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;">
		<tr>
			<td width="20%" align="left"><img src="../img/logo.png" width="126px" /></td>
			<td width="60%" align="center" ><h2>Detalhes da Prova</h2></td>
			<td width="20%" align="left"></td>
		</tr>
		<tr>
			<td colspan="3" width="100%" align="center" ><h4>'.($nomeCampeonato).' - '.($nomeEtapa).'</h4></td>
			
		</tr>
	</table>
';
$footer = '<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: justify; padding-top: 3mm; ">Este documento pode conter informação confidencial ou privilegiada, sendo seu sigilo protegido. Se você não for o destinatário ou a pessoa autorizada a receber este documento, não pode usar, copiar ou divulgar as informações nele contidos ou tomar qualquer ação baseada nessas informações. Se você recebeu este documento por engano, por favor, destrua-o imediatamente. Agradecemos sua cooperação.<br><div style="font-size: 9pt; text-align: center; padding-top: 3mm; ">Página {PAGENO} de {nb}</div></div>';
		    

//==============================================================
//==============================================================
//==============================================================
include("../imprimir/mpdf.php");

function tirarAcentos($string){
    return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
}

$mpdf=new mPDF('utf-8','A4-L','','','15','15','28','30'); 

$mpdf->SetHTMLHeader($header);


$mpdf->SetHTMLFooter($footer);

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents('../imprimir/examples/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,2);

$mpdf->Output('mpdf.pdf','I');
exit;
//==============================================================
//==============================================================
//==============================================================


?>