<?php
	session_start();
	include '../functions/conexao.php';
	require '../functions/crud.php';
	
	header('Content-Type: text/html; charset=utf-8');
	$idEtapa = $_REQUEST['idEtapa'];
?>

  <style>
  #listOrigem, #listDestino {
    border: 1px solid #eee;
    width:265px;
    min-height: 20px;
    list-style-type: none;
    margin: 0;
    padding: 5px 0 0 0;
    float: left;
    margin-right: 10px;
  }
  #listOrigem li, #listDestino li {
    margin: 0 5px 5px 5px;
    padding: 5px;
    font-size: 1.2em;
    width: 250px;
  }
  </style>
  
  <script>
	$(function() {
		$( "#listOrigem, #listDestino" ).sortable({
			connectWith: ".connectedSortable"
		}).disableSelection();
	});

	

	function SalvaComposicaoEtapa(idEquipe){
		alert('Contate o suporte');
		/*$.post('../ajax/controller.php',{
		acao:"excluiEquipeComposicao",
		idEquipe:idEquipe
		},function(retorno){
			if(retorno=='ok'){
				$("ul#listOrigem > li.ui-state-highlight").each(function() {
					var LI = $(this);
					//console.log(LI.val() + ' - ' + LI.text());
					//console.log("<?php echo $idEtapa . '-'. $idEquipe; ?>");
					$.post('../ajax/controller.php',{
					acao:"insertEquipeComposicao",
					idEquipe:idEquipe,
					idCompetidor:LI.val()
					},function(retorno){
						if(retorno=='ok'){
							//alert('Salvo com sucesso!');
						}else{
							alert('Erro, informe ao suporte!');
						}
					});					
					
					
				});
			}else{
				alert('Erro, informe ao suporte!');
			}
		});*/
	}
	
	
  </script>
  
<div class="panel-body">
	<div class="row">
		<div class="col-lg-6"> 
			<h4>Equipes nessa etapa:</h4>
			<ul id="listOrigem" class="connectedSortable">
				<?php retornaEtapasComposicaoAdicionadas($idEtapa) ?>
			</ul>
		</div>

		<div class="col-lg-6"> 	
			<h4>Equipes disponíveis:</h4>
			<ul id="listDestino" class="connectedSortable">
				<?php retornaEtapasComposicaoDisponiveis($idEtapa) ?>
			</ul>
		</div>
	</div>


	<div class="row">
		<div class="col-lg-12"> 
		
			<div class="span6 ">
				<button id="botaoAtualizaInteresses" name="botaoAtualizaInteresses" type='button' class='btn btn-success' onClick="SalvaComposicaoEtapa(<?php echo $idEtapa ?>)" style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar alterações</button>					
			</div>	
		</div>		
	</div>
</div>
 
