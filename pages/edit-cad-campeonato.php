<?php
	include '../functions/conexao.php';
	require '../functions/crud.php';
	header('Content-Type: text/html; charset=utf-8');
	$idCampeonato = $_REQUEST['idCampeonato'];
	$campeonato = retornaCampeonato($idCampeonato);
?>
	<script type="text/javascript" src="../js/custom.js"></script>
	<div class="panel-body">
		<form role='form' method='post' action='' name='form_editCadCampeonato'>
			<fieldset>
				<input type="hidden" name="editCadIdCampeonato" id="editCadIdCampeonato" value="<?php echo $campeonato -> id;?>">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label>Nome Campeonato</label>
							<input id="editCadNomeCampeonato" name="editCadNomeCampeonato" class="form-control upcase" placeholder="Nome Campeonato" required value="<?php echo $campeonato->descricao;?>">
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label>Nome Campeonato (Impressão App)</label>
							<input id="editCadNomeCampeonatoApp" name="editCadNomeCampeonatoApp" class="form-control upcase" placeholder="Nome Campeonato App" required value="<?php echo $campeonato->descricao_app;?>">
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label>Imagem</label><br>
							Selecione uma imagem: <input id="editFile" type="File" name="img">
							<div id="editaImagemCampeonado">
								<img src="data:image/png;base64, <?php echo $campeonato->imagem;?>" alt="Logo Campeonato" />
							</div>
							<input type="hidden" id="editCadImagemCampeonado" value="<?php echo $campeonato->imagem;?>" name="editCadImagemCampeonado">
						</div>
					</div>
				</div>
			</fieldset>
			<div class="span6 pull-right">
				<button id="botaoEditaCampeonato" name="botaoEditaCampeonato" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
			</div>
		</form>
	</div>
	<script>

		$('#editFile').change(function(){
			var files = document.getElementById('editFile').files;
			if (files.length > 0) {
				getBase64(files[0]);
			}
		});

		function getBase64(editFile) {
			var reader = new FileReader();
			var base64_str;
			reader.readAsDataURL(editFile);
			reader.onload = function () {
				base64_str = reader.result;
				base64_str = base64_str.replace('data:image/jpeg;base64,', "");
				$('#editaImagemCampeonado').html('<img src="data:image/jpeg;base64, '+base64_str+'" alt="Logo Campeonato" />');
				$('#editCadImagemCampeonado').val(base64_str);

			};
			reader.onerror = function (error) {
				console.log('Error: ', error);
			};
		}
	</script>
