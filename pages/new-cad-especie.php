	<script type="text/javascript" src="../js/custom.js"></script>
	<div class="panel-body">		
		<form role='form' method='post' action='' name='form_newCadEspecie'>
			<fieldset>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label>Nome Espécie</label>
							<input id="newCadNomeEspecie" name="newCadNomeEspecie" class="form-control upcase" placeholder="Nome Espécie" required>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label>Prioridade Espécie</label>
							<input id="newCadPrioridadeEspecie" name="newCadPrioridadeEspecie" class="form-control" placeholder="Prioridade Espécie" required type="number">
						</div>
					</div>
				</div>
			</fieldset>	
			<div class="span6 pull-right">
				<button id="botaoInsereEspecie" name="botaoInsereEspecie" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
			</div>
		</form>
	</div>	