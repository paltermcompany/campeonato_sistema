<?php
    session_start();
    include '../functions/conexao.php';
    require '../functions/crud.php';
    header('Content-Type: text/html; charset=utf-8');
?>

<style>

td.details-control {
    background: url('../img/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('../img/details_close.png') no-repeat center center;
}
</style>

<script type="text/javascript" src="../js/custom.js"></script>
<script type="text/javascript" src="../bower_components/bootstrap-daterangepicker-master/moment.js"></script>
<script type="text/javascript" src="../bower_components/bootstrap-daterangepicker-master/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../bower_components/bootstrap-daterangepicker-master/daterangepicker.css" />

<script>

    $(document).ready(function() {

        idCampeonato = 0;

        var tableResultadosGeral = $('#tableResultadosGeral').DataTable( {
            "ajax": {
                    url: "../ajax/controller.php",
                    type: "POST",
                    data : function(d){
                        d.acao = "resultadosCampeonatos",
                        d.idCampeonato = idCampeonato
                    },
                    beforeSend : function() {
                        $('#resultadoTotalEtapa').html('<tr><td colspan="6"><center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center></td><tr>');
                    },
                    complete: function() {
                      //alert("carregou");
                    }
                },
            "columns": [
                /*{
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },*/
                {"data": "descricao_etapa"},
                {"data": "uf_realizacao"},
                {"data": "data_etapa"},
                {"data": "total_quantidade"},
                {"data": "total_peso"}
            ],
            "language": {
                "url": "../bower_components/datatables/Portuguese-Brasil.json"
            },
            "processing": true,
            "serverSide": false,
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "destroy": true,
            "ordering": false,
            "info": false,
            "autoWidth": false
        } );



        $('.botaoBuscaResultados').on('click', function () {
            idCampeonato = $('#filtroEquipeCampeonatoResultado option:selected').val();
            tableResultadosGeral.ajax.reload();

        } );
    });

</script>

<style>

.barraBotoes {
  float: right;
  margin-right:5px;
  font-size: 21px;
  font-weight: bold;
  line-height: 1;
  color: #000;
  text-shadow: 0 1px 0 #fff;
  filter: alpha(opacity=20);
  opacity: .2;
  -webkit-appearance: none;
  padding: 0;
  cursor: pointer;
  background: transparent;
  border: 0;

</style>

<div id="propria">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Resultados Campeonato <button onClick="naoImplementado()" type="button" class="barraBotoes glyphicon glyphicon-print text-muted" ></button></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form name="form_filtraResultados" id="form_filtraResultados" method="post" >
                <div class="well form-group">
                    <h4>Filtros</h4>
                    <div class="row">
                        <div class="col-lg-3">
                            <label>Campeonato</label>
                            <select id="filtroEquipeCampeonatoResultado" name="filtroEquipeCampeonatoResultado" class="form-control">
                                <?php
                                    $campeonatos = retornaCampeonatos('','');
                                    foreach ($campeonatos as $result){
                                        echo '<option value="'.$result->id.'">'.$result->descricao.'</option>';
                                    }
                                 ?>
                            </select>
                        </div>

                        <div class="col-lg-1 pull-right">
                            <div class="input-group">
                                <button class="btn btn-primary botaoBuscaResultados" name="buttonFiltraResultados" id="buttonFiltraResultados" style="background-color:#005081" type="button"><b>Filtrar</b></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>


            <table id="tableResultadosGeral" class="table table-striped table-bordered table-hover" >
                <thead>
                    <tr>
                        <th colspan="5"><h3><b>Dados Gerais sobre Campeonato</b></h3></th>
                    </tr>
                    <tr>
                        <th>Etapa</th>
                        <th>UF</th>
                        <th>Data Etapa</th>
                        <th>Qtd Total Peixes</th>
                        <th><bdi><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?></bdi> Total Peixes</th>
                    </tr>
                </thead>
                <tbody id="resultadoTotalEtapa">
                    <!--RESULTADO GERAL-->
                </tbody>
            </table>

        </div>
    </div>
</div>
