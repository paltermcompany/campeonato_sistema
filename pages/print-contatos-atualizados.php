<?php
	session_start();
	
	header('Content-Type: text/html; charset=utf-8');
	include '../functions/conexao.php';
	require '../functions/crud.php';
	
        $dataDe = $_REQUEST['dataDe'];
        $dataAte = $_REQUEST['dataAte'];
                
	$contatos = '';
	if ($resultados = retornaQtdContatosAtualizados($dataDe, $dataAte)){
            $data = '';
            $dataAnterior = '';
            $total = 0;
            $subTotal = 0;
            $i=0;
            $trSub = '';
            
            foreach($resultados as $resultado){
                $i++;
                $total = $total + $resultado->TOTAL; 
                $subTotal = $subTotal + $resultado->TOTAL;
                
                $subGrupo = retornaQtdContatosAtualizadosAgrupado(date("Y-m-d", strtotime($resultado -> DATA_MODIFICACAO)));
                
                if($i == $subGrupo->TOTAL){
                    $trSub .= '<tr >';
                    $trSub .= '<td style="white-space: nowrap"><b>Total em '.trim(date("d/m/Y", strtotime($resultado -> DATA_MODIFICACAO))).'</b></td><td style="white-space: nowrap" align="right"><b>'.$subTotal.'</b></td>';
                    $trSub .= '</tr>';
                    $trSub .= '<tr bgcolor="white">';
                    $trSub .= '<td style="white-space: nowrap" colspan="2"></td>';
                    $trSub .= '</tr>';
                    $subTotal = 0;
                    $i=0;
                }else{
                    $trSub = '';
                }
                

               if($data == trim(date("d/m/Y", strtotime($resultado -> DATA_MODIFICACAO)))){
                    $contatos .= '<tr>';
                    $contatos .= '<td style="white-space: nowrap" align="left">'.$resultado->UF.'</td>';
                    $contatos .= '<td style="white-space: nowrap" align="right">'.$resultado->TOTAL.'</td>'; 
                    $contatos .= '</tr>';
                    $contatos .= $trSub;
                }else{
                    $contatos .= '<tr bgcolor="silver">';
                    $contatos .= '<td style="white-space: nowrap" align="center" colspan="2"><b>'.trim(date("d/m/Y", strtotime($resultado -> DATA_MODIFICACAO))).'</b></td>';
                    $contatos .= '</tr>';
                    
                    $contatos .= '<tr>';
                    $contatos .= '<td style="white-space: nowrap" align="left">'.$resultado->UF.'</td>';
                    $contatos .= '<td style="white-space: nowrap" align="right">'.$resultado->TOTAL.'</td>'; 
                    $contatos .= '</tr>';
                    $contatos .= $trSub;
                    $data = trim(date("d/m/Y", strtotime($resultado -> DATA_MODIFICACAO)));
                }
            }
            
            if ($resultadosTotal = retornaQtdContatosAtualizadosTotalUF($dataDe, $dataAte)){
                $contatos .= '<tr bgcolor="silver">';
                $contatos .= '<td style="white-space: nowrap" align="left"><h4>Total Geral</h4></td>';
                $contatos .= '<td style="white-space: nowrap" align="right">'; 
                $contatos .= '</tr>';
                foreach($resultadosTotal as $resultado){
                    $contatos .= '<tr>';
                    $contatos .= '<td style="white-space: nowrap" align="left">'.$resultado->UF.'</td>';
                    $contatos .= '<td style="white-space: nowrap" align="right">'.$resultado->TOTAL.'</td>'; 
                    $contatos .= '</tr>';
                    
                }
                $contatos .= '<tr>';
                $contatos .= '<td style="white-space: nowrap" align="left"><b>Total Geral</b></td>';
                $contatos .= '<td style="white-space: nowrap" align="right"><b>'.$total.'</b></td>'; 
                $contatos .= '</tr>';
            }
	}else{
            $contatos .= '<td style="white-space: nowrap" colspan="2">Sem Resultados</td>';
	}
	
	


$html = '
	
	<h4><b>Contatos Atualizados</b></h4>
	<table width="100%" >
            
            <tbody id="detalhesModalidades">
                <tr>
                    '.$contatos.'
                </tr>
            </tbody>
	</table>
	

';


	

$header = '
	<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;">
		<tr>
			<td width="20%" align="left"><img src="../img/logo.png" width="126px" /></td>
			<td width="60%" align="center" ><h2>Contatos Atualizados por UF</h2></td>
			<td width="20%" align="left"></td>
		</tr>
		
	</table>
';
$footer = '<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: justify; padding-top: 3mm; ">Este documento pode conter informação confidencial ou privilegiada, sendo seu sigilo protegido. Se você não for o destinatário ou a pessoa autorizada a receber este documento, não pode usar, copiar ou divulgar as informações nele contidos ou tomar qualquer ação baseada nessas informações. Se você recebeu este documento por engano, por favor, destrua-o imediatamente. Agradecemos sua cooperação.<br><div style="font-size: 9pt; text-align: center; padding-top: 3mm; ">Página {PAGENO} de {nb}</div></div>';
		    

//==============================================================
//==============================================================
//==============================================================
include("../imprimir/mpdf.php");

function tirarAcentos($string){
    return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
}

$mpdf=new mPDF('utf-8','A4','','','15','15','28','30'); 

$mpdf->SetHTMLHeader($header);


$mpdf->SetHTMLFooter($footer);

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents('../imprimir/examples/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,2);

$mpdf->Output('mpdf.pdf','I');
exit;
//==============================================================
//==============================================================
//==============================================================


?>