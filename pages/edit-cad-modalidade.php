<?php	
	include '../functions/conexao.php';
	require '../functions/crud.php';
	header('Content-Type: text/html; charset=utf-8');
	$idModalidade = $_REQUEST['idModalidade'];
	$modalidade = retornaModalidade($idModalidade);
?>
	<script type="text/javascript" src="../js/custom.js"></script>
	<div class="panel-body">		
		<form role='form' method='post' action='' name='form_editCadModalidade'>
			<fieldset>
				<input type="hidden" name="editCadIdModalidade" id="editCadIdModalidade" value="<?php echo $modalidade -> id;?>">	
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label>Nome Modalidade</label>
							<input id="editCadNomeModalidade" name="editCadNomeModalidade" class="form-control upcase" placeholder="Nome Espécie" required value="<?php echo $modalidade->descricao;?>">
						</div>
					</div>
				</div>
			</fieldset>	
			<div class="span6 pull-right">
				<button id="botaoEditaModalidade" name="botaoEditaModalidade" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
			</div>
		</form>
	</div>	