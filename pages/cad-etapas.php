<?php
    session_start();
    include '../functions/conexao.php';
    require '../functions/crud.php';
?>

<script type="text/javascript" src="../js/custom.js"></script>

<script>
    var tableCadEtapas;

    function filtraEtapas(){
        tableCadEtapas.ajax.reload();
    }

    $(document).ready(function() {
        tableCadEtapas = $('#tableFiltroCadEtapas').DataTable( {
            "ajax": {
                    url: "../ajax/controller.php",
                    type: "POST",
                    data : function(d){
                        d.acao = "carregaEtapas",
                        d.filtroEtapaNome = $( "#filtroEtapaNome" ).val(),
                        d.filtroEtapaCampeonato = $( "#filtroEtapaCampeonato" ).val()
                    },
                    beforeSend : function() {
                        $('#buttonFiltraEtapas').html('Aguarde').attr('disabled', true);
                    },
                    complete : function() {
                        $('#buttonFiltraEtapas').html('Filtrar').attr('disabled', false);
                    }
                },
            "columns": [
                {"data": "id_etapa"},
                {"data": "campeonato"},
                {"data": "nome_etapa"},
                {"data": "data_etapa"},
                {
                    "orderable": false,
                    "defaultContent": '<div class="btn-group btn-group-xs"><button type="button" class="btn btn-warning btnAlteraEtapa"><span class="glyphicon glyphicon-edit"></span> </button><button type="button" class="btn btn-danger" onclick="naoImplementado()"><span class="glyphicon glyphicon-remove"></span></button></div>'
                },
            ],
            "language": {
                "url": "../bower_components/datatables/Portuguese-Brasil.json"
            },
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": true,
            "autoWidth": false
        } );

        $('#tableFiltroCadEtapas tbody').on( 'click', 'button', '.btnAlteraEtapa', function () {
            var data = tableCadEtapas.row( $(this).parents('tr') ).data();
            console.log(data);
            abreModalEditaEtapa(data.id_etapa);
        } );

    });
</script>

<div id="propria">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cadastro de Etapas  <button onClick="abreModalNovaEtapa()" type="button" class="close glyphicon glyphicon-plus text-muted" ><span aria-hidden="true"></span></button></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form name="form_filtraEtapas" id="form_filtraEtapas" method="post" >
                <div class="well form-group">
                    <h4>Filtros</h4>
                    <div class="row">
                        <div class="col-lg-8">
                            <label>Descrição</label>
                            <input type="text" id="filtroEtapaNome" name="filtroEtapaNome" class="form-control upcase">
                        </div>

                        <div class="col-lg-3">
                            <label>Campeonato</label>
                            <select id="filtroEtapaCampeonato" name="filtroEtapaCampeonato" class="form-control">
                                <option value="0">Todos</option>
                                <?php
                                    $campeonatos = retornaCampeonatos('', '');
                                    foreach ($campeonatos as $result) {
                                    	echo '<option value="' . $result->id . '">' . $result->descricao . '</option>';
                                    }
                                ?>
                            </select>
                        </div>

                        <div class="col-lg-1 pull-right">
                        <label></label>
                            <div class="input-group">
                                <button class="btn btn-primary" name="buttonFiltraEtapas" id="buttonFiltraEtapas" style="background-color:#005081" type="button" onClick="filtraEtapas()"><b>Filtrar</b></button>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
            <div class="row" >
                <div class="col-lg-12" id="resultadoFiltroEtapas">
                    <table id="tableFiltroCadEtapas" class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Campeonato</th>
                                <th>Descrição</th>
                                <th>Data Realização</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody >
                            <!--RESULTADO FILTRO ETAPAS-->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

