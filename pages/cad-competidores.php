<?php
	session_start();
	include '../functions/conexao.php';
	require '../functions/crud.php';
?>

<script type="text/javascript" src="../js/custom.js"></script>

<script>
	var tableFiltroCompetidores;
	$(document).ready(function() {
		tableFiltroCompetidores = $('#tableFiltroCompetidores').DataTable({
			"ajax": {
				url: "../ajax/controller.php",
				type: "POST",
				data :function(d){
					d.acao = "carregaCompetidores",
					d.filtroCompetidorNome = $('#filtroCompetidorNome').val()
					d.filtroCompetidorApelido = $('#filtroCompetidorApelido').val()
					d.filtroCompetidorCPF = $('#filtroCompetidorCPF').val()
				},
				beforeSend : function() {
                    $('#buttonFiltraCompetidores').html('Aguarde').attr('disabled', true);
                },
                complete : function() {
                    $('#buttonFiltraCompetidores').html('Filtrar').attr('disabled', false);
                }
			},
			"columns": [
				{"data": "id_competidor"},
				{"data": "nome_competidor"},
				{"data": "tam_camiseta"},
				{"data": "acoes"}
			],
			"language": {
				"url": "../bower_components/datatables/Portuguese-Brasil.json"
			},
			"paging": true,
			"lengthChange": true,
			"searching": false,
			"destroy": true,
			"ordering": true,
			"order": [[ 1, "asc" ]],
			"info": true,
			"autoWidth": false
		} );
	});

	function filtraCompetidores(){
		tableFiltroCompetidores.ajax.reload();
	}
</script>

<div id="propria">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Cadastro de Competidores  <button onClick="abreModalNovoCompetidor()" type="button" class="close glyphicon glyphicon-plus text-muted" ><span aria-hidden="true"></span></button></h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<form name="form_filtraCompetidores" id="form_filtraCompetidores" >
				<div class="well form-group">
					<h4>Filtros</h4>
					<div class="row">
						<div class="col-lg-5">
							<label>Nome</label>
							<input type="text" id="filtroCompetidorNome" name="filtroCompetidorNome" class="form-control upcase">
						</div>
						<div class="col-lg-4">
							<label>Apelido</label>
							<input type="text" id="filtroCompetidorApelido" name="filtroCompetidorApelido" class="form-control upcase">
						</div>
						<div class="col-lg-2">
							<label>CPF</label>
							<input type="text" id="filtroCompetidorCPF" name="filtroCompetidorCPF" class="form-control">
						</div>

						<div class="col-lg-1 pull-right">
						<label></label>
							<div class="input-group">
								<button class="btn btn-primary" name="buttonFiltraCompetidores" id="buttonFiltraCompetidores" style="background-color:#005081" type="button" onclick="filtraCompetidores();"><b>Filtrar</b></button>
							</div>
						</div>
					</div>
				</div>
			</form>
			<div class="row" >
				<div class="col-md-12" id="resultadoFiltroCompetidores">
					<table id="tableFiltroCompetidores" class="table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Nome Competidor</th>
								<th>Tam. Camiseta</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody id="resultadoLancamentosPorEtapa">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$('#filtroCompetidorCPF').mask('000.000.000-00', {reverse: true});
</script>
