    <script type="text/javascript" src="../js/custom.js"></script>
    <div class="panel-body">
        <form role='form' method='post' action='' name='form_newCadColetor'>
            <fieldset>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Nome Coletor</label>
                            <input id="newCadNomeColetor" name="newCadNomeColetor" class="form-control upcase" placeholder="Nome Coletor" required>
                        </div>
                        <div class="form-group">
                            <label>Serial Coletor</label>
                            <input id="newCadSerialColetor" name="newCadSerialColetor" class="form-control upcase" placeholder="Serial Coletor" required>
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="span6 pull-right">
                <button id="botaoInsereColetor" name="botaoInsereColetor" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
            </div>
        </form>
    </div>
