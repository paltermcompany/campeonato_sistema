<?php
	session_start();
	header('Content-Type: text/html; charset=utf-8');
	include '../functions/conexao.php';
	require '../functions/crud.php';
	$idEtapa = $_REQUEST['idEtapa'];

	$ranking = '';
	if ($resultado = retornaCompetidoresEtapa($idEtapa)){

		foreach($resultado as $result){
			$ranking.= '<tr style="white-space: nowrap">';

				if($result->SUPLENTE == 'S'){
					$ranking.= '<td style="white-space: nowrap" align="left">'.($result->NOME).' - <strong>SUPLENTE</strong></td>';
				}else{
					$ranking.= '<td style="white-space: nowrap" align="left">'.($result->NOME).'</td>';
				}

				$ranking.= '<td style="white-space: nowrap" align="left">'.($result->EQUIPE).'</td>';

				if($result->ID_PULSEIRA==0){
					$ranking.= '<td align="right">N/D</td>';
				}else{
					$ranking.= '<td align="right">'.$result->ID_PULSEIRA.'</td>';
				}

				$ranking.= '<td style="white-space: nowrap" align="right">'.($result->CPF).'</td>';
				$ranking.= '<td style="white-space: nowrap" align="right">'.($result->TAMANHO_CAMISETA).'</td>';
				$ranking.= '<td style="white-space: nowrap" align="left">'.($result->APELIDO).'</td>';
				$ranking.= '<td style="white-space: nowrap" align="left">'.($result->EQUIPE).' - ' .($result->APELIDO).'</td>';


			$ranking.= '</tr>';
		}
	}else{
		$ranking.= '<tr>';
			$ranking.= '<td colspan="7">Sem Resultados</td>';
		$ranking.= '</tr>';
	}

$html = '


	<table width="100%" class="bpmTopic">
		<thead>
			<tr>
				<th width="20%" align="left">Competidor</th>
				<th width="10%" align="left">Equipe</th>
				<th width="6%" align="right">Pulseira</th>
				<th width="14%" align="right">CPF</th>
				<th width="10%" align="right">Tam.</th>
				<th width="15%" align="left">Apelido</th>
				<th width="20%" align="left">Impressão Camiseta</th>

			</tr>
		</thead>
		<tbody id="resultadoListarCompetidoresImpressao">

			'.$ranking.'

		</tbody>
	</table>

';
	$etapa = retornaEtapa($idEtapa);
	$nomeEtapa = $etapa->DESCRICAO;
	$campeonato = retornaCampeonato($etapa->ID_CAD_CAMPEONATO);
	$nomeCampeonato = ($campeonato->descricao);
	$dataEtapa = $etapa->DATA_ETAPA;


	$header = '
		<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;">
			<tr>
				<td width="20%" align="left"><img src="../img/logo.png" width="126px" /></td>
				<td width="60%" align="center" ><h2>Relação de Competidores</h2></td>
				<td width="20%" align="left"></td>
			</tr>
			<tr>
				<td colspan="3" width="100%" align="center" ><h4>'.($nomeCampeonato).' - '.($nomeEtapa). ' - ' . $dataEtapa .'</h4></td>

			</tr>
		</table>
	';
$footer = '<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: justify; padding-top: 3mm; ">Este documento pode conter informação confidencial ou privilegiada, sendo seu sigilo protegido. Se você não for o destinatário ou a pessoa autorizada a receber este documento, não pode usar, copiar ou divulgar as informações nele contidos ou tomar qualquer ação baseada nessas informações. Se você recebeu este documento por engano, por favor, destrua-o imediatamente. Agradecemos sua cooperação.<br><div style="font-size: 9pt; text-align: center; padding-top: 3mm; ">Página {PAGENO} de {nb}</div></div>';


//==============================================================
//==============================================================
//==============================================================


include("../imprimir/mpdf.php");

function tirarAcentos($string){
    return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
}

$mpdf=new mPDF('utf-8','A4-L','','','15','15','28','30');

/*

$this->mpdf->mPDF('utf-8','A4','','','15','15','28','18');
When 15=margin-left, 15=margin-right, 28=margin-top, 18=margin-bottom*/


$mpdf->SetHTMLHeader($header);



$mpdf->SetHTMLFooter($footer);

$mpdf->SetDisplayMode('fullpage');



// LOAD a stylesheet
$stylesheet = file_get_contents('../imprimir/examples/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,2);

$mpdf->Output('mpdf.pdf','I');
exit;
//==============================================================
//==============================================================
//==============================================================


?>
