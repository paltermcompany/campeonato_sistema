<?php
	session_start();
	header('Content-Type: text/html; charset=utf-8');

	include '../functions/conexao.php';
	require '../functions/crud.php';

?>

<script type="text/javascript" src="../js/custom.js"></script>

<script>
    $( document ).ready(function() {
        //alert(typeof refreshIdMod );
        if(typeof refreshIdMod == 'undefined') {
            refreshIdMod = setTimeout( checkAcompanhamentoMod, 10);
        }
    });

    function moderaMedida(status, idPesagem){
        palavra = '';
        if(status == 'aprova'){
            palavra = 'APROVAÇÃO';
        }else{
            palavra = 'REPROVAÇÃO';
        }
        if(typeof idPesagem == 'number'){

            if (confirm("Confirma "+palavra+" desta medida?")){
                if(status=='reprova'){
                    var obs = prompt("Informe o motivo da reprovação:", "");
                    while (obs.length == 0) {
                        obs = prompt("Informe o motivo da reprovação");
                    }
                }else{
                    obs = '';
                }
                $.post('../ajax/controller.php',{
                    acao:"moderacaoMedidas",
                    idPesagem:idPesagem,
                    statusModeracao: status,
                    obs : obs.toUpperCase()
                },function(retorno){
                    if(retorno=='ok'){
                        if(status == 'aprova'){
                            alert('Aprovado!');
                        }else{
                            alert('Reprovado!');
                        }
                        $('#linhaMod'+idPesagem).remove();
                    }else{
                        alert(retorno);
                    }
                })
            }
        }
    }

	function checkAcompanhamentoMod() {
        //console.log('atualiza pesagens moderacao');
		idEtapa = $('#idEtapaModeracao').val();
        if(idEtapa!=''){
            if(typeof idEtapa != 'undefined'){
                $.post('../ajax/controller.php',{
                    acao:"acompanhamentoLiveModeracao",
                    idEtapa:idEtapa,
                    ultimaVerificacao: $('#ultimaVerificacaoModeracao').val()
                },function(retorno){
                    if(retorno!=''){
                        $( ".linhaLive" ).removeClass( 'invalid' );
                        $( ".linhaLive" ).removeClass( 'excluido' );
                    }
                    $('#acompanhamentoLiveModeracao').prepend(retorno);

                }).always(function() {
                    refreshIdMod = setTimeout( checkAcompanhamentoMod, 1000 );
                });
            }else{
                //console.log("caiu else mod " + idEtapa);
                refreshIdMod = undefined;
            }
        }
	}



</script>


<style>
@-webkit-keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
@-moz-keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
@-o-keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
@keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
.invalid {
  -webkit-animation: invalid 1s infinite; /* Safari 4+ */
  -moz-animation:    invalid 1s infinite; /* Fx 5+ */
  -o-animation:      invalid 1s infinite; /* Opera 12+ */
  animation:         invalid 1s infinite; /* IE 10+ */
}

@-webkit-keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
@-moz-keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
@-o-keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
@keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
.excluido {
  -webkit-animation: excluido 1s infinite; /* Safari 4+ */
  -moz-animation:    excluido 1s infinite; /* Fx 5+ */
  -o-animation:      excluido 1s infinite; /* Opera 12+ */
  animation:         excluido 1s infinite; /* IE 10+ */
}

td {
    padding: 1em;
}
}
</style>

<div id="propria">
	<div id="live">
		<div class="row">
			<div class="col-lg-12">
				<?php
					if($etapa = retornaEtapaHoje('S')){
						echo '<h1 class="page-header">Moderação de medidas <small id="etapa" align="center">'.$etapa->DESCRICAO.'</small></h1>';
						echo '<input type="hidden" name="idEtapaModeracao" id="idEtapaModeracao" value="'.$etapa->ID.'">';
					}else{
						echo '<h1 class="page-header">Moderação de medidas <small id="etapa" align="center">Nenhuma etapa moderada sendo realizada hoje</small></h1>';
						echo '<input type="hidden" name="idEtapaModeracao" id="idEtapaModeracao" value="">';
					}
				?>

			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<input type="hidden" name="ultimaVerificacaoModeracao" id="ultimaVerificacaoModeracao" value="">

				<div class="well form-group">
					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive" id="tableExportaClientes2">
								<table class="table table-bordered table-hover" >
									<thead>
										<tr>
											<!--<th>#</th>-->
											<th>Comprovante</th>
											<th>Hora</th>
											<th>Equipe</th>
											<th>Competidor</th>
											<th>Espécie</th>
											<th>Modalidade</th>
											<th>Isca</th>
											<th>Medida</th>
											<th>Obs</th>
											<th>Ação</th>
										</tr>
									</thead>
									<tbody id="acompanhamentoLiveModeracao">
										<!--RESULTADO GERAL-->
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>


