<?php	
	include_once '../functions/conexao.php';
	include_once '../functions/crud.php';	
	$idUsuario = $_REQUEST['idUsuario'];
	$usuario = retornaDadosUsuario($idUsuario);
?>

<script type="text/javascript" src="../js/custom.js"></script>

<div id="propria">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Perfil do usuário</h2>
		</div>
	</div> 
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="panel-body">
						<form role='form' method='post' action='' name='form_atualizaPerfilUsuario'>
							<fieldset >
								<input type="hidden" id="emailPerfilCodigo" name="emailPerfilCodigo" value="<?php echo $idUsuario; ?>"/>
								
								<div class="row">
									<div class="col-lg-3">
										<div class="form-group">
											<label>Código</label>
											<input id="emailPerfilCodigo2" name="emailPerfilCodigo2" class="form-control" value="<?php echo $idUsuario; ?> " disabled>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label>Nome do Usuário</label>
											<input id="nomePerfilUsuario" name="nomePerfilUsuario" class="form-control upcase" value="<?php echo $usuario -> NOME; ?>" disabled>
										</div>
									</div>	
								</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Senha</label>
											<input id="senhaPerfilUsuario" name="senhaPerfilUsuario" class="form-control" value="<?php echo $usuario -> SENHA; ?>">
										</div>
									</div>
								</div>

								<div class="span6 pull-right">
									<button id="botaoAtualizaPerfilUsuario" name="botaoAtualizaPerfilUsuario" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar alterações</button>
								</div>
							</fieldset>	
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
