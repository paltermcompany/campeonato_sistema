<?php	
	include '../functions/conexao.php';
	require '../functions/crud.php';
	header('Content-Type: text/html; charset=utf-8');
	$idEquipe = $_REQUEST['idEquipe'];
	$equipe = retornaEquipe($idEquipe);
?>
<script type="text/javascript" src="../js/custom.js"></script>
	<div class="panel-body">
		<form role='form' method='post' action='' name='form_editCadEquipe'>
			<fieldset>
				<input type="hidden" name="editCadIdEquipe" id="editCadIdEquipe" value="<?php echo $equipe -> ID;?>">	
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label>Nome Equipe</label>
							<input id="editCadNomeEquipe" name="editCadNomeEquipe" class="form-control upcase" placeholder="Nome Equipe" required value="<?php echo $equipe -> DESCRICAO;?>">
						</div>
						<div class="form-group">
							<label>Campeonato</label>
							<select name="editCadCampeonatoEquipe" id="editCadCampeonatoEquipe" class="form-control upcase" onchange="filtraEtapasComboeditCad($( '#editCadCampeonatoEquipe option:selected' ).val())">
								<?php
									$campeonatos = retornaCampeonatos('', 'A');
									foreach ($campeonatos as $result){
										echo '<option value="'.$result->id.'">'.$result->descricao.'</option>';
									}	
								 ?>
							</select>
						</div>
						
						<div class="form-group">
							<label>Etapa</label>
							<select id="editCadEtapaEquipe" name="editCadEtapaEquipe" class="form-control">
							<?php
								$etapas = retornaEtapas('', $equipe -> ID_CAD_CAMPEONATO);
								foreach ($etapas as $result){
									echo '<option value="'.$result->id.'">'.$result->DESCRICAO.'</option>';
								}	
							 ?>						
							</select>
						</div>	
						
					</div>
				</div>
			</fieldset>	
			<div class="span6 pull-right">
				<button id="botaoEditaEquipe" name="botaoEditaEquipe" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
			</div>
		</form>
	</div>	
	
	<script>
		$("select[name='editCadCampeonatoEquipe']").val('<?php echo $equipe -> ID_CAD_CAMPEONATO;?>');
		$("select[name='editCadEtapaEquipe']").val('<?php echo $equipe -> ID_CAD_ETAPA;?>');
	</script>	