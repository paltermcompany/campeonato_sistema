<?php
	include '../functions/conexao.php';
	require '../functions/crud.php';
	header('Content-Type: text/html; charset=utf-8');
	$idIsca = $_REQUEST['idIsca'];
	$Isca = retornaIsca($idIsca);
?>
	<script type="text/javascript" src="../js/custom.js"></script>
	<div class="panel-body">
		<form role='form' method='post' action='' name='form_editCadIsca'>
			<fieldset>
				<input type="hidden" name="editCadIdIsca" id="editCadIdIsca" value="<?php echo $Isca -> id;?>">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label>Nome Isca</label>
							<input id="editCadNomeIsca" name="editCadNomeIsca" class="form-control upcase" placeholder="Nome Espécie" required value="<?php echo $Isca->descricao;?>">
						</div>
					</div>
				</div>
			</fieldset>
			<div class="span6 pull-right">
				<button id="botaoEditaIsca" name="botaoEditaIsca" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
			</div>
		</form>
	</div>
