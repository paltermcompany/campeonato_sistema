<?php
	include '../functions/conexao.php';
	require '../functions/crud.php';
	$idEtapa = $_REQUEST['idEtapa'];
	$etapa = retornaEtapa($idEtapa);
	$datasRealizacao = retornaDatasEtapa($idEtapa);
	$tiposOrgRaia = retornaTiposOrgRaias();
	$especies = retornaEspecies('', '');
	$peixesAlvo = retornaPeixesAlvo($idEtapa);
	$peixesAlvo = explode(',',$peixesAlvo -> PEIXES_ALVO);

?>
<script>
	function exibeDadosGeraisEtapa(){
		$( "#dadosGeraisEtapa" ).removeClass( 'hidden' );
		$( "#cadColetoresEtapa" ).addClass( 'hidden' );
		$( "#cadTipoOrgRaias" ).addClass( 'hidden' );
		$( "#cadBaterias" ).addClass( 'hidden' );
	}

	function exibeColetoresEtapa(){
		$( "#dadosGeraisEtapa" ).addClass( 'hidden' );
		$( "#cadColetoresEtapa" ).removeClass( 'hidden' );
		$( "#cadTipoOrgRaias" ).addClass( 'hidden' );
		$( "#cadBaterias" ).addClass( 'hidden' );
	}

	function exibeOrgEtapa(){
		$( "#dadosGeraisEtapa" ).addClass( 'hidden' );
		$( "#cadColetoresEtapa" ).addClass( 'hidden' );
		$( "#cadTipoOrgRaias" ).removeClass( 'hidden' );
		$( "#cadBaterias" ).addClass( 'hidden' );
	}

	function exibeBaterias(){
		$( "#dadosGeraisEtapa" ).addClass( 'hidden' );
		$( "#cadColetoresEtapa" ).addClass( 'hidden' );
		$( "#cadTipoOrgRaias" ).addClass( 'hidden' );
		$( "#cadBaterias" ).removeClass( 'hidden' );
	}

	function alteraUnidadeMedida(idTipoUnidadeMedida){
		if(idTipoUnidadeMedida==1){
			$('#editCadDescricaoUnidadeMedida').html('Kg');
		}else{
			if(idTipoUnidadeMedida==2){
				$('#editCadDescricaoUnidadeMedida').html('Metros');
			}
		}
	}

</script>
<script type="text/javascript" src="../js/custom.js"></script>
<ul class="nav nav-tabs">
  	<li><a href="#" onclick="exibeDadosGeraisEtapa()">Dados Gerais</a></li>
  	<li><a href="#" onclick="exibeColetoresEtapa()">Coletores</a></li>
  	<li><a href="#" onclick="exibeOrgEtapa()">Organização de Raias</a></li>
  	<li><a href="#" onclick="exibeBaterias()">Baterias Prova</a></li>
</ul>

<div class="panel-body">
	<div id="dadosGeraisEtapa">
		<form role='form' method='post' action='' name='form_editCadEtapa'>
			<fieldset>
				<div class="row">
					<div class="col-lg-12">
					<input type="hidden" name="editCadIdEtapa" id="editCadIdEtapa" value="<?php echo $etapa -> ID;?>">

						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Nome Etapa</label>
									<input id="editCadNomeEtapa" name="editCadNomeEtapa" class="form-control upcase" placeholder="Nome Etapa" required value="<?php echo $etapa -> DESCRICAO;?>">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Nome Etapa (Impressão App)</label>
									<input id="editCadNomeEtapaApp" name="editCadNomeEtapaApp" class="form-control upcase" placeholder="Nome Etapa" required value="<?php echo $etapa -> DESCRICAO_APP;?>">
								</div>
							</div>

						</div>

						<input type="hidden" name="editPeixeAlvoEtapa" id="editPeixeAlvoEtapa" value="">

						<div class="row">
							<div class="col-lg-6">
								<label>Campeonato</label>
								<select name="editCadCampeonatoEtapa" id="editCadCampeonatoEtapa" class="form-control">
									<?php
										$campeonatos = retornaCampeonatos('', 'A');
										foreach ($campeonatos as $result){
											echo '<option value="'.$result->id.'">'.$result->descricao.'</option>';
										}
									 ?>
								</select>
							</div>
							<div class="col-lg-2">
								<div class="form-group">
									<label>Qtd. Equipes</label>
									<input id="editCadQtdEquipesEtapa" name="editCadQtdEquipesEtapa" class="form-control upcase" disabled value="<?php echo $etapa -> QTD_EQUIPES;?>">
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label>UF Realização</label>
                                    <select id="editCadUFEtapa" name="editCadUFEtapa" class="form-control" required>
                                        <?php
                                            if ($estados = retornaUFs()){
                                                foreach($estados as $estado){
                                                    echo '<option value="'.$estado->id.'">'.$estado->descricao.' ('.$estado->sigla.')</option>';
                                                }
                                            }else{
                                                echo '<option value="nenhumEstado">Nenhum estado encontrado</option>';
                                            }
                                        ?>

                                    </select>
								</div>
							</div>

						</div>
						<div class="form-group">
							<div class="row ">

								<div class="col-lg-6">
									<label>Tipo de Classificação</label>
									<select name="editCadTipoClassificacaoEtapa" id="editCadTipoClassificacaoEtapa" class="form-control">
										<option value="1">Considerar todos exemplares, exceto cancelados</option>;
										<option value="2">Considerar 3 maiores exemplares, exceto cancelados</option>;
									</select>
								</div>

								<div class="col-lg-2">
									<label>Peixe Alvo</label>

									<div >
										<select id="chkveg" name="chkveg" multiple="multiple" class="opcao-peixes" >
											<?php
												foreach ($especies as $especie) {
													if(in_array($especie -> id, $peixesAlvo)){
												 		echo '<option selected value="'.$especie -> id.'" class="opcao-peixe active ">'.$especie -> descricao.'</option>';
												 	}else{
												 		echo '<option value="'.$especie -> id.'" class="opcao-peixe">'.$especie -> descricao.'</option>';
												 	}
												 }
											?>
										</select>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-lg-2">
									<label>Unidade Medida</label>
									<select name="editCadUnidadeMedidaEtapa" id="editCadUnidadeMedidaEtapa" class="form-control" onchange="alteraUnidadeMedida($( '#editCadUnidadeMedidaEtapa option:selected' ).val())">
										<?php
											$unidadesMedida = retornaUnidadesMedida('');
											foreach ($unidadesMedida as $result){
												echo '<option value="'.$result->id.'">'.$result->descricao.'</option>';
											}
										 ?>
									</select>
								</div>
								<div class="col-lg-4">
									<label>Moderação Medidas</label>
										<select name="editCadModeracaoEtapa" id="editCadModeracaoEtapa" class="form-control">
											<option value="N">Não usar sistema de moderação</option>;
											<option value="S">Usar sistema de moderação</option>;
									</select>
								</div>
								<div class="col-lg-3">
									<label>Exemplar Mínimo</label>
									<label><sub id="editCadDescricaoUnidadeMedida"></sub></label>
									<input id="editCadPesoMinimo" type="number" step="0.01" min="0" name="editCadPesoMinimo" class="form-control" placeholder="Exemplar Mínimo" required value="<?php echo $etapa -> peso_minimo;?>">
								</div>
								<div class="col-lg-3">
									<label>Quantidade Baterias</label>
									<input id="editCadQtdBaterias" type="number" step="1" min="0" name="editCadQtdBaterias" class="form-control" placeholder="Qtd Baterias" readonly="true" value="<?php echo $etapa -> QTD_BATERIAS;?>">
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="editDataEtapa">Data Início</label>
							<div class="input-group date time form_datetime" data-link-field="editDataEtapa">
								<input type="text" name="editDataEtapa2" id="editDataEtapa2" class="form-control" type="text" readonly required value="<?php echo date("d/m/Y", strtotime($etapa -> DATA_ETAPA));?>">
								<span class="input-group-addon"><span class="glyphicon glyphicon-remove" ></span></span>
								<span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
							</div>
							<input type="hidden" name="editDataEtapa" id="editDataEtapa" value="<?php echo $etapa -> DATA_ETAPA;?>">
						</div>

						<div class="form-group">
							<label>Datas Realização</label>
							<div class="input-group date form-group" id="datepicker">

							    <input type="text" class="form-control" id="editCadDatasEtapa" name="editCadDatasEtapa" placeholder="Datas Realização" required value="<?php echo $datasRealizacao -> DATAS_REALIZACAO;?>" />
							    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i><span class="count"><?php echo $datasRealizacao -> TOTAL_DATAS;?></span></span>
							</div>
						</div>

						<div class="col-lg-12">
							<div class="form-group">
								<label>Imagem</label><br>
								Selecione uma imagem: <input id="editFileEtapa" type="File" name="img">
								<div id="editaImagemEtapa">
									<img src="data:image/png;base64, <?php echo $etapa->imagem;?>" alt="Logo Etapa" />
								</div>
								<input type="hidden" id="editCadImagemEtapa" value="<?php echo $etapa->imagem;?>" name="editCadImagemEtapa">
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			<div class="span6 pull-right">
				<button id="botaoEditaEtapa" name="botaoEditaEtapa" type='submit' class='btn btn-success btn-salva' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
			</div>
		</form>
	</div>
	<div id="cadColetoresEtapa" class="hidden">

		<style>
			.listagemCompetidores {
			  overflow: auto;
			  height:500px;
			}
			ol li a:active {
			font-weight: bold;
			}

			.opcao-peixe {
			  padding: 4px 20px 3px 20px;

			}

		</style>

		<div class="panel-body">
	        <div class="col-lg-6 listagemCompetidores bg-success">
	            <b>Coletores cadastrados:</b>
	            <div id="listColetoresCadastradosEtapa" class="list-group">

	            </div>
	        </div>

	        <div class="col-lg-6 listagemCompetidores">
	            Coletores disponíveis:
	            <!--<div class="form-group">
	                <div class="input-group">
	                    <input type="text" class="form-control" id="nomeColetorBusca" placeholder="Buscar coletores...">
	                    <span class="input-group-btn">
	                        <button class="btn btn-default" type="button" onclick="buscarColetor()" >Buscar!</button>
	                    </span>
	                </div>
	            </div>-->
	            <div id="listDestinoColetores" class="list-group">

	            </div>
	        </div>
		</div>
	</div>

	<div id="cadTipoOrgRaias" class="hidden">
		<style>

			.selected{
				position: relative;
				opacity: 0.3;
				background-color: blue;
			}

			.container {
				position: relative;
				width: 100%;
				max-width: 400px;
			}

			.image {
				display: block;
				width: 100%;
				height: auto;
			}

			.overlay {
				position: absolute;
				top: 0;
				bottom: 0;
				left: 0;
				right: 0;
				height: 100%;
				width: 100%;
				opacity: 0;
				transition: .3s ease;
				background-color: blue;
			}

			.container:hover .overlay{
				opacity: 0.3;
			}

			.icon {
				color: white;
				font-size: 100px;
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -50%);
				-ms-transform: translate(-50%, -50%);
				text-align: center;
			}

			.fa-user:hover {
				color: #eee;
			}




		</style>

		<script>

			$(".orgRaias").click(function(){
				if ($('.orgRaias').hasClass('selected')){
					$( ".orgRaias" ).removeClass( 'selected' );
				}
				var id = $(this).attr("id");
				alteraTipoOrganizacaoRaias($(this).attr('data'));

				$('#editCadTipoOrgRaiaSelecionada').val($(this).attr('data'));
				$('#'+id).addClass("selected" );
			});

		</script>

		<div class="panel-body">
	        <div class="col-lg-12">
				<?php
	        		foreach ($tiposOrgRaia as $orgRaia) {
	        			if($orgRaia -> id == $etapa -> ID_CAD_TIPO_ORG_RAIA){
	        				$selected = 'selected';
	        			}else{
	        				$selected = '';
	        			}
	        			echo '<div class="col-lg-4">';
			        	echo	$orgRaia -> descricao;
			        	echo	'<div id="tipoRaiaOrg'.$orgRaia -> id.'" class="container orgRaias '.$selected.'" data="'.$orgRaia -> id.'">';
			        	echo		'<a href="#tipoRaiaOrg'.$orgRaia -> id.'" >';
						echo			'<img src="../img/organizacao_raias/'.$orgRaia -> imagem.'" alt="Avatar" class="image">';
						echo			'<div class="overlay">';
						echo				'<a href="#" class="icon">';
						echo				'<i class="fa fa-check-square"></i>';
						echo				'</a>';
						echo			'</div>';
						echo		'</a>';
						echo	'</div>';
						echo '</div>';
	        		}
	        	?>
	        </div>
		</div>
	</div>

	<div id="cadBaterias" class="hidden">

		<style>
			.listagemCompetidores {
			  overflow: auto;
			  height:500px;
			}
			ol li a:active {
			font-weight: bold;
			}

			.opcao-peixe {
			  padding: 4px 20px 3px 20px;

			}

		</style>

		<div class="panel-body">
	        <div class="col-lg-6 listagemCompetidores bg-success">
	            <b>Baterias cadastrados:</b>
	            <div id="listBateriasCadastradasEtapa" class="list-group">

	            </div>
	        </div>

	        <div class="col-lg-6">
	            Adicionar bateria:
	            <!--<div class="form-group">
	                <div class="input-group">
	                    <input type="text" class="form-control" id="nomeColetorBusca" placeholder="Buscar coletores...">
	                    <span class="input-group-btn">
	                        <button class="btn btn-default" type="button" onclick="buscarColetor()" >Buscar!</button>
	                    </span>
	                </div>
	            </div>-->
	            <div class="form-group">

					<div class="row">

						<div class="col-lg-5">
							<div class="form-group">
								<label>Hora Início</label>
								<input id="editCadInicioNovaBateria" name="editCadInicioNovaBateria" type="time" class="form-control " placeholder="08:00" >
							</div>
						</div>
						<div class="col-lg-5">
							<div class="form-group">
								<label>Hora Termino</label>
								<input id="editCadTerminoNovaBateria" name="editCadTerminoNovaBateria" type="time"  class="form-control " placeholder="18:00" >
							</div>


						</div>

						<div class="col-lg-2">
							<label>&nbsp;</label>
							<button id="botaoAdicionaBateria" name="botaoAdicionaBateria" type="button" onclick="insereBateriaEtapa($('#editCadInicioNovaBateria').val(), $('#editCadTerminoNovaBateria').val())" class="btn btn-success" ><i class="glyphicon glyphicon-plus"></i>
						</div>


					</div>
	            </div>
	        </div>
		</div>
	</div>

</div>

<script type='text/javascript'>
	$( document ).ready(function() {
		$("select[name='editCadCampeonatoEtapa']").val(<?php echo $etapa -> ID_CAD_CAMPEONATO;?>);
		$("select[name='editCadUnidadeMedidaEtapa']").val(<?php echo $etapa -> id_cad_unidade_medida;?>);
		$("select[name='editCadModeracaoEtapa']").val('<?php echo $etapa -> ETAPA_MODERADA;?>');
		$("select[name='editCadTipoClassificacaoEtapa']").val(<?php echo $etapa -> ID_CAD_TIPO_CLASSIFICACAO;?>);
		$("select[name='editCadUFEtapa']").val(<?php echo $etapa -> ID_CAD_UF_REALIZACAO;?>);

		carregaColetoresDisponiveis(<?php echo $etapa -> ID;?>);
		carregaColetoresNaEtapa(<?php echo $etapa -> ID;?>);
		alteraUnidadeMedida($( '#editCadUnidadeMedidaEtapa option:selected' ).val());
		carregaBateriasEtapa(<?php echo $etapa -> ID;?>);


		$('.form_datetime').datetimepicker({
			weekStart: 0,
			todayBtn:  1,
			autoclose: 1,
			format: 'dd/mm/yyyy',
			todayHighlight: 1,
			startView: 2,
			minView: 2,
			forceParse: 0,
			language: 'pt-BR',
			showMeridian: 1
		}).on('changeDate', function(e) {
	        var jsDate = $('#editDataEtapa2').val();
	        jsDate instanceof Date; // -> true
			$( "#editCadDatasEtapa" ).datepicker('setStartDate', jsDate);
			$( "#editCadDatasEtapa" ).datepicker('setDate', jsDate);
	    });;


		$('#editCadDatasEtapa').datepicker({
	        startDate: "<?php echo date("d/m/Y", strtotime($etapa -> DATA_ETAPA));?>",
	        multidate: true,
	        format: "dd/mm/yyyy",
	        daysOfWeekHighlighted: "5,6",
	        //datesDisabled: ['31/08/2017'],
	        language: 'pt-BR'
	    }).on('changeDate', function(e) {
	        $('.count').text(' ' + e.dates.length);
	    });

		$('#chkveg').multiselect({
			includeSelectAllOption: true,
		    maxHeight: 400,
		    //buttonWidth: 300,
		    numberDisplayed: 2,
		    nSelectedText: 'selecionados',
		    nonSelectedText: 'Nada selecionado',
		    allSelectedText: 'Todos selecionados'
		});

		$( ".opcao-peixes" )
			.change(function() {
				$('#editPeixeAlvoEtapa').val($('#chkveg').val());
			}).trigger( "change" );


	});

	$('#editFileEtapa').change(function(){
		var files = document.getElementById('editFileEtapa').files;
		if (files.length > 0) {
			getBase64(files[0]);
		}
	});

	function getBase64(editFileEtapa) {
		var reader = new FileReader();
		var base64_str;
		reader.readAsDataURL(editFileEtapa);
		reader.onload = function () {
			base64_str = reader.result;
			base64_str = base64_str.replace('data:image/jpeg;base64,', "");
			$('#editaImagemEtapa').html('<img src="data:image/jpeg;base64, '+base64_str+'" alt="Logo Etapa" />');
			$('#editCadImagemEtapa').val(base64_str);
		};
		reader.onerror = function (error) {
			console.log('Error: ', error);
		};
	}

	function alteraTipoOrganizacaoRaias(idCadTipoOrgRaias){
		$.post('../ajax/controller.php',{
			acao:"alteraTipoOrganizacaoRaias",
			idTipoOrgRaias:idCadTipoOrgRaias,
			idEtapa:<?php echo $idEtapa;?>
		},function(retorno){
		    if(retorno=='ok'){
		    	/*ok*/
		    }else{
		        alert('Erro ao inserir, informe ao suporte!');
		        alert(retorno);
		    }
		});
	}

	function carregaColetoresDisponiveis(idEtapa){
		$.post('../ajax/controller.php',{
			acao:"retornaColetoresDisponiveisEtapa",
			idEtapa:<?php echo $idEtapa;?>
		},function(retorno){
			$coletores = JSON.parse(retorno);
			$('#listDestinoColetores').empty();

			$.each( $coletores, function( key, data ) {
                $.each( data, function( key, coletor ) {
                	$('#listDestinoColetores').append('<a class="list-group-item clearfix">' + coletor.nome_coletor + '<br><sub> Serial: ' + coletor.serial_coletor + '</sub><span class="pull-right"><span class="btn btn-xs btn-default" onclick="insereComposicaoColetores('+ coletor.id_coletor +'); event.stopPropagation();"><span class="glyphicon glyphicon-plus" ></span></span></span></a>');
            	});
            });
		});
	}

	function carregaColetoresNaEtapa(idEtapa){
		$.post('../ajax/controller.php',{
			acao:"retornaColetoresEtapa",
			idEtapa:<?php echo $idEtapa;?>
		},function(retorno){
			$coletores = JSON.parse(retorno);
			$('#listColetoresCadastradosEtapa').empty();

			$.each( $coletores, function( key, data ) {
                $.each( data, function( key, coletor ) {
					if(coletor.id_cad_fiscal == null){
						$qrCode= '<span class="btn btn-xs btn-danger" data-toggle="modal" href="#modalAux" onclick="informarFiscalColetorEtapa('+ coletor.id_composicao +')"><span class="glyphicon glyphicon-user"></span> N/D</span>';
					}else{
						$qrCode= '<span class="btn btn-xs btn-default" data-toggle="modal" href="#modalAux" onclick="informarFiscalColetorEtapa('+ coletor.id_composicao +')"><span class="glyphicon glyphicon-user"></span> '+ coletor.nome_fiscal +'</span>';
					}

                	$('#listColetoresCadastradosEtapa').append('<a class="list-group-item clearfix">' + coletor.nome_coletor + '<br><sub> Serial: ' + coletor.serial_coletor + '</sub><span class="pull-right">'+$qrCode+'<span class="btn btn-xs btn-default" onclick="excluirComposicaoColetor('+ coletor.id_coletor +'); event.stopPropagation();"><span class="glyphicon glyphicon-remove"></span></span></span></a>');
            	});
            });
		});
	}

	function carregaBateriasEtapa(idEtapa){
		$.post('../ajax/controller.php',{
			acao:"retornaBateriasEtapa",
			idEtapa:<?php echo $idEtapa;?>
		},function(retorno){
			$baterias = JSON.parse(retorno);
			$('#listBateriasCadastradasEtapa').empty();

			$.each( $baterias, function( key, data ) {
				$i = 0;
                $.each( data, function( key, bateria ) {
                	$i++;

                	$('#listBateriasCadastradasEtapa').append('<a class="list-group-item clearfix">Bateria ' + $i + '<br><b> Horarios: ' + bateria.horario_inicio + ' - ' + bateria.horario_termino + '</b><span class="pull-right"><span class="btn btn-xs btn-default" onclick="excluirBateriaEtapa('+ bateria.id_bateria +'); event.stopPropagation();"><span class="glyphicon glyphicon-remove"></span></span></span></a>');
            	});
            });
		});
	}

	function insereComposicaoColetores(idColetor){
        if (confirm("Deseja adicionar esse coletor à etapa selecionada?") == true){
            $.post('../ajax/controller.php',{
            	acao:"insertColetorComposicao",
            	idColetor:idColetor,
            	idEtapa:<?php echo $idEtapa;?>
            },function(retorno){
                if(retorno=='ok'){
                	carregaColetoresDisponiveis(<?php echo $idEtapa;?>);
                	carregaColetoresNaEtapa(<?php echo $idEtapa;?>);
                }else{
                    alert('Erro ao inserir, informe ao suporte!');
                    alert(retorno);
                }
            });
        };
    };

    function insereBateriaEtapa(horario_inicio, horario_termino){
    	if(($('#editCadInicioNovaBateria').val() != '') && ($('#editCadTerminoNovaBateria').val() != '')){
	        if (confirm("Deseja adicionar esse coletor à etapa selecionada?") == true){
	            $.post('../ajax/controller.php',{
	            	acao:"insertBateriaEtapa",
	            	horario_inicio:horario_inicio,
	            	horario_termino:horario_termino,
	            	idEtapa:<?php echo $idEtapa;?>
	            },function(retorno){
	                if(retorno=='ok'){
	                	carregaBateriasEtapa(<?php echo $idEtapa;?>);
	                }else{
	                    alert('Erro ao inserir, informe ao suporte!');
	                    alert(retorno);
	                }
	            });
	        };
	    };
    };

    function insereComposicaoFiscal(idComposicao){
    	idFiscal = $("input[name=optradioFiscal]:checked").val();
    	if(idFiscal != undefined){
	        if (confirm("Deseja associar esse fiscal ao coletor selecionado?") == true){
	            $.post('../ajax/controller.php',{
	            	acao:"insertFiscalComposicao",
	            	idFiscal:idFiscal,
	            	idComposicao:idComposicao
	            },function(retorno){
	                if(retorno=='ok'){
	                	carregaColetoresNaEtapa(<?php echo $idEtapa;?>);
	                	$('#modalAux').modal('hide');
	                }else{
	                    alert('Erro ao inserir, informe ao suporte!');
	                    alert(retorno);
	                }
	            });
	        };
	    }
    };

    function excluirComposicaoColetor(idColetor){
        if (confirm("Deseja excluir esse coletor da etapa selecionada?") == true){
            $.post('../ajax/controller.php',{
            	acao:"deleteColetorComposicao",
            	idColetor:idColetor,
            	idEtapa:<?php echo $idEtapa;?>
            },function(retorno){
                if(retorno=='ok'){
                	carregaColetoresNaEtapa(<?php echo $idEtapa;?>);
                	carregaColetoresDisponiveis(<?php echo $idEtapa;?>);
                }else{
                    alert('Erro ao inserir, informe ao suporte!');
                    alert(retorno);
                }
            });
        };
    };

    function excluirBateriaEtapa(idBateria){
        if (confirm("Deseja excluir essa bateria da etapa selecionada?") == true){
            $.post('../ajax/controller.php',{
            	acao:"deleteBateriaEtapa",
            	idBateria:idBateria,
            	idEtapa:<?php echo $idEtapa;?>
            },function(retorno){
                if(retorno=='ok'){
                	carregaBateriasEtapa(<?php echo $idEtapa;?>);
                }else{
                    alert('Erro ao inserir, informe ao suporte!');
                    alert(retorno);
                }
            });
        };
    };

    function informarFiscalColetorEtapa(idComposicao){
		$('#titleModalAux').html("Selecionar Fiscal de Provas");
		$('#bodyModalAux').empty();

		$.post('../ajax/controller.php',{
        	acao:"retornaFiscaisDisponiveisEtapa",
        	idEtapa:<?php echo $idEtapa;?>
        },function(retorno){
        	$fiscais = JSON.parse(retorno);
            $.each( $fiscais, function( key, data ) {
            	if(data.length > 0){
	                $.each( data, function( key, fiscal ) {
						$('#bodyModalAux').append('<div class="radio"><label><input type="radio" name="optradioFiscal" value="'+fiscal.id_fiscal+'">'+fiscal.nome_fiscal+'<sub> CPF: '+fiscal.cpf_fiscal+'</sub></label></div>');
						$('#footerModalAux').html('<a href="#" data-dismiss="modal" class="btn">Fechar</a>');
						$('#footerModalAux').append('<a href="#" class="btn btn-primary" onclick="insereComposicaoFiscal('+idComposicao+')">Salvar</a>');
	            	});
	            }else{
	            	$('#bodyModalAux').append('Todos os fiscais cadastrados no sistema já estão registrados em etapas!');
	            	$('#footerModalAux').html('<a href="#" data-dismiss="modal" class="btn">Fechar</a>');

	            }
            });
        });


	};


</script>
