<?php
	session_start();

	header('Content-Type: text/html; charset=utf-8');
	include '../functions/conexao.php';
	require '../functions/crud.php';
	$idEtapa = $_REQUEST['idEtapa'];
	$horario = $_REQUEST['horario'];
	$rankingcompleto = $_REQUEST['rankingcompleto'];
	$mostraNome = $_REQUEST['mostranome'];

	if(strlen($horario) <= 10){
		$horario = '';
	}

	$geral = '';
	if ($resultado = retonaResultadoEtapaGeral($idEtapa, $horario)){
		$geral .=  '<td width="15%" align="left"></td>';
		$geral .=  '<td width="25%" align="right">'.$resultado -> total_peixes.'</td>';
		$geral .=  '<td width="25%" align="right">'.$resultado -> total_pesado.' Kg</td>';
		$geral .=  '<td width="35%" align="left"></td>';
	}

	$maiorPeixe = '';

	if ($resultado = retornaResultadoEtapaMaiorPeixe($idEtapa, $horario, 0)){

		$maiorPeixe .= '<td style="white-space: nowrap" style="padding: 10 " align="left">____________________________________</td>';
		$maiorPeixe .= '<td style="white-space: nowrap" style="padding: 10 " align="right">'.$resultado -> peso.' Kg</td>';
		$maiorPeixe .= '<td style="white-space: nowrap" style="padding: 10 " align="left">'.($resultado -> especie).'</td>';
		$maiorPeixe .= '<td style="white-space: nowrap" style="padding: 10 " align="left">'.($resultado -> modalidade).'</td>';
		$maiorPeixe .= '<td style="white-space: nowrap" style="padding: 10 " align="left">'.($resultado -> isca).'</td>';

		$competidores = retornaNomesEquipe($resultado->id_equipe);
		$nomes = '';
		foreach($competidores as $result){
			if($result->id_competidor == $resultado -> ID_CAD_COMPETIDOR){
				switch ($mostraNome) {
				    case 'apelido':
				    	$nomes .= ' - <b>'.$result -> apelido.' ('.$result -> primeiro_nome.')</b>';
				        break;
				    case 'completo':
				        $nomes .= ' - <b>'.$result -> competidor.'</b>';
				        break;
				    case 'primeiro':
				        $nomes .= ' - <b>'.$result -> primeiro_nome.'</b>';
				        break;
				}
			}else{
				switch ($mostraNome) {
				    case 'apelido':
				    	$nomes .= ' - '.$result -> apelido.' ('.$result -> primeiro_nome .')';
				        break;
				    case 'completo':
				        $nomes .= ' - '.$result -> competidor;
				        break;
				    case 'primeiro':
				        $nomes .= ' - '.$result -> primeiro_nome;
				        break;
				}

			}
		}
		$maiorPeixe .= '<td style="white-space: nowrap" style="padding: 10 "  align="left">'.($resultado -> equipe) .$nomes.'</td>';
	}else{
		$maiorPeixe .= '<td style="white-space: nowrap" colspan="5">Sem Resultados</td>';
	}



	$ranking = '';
	if($rankingcompleto=='false'){
		if ($resultado = retornaResultadoEtapaRankingHorario($idEtapa, $horario)){
			$i=count($resultado);
			foreach($resultado as $result){
				$ranking.= '<tr>';
					$ranking.= '<td style="white-space: nowrap " style="padding: 10 " align="right">'.$i--.'</td>';
					$ranking.= '<td style="white-space: nowrap" style="padding: 10 " align="left">_________________________________________________</td>';
					$ranking.= '<td style="white-space: nowrap" style="padding: 10 " align="right">'.$result -> peso.' Kg</td>';
					$ranking.= '<td style="white-space: nowrap" style="padding: 10 " align="right">'.$result -> quantidade.'</td>';

					$competidores = retornaNomesEquipe($result -> id_equipe);
					$nomes = '';
					foreach($competidores as $result1){
						//$nomes .= ' - '.($result1->competidor);
						switch ($mostraNome) {
						    case 'apelido':
						    	$nomes .= ' - <b>'.($result1 -> apelido).'</b> ('.$result1 -> primeiro_nome.')';
						        break;
						    case 'completo':
						        $nomes .= ' - '.($result1 -> competidor);
						        break;
						    case 'primeiro':
						        $nomes .= ' - '.($result1 -> primeiro_nome);
						        break;
						}
					}
					$ranking.= '<td align="left" style="padding: 10 " >'.($result -> equipe) .$nomes.'</td>';
				$ranking.= '</tr>';
			}
		}else{
			$ranking.= '<tr>';
				$ranking.= '<td colspan="5">Sem Resultados</td>';
			$ranking.= '</tr>';
		}
	}else{
		if ($resultado = retornaResultadoEtapaRankingHorarioTudo($idEtapa, $horario)){
			$i=count($resultado);
            //$i=1;
			foreach($resultado as $result){
				$ranking.= '<tr>';
					$ranking.= '<td style="white-space: nowrap" align="right">'.$i++.'</td>';
					$ranking.= '<td style="white-space: nowrap" align="left">_________________________________________________</td>';
					$ranking.= '<td style="white-space: nowrap" align="right">'.$result -> peso.' Kg</td>';
					$ranking.= '<td style="white-space: nowrap" align="right">'.$result -> quantidade.'</td>';

					$competidores = retornaNomesEquipe($result -> id_equipe);
					$nomes = '';
					foreach($competidores as $result1){
						$nomes .= ' - '.($result1 -> competidor);
					}

					$ranking.= '<td align="left">'.($result -> equipe) .$nomes.'</td>';
				$ranking.= '</tr>';
			}
		}else{
			$ranking.= '<tr>';
				$ranking.= '<td colspan="5">Sem Resultados</td>';
			$ranking.= '</tr>';
		}
	}

$html = '
	<h4><b>Dados Gerais sobre Etapa</b></h4>
	<table width="100%" class="bpmTopic">
		<thead>
			<tr>
				<td width="15%" align="left"></td>
				<th width="25%" align="right">Qtd Total Peixes</th>
				<th width="25%" align="right">Peso Total Peixes</th>
				<td width="35%" align="left"></td>

			</tr>
		</thead>
		<tbody id="resultadoTotalEtapa">
			<tr>
				'.$geral.'
			</tr>
		</tbody>
	</table>

	<h4><b>Maior Peixe</b></h4>
	<table width="100%" class="bpmTopic">
		<thead>
			<tr>

				<th width="20%" align="left">Entrega Troféu</th>
				<th width="5%" align="right">Peso</th>
				<th width="10%" align="left">Espécie</th>
				<th width="10%" align="left">Modalidade</th>
				<th width="10%" align="left">Isca</th>
				<th width="35%" align="left">Equipe/Competidores</th>
			</tr>
		</thead>
		<tbody id="resultadoMaiorPeixeEtapa">
			<tr>
				'.$maiorPeixe.'
			</tr>
		</tbody>
	</table>

	<h4><b>Ranking</b></h4>
	<table width="100%" class="bpmTopic">
		<thead>
			<tr>
				<th width="5%" align="right">#</th>
				<th width="20%" align="left">Entrega Troféu</th>
				<th width="10%" align="right">Peso</th>
				<th width="15%" align="right">Peixes</th>
				<th width="50%" align="left"> Equipe/Competidores</th>
			</tr>
		</thead>
		<tbody id="resultadoRankingColocacoes">

			'.$ranking.'

		</tbody>
	</table>

';

	$etapa = retornaEtapa($idEtapa);


	$nomeEtapa = $etapa->DESCRICAO;
	$dataEtapa = $etapa->DATA_ETAPA;
	$campeonato = retornaCampeonato($etapa->ID_CAD_CAMPEONATO);

	$nomeCampeonato = $campeonato->descricao;


$header = '
	<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;">
		<tr>
			<td width="20%" align="left"><img src="../img/logo.png" width="126px" /></td>
			<td width="60%" align="center" ><h2>Apuração de Resultados</h2></td>
			<td width="20%" align="left"></td>
		</tr>
		<tr>
			<td colspan="3" width="100%" align="center" ><h4>'.($nomeCampeonato).' - '.($nomeEtapa). ' - ' . $dataEtapa .'</h4></td>

		</tr>
	</table>
';
$footer = '<div style="border-top: 1px solid #000000; font-size: 9pt; text-align: justify; padding-top: 3mm; ">Este documento pode conter informação confidencial ou privilegiada, sendo seu sigilo protegido. Se você não for o destinatário ou a pessoa autorizada a receber este documento, não pode usar, copiar ou divulgar as informações nele contidos ou tomar qualquer ação baseada nessas informações. Se você recebeu este documento por engano, por favor, destrua-o imediatamente. Agradecemos sua cooperação.<br><div style="font-size: 9pt; text-align: center; padding-top: 3mm; ">Página {PAGENO} de {nb}</div></div>';


//==============================================================
//==============================================================
//==============================================================
include("../imprimir/mpdf.php");

function tirarAcentos($string){
    return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
}

$mpdf=new mPDF('utf-8','A4-L','','','15','15','28','30');

    /*
    $mpdf = new mPDF('',    // mode - default ''
     '',    // format - A4, for example, default ''
     0,     // font size - default 0
     '',    // default font family
     15,    // margin_left
     15,    // margin right
     16,     // margin top
     16,    // margin bottom
     9,     // margin header
     9,     // margin footer
     'L');  // L - landscape, P - portrait
     */

$mpdf->SetHTMLHeader($header);


$mpdf->SetHTMLFooter($footer);

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents('../imprimir/examples/mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,2);

$mpdf->Output('mpdf.pdf','I');
exit;
//==============================================================
//==============================================================
//==============================================================


?>
