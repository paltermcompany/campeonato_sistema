	<script type="text/javascript" src="../js/custom.js"></script>
	<div class="panel-body">		
		<form role='form' method='post' action='' name='form_newCadModalidade'>
			<fieldset>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label>Nome Modalidade</label>
							<input id="newCadNomeModalidade" name="newCadNomeModalidade" class="form-control upcase" placeholder="Nome Modalidade" required>
						</div>
					</div>
				</div>
			</fieldset>	
			<div class="span6 pull-right">
				<button id="botaoInsereModalidade" name="botaoInsereModalidade" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
			</div>
		</form>
	</div>	