<?php
	include_once '../functions/conexao.php';
	include_once '../functions/crud.php';	
?>

<script type="text/javascript" src="../js/custom.js"></script>

<div id="propria">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Usuários e Permissões</h2>
		</div>
	</div> 
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="col-lg-2">
						<table class="table table-striped table-hover" id="tableListaEtapas" name="tableListaEtapas">
							<thead>
								<th>Usuário <button onClick="naoImplementado()" type="button" class="close glyphicon glyphicon-plus text-muted" ><span aria-hidden="true"></span></button></th>
							</thead>
							<tbody>
							<?php 
							$resultado = retornaUsuariosSistema();							
							foreach ($resultado as $result){
								if ($result -> ativo == 0){
									$badge = ' <span class="label label-danger label-as-badge">Usuário desativado</span>';
								}else{
									$badge = '';
								}
								?>
								
								<tr class="odd gradeX">
									<td class="col-md-10" onClick="retornaPermissoes('<?php echo $result -> id?>')" style="cursor:pointer"><?php echo $result -> nome . $badge?></td>
								</tr>
							<?php
							}	
							?>
							</tbody>
						</table>
					</div>
					<div class="col-lg-10">
						<div class="panel-body">
							<div name="tabelaPermissoes" id="tabelaPermissoes">
							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
