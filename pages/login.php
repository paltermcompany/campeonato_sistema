<?php
	session_start();
	//session_destroy();
	header('Content-Type: text/html; charset=utf-8');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="author" content="Douglas Lessing">
	<meta charset="UTF-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Campeonatos FishTV</title>
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../js/custom.js"></script>
	<link rel="shortcut icon" type="image/ico" href="../favicon.ico"/>

</head>
<body>
	<div class="container">
		<div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
						<div class='text-center'><img src='../img/logo.png' style="width: 200px" alt='Logo'></div><br>
                        <h3 class="panel-title text-center">Informe as credenciais de acesso:</h3>
                    </div>
                    <div class="panel-body">
						<form role="form" name="form_login" id="form_login" method="post" action="" >
                            <fieldset>
                                <div class="form-group">
                                    <div class="retornar" id="retornar" > </div>
									<input type="text" name="login" id="login" class="form-control" placeholder="Login" required autofocus>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="senha" id="senha" class="form-control" style="" placeholder="Senha" required>
                                </div>
								<button class="btn btn-lg btn-block btn-primary " style="margin-top:5px;background-color:#005081;" type="submit">
									Logar
								</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
