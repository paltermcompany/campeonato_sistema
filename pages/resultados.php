<?php
	session_start();
	include '../functions/conexao.php';
	require '../functions/crud.php';
	header('Content-Type: text/html; charset=utf-8');
?>

<style>

td.details-control {
    background: url('../img/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('../img/details_close.png') no-repeat center center;
}
</style>

<script type="text/javascript" src="../js/custom.js"></script>
<script type="text/javascript" src="../bower_components/bootstrap-daterangepicker-master/moment.js"></script>
<script type="text/javascript" src="../bower_components/bootstrap-daterangepicker-master/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../bower_components/bootstrap-daterangepicker-master/daterangepicker.css" />

<script>

    $(document).ready(function() {
        filtraEtapasComboResultados($( '#filtroEquipeCampeonatoResultado option:selected' ).val());

        formata($('#dataNews2Cad').val());

        idEtapa = 0;


        horario = '';
        mostraNome = 'apelido';
        rankingCompleto = false;

        var tableResultadosRankingFinal = $('#tableResultadosRankingFinal').DataTable( {

            "ajax": {
                    url: "../ajax/controller.php",
                    type: "POST",
                    data : function(d){
                        d.acao = "resultadosEtapaRankingSemEntregaTrofeu",
                        d.rankingCompleto = rankingCompleto,
                        d.idEtapa = idEtapa,
                        d.horario = horario,
                        d.mostraNome = mostraNome;
                    },
                    beforeSend : function() {
                        $('#resultadoRankingColocacoes').html('<tr><td colspan="6"><center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center></td><tr>');

                    },
                    complete: function() {
                      //alert("carregou");
                    }
                },
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {"data": "colocacao"},
                {"data": "peso"},
                {"data": "quantidade"},
                {"data": "equipe"}
            ],
            "language": {
                "url": "../bower_components/datatables/Portuguese-Brasil.json"
            },
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "destroy": true,
            "ordering": false,
            "info": false,
            "autoWidth": false
        } );

        var tableResultadosMaiorPeixe = $('#tableResultadosMaiorPeixe').DataTable( {
            "ajax": {
                    url: "../ajax/controller.php",
                    type: "POST",
                    data : function(d){
                        d.acao = "resultadosEtapaMaiorPeixeSemEntregaTrofeu",
                        d.idEtapa = idEtapa,
                        d.horario = horario,
                        d.mostraNome = mostraNome;
                    },
                    beforeSend : function() {
                        $('#resultadoMaiorPeixeEtapa').html('<tr><td colspan="6"><center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center></td><tr>');
                    },
                    complete: function() {
                      //alert("carregou");
                    }
                },
            "columns": [
                /*{
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },*/
                {"data": "peso"},
                {"data": "especie"},
                {"data": "isca"},
                {"data": "modalidade"},
                {"data": "competidores"}
            ],
            "language": {
                "url": "../bower_components/datatables/Portuguese-Brasil.json"
            },
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "destroy": true,
            "ordering": false,
            "info": false,
            "autoWidth": false
        } );

        var tableResultadosGeral = $('#tableResultadosGeral').DataTable( {
            "ajax": {
                    url: "../ajax/controller.php",
                    type: "POST",
                    data : function(d){
                        d.acao = "resultadosEtapa",
                        d.idEtapa = idEtapa,
                        d.horario = horario
                    },
                    beforeSend : function() {
                        $('#resultadoTotalEtapa').html('<tr><td colspan="6"><center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center></td><tr>');
                    },
                    complete: function() {
                      //alert("carregou");
                    }
                },
            "columns": [
                /*{
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },*/
                {"data": "total_quantidade"},
                {"data": "total_peso"}
            ],
            "language": {
                "url": "../bower_components/datatables/Portuguese-Brasil.json"
            },
            "processing": true,
            "serverSide": false,
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "destroy": true,
            "ordering": false,
            "info": false,
            "autoWidth": false
        } );

        $('#tableResultadosRankingFinal tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = tableResultadosRankingFinal.row( tr );

            var CurItemIndex = tableResultadosRankingFinal.row( $(this).parents('tr') ).index();;

            var CurTableIndexs = tableResultadosRankingFinal.rows().indexes();
            var CurIndexArrayKey = CurTableIndexs.indexOf( CurItemIndex );

            linhasResultados = $('#tableResultadosRankingFinal tr.odd').length;
            linhasResultados += $('#tableResultadosRankingFinal tr.even').length;
            //alert(linhasResultados);

            //var primeiroItemIndex = CurTableIndexs[0];
            var primeiroItemIndex = CurTableIndexs[linhasResultados-1];
            var rowPrimeira = tableResultadosRankingFinal.rows( primeiroItemIndex );

            //var segundoItemIndex = CurTableIndexs[1];
            var segundoItemIndex = CurTableIndexs[linhasResultados-2];
            var rowSegunda = tableResultadosRankingFinal.rows( segundoItemIndex );

            //var terceiroItemIndex = CurTableIndexs[2];
            var terceiroItemIndex = CurTableIndexs[linhasResultados-3];
            var rowTerceira = tableResultadosRankingFinal.rows( terceiroItemIndex );

            //var quartoItemIndex = CurTableIndexs[3];
            var quartoItemIndex = CurTableIndexs[linhasResultados-4];
            var rowQuarta = tableResultadosRankingFinal.rows( quartoItemIndex );

            /*var PrevItemIndex = CurTableIndexs[ CurIndexArrayKey - 1];
            var NextItemIndex = CurTableIndexs[ CurIndexArrayKey + 1];

            var rowAnterior = tableResultadosRankingFinal.rows( PrevItemIndex );
            var rowProxima = tableResultadosRankingFinal.rows( NextItemIndex );*/


            if ( row.child.isShown() ) {
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                row.child( format(row.data(), rowPrimeira.data()[0], rowSegunda.data()[0], rowTerceira.data()[0], rowQuarta.data()[0]) ).show();
                tr.addClass('shown');
            }
        } );


        $('.botaoBuscaResultados').on('click', function () {
            idEtapa = $('#filtroEquipeEtapaResultado option:selected').val();

            $.post('../ajax/controller.php',{
                acao:"unidadeMedidaEtapa",
                idEtapa:idEtapa
            },function(retorno){
                retorno = JSON.parse(retorno);
                $.each( retorno, function( key, value ) {
                    document.cookie = "cookieDescricaoUnidadeMedida="+value.descricao;
                    document.cookie = "cookieAbreviacaoUnidadeMedida="+value.abreviacao;
                });
            });

            checarhorario = $('#dataNews2Cad').val();
            if(checarhorario != ''){
                horario = $('#dataNewsCad').val();
            }else{
                horario = '';
            }
            if($("#checkTodosRankings").is(':checked')){
                rankingCompleto = true;
            }else{
                rankingCompleto = false;
            }
            if($("#checkNomeCompleto").is(':checked')){
                mostraNome = "completo";
            }else{
                if($("#checkPrimeiroNome").is(':checked')){
                    mostraNome = "primeiro";
                }else{
                    if($("#checkApelido").is(':checked')){
                        mostraNome = "apelido";
                    }
                }
            }

            tableResultadosGeral.ajax.reload();
            tableResultadosMaiorPeixe.ajax.reload();
            tableResultadosRankingFinal.ajax.reload();

        } );

    });

    function formata($data){
        $('.form_datetime2').datetimepicker({
            weekStart: 0,
            startDate: $data + ' 00:00:00',
            initialDate: $data,
            todayBtn:  false,
            autoclose: 1,
            format: 'hh:ii',
            minuteStep: 5,
            todayHighlight: false,
            startView: 1,
            minView: 0,
            maxView: 1,
            forceParse: 0,
            language: 'pt-BR',
            daysOfWeekDisabled: [],
            showMeridian: false
        });
        $('.form_datetime2').datetimepicker('update');
        $('.form_datetime2').datetimepicker('setStartDate', $data + ' 00:00:00');
        $('.form_datetime2').datetimepicker('setEndDate', $data + ' 23:59:59');
    };


    function format(d, p, s, t, q){
        checarhorario = $('#dataNews2Cad').val();
        if(checarhorario != ''){
            horario = $('#dataNewsCad').val();
        }else{
            horario = '';
        }
        tabela = '';

        var peso = (parseFloat(d.peso.replace(',','.')));
        var pesoP = (parseFloat(p.peso.replace(',','.')));
        var pesoS = (parseFloat(s.peso.replace(',','.')));
        var pesoT = (parseFloat(t.peso.replace(',','.')));
        var pesoQ = (parseFloat(q.peso.replace(',','.')));

        $.post('../ajax/controller.php',{
            acao:"resultadosLancamentosEtapa",
            idEtapa:d.id_etapa,
            equipe:d.id_equipe,
            horario:horario,
            modalidade:0,
            isca:0
        },function(retorno){
            retorno = JSON.parse(retorno);

            $.each( retorno, function( key0, value0 ) {

                var arrayMaioresExemplares = $.map(value0, function(value, index) { return [value]; });
                arrayMaioresExemplares.sort(function(a, b) {return parseFloat(b.peso) - parseFloat(a.peso)});
                arrayMaioresExemplares.length = 3;

                $.each( value0, function( key, value ) {
                    if(arrayMaioresExemplares.find(x => x.comprovante === value.comprovante)){
                        $achou = 'bg-primary';
                    }else{
                        $achou = '';
                    }

                    tabela +=   '<div class="row '+$achou+'">'+
                                    '<div class="col-md-1" style="white-space:nowrap"><div class="pull-right">...'+value.comprovante.slice(-10)+'</div></div>'+
                                    '<div class="col-md-3" style="white-space:nowrap">'+value.competidor+'</div>'+
                                    '<div class="col-md-1" style="white-space:nowrap"><div class="pull-right">'+value.hora+'</div></div>'+
                                    '<div class="col-md-2" style="white-space:nowrap">'+value.especie+'</div>'+
                                    '<div class="col-md-2" style="white-space:nowrap">'+value.modalidade+'</div>'+
                                    '<div class="col-md-2" style="white-space:nowrap">'+value.isca+'</div>'+
                                    '<div class="col-md-1" style="white-space:nowrap"><div class="pull-right">'+(value.peso).replace('.', ',')+' <?php echo $_COOKIE['cookieAbreviacaoUnidadeMedida'];?></div></div>'+
                                '</div>';
                });
            });

            tabela =    '<div class="col-md-12 well well-sm" >'+
                            '<div class="row">'+
                                '<div class="col-md-2"><b>Colocação Atual: </b>'+d.colocacao+'</div>'+
                                '<div class="col-md-2"><b>Diferença para Primeiro: </b>'+(pesoP - peso).toFixed(3).replace('.', ',')+' Kg</div>'+
                                '<div class="col-md-2"><b>Diferença para Segundo: </b>'+(pesoS - peso).toFixed(3).replace('.', ',')+' Kg</div>'+
                                '<div class="col-md-2"><b>Diferença para Terceiro: </b>'+(pesoT - peso).toFixed(3).replace('.', ',')+' Kg</div>'+
                                '<div class="col-md-4"><b>Diferença para Quarto: </b>'+(pesoQ - peso).toFixed(3).replace('.', ',')+' Kg</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-12 well well-sm" >'+
                            '<div class="row">'+
                                '<div class="col-md-1"><div class="pull-right"><b>Comprovante</b></div></div>'+
                                '<div class="col-md-3"><b>Competidor</b></div>'+
                                '<div class="col-md-1"><div class="pull-right"><b>Hora</b></div></div>'+
                                '<div class="col-md-2"><b>Espécie</b></div>'+
                                '<div class="col-md-2"><b>Modalidade</b></div>'+
                                '<div class="col-md-2"><b>Isca</b></div>'+
                                '<div class="col-md-1"><div class="pull-right"><b><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?></b></div></div>'+
                            '</div>'+
                                tabela +
                        '</div>';

            $('#rankingDetalheGeral').html(tabela);
        });

        return '<div id="rankingDetalheGeral"></div>';
    }

    function filtraEtapasComboResultados(idCampeonato){
        $('#filtroEquipeEtapaResultado').html('');
        $.post('../ajax/controller.php',{
            acao:"listaEtapas",
            idCampeonato:idCampeonato
        },function(retorno){
            $('#filtroEquipeEtapaResultado').html(retorno);
            pegaDataEtapa($( '#filtroEquipeEtapaResultado option:selected' ).val());
        });
    }

    function pegaDataEtapa(idEtapa){
        $.post('../ajax/controller.php',{
            acao:"pegaDataEtapa",
            idEtapa:idEtapa
        },function(retorno){
            $('#dataNews2Cad').val();
            $('#dataNewsCad').val(retorno);
            formata(retorno);
        });
    }
</script>

<style>

.barraBotoes {
  float: right;
  margin-right:5px;
  font-size: 21px;
  font-weight: bold;
  line-height: 1;
  color: #000;
  text-shadow: 0 1px 0 #fff;
  filter: alpha(opacity=20);
  opacity: .2;
  -webkit-appearance: none;
  padding: 0;
  cursor: pointer;
  background: transparent;
  border: 0;

</style>

<div id="propria">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Resultados Etapa <button onClick="imprimiResultadoEtapas($('#filtroEquipeEtapaResultado option:selected').val(),$('#dataNewsCad').val(), $('#checkTodosRankings').is(':checked'), $('input[name=\'mostranome\']:checked').val() )" type="button" class="barraBotoes glyphicon glyphicon-print text-muted" ></button></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form name="form_filtraResultados" id="form_filtraResultados" method="post" >
                <div class="well form-group">
                    <h4>Filtros</h4>
                    <div class="row">
                        <div class="col-lg-3">
                            <label>Campeonato</label>
                            <select id="filtroEquipeCampeonatoResultado" name="filtroEquipeCampeonatoResultado" class="form-control" onchange="filtraEtapasComboResultados($( '#filtroEquipeCampeonatoResultado option:selected' ).val())">
                                <?php
                                    $campeonatos = retornaCampeonatos('','');
                                    foreach ($campeonatos as $result){
                                        echo '<option value="'.$result->id.'">'.$result->descricao.'</option>';
                                    }
                                 ?>
                            </select>
                        </div>

                        <div class="col-lg-2">
                            <label>Etapa</label>
                            <select id="filtroEquipeEtapaResultado" name="filtroEquipeEtapaResultado" class="form-control" onchange="pegaDataEtapa($( '#filtroEquipeEtapaResultado option:selected' ).val())">

                            </select>
                        </div>

                        <div class="col-lg-2">
                            <label for="dataNewsCad">Simular Horário</label>
                            <div class="input-group date time form_datetime2" data-link-field="dataNewsCad">
                                <input type="text" name="dataNews2Cad" id="dataNews2Cad" class="form-control" type="text" readonly value="">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-remove" ></span></span>
                                <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                            </div>
                            <input type="hidden" name="dataNewsCad" id="dataNewsCad" value="<?php echo date('Y-m-d H:i:s', time()); ?>"/>
                        </div>

                        <div class="col-lg-2">
                            <div class="checkbox">
                                <label><input id="checkTodosRankings" type="checkbox">Exibir Ranking Completo</label>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="radio">
                                <label><input name="mostranome" id="checkNomeCompleto" type="radio" value='completo'>Exibir Nome Completo</label>
                                <label><input name="mostranome" id="checkPrimeiroNome" type="radio" value='primero'>Exibir Primeiro Nome</label>
                                <label><input name="mostranome" id="checkApelido" checked type="radio" value='apelido'>Exibir Apelido</label>
                            </div>
                        </div>

                        <div class="col-lg-1 pull-right">
                            <div class="input-group">
                                <!--<button class="btn btn-primary" name="buttonFiltraResultados" id="buttonFiltraResultados" style="background-color:#005081" type="button" onclick="filtraEtapasResultado()"><b>Filtrar</b></button>-->
                                <button class="btn btn-primary botaoBuscaResultados" name="buttonFiltraResultados" id="buttonFiltraResultados" style="background-color:#005081" type="button"><b>Filtrar</b></button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>


            <table id="tableResultadosGeral" class="table table-striped table-bordered table-hover" >
                <thead>
                    <tr>
                        <th colspan="3"><h3><b>Dados Gerais sobre Etapa</b></h3></th>
                    </tr>
                    <tr>
                        <!--<th></th>-->
                        <th>Qtd Total Peixes</th>
                        <th><bdi><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?></bdi> Total Peixes</th>
                    </tr>
                </thead>
                <tbody id="resultadoTotalEtapa">
                    <!--RESULTADO GERAL-->
                </tbody>
            </table>

            <table id="tableResultadosMaiorPeixe" class="table table-striped table-bordered table-hover" >
                <thead>
                    <tr>
                        <th colspan="6"><h3><b>Maior Peixe</b></h3></th>
                    </tr>
                    <tr>
                        <!--<th></th>-->
                        <th><bdi><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?></bdi></th>
                        <th>Espécie</th>
                        <th>Isca</th>
                        <th>Modalidade</th>
                        <th>Equipe/Competidores</th>
                    </tr>
                </thead>
                <tbody id="resultadoMaiorPeixeEtapa">
                    <!--RESULTADO MAIOR PEIXE-->

                </tbody>
            </table>

            <table id="tableResultadosRankingFinal" class="table table-striped table-bordered table-hover" >
                <thead>
                    <tr >
                        <th colspan="5"><h3><b>Ranking</b></h3></th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>Colocação</th>
                        <th><bdi><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?></bdi> total</th>
                        <th>Quantidade Capturas</th>
                        <th>Equipe/Competidores</th>
                    </tr>
                </thead>
                <tbody id="resultadoRankingColocacoes">
                    <!--RESULTADO RANKING-->
                </tbody>
            </table>
        </div>
    </div>
</div>


