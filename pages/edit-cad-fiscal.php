<?php
    include '../functions/conexao.php';
    require '../functions/crud.php';
    header('Content-Type: text/html; charset=utf-8');
    $idFiscal = $_REQUEST['idFiscal'];
    $fiscal = retornaFiscal($idFiscal);
?>
    <script type="text/javascript" src="../js/custom.js"></script>
    <div class="panel-body">
        <form role='form' method='post' action='' name='form_editCadFiscal'>
            <fieldset>
                <input type="hidden" name="editCadIdFiscal" id="editCadIdFiscal" value="<?php echo $fiscal -> id;?>">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Nome Fiscal</label>
                            <input id="editCadNomeFiscal" name="editCadNomeFiscal" class="form-control upcase" placeholder="Nome Fiscal" required value="<?php echo $fiscal -> nome;?>">
                        </div>

                        <div class="form-group">
                            <label>CPF Fiscal</label>
                            <input id="editCadCPFFiscal" name="editCadCPFFiscal" class="form-control upcase" placeholder="CPF Fiscal" required value="<?php echo $fiscal -> cpf;?>">
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="span6 pull-right">
                <button id="botaoEditaFiscal" name="botaoEditaFiscal" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
            </div>
        </form>
    </div>
