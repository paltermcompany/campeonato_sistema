<?php
    session_start();
    include '../functions/conexao.php';
    require '../functions/crud.php';

?>

<script type="text/javascript" src="../js/custom.js"></script>

<script>
    var tableFiltraFiscais;

    function filtraFiscais(){
        tableFiltraFiscais.ajax.reload();
    }

    $(document).ready(function() {
        tableFiltraFiscais = $('#tableFiltraFiscais').DataTable({
            "ajax": {
                url: "../ajax/controller.php",
                type: "POST",
                data :function(d){
                    d.acao = "carregaFiscais",
                    d.filtroFiscalNome = $('#filtroFiscalNome').val()
                },
                beforeSend : function() {
                    $('#buttonFiltraFiscais').html('Aguarde').attr('disabled', true);
                },
                complete : function() {
                    $('#buttonFiltraFiscais').html('Filtrar').attr('disabled', false);
                }
            },
            "columns": [
                {"data": "id_fiscal"},
                {"data": "nome_fiscal"},
                {
                    "orderable": false,
                    "defaultContent": '<div class="btn-group btn-group-xs"><button type="button" class="btn btn-warning btnAlteraFiscal"><span class="glyphicon glyphicon-edit"></span> </button><button type="button" class="btn btn-danger" onclick="naoImplementado()"><span class="glyphicon glyphicon-remove"></span></button></div>'
                },
            ],
            "language": {
                "url": "../bower_components/datatables/Portuguese-Brasil.json"
            },
            "paging": true,
            "lengthChange": true,
            "searching": false,
            "destroy": true,
            "ordering": true,
            "order": [[ 1, "asc" ]],
            "info": true,
            "autoWidth": false
        } );
        $('#tableFiltraFiscais tbody').on( 'click', 'button', '.btnAlteraFiscal', function () {
            var data = tableFiltraFiscais.row( $(this).parents('tr') ).data();
            console.log(data);
            abreModalEditaFiscal(data.id_fiscal);
        } );
    });
</script>

<div id="propria">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cadastro de Fiscais  <button onClick="abreModalNovoFiscal()" type="button" class="close glyphicon glyphicon-plus text-muted" ><span aria-hidden="true"></span></button></h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <form name="form_filtraFiscais" id="form_filtraFiscais" method="post" >
                <div class="well form-group">
                    <h4>Filtros</h4>
                    <div class="row">
                        <div class="col-lg-11">
                            <label>Descrição</label>
                            <input type="text" id="filtroFiscalNome" name="filtroFiscalNome" class="form-control upcase">
                        </div>

                        <div class="col-lg-1 pull-right">
                        <label></label>
                            <div class="input-group">
                                <button class="btn btn-primary" name="buttonFiltraFiscais" id="buttonFiltraFiscais" style="background-color:#005081" type="button" onclick="filtraFiscais()"><b>Filtrar</b></button>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
            <div class="row" >
                <div class="col-lg-12">
                    <table id="tableFiltraFiscais" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Descrição</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody id="resultadoFiltroFiscais">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

