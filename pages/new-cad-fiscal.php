    <script type="text/javascript" src="../js/custom.js"></script>
    <div class="panel-body">
        <form role='form' method='post' action='' name='form_newCadFiscal'>
            <fieldset>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Nome Fiscal</label>
                            <input id="newCadNomeFiscal" name="newCadNomeFiscal" class="form-control upcase" placeholder="Nome Fiscal" required>
                        </div>
                        <div class="form-group">
                            <label>CPF Fiscal</label>
                            <input id="newCadCPFFiscal" name="newCadCPFFiscal" class="form-control upcase" placeholder="CPF Fiscal" required>
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="span6 pull-right">
                <button id="botaoInsereFiscal" name="botaoInsereFiscal" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
            </div>
        </form>
    </div>
