	<script type="text/javascript" src="../js/custom.js"></script>
	<div class="panel-body">		
		<form role='form' method='post' action='' name='form_newCadCampeonado'>
			<fieldset>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<label>Nome Campeonado</label>
							<input id="newCadNomeCampeonado" name="newCadNomeCampeonado" class="form-control upcase" placeholder="Nome Campeonado" required>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="form-group">
							<label>Nome Campeonado (Impressão App)</label>
							<input id="newCadNomeCampeonadoApp" name="newCadNomeCampeonadoApp" class="form-control upcase" placeholder="Nome Campeonado" required>
						</div>
					</div>					
					<div class="col-lg-12">
						<div class="form-group">
							<label>Imagem</label><br>
							Selecione uma imagem: <input id="file" type="file" name="img">
							<div id="imagemCampeonado">
							
							</div>
							<input type="hidden" id="newCadImagemCampeonado" value="" name="newCadImagemCampeonado">
						</div>
					</div>
				</div>
			</fieldset>	
			<div class="span6 pull-right">
				<button id="botaoInsereCampeonado" name="botaoInsereCampeonado" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
			</div>
		</form>
	</div>	
	
	<script>
		
		$('#file').change(function(){
			var files = document.getElementById('file').files;
			if (files.length > 0) {
				getBase64(files[0]);
			}
		});
		
		function getBase64(file) {
			var reader = new FileReader();
			var base64_str;
			reader.readAsDataURL(file);
			reader.onload = function () {
				base64_str = reader.result;
				base64_str = base64_str.replace('data:image/jpeg;base64,', "");
				$('#imagemCampeonado').html('<img src="data:image/jpeg;base64, '+base64_str+'" alt="Logo Campeonato" />');
				$('#newCadImagemCampeonado').val(base64_str);

			};
			reader.onerror = function (error) {
				console.log('Error: ', error);
			};
		}
	</script>