<?php
    session_start();
    header('Content-Type: text/html; charset=utf-8');
    include '../functions/conexao.php';
    require '../functions/crud.php';
?>
<html>
<body>
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <script src="../dist/js/sb-admin-2.js"></script>
    <script src="../js/custom.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/custom.js"></script>
<script>
    var tableResultadosGeral;
    var tableResultadosMaiorPeixe;
    var tableResultadosRankingFinal;

	$( document ).ready(function() {
            idEtapa = $( '#idEtapaAcompanhamentoLive').val();

            $.post('../ajax/controller.php',{
                acao:"unidadeMedidaEtapa",
                idEtapa:idEtapa
            },function(retorno){
                retorno = JSON.parse(retorno);
                $.each( retorno, function( key, value ) {
                    document.cookie = "cookieDescricaoUnidadeMedida="+value.descricao;
                    document.cookie = "cookieAbreviacaoUnidadeMedida="+value.abreviacao;
                });
            });

            tableResultadosGeral = $('#tableResultadosGeral').DataTable( {
                "ajax": {
                        url: "../ajax/controller.php",
                        type: "POST",
                        data : function(d){
                            d.acao = "resultadosEtapa",
                            d.idEtapa = idEtapa,
                            d.horario = ""
                        }
                    },
                "columns": [
                    {
                        "className": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {"data": "total_quantidade"},
                    {"data": "total_peso"}
                ],
                "language": {
                    "url": "../bower_components/datatables/Portuguese-Brasil.json"
                },
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "destroy": true,
                "ordering": false,
                "info": false,
                "autoWidth": false
            } );


            tableResultadosMaiorPeixe = $('#tableResultadosMaiorPeixe').DataTable( {
                "ajax": {
                        url: "../ajax/controller.php",
                        type: "POST",
                        data : function(d){
                            d.acao = "resultadosEtapaMaiorPeixeSemEntregaTrofeu",
                            d.idEtapa = idEtapa,
                            d.horario = "",
                            d.mostraNome = "apelido";
                        }
                    },
                "columns": [
                    {
                        "className": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {"data": "peso"},
                    {"data": "especie"},
                    {"data": "isca"},
                    {"data": "modalidade"},
                    {"data": "competidores"}
                ],
                "language": {
                    "url": "../bower_components/datatables/Portuguese-Brasil.json"
                },
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "destroy": true,
                "ordering": false,
                "info": false,
                "autoWidth": false
            } );

            tableResultadosRankingFinal = $('#tableResultadosRankingFinal').DataTable( {
                "ajax": {
                        url: "../ajax/controller.php",
                        type: "POST",
                        data : function(d){
                            d.acao = "resultadosEtapaRankingSemEntregaTrofeu",
                            d.rankingCompleto = false,
                            d.idEtapa = idEtapa,
                            d.horario = "",
                            d.mostraNome = "apelido";
                        }
                    },
                "columns": [
                    {
                        "className": 'details-control',
                        "orderable": false,
                        "data": null,
                        "defaultContent": ''
                    },
                    {"data": "colocacao"},
                    {"data": "peso"},
                    {"data": "quantidade"},
                    {"data": "equipe"}
                ],
                "language": {
                    "url": "../bower_components/datatables/Portuguese-Brasil.json"
                },
                "paging": false,
                "lengthChange": true,
                "searching": false,
                "destroy": true,
                "ordering": false,
                "info": false,
                "autoWidth": false
            } );


            filtraEtapasResultadoLive();
            filtraDetalhes();
            g_iCount = 30;
            startCountdown();
	});

	function startCountdown(){
            if((g_iCount - 1) >= 0){
                g_iCount = g_iCount - 1;

                $('#numberCountdown').html(' atualiza em: ' + g_iCount);
                setTimeout('startCountdown()',1000);
                if(g_iCount==0){
                    checkPrevias();
                    console.log('checkando prévias');
                }
            }
	}

	$primeiraVez = true;
	var g_iCount = new Number();

	function filtraEtapasResultadoLive(){
            idEtapa = $('#idEtapaAcompanhamentoLive').val();

            if(idEtapa!=''){
                $.post('../ajax/controller.php',{
                    acao:"acompanhamentoLive",
                    idEtapa:idEtapa,
                    ultimaVerificacao: $('#ultimaVerificacao').val(),
                    idUltimaVerificacao: $('#idUltimaVerificacao').val()
                },function(retorno){
                    if(retorno!=''){
                        $( ".linhaLive" ).removeClass( 'invalid' );
                        $( ".linhaLive" ).removeClass( 'excluido' );
                    }
                    $('#acompanhamentoLive').prepend(retorno);
                    if($primeiraVez){
                        console.log('primeira vez');
                        checkPrevias();
                        $primeiraVez = false;
                    }
                });
            }
	}

	function filtraDetalhes(){
            idEtapa = $('#idEtapaAcompanhamentoLive').val();
            if(idEtapa!=''){
                $.post('../ajax/controller.php',{
                    acao:"detalhesModalidade",
                    idEtapa:idEtapa
                },function(retorno){
                    $('#resultadoDetalhesModalidade').html(retorno);
                });

                $.post('../ajax/controller.php',{
                    acao:"detalhesEspecie",
                    idEtapa:idEtapa
                },function(retorno){
                    $('#resultadoDetalhesEspecie').html(retorno);
                });

                $.post('../ajax/controller.php',{
                    acao:"detalhesIsca",
                    idEtapa:idEtapa
                },function(retorno){
                    $('#resultadoDetalhesIsca').html(retorno);
                });

                $.post('../ajax/controller.php',{
                    acao:"detalhesHora",
                    idEtapa:idEtapa
                },function(retorno){
                    $('#resultadoDetalhesHora').html(retorno);
                });
            }
	}

	function exibeAcomLive(){
            $( "#previas" ).addClass( 'hidden' );
            $( "#live" ).removeClass( 'hidden' );
            $( "#detalhes" ).addClass( 'hidden' );
	}

	function exibeAcomPrevias(){
            $( "#previas" ).removeClass( 'hidden' );
            $( "#live" ).addClass( 'hidden' );
            $( "#detalhes" ).addClass( 'hidden' );
	}

	function exibeAcomDetalhes(){
            $( "#previas" ).addClass( 'hidden' );
            $( "#live" ).addClass( 'hidden' );
            $( "#detalhes" ).removeClass( 'hidden' );
	}

	function filtraEtapasResultadoDashboard(){
            tableResultadosGeral.ajax.reload();
            tableResultadosMaiorPeixe.ajax.reload();
            tableResultadosRankingFinal.ajax.reload();

		/*idEtapa = $( '#idEtapaAcompanhamentoLive').val();
		if(idEtapa!=''){


			$('#resultadoPreviaEtapa').html('');
			$.post('../ajax/controller.php',{
				acao:"resultadosEtapa",
				idEtapa:idEtapa
			},function(retorno){
				$('#resultadoPreviaEtapa').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
				$('#resultadoPreviaEtapa').html(retorno);
			});*/

			/*$('#resultadoPreviaMaiorPeixeEtapa').html('');
			$.post('../ajax/controller.php',{
				acao:"resultadosEtapaMaiorPeixeSemEntregaTrofeu",
				idEtapa:idEtapa
			},function(retorno){
				$('#resultadoPreviaMaiorPeixeEtapa').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
				$('#resultadoPreviaMaiorPeixeEtapa').html(retorno);
			});*/

			/*$('#resultadoPreviaRankingColocacoes').html('');
			$.post('../ajax/controller.php',{
				acao:"resultadosEtapaRankingSemEntregaTrofeu",
				idEtapa:idEtapa,
				horario:''
			},function(retorno){
				$('#resultadoPreviaRankingColocacoes').html('<center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center>');
				$('#resultadoPreviaRankingColocacoes').html(retorno);
			});
		}*/
	}

	var refreshId = setInterval(function(){ checkAcompanhamentoLive(); }, 5000);

	function checkAcompanhamentoLive() {
            filtraEtapasResultadoLive();

	}

	function checkPrevias() {
            filtraEtapasResultadoDashboard();
            filtraDetalhes();
            //alert('30');
            g_iCount = 30;
	}

</script>


<style>
@-webkit-keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
@-moz-keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
@-o-keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
@keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
.invalid {
  -webkit-animation: invalid 1s infinite; /* Safari 4+ */
  -moz-animation:    invalid 1s infinite; /* Fx 5+ */
  -o-animation:      invalid 1s infinite; /* Opera 12+ */
  animation:         invalid 1s infinite; /* IE 10+ */
}

@-webkit-keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
@-moz-keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
@-o-keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
@keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
.excluido {
  -webkit-animation: excluido 1s infinite; /* Safari 4+ */
  -moz-animation:    excluido 1s infinite; /* Fx 5+ */
  -o-animation:      excluido 1s infinite; /* Opera 12+ */
  animation:         excluido 1s infinite; /* IE 10+ */
}

td {
    padding: 1em;
}

</style>

<div class="col-lg-12">
	<ul class="nav nav-tabs">
            <li><a href="#" onclick="exibeAcomLive()">ACOMPANHAMENTO AO VIVO</a></li>
            <li><a href="#" onclick="exibeAcomPrevias()">PRÉVIA DOS RESULTADOS</a></li>
            <li><a href="#" onclick="exibeAcomDetalhes()">DETALHES PROVA</a></li>
	</ul>
	<div id="live">
            <div class="row">
                <div class="col-lg-12">
                    <?php
                        if($etapa = retornaEtapaHoje(null)){
                            echo '<h1 class="page-header">Acompanhamento ao vivo <small id="etapa" align=center>'.$etapa->DESCRICAO.'</small></h1>';
                            echo '<input type="hidden" name="idEtapaAcompanhamentoLive" id="idEtapaAcompanhamentoLive" value="'.$etapa->ID.'">';
                        }else{
                            echo '<h1 class="page-header">Acompanhamento ao vivo <small id="etapa" align=center>Nenhuma etapa sendo realizada hoje</small></h1>';
                            echo '<input type="hidden" name="idEtapaAcompanhamentoLive" id="idEtapaAcompanhamentoLive" value="">';
                        }
                    ?>
                </div>
            </div>

            <div  class="row">
                <div class="col-lg-12">
                    <input type="hidden" name="ultimaVerificacao" id="ultimaVerificacao" value="">
                    <input type="hidden" name="idUltimaVerificacao" id="idUltimaVerificacao" value="">
                    <div class="well form-group">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive" id="tableExportaClientes2">
                                    <table class="table table-bordered table-hover" >
                                        <thead>
                                            <tr>
                                                <!--<th>#</th>-->
                                                <th>Comprovante</th>
                                                <!--<th>Etapa</th>-->
                                                <th>Data/Hora</th>
                                                <th>Equipe</th>
                                                <th>Competidor</th>
                                                <th>Espécie</th>
                                                <th>Modalidade</th>
                                                <th>Isca</th>
                                                <!--<th>Ponto Pesagem</th>-->
                                                <th><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?></th>
                                                <th>Obs</th>
                                            </tr>
                                        </thead>
                                        <tbody id="acompanhamentoLive">
                                            <!--RESULTADO GERAL-->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
	</div>

	<div id="previas" class="hidden">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Prévia de Resultados <small id='numberCountdown' align=center></small></h1>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="well form-group">
                        <table id="tableResultadosGeral" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th colspan="3"><h3><b>Dados Gerais sobre Etapa</b></h3></th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th>Qtd Total Peixes</th>
                                    <th><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?> Total Peixes</th>
                                </tr>
                            </thead>
                            <tbody id="resultadoPreviaEtapa">
                                <!--RESULTADO GERAL-->
                            </tbody>
                        </table>

                        <table id="tableResultadosMaiorPeixe" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th colspan="6"><h3><b>Maior Peixe</b></h3></th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?></th>
                                    <th>Espécie</th>
                                    <th>Modalidade</th>
                                    <th>Isca</th>
                                    <th>Equipe/Competidores</th>
                                </tr>
                            </thead>
                            <tbody id="resultadoPreviaMaiorPeixeEtapa">
                                <!--RESULTADO MAIOR PEIXE-->
                            </tbody>
                        </table>

                        <table id= "tableResultadosRankingFinal" class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th colspan="5"><h3><b>Ranking</b></h3></th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th>Colocação</th>
                                    <th><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?></th>
                                    <th>Quantidade</th>
                                    <th>Equipe/Competidores</th>
                                </tr>
                            </thead>
                            <tbody id="resultadoPreviaRankingColocacoes">
                                <!--RESULTADO RANKING-->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
	</div>


	<div id="detalhes" class="hidden">
		<div class="row">
			<div class="col-lg-12">
				<?php
					if($etapa = retornaEtapaHoje(null)){
						echo '<h1 class="page-header">Detalhes da prova <small id="etapa" align=center>'.$etapa->DESCRICAO.'</small></h1>';
						echo '<input type="hidden" name="idEtapaAcompanhamentoDetalhe" id="idEtapaAcompanhamentoDetalhe" value="'.$etapa->ID.'">';
					}else{
						echo '<h1 class="page-header">Acompanhamento ao vivo <small id="etapa" align=center>Nenhuma etapa sendo realizada hoje</small></h1>';
						echo '<input type="hidden" name="idEtapaAcompanhamentoDetalhe" id="idEtapaAcompanhamentoDetalhe" value="">';
					}
				?>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="well form-group">
					<div class="row" >
						<div class="col-lg-12">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" >
									<thead>
										<tr>
											<th>Modalidade</th>
											<th style="width:30%">Quantidade</th>
										</tr>
									</thead>
									<tbody id="resultadoDetalhesModalidade">
										<!--MODALIDADES-->
									</tbody>
								</table>
							</div>

							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" >
									<thead>
										<tr>
											<th>Espécie</th>
											<th style="width:30%">Quantidade</th>
										</tr>
									</thead>
									<tbody id="resultadoDetalhesEspecie">
										<!--ESPECIES-->
									</tbody>
								</table>
							</div>

							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" >
									<thead>
										<tr>
											<th>Isca</th>
											<th style="width:30%">Quantidade</th>
										</tr>
									</thead>
									<tbody id="resultadoDetalhesIsca">
										<!--ISCAS-->
									</tbody>
								</table>
							</div>

							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" >
									<thead>
										<tr>
											<th>Horário</th>
											<th style="width:30%">Quantidade</th>
										</tr>
									</thead>
									<tbody id="resultadoDetalhesHora">
										<!--ISCAS-->
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

	<link rel="stylesheet" href="../bower_components/jquery-ui/jquery-ui.css">
	<script src="../bower_components/jquery-ui/jquery-ui.js"></script>
	<script src="../bower_components/datatables/jquery.dataTables.min.js"></script>
	<script src="../bower_components/datatables/dataTables.bootstrap.min.js"></script>
</body>

</html>
