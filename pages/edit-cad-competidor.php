<?php
	include '../functions/conexao.php';
	require '../functions/crud.php';
	header('Content-Type: text/html; charset=utf-8');
	$idCompetidor = $_REQUEST['idCompetidor'];
	$competidor = retornaCompetidor($idCompetidor);
?>
	<script type="text/javascript" src="../js/custom.js"></script>

    <script>
        function exibeDadosGeraisCompetidor(){
            $( "#dadosGeraisCompetidor" ).removeClass( 'hidden' );
            $( "#dadosEtapasParticipou" ).addClass( 'hidden' );
        }

        function exibeDadosEtapasParticipou(){
            $( "#dadosGeraisCompetidor" ).addClass( 'hidden' );
            $( "#dadosEtapasParticipou" ).removeClass( 'hidden' );
        }
    </script>

    <ul class="nav nav-tabs">
        <li><a href="#" onclick="exibeDadosGeraisCompetidor()">Dados Competidor</a></li>
        <li><a href="#" onclick="exibeDadosEtapasParticipou()">Etapas</a></li>
    </ul>

	<div class="panel-body">
        <div id="dadosGeraisCompetidor">
    		<div id='alerta'></div>
    		<form role='form' method='post' action='' name='form_editCadCompetidor'>
    			<fieldset>
                    <input type="hidden" name="editCadIdCompetidor" id="editCadIdCompetidor" value="<?php echo $competidor -> ID;?>">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Nome Competidor</label>
                                <input id="editCadNomeCompetidor" name="editCadNomeCompetidor" class="form-control upcase" placeholder="Nome Competidor" value="<?php echo $competidor -> NOME;?>" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>CPF</label>
                                <input id="editCadCPFCompetidor" name="editCadCPFCompetidor" class="form-control upcase" placeholder="CPF" value="<?php echo $competidor -> CPF;?>" required>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <label>Apelido</label>
                                <input id="editCadApelidoCompetidor" name="editCadApelidoCompetidor" class="form-control upcase" placeholder="Apelido" value="<?php echo $competidor -> APELIDO;?>" required>
                            </div>
                        </div>
                    </div>


                    <div class="row">

                        <div class="col-lg-2">
                            <div class="form-group">
                                <label>Tam. Camiseta</label>
                                <select id="editCadTamanhoCompetidor" name="editCadTamanhoCompetidor" class="form-control">
                                    <option value="">Selecione</option>
                                    <optgroup label="Adulto">
                                        <?php
                                        foreach( ['PP','P','M','G','GG','EX','G3','G4','G5','G6','G7'] as $tamanho ) {
                                            echo '<option value="'.$tamanho.'">'.$tamanho.'</option>';                                    
                                        }
                                        ?>
                                    </optgroup>
                                    <optgroup label="Infantil">
                                        <?php
                                        foreach( ['04','06','08','10','12'] as $tamanho ) {
                                            echo '<option value="'.$tamanho.'">'.$tamanho.'</option>';                                    
                                        }
                                        ?>
                                    </optgroup>
                                    <!--
                                    <option value="N/I">N/I</option>
                                    <option value="PP">PP</option>
                                    <option value="P">P</option>
                                    <option value="M">M</option>
                                    <option value="G">G</option>
                                    <option value="GG">GG</option>
                                    <option value="XGG">XGG</option>
                                    <option value="XGG1">XGG1</option>
                                    <option value="XGG2">XGG2</option>
                                    <option value="XGG3">XGG3</option>
                                    <option value="XGG4">XGG4</option>
                                    <option value="XGG5">XGG5</option>
                                    <option value="XGG6">XGG6</option>
                                    <option value="XGG7">XGG7</option>
                                    -->
                                </select>
                            </div>
                        </div>

                        <!-- Modelagem camiseta -->
                        <input type="hidden" name="editCadModelagemCompetidor" value="Normal">
                        <!--
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Modelagem Camiseta</label>
                                <select id="editCadModelagemCompetidor" name="editCadModelagemCompetidor" class="form-control">
                                    <option value="Normal">Normal</option>
                                    <option value="Baby Look">Baby Look</option>
                                </select>
                            </div>
                        </div>
                        -->

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="editCadNascimentoCompetidor">Nascimento</label>
                                <div class="input-group date time form_datetime" data-link-field="editCadNascimentoCompetidor">
                                    <input type="text" name="editCadNascimentoCompetidor2" id="editCadNascimentoCompetidor2" class="form-control" type="text" readonly value="<?php echo date("d/m/Y", strtotime($competidor->DATA_NASCIMENTO));?>">
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-remove" ></span></span>
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>
                                </div>
                                <input type="hidden" name="editCadNascimentoCompetidor" id="editCadNascimentoCompetidor" value="<?php echo $competidor -> DATA_NASCIMENTO;?>">
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label>Telefone:</label>
                                <input id="editCadTelefoneCompetidor" name="editCadTelefoneCompetidor" class="form-control upcase" placeholder="Telefone" value="<?php echo $competidor -> CELULAR;?>" >
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>E-mail</label>
                                <input id="editCadEmailCompetidor" name="editCadEmailCompetidor" class="form-control upcase" placeholder="E-mail" value="<?php echo $competidor -> EMAIL;?>" >
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <label>Sexo</label>
                            <select id="editCadSexoCompetidor" name="editCadSexoCompetidor" class="form-control">
                                <option value="M">Masculino</option>
                                <option value="F">Feminino</option>
                            </select>
                        </div>
                    </div>

                    <div class="well well-sm">
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label>Logradouro</label>
                                    <input id="editCadEnderecoCompetidor" name="editCadEnderecoCompetidor" class="form-control upcase" placeholder="Endereço" value="<?php echo $competidor -> ENDERECO;?>" >
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Número</label>
                                    <input id="editCadNumeroEndCompetidor" name="editCadNumeroEndCompetidor" class="form-control" placeholder="Número" value="<?php echo $competidor -> NUM;?>" >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-10">
                                <div class="form-group">
                                    <label>Complemento</label>
                                    <input id="editCadComplementoCompetidor" name="editCadComplementoCompetidor" class="form-control upcase" placeholder="Complemento" value="<?php echo $competidor -> COMPLEMENTO;?>">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label>CEP</label>
                                    <input id="editCadCEPCompetidor" name="editCadCEPCompetidor" class="form-control upcase" placeholder="CEP" value="<?php echo $competidor -> CEP;?>" >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Bairro</label>
                                    <input id="editCadBairroCompetidor" name="editCadBairroCompetidor" class="form-control upcase" placeholder="Bairro" value="<?php echo $competidor -> BAIRRO;?>" >
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Cidade</label>
                                    <input id="editCadCidadeCompetidor" name="editCadCidadeCompetidor" class="form-control upcase" placeholder="Cidade" value="<?php echo $competidor -> CIDADE;?>" >
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>UF</label>
                                    <select id="editCadUFCompetidor" name="editCadUFCompetidor" class="form-control" >
                                        <?php
                                            if ($estados = retornaUFs()){
                                                foreach($estados as $estado){
                                                    echo '<option value="'.$estado->sigla.'">'.$estado->descricao.' ('.$estado->sigla.')</option>';
                                                }
                                            }else{
                                                echo '<option value="nenhumEstado">Nenhum estado encontrado</option>';
                                            }
                                        ?>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

    			</fieldset>
    			<div class="span6 pull-right">
                    <button id="botaoEditaCompetidor" name="botaoEditaCompetidor" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
    			</div>
    		</form>
        </div>
        <div id="dadosEtapasParticipou" class="hidden">
            <div class="well well-sm">
                <ul class="list-group">
                    <?php
                    if ($etapas = retornaEtapasCompetidor($idCompetidor)){
                        foreach($etapas as $etapa){
                            echo '<li class="list-group-item">'.$etapa -> CAMPEONATO . ' - ' . $etapa -> ETAPA . ' (' . date("d/m/Y", strtotime($etapa -> DATA_ETAPA)).')</li>';
                        }
                    }else{
                        echo '<li class="list-group-item">Nenhuma etapa</li>';
                    }
                    ?>

                </ul>
            </div>
        </div>
	</div>

	<script>

        $( document ).ready(function() {

            $("select[name='editCadTamanhoCompetidor']").val('<?php echo $competidor -> TAMANHO_CAMISETA;?>');
            $("select[name='editCadModelagemCompetidor']").val('<?php echo $competidor -> MODELAGEM_CAMISETA;?>');
            $("select[name='editCadUFCompetidor']").val('<?php echo $competidor -> UF;?>');
            $("select[name='editCadSexoCompetidor']").val('<?php echo $competidor -> SEXO;?>');

            $('#editCadCPFCompetidor').mask('000.000.000-00', {reverse: true});
            function filtraEtapasCombo(idCampeonato){
                $('#filtroEquipeEtapa').html('');
                $.post('../ajax/controller.php',{
                    acao:"listaEtapas",
                    idCampeonato:idCampeonato
                },function(retorno){
                    $('#filtroEquipeEtapa').html('<option value="-1">Todos</option>');
                    $('#filtroEquipeEtapa').append(retorno);
                });
            }

            $('.form_datetime').datetimepicker({
                weekStart: 0,
                todayBtn:  1,
                autoclose: 1,
                format: 'dd/mm/yyyy',
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                language: 'pt-BR',
                showMeridian: 1
            }).on('changeDate', function(e) {
                var jsDate = $('#editCadNascimentoCompetidor2').val();
                jsDate instanceof Date; // -> true

            });
        });
	</script>
