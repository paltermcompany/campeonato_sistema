<?php
	session_start();
	include '../functions/conexao.php';
	require '../functions/crud.php';
?>

<script type="text/javascript" src="../js/custom.js"></script>

<script>
	var tableCadEspecies;

	function filtraEspecies(){
		tableCadEspecies.ajax.reload();
	}

	$(document).ready(function() {
		tableCadEspecies = $('#tableCadEspecies').DataTable({
			"ajax": {
				url: "../ajax/controller.php",
				type: "POST",
				data : function(d){
					d.acao = "carregaEspecies",
					d.filtroEspecieNome = $('#filtroEspecieNome').val(),
					d.filtroEspeciePrioridade = $('#filtroEspeciePrioridade').val()
				},
			    beforeSend : function() {
                    $('#buttonFiltraEspecies').html('Aguarde').attr('disabled', true);
                },
                complete : function() {
                    $('#buttonFiltraEspecies').html('Filtrar').attr('disabled', false);
                }
			},
			"columns": [
				{"data": "id_especie"},
				{"data": "especie"},
				{"data": "prioridade"},
				{
                    "orderable": false,
                    "defaultContent": '<div class="btn-group btn-group-xs"><button type="button" class="btn btn-warning btnAlteraEspecie"><span class="glyphicon glyphicon-edit"></span> </button><button type="button" class="btn btn-danger" onclick="naoImplementado()"><span class="glyphicon glyphicon-remove"></span></button></div>'
                },
			],
			"language": {
				"url": "../bower_components/datatables/Portuguese-Brasil.json"
			},
			"paging": true,
			"lengthChange": true,
			"searching": false,
			"destroy": true,
			"ordering": true,
			"order": [[ 1, "asc" ]],
			"info": true,
			"autoWidth": false
		} );
		$('#tableCadEspecies tbody').on( 'click', 'button', '.btnAlteraEspecie', function () {
            var data = tableCadEspecies.row( $(this).parents('tr') ).data();
            console.log(data);
            abreModalEditaEspecie(data.id_especie);
        } );
	});
</script>

<div id="propria">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Cadastro de Espécies  <button onClick="abreModalNovaEspecie()" type="button" class="close glyphicon glyphicon-plus text-muted" ><span aria-hidden="true"></span></button></h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<form name="form_filtraEspecies" id="form_filtraEspecies" method="post" >
				<div class="well form-group">
					<h4>Filtros</h4>
					<div class="row">
						<div class="col-lg-10">
							<label>Descrição</label>
							<input type="text" id="filtroEspecieNome" name="filtroEspecieNome" class="form-control upcase">
						</div>
						<div class="col-lg-1">
							<label>Prioridade</label>
							<input type="text" id="filtroEspeciePrioridade" name="filtroEspeciePrioridade" class="form-control">
						</div>

						<div class="col-lg-1 pull-right">
						<label></label>
							<div class="input-group">
								<button class="btn btn-primary" name="buttonFiltraEspecies" id="buttonFiltraEspecies" style="background-color:#005081" type="button" onClick="filtraEspecies()"><b>Filtrar</b></button>
							</div>
						</div>
					</div>

				</div>
			</form>
			<div class="row" >
				<div class="col-lg-12">
					<table id="tableCadEspecies" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Descrição</th>
								<th>Prioridade</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody id="resultadoFiltroEspecies">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

