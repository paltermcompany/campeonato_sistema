<?php
	session_start();
	include '../functions/conexao.php';
	require '../functions/crud.php';
	header('Content-Type: text/html; charset=utf-8');
?>

<script type="text/javascript" src="../js/custom.js"></script>
<script type="text/javascript" src="../bower_components/bootstrap-daterangepicker-master/moment.js"></script>
<script type="text/javascript" src="../bower_components/bootstrap-daterangepicker-master/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../bower_components/bootstrap-daterangepicker-master/daterangepicker.css" />

<script>
	function imprimirStatusProjetos(){
		//window.open('report-projects-print.php','_blank');
		return false;
	};
	filtraEtapasComboResultados($( '#filtroEquipeCampeonatoResultado3 option:selected' ).val())


	function filtraEtapasComboResultados(idCampeonato){
		$('#filtroEquipeEtapaResultado3').html('');
		$.post('../ajax/controller.php',{
			acao:"listaEtapas",
			idCampeonato:idCampeonato
		},function(retorno){
			$('#filtroEquipeEtapaResultado3').append(retorno);
		});

	}


	function filtraEtapasCompetidores(){
		idEtapa = $( '#filtroEquipeEtapaResultado3 option:selected' ).val();
		$.post('../ajax/controller.php',{
			acao:"detalhesModalidade",
			idEtapa:idEtapa
		},function(retorno){
			$('#resultadoDetalhesModalidade').html(retorno);
		});

		$.post('../ajax/controller.php',{
			acao:"detalhesEspecie",
			idEtapa:idEtapa
		},function(retorno){
			$('#resultadoDetalhesEspecie').html(retorno);
		});

		$.post('../ajax/controller.php',{
			acao:"detalhesIsca",
			idEtapa:idEtapa
		},function(retorno){
			$('#resultadoDetalhesIsca').html(retorno);
		});

		$.post('../ajax/controller.php',{
			acao:"detalhesHora",
			idEtapa:idEtapa
		},function(retorno){
			$('#resultadoDetalhesHora').html(retorno);
		});

		$.post('../ajax/controller.php',{
			acao:"detalhesQuadrantes",
			idEtapa:idEtapa
		},function(retorno){
			$('#resultadoDetalhesQuadrante').html(retorno);
		});

		$.post('../ajax/controller.php',{
			acao:"detalhesEstatisticasEquipes",
			idEtapa:idEtapa
		},function(retorno){
			$('#resultadoDetalhesEstatisticaEquipes').html(retorno);
		});
	}
</script>

<style>

.barraBotoes {
  float: right;
  margin-right:5px;
  font-size: 21px;
  font-weight: bold;
  line-height: 1;
  color: #000;
  text-shadow: 0 1px 0 #fff;
  filter: alpha(opacity=20);
  opacity: .2;
  -webkit-appearance: none;
  padding: 0;
  cursor: pointer;
  background: transparent;
  border: 0;

</style>

<div id="propria">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Detalhes por Etapa<button onClick="imprimiDetalhesEtapa($('#filtroEquipeEtapaResultado3 option:selected').val())" type="button" class="barraBotoes glyphicon glyphicon-print text-muted" ></button></h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<form name="form_filtraResultadosDetalhesEtapa" id="form_filtraResultadosDetalhesEtapa" method="post" >
				<div class="well form-group">
					<h4>Filtros</h4>
					<div class="row">
						<div class="col-lg-3">
							<label>Campeonato</label>
							<select id="filtroEquipeCampeonatoResultado3" name="filtroEquipeCampeonatoResultado3" class="form-control" onchange="filtraEtapasComboResultados($( '#filtroEquipeCampeonatoResultado3 option:selected' ).val())">
								<?php
									$campeonatos = retornaCampeonatos('', '');
									foreach ($campeonatos as $result){
										echo '<option value="'.$result->id.'">'.$result->descricao.'</option>';
									}
								 ?>
							</select>
						</div>

						<div class="col-lg-2">
							<label>Etapa</label>
							<select id="filtroEquipeEtapaResultado3" name="filtroEquipeEtapaResultado3" class="form-control">
								<?php
									$etapas = retornaEtapas('', 0);
									foreach ($etapas as $result){
										echo '<option value="'.$result->id.'">'.$result->DESCRICAO.'</option>';
									}
								 ?>
							</select>
						</div>

						<div class="col-lg-1 pull-right">
						<label></label>
							<div class="input-group">
								<button class="btn btn-primary" name="buttonFiltraCompetidores" id="buttonFiltraCompetidores" style="background-color:#005081" type="button" onclick="filtraEtapasCompetidores()"><b>Filtrar</b></button>
							</div>
						</div>
					</div>

				</div>
			</form>

			<div class="row" >
				<div class="col-lg-12">
					<div class="table-responsive" id="tableLancamentosEtapa2">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" >
									<thead>
										<tr>
											<th>Modalidade</th>
											<th style="width:30%">Quantidade</th>
										</tr>
									</thead>
									<tbody id="resultadoDetalhesModalidade">
										<!--MODALIDADES-->
									</tbody>
								</table>
							</div>

							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" >
									<thead>
										<tr>
											<th>Espécie</th>
											<th style="width:30%">Quantidade</th>
										</tr>
									</thead>
									<tbody id="resultadoDetalhesEspecie">
										<!--ESPECIES-->
									</tbody>
								</table>
							</div>

							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" >
									<thead>
										<tr>
											<th>Isca</th>
											<th style="width:30%">Quantidade</th>
										</tr>
									</thead>
									<tbody id="resultadoDetalhesIsca">
										<!--ISCAS-->
									</tbody>
								</table>
							</div>

							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" >
									<thead>
										<tr>
											<th>Horário</th>
											<th style="width:30%">Quantidade</th>
										</tr>
									</thead>
									<tbody id="resultadoDetalhesHora">
										<!--HORARIOS-->
									</tbody>
								</table>
							</div>

							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" >
									<thead>
										<tr>
											<th style="width:20%">Quadrante</th>
											<th style="width:20%">Tempo</th>
											<th style="width:20%">Qtd Pesagens</th>
											<th style="width:20%">Peso Total</th>
										</tr>
									</thead>
									<tbody id="resultadoDetalhesQuadrante">
										<!--QUADRANTES-->
									</tbody>
								</table>
							</div>

							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover" >
									<thead>
										<tr>
											<th style="width:60%">Estatisticas de Equipes</th>
											<th style="width:40%">Total</th>

										</tr>
									</thead>
									<tbody id="resultadoDetalhesEstatisticaEquipes">
										<!--INFORMACAO EQUIPES-->
									</tbody>
								</table>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
