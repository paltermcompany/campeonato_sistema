<?php
	session_start();
	header('Content-Type: text/html; charset=utf-8');

	include '../functions/conexao.php';
	require '../functions/crud.php';

?>

<script type="text/javascript" src="../js/custom.js"></script>

<script>
	var tableResultadosGeralDash;
	var tableResultadosMaiorPeixeDash;
	var tableResultadosRankingFinalDash;

	$( document ).ready(function() {
        idEtapa = $( '#idEtapaAcompanhamentoLive').val();

        $.post('../ajax/controller.php',{
            acao:"unidadeMedidaEtapa",
            idEtapa:idEtapa
        },function(retorno){
            if(retorno != ''){
                retorno = JSON.parse(retorno)
                $.each( retorno, function( key, value ) {
                    document.cookie = "cookieDescricaoUnidadeMedida="+value.descricao;
                    document.cookie = "cookieAbreviacaoUnidadeMedida="+value.abreviacao;
                });
            }else{
                document.cookie = "cookieDescricaoUnidadeMedida=";
                document.cookie = "cookieAbreviacaoUnidadeMedida=";
            }
        });

        tableResultadosGeralDash = $('#tableResultadosGeralDash').DataTable( {
            "ajax": {
                    url: "../ajax/controller.php",
                    type: "POST",
                    data : function(d){
                        d.acao = "resultadosEtapa",
                        d.idEtapa = idEtapa,
                        d.horario = ""
                    }
                },
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {"data": "total_quantidade"},
                {"data": "total_peso"}
            ],
            "language": {
                "url": "../bower_components/datatables/Portuguese-Brasil.json"
            },
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "destroy": true,
            "ordering": false,
            "info": false,
            "autoWidth": false
        } );

        tableResultadosMaiorPeixeDash = $('#tableResultadosMaiorPeixeDash').DataTable( {
            "ajax": {
                    url: "../ajax/controller.php",
                    type: "POST",
                    data : function(d){
                        d.acao = "resultadosEtapaMaiorPeixeSemEntregaTrofeu",
                        d.idEtapa = idEtapa,
                        d.horario = "",
                        d.mostraNome = "apelido";
                    }
                },
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {"data": "peso"},
                {"data": "especie"},
                {"data": "isca"},
                {"data": "modalidade"},
                {"data": "competidores"}
            ],
            "language": {
                "url": "../bower_components/datatables/Portuguese-Brasil.json"
            },
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "destroy": true,
            "ordering": false,
            "info": false,
            "autoWidth": false
        } );

        tableResultadosRankingFinalDash = $('#tableResultadosRankingFinalDash').DataTable( {
            "ajax": {
                    url: "../ajax/controller.php",
                    type: "POST",
                    data : function(d){
                        d.acao = "resultadosEtapaRankingSemEntregaTrofeu",
                        d.rankingCompleto = false,
                        d.idEtapa = idEtapa,
                        d.horario = "",
                        d.mostraNome = "apelido";
                    }
                },
            "columns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {"data": "colocacao"},
                {"data": "peso"},
                {"data": "quantidade"},
                {"data": "equipe"}
            ],
            "language": {
                "url": "../bower_components/datatables/Portuguese-Brasil.json"
            },
            "paging": false,
            "lengthChange": true,
            "searching": false,
            "destroy": true,
            "ordering": false,
            "info": false,
            "autoWidth": false
        } );

        //alert(typeof refreshId );
        if(typeof refreshId == 'undefined') {
        	//alert('oi');
			refreshId = setTimeout( checkAcompanhamentoLive, 10);
		}
    });


	function exibeAcomLive(){
		$( "#previas" ).addClass( 'hidden' );
		$( "#live" ).removeClass( 'hidden' );
	}

	function exibeAcomPrevias(){
		$( "#previas" ).removeClass( 'hidden' );
		$( "#live" ).addClass( 'hidden' );
	}

	function checkAcompanhamentoLive() {
		idEtapa = $('#idEtapaAcompanhamentoLive').val();
		if(idEtapa != ''){
			if(typeof idEtapa != 'undefined'){
				$.post('../ajax/controller.php',{
					acao:"acompanhamentoLive",
					idEtapa:idEtapa,
					ultimaVerificacao: $('#ultimaVerificacao').val(),
					idUltimaVerificacao: $('#idUltimaVerificacao').val()
				},function(retorno){
					if(retorno!=''){
						$( ".linhaLive" ).removeClass( 'invalid' );
						$( ".linhaLive" ).removeClass( 'excluido' );
						//alert(retorno);
					}
					$('#acompanhamentoLive').prepend(retorno);
					//console.log('loop: ' + retorno);
				}).always(function() {
					refreshId = setTimeout( checkAcompanhamentoLive, 1000 );
				});
			}else{
				refreshId = undefined;
			}
		}
	}

	function checkPrevias() {
		idEtapa = $( '#idEtapaAcompanhamentoLive').val();
		if(idEtapa!=''){
			tableResultadosGeralDash.ajax.reload();
	        tableResultadosMaiorPeixeDash.ajax.reload();
	        tableResultadosRankingFinalDash.ajax.reload();
		}
	}

</script>


<style>
@-webkit-keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
@-moz-keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
@-o-keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
@keyframes invalid {
  from { background-color: green; }
  to { background-color: inherit; }
}
.invalid {
  -webkit-animation: invalid 1s infinite; /* Safari 4+ */
  -moz-animation:    invalid 1s infinite; /* Fx 5+ */
  -o-animation:      invalid 1s infinite; /* Opera 12+ */
  animation:         invalid 1s infinite; /* IE 10+ */
}

@-webkit-keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
@-moz-keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
@-o-keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
@keyframes excluido {
  from { background-color: red; }
  to { background-color: inherit; }
}
.excluido {
  -webkit-animation: excluido 1s infinite; /* Safari 4+ */
  -moz-animation:    excluido 1s infinite; /* Fx 5+ */
  -o-animation:      excluido 1s infinite; /* Opera 12+ */
  animation:         excluido 1s infinite; /* IE 10+ */
}

td {
    padding: 1em;
}
}
</style>

<div id="propria">
	<ul class="nav nav-tabs">
	  	<li><a href="#" onclick="exibeAcomLive()">ACOMPANHAMENTO AO VIVO</a></li>
	  	<li><a href="#" onclick="exibeAcomPrevias()">PRÉVIA DOS RESULTADOS</a></li>
	</ul>
	<div id="live">
		<div class="row">
			<div class="col-lg-12">
				<?php
					if($etapa = retornaEtapaHoje(null)){
						echo '<h1 class="page-header">Acompanhamento ao vivo <small id="etapa" align=center>'.$etapa->DESCRICAO.'</small></h1>';
						echo '<input type="hidden" name="idEtapaAcompanhamentoLive" id="idEtapaAcompanhamentoLive" value="'.$etapa->ID.'">';
					}else{
						echo '<h1 class="page-header">Acompanhamento ao vivo <small id="etapa" align=center>Nenhuma etapa sendo realizada hoje</small></h1>';
						echo '<input type="hidden" name="idEtapaAcompanhamentoLive" id="idEtapaAcompanhamentoLive" value="">';
					}
				?>

			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<input type="hidden" name="ultimaVerificacao" id="ultimaVerificacao" value="">
				<input type="hidden" name="idUltimaVerificacao" id="idUltimaVerificacao" value="">

				<div class="well form-group">
					<div class="row">
						<div class="col-lg-12">
							<div class="table-responsive" id="tableExportaClientes2">
								<table class="table table-bordered table-hover" >
									<thead>
										<tr>
											<!--<th>#</th>-->
											<th>Comprovante</th>
											<!--<th>Etapa</th>-->
											<th>Data/Hora</th>
											<th>Equipe</th>
											<th>Competidor</th>
											<th>Espécie</th>
											<th>Modalidade</th>
											<th>Isca</th>
											<!--<th>Ponto Pesagem</th>-->
											<th><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?></th>
											<th>Obs</th>
										</tr>
									</thead>
									<tbody id="acompanhamentoLive">
										<!--RESULTADO GERAL-->
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="previas" class="hidden">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Prévia de Resultados <small id='numberCountdown' align=center></small></h1>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-12">
				<div class="well form-group">
					<div class="row" >
						<div class="col-lg-12">
							<div class="table-responsive" id="tableExportaClientes1">
								<table id="tableResultadosGeralDash" class="table table-bordered table-hover"  >
									<thead>
										<tr>
											<th colspan="3"><h3><b>Dados Gerais sobre Etapa</b></h3></th>
										</tr>
										<tr>
											<th></th>
											<th>Qtd Total Peixes</th>
											<th><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?> Total Peixes</th>
										</tr>
									</thead>
									<tbody id="resultadoPreviaEtapa">
										<!--RESULTADO GERAL-->
									</tbody>
								</table>

								<table id="tableResultadosMaiorPeixeDash" class="table table-striped table-bordered table-hover" >
									<thead>
										<tr>
											<th colspan="6"><h3><b>Maior Peixe</b></h3></th>
										</tr>
										<tr>
											<th></th>
											<th><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?></th>
											<th>Espécie</th>
											<th>Isca</th>
											<th>Modalidade</th>
											<th>Equipe/Competidores</th>
										</tr>
									</thead>
									<tbody id="resultadoPreviaMaiorPeixeEtapa">
										<!--RESULTADO MAIOR PEIXE-->
									</tbody>
								</table>

								<table id="tableResultadosRankingFinalDash" class="table table-striped table-bordered table-hover" >
									<thead>
										<tr>
											<th colspan="6"><h3><b>Ranking</b></h3></th>
										</tr>
										<tr>
											<th></th>
											<th>Colocação</th>
											<th><?php echo $_COOKIE['cookieDescricaoUnidadeMedida'];?></th>
											<th>Quantidade</th>
											<th>Equipe/Competidores</th>
										</tr>
									</thead>
									<tbody id="resultadoPreviaRankingColocacoes">
										<!--RESULTADO RANKING-->
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


