<?php
	session_start();
	include '../functions/conexao.php';
	require '../functions/crud.php';
	header('Content-Type: text/html; charset=utf-8');
?>

<script type="text/javascript" src="../js/custom.js"></script>
<script type="text/javascript" src="../bower_components/bootstrap-daterangepicker-master/moment.js"></script>
<script type="text/javascript" src="../bower_components/bootstrap-daterangepicker-master/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="../bower_components/bootstrap-daterangepicker-master/daterangepicker.css" />

<script>
	function imprimirStatusProjetos(){
		//window.open('report-projects-print.php','_blank');
		return false;
	};
	filtraEtapasComboResultados($( '#filtroEquipeCampeonatoResultado2 option:selected' ).val())


	function filtraEtapasComboResultados(idCampeonato){
		$('#filtroEquipeEtapaResultado2').html('');
		$.post('../ajax/controller.php',{
			acao:"listaEtapas",
			idCampeonato:idCampeonato
		},function(retorno){
			$('#filtroEquipeEtapaResultado2').append(retorno);
		});
	}


	function filtraEtapasCompetidores(){
		idEtapa = $( '#filtroEquipeEtapaResultado2 option:selected' ).val();
		$('#resultadoCompetidoresPorEtapa').html('');
		$.post('../ajax/controller.php',{
			acao:"resultadosCompetidoresEtapa",
			idEtapa:idEtapa
		},function(retorno){
			$('#resultadoCompetidoresPorEtapa').html('<td colspan="4"><center><i class="fa fa-5x fa-sync fa-spin"></i><br>Carregando...</center></td>');
			$('#resultadoCompetidoresPorEtapa').html(retorno);
		});
	}
</script>

<style>

.barraBotoes {
  float: right;
  margin-right:5px;
  font-size: 21px;
  font-weight: bold;
  line-height: 1;
  color: #000;
  text-shadow: 0 1px 0 #fff;
  filter: alpha(opacity=20);
  opacity: .2;
  -webkit-appearance: none;
  padding: 0;
  cursor: pointer;
  background: transparent;
  border: 0;

</style>

<div id="propria">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Competidores por Etapa <button onClick="imprimiCompetidoresEtapa($( '#filtroEquipeEtapaResultado2 option:selected' ).val())" type="button" class="barraBotoes glyphicon glyphicon-print text-muted" ></button></h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<form name="form_filtraResultadosCompetidoresEtapa" id="form_filtraResultadosCompetidoresEtapa" method="post" >
				<div class="well form-group">
					<h4>Filtros</h4>
					<div class="row">
						<div class="col-lg-3">
							<label>Campeonato</label>
							<select id="filtroEquipeCampeonatoResultado2" name="filtroEquipeCampeonatoResultado2" class="form-control" onchange="filtraEtapasComboResultados($( '#filtroEquipeCampeonatoResultado2 option:selected' ).val())">
								<?php
									$campeonatos = retornaCampeonatos('', '');
									foreach ($campeonatos as $result){
										echo '<option value="'.$result->id.'">'.$result->descricao.'</option>';
									}
								 ?>
							</select>
						</div>

						<div class="col-lg-2">
							<label>Etapa</label>
							<select id="filtroEquipeEtapaResultado2" name="filtroEquipeEtapaResultado2" class="form-control">
								<?php
									$etapas = retornaEtapas('', 0);
									foreach ($etapas as $result){
										echo '<option value="'.$result->id.'">'.$result->DESCRICAO.'</option>';
									}
								 ?>
							</select>
						</div>

						<div class="col-lg-1 pull-right">
						<label></label>
							<div class="input-group">
								<button class="btn btn-primary" name="buttonFiltraCompetidores" id="buttonFiltraCompetidores" style="background-color:#005081" type="button" onclick="filtraEtapasCompetidores()"><b>Filtrar</b></button>
							</div>
						</div>
					</div>

				</div>
			</form>

			<div class="row" >
				<div class="col-lg-12">
					<div class="table-responsive" id="tableLancamentosEtapa2">
						<table class="table table-striped table-bordered table-hover" >
							<thead>
								<tr>
									<th>Competidor</th>
									<th>Equipe</th>
									<th>Pulseira</th>
									<th>CPF</th>
									<th>Camiseta</th>
									<th>Apelido</th>
									<th>Patrocinador</th>
								</tr>
							</thead>
							<tbody id="resultadoCompetidoresPorEtapa">
								<!--RESULTADO LANCAMENTOS COMPETIDORES-->
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
