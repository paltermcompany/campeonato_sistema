<?php
	session_start();
	include '../functions/conexao.php';
	require '../functions/crud.php';

?>

<script type="text/javascript" src="../js/custom.js"></script>

<script>
	var tableFiltraIscas;

	function filtraIscas(){
		tableFiltraIscas.ajax.reload();
	}

	$(document).ready(function() {
		tableFiltraIscas = $('#tableFiltraIscas').DataTable({
			"ajax": {
				url: "../ajax/controller.php",
				type: "POST",
				data :function(d){
					d.acao = "carregaIscas",
					d.filtroIscaNome = $('#filtroIscaNome').val()
				},
				beforeSend : function() {
                    $('#buttonFiltraIscas').html('Aguarde').attr('disabled', true);
                },
                complete : function() {
                    $('#buttonFiltraIscas').html('Filtrar').attr('disabled', false);
                }
			},
			"columns": [
				{"data": "id_isca"},
				{"data": "nome_isca"},
				{
                    "orderable": false,
                    "defaultContent": '<div class="btn-group btn-group-xs"><button type="button" class="btn btn-warning btnAlteraIsca"><span class="glyphicon glyphicon-edit"></span> </button><button type="button" class="btn btn-danger" onclick="naoImplementado()"><span class="glyphicon glyphicon-remove"></span></button></div>'
                },
			],
			"language": {
				"url": "../bower_components/datatables/Portuguese-Brasil.json"
			},
			"paging": true,
			"lengthChange": true,
			"searching": false,
			"destroy": true,
			"ordering": true,
			"order": [[ 1, "asc" ]],
			"info": true,
			"autoWidth": false
		} );

		$('#tableFiltraIscas tbody').on( 'click', 'button', '.btnAlteraIsca', function () {
            var data = tableFiltraIscas.row( $(this).parents('tr') ).data();
            console.log(data);
            abreModalEditaIsca(data.id_isca);
        } );
	});
</script>

<div id="propria">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Cadastro de Iscas  <button onClick="abreModalNovaIsca()" type="button" class="close glyphicon glyphicon-plus text-muted" ><span aria-hidden="true"></span></button></h1>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<form name="form_filtraIscas" id="form_filtraIscas" method="post" >
				<div class="well form-group">
					<h4>Filtros</h4>
					<div class="row">
						<div class="col-lg-11">
							<label>Descrição</label>
							<input type="text" id="filtroIscaNome" name="filtroIscaNome" class="form-control upcase">
						</div>

						<div class="col-lg-1 pull-right">
						<label></label>
							<div class="input-group">
								<button class="btn btn-primary" name="buttonFiltraIscas" id="buttonFiltraIscas" style="background-color:#005081" type="button" onclick="filtraIscas()"><b>Filtrar</b></button>
							</div>
						</div>
					</div>

				</div>
			</form>
			<div class="row" >
				<div class="col-lg-12">
					<table id="tableFiltraIscas" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Descrição</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody id="resultadoFiltroIscas">

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

