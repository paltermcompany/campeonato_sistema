<?php
    include '../functions/conexao.php';
    require '../functions/crud.php';
    header('Content-Type: text/html; charset=utf-8');
    $idColetor = $_REQUEST['idColetor'];
    $coletor = retornaColetor($idColetor);
?>
    <script type="text/javascript" src="../js/custom.js"></script>
    <div class="panel-body">
        <form role='form' method='post' action='' name='form_editCadColetor'>
            <fieldset>
                <input type="hidden" name="editCadIdColetor" id="editCadIdColetor" value="<?php echo $coletor -> id;?>">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Nome Coletor</label>
                            <input id="editCadNomeColetor" name="editCadNomeColetor" class="form-control upcase" placeholder="Nome Coletor" required value="<?php echo $coletor -> descricao;?>">
                        </div>

                        <div class="form-group">
                            <label>Serial Coletor</label>
                            <input id="editCadSerialColetor" name="editCadSerialColetor" class="form-control upcase" placeholder="Nome Coletor" required value="<?php echo $coletor -> serial;?>">
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="span6 pull-right">
                <button id="botaoEditaIsca" name="botaoEditaIsca" type='submit' class='btn btn-success' style="background-color:#5FB887"><i class='glyphicon glyphicon-floppy-disk'></i> Salvar</button>
            </div>
        </form>
    </div>
