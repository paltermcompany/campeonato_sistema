<?php
//função de login
function login($login, $senha) {
	$pdo = conecta();
	try {
		$logar = $pdo -> prepare("SELECT * FROM cad_usuarios WHERE nome= :login AND senha= :senha");
		$logar -> bindValue(':login', $login, PDO::PARAM_STR);
		$logar -> bindValue(':senha', $senha, PDO::PARAM_STR);
		$logar -> execute();

		if ($logar -> rowCount() == 1) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function confirmado($login) {
	$pdo = conecta();
	try {
		$logar = $pdo -> prepare("SELECT nome FROM cad_usuarios WHERE nome= :login and ativo='S'");
		$logar -> bindValue(':login', $login, PDO::PARAM_STR);
		$logar -> execute();

		if ($logar -> rowCount() == 1) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

//pega login
function pegalogin($login) {
	$pdo = conecta();
	try {
		$bylogin = $pdo -> prepare("SELECT id, nome FROM cad_usuarios WHERE nome= :login");
		$bylogin -> bindValue(':login', $login, PDO::PARAM_STR);
		$bylogin -> execute();

		if ($bylogin -> rowCount() == 1) {
			return $bylogin -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

//função verifica sessão
function logado($sessao) {
	if (!isset($_SESSION[$sessao]) || empty($_SESSION[$sessao])) {
		header("Location: index.php");
	} else {
		return TRUE;
	}
}
?>