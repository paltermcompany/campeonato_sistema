<?php
date_default_timezone_set('America/Sao_Paulo');

function insertEspecie($nomeEspecie, $prioridade){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("INSERT INTO cad_especies (descricao, prioridade) values (:nome_especie, :prioridade)");
		$cadastrar -> bindValue(':nome_especie', $nomeEspecie, PDO::PARAM_STR);
		$cadastrar -> bindValue(':prioridade', $prioridade, PDO::PARAM_STR);
		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function insertModalidade($nomeModalidade){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("INSERT INTO cad_modalidades (descricao) values (:nomeModalidade)");
		$cadastrar -> bindValue(':nomeModalidade', $nomeModalidade, PDO::PARAM_STR);
		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function insertIsca($nomeIsca){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("INSERT INTO cad_iscas (descricao) values (:nomeIsca)");
		$cadastrar -> bindValue(':nomeIsca', $nomeIsca, PDO::PARAM_STR);
		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function insertColetor($nomeColetor, $serialColetor){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("INSERT INTO cad_coletores (descricao, serial) values (:nome, :serial)");
		$cadastrar -> bindValue(':nome', $nomeColetor, PDO::PARAM_STR);
		$cadastrar -> bindValue(':serial', $serialColetor, PDO::PARAM_STR);
		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function insertFiscal($nomeFiscal, $cpfFiscal){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("INSERT INTO cad_fiscais (nome, cpf) values (:nomeFiscal, :cpfFiscal)");
		$cadastrar -> bindValue(':nomeFiscal', $nomeFiscal, PDO::PARAM_STR);
		$cadastrar -> bindValue(':cpfFiscal', $cpfFiscal, PDO::PARAM_STR);
		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function insertCompetidor($nomeCompetidor, $tamanhoCompetidor, $apelidoCompetidor, $CPFCompetidor, $endereco, $num, $complemento, $bairro, $cidade, $uf, $pais, $cep, $celular, $email, $sexo, $data_nascimento, $modelagem){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("	INSERT INTO cad_competidores
											(nome, tamanho_camiseta, apelido, cpf, endereco, num, complemento, bairro, cidade, uf, pais, cep, modelagem_camiseta, celular, email, sexo, data_nascimento)
										VALUES
											(:nomeCompetidor, :tamanhoCompetidor, :apelidoCompetidor, :cpf_competidor, :endereco, :num, :complemento, :bairro, :cidade, :uf, :pais, :cep, :modelagem_camiseta, :celular, :email, :sexo, :data_nascimento)");

		$cadastrar -> bindValue(':nomeCompetidor', $nomeCompetidor, PDO::PARAM_STR);
		$cadastrar -> bindValue(':tamanhoCompetidor', $tamanhoCompetidor, PDO::PARAM_STR);
		$cadastrar -> bindValue(':apelidoCompetidor', $apelidoCompetidor, PDO::PARAM_STR);
		$cadastrar -> bindValue(':cpf_competidor', $CPFCompetidor, PDO::PARAM_STR);
        $cadastrar -> bindValue(':endereco', $endereco, PDO::PARAM_STR);
        $cadastrar -> bindValue(':num', $num, PDO::PARAM_STR);
        $cadastrar -> bindValue(':complemento', $complemento, PDO::PARAM_STR);
        $cadastrar -> bindValue(':bairro', $bairro, PDO::PARAM_STR);
        $cadastrar -> bindValue(':cidade', $cidade, PDO::PARAM_STR);
        $cadastrar -> bindValue(':uf', $uf, PDO::PARAM_STR);
        $cadastrar -> bindValue(':pais', $pais, PDO::PARAM_STR);
        $cadastrar -> bindValue(':cep', $cep, PDO::PARAM_STR);
        $cadastrar -> bindValue(':celular', $celular, PDO::PARAM_STR);
        $cadastrar -> bindValue(':email', $email, PDO::PARAM_STR);
        $cadastrar -> bindValue(':sexo', $sexo, PDO::PARAM_STR);
        $cadastrar -> bindValue(':data_nascimento', $data_nascimento, PDO::PARAM_STR);
        $cadastrar -> bindValue(':modelagem_camiseta', $modelagem, PDO::PARAM_STR);

		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function insertEtapa($nomeEtapa, $nomeEtapaApp, $newCadCampeonatoEtapa, $dataEtapa, $imagemEtapa, $imagemEtapa_pb, $peso_minimo, $unidadeMedida, $etapaModerada){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("	INSERT INTO cad_etapas
											(id_cad_campeonato, descricao, descricao_app, data_etapa, imagem, imagem_pb, peso_minimo, id_cad_unidade_medida, etapa_moderada)
										VALUES
											(:idCampeonato, :nome, :nomeApp, :data, :imagem, :imagem_pb, :peso_minimo, :id_cad_unidade_medida, :etapa_moderada)");
		$cadastrar -> bindValue(':nomeApp', $nomeEtapaApp, PDO::PARAM_STR);
		$cadastrar -> bindValue(':nome', $nomeEtapa, PDO::PARAM_STR);
		$cadastrar -> bindValue(':idCampeonato', $newCadCampeonatoEtapa, PDO::PARAM_STR);
		$cadastrar -> bindValue(':data', $dataEtapa, PDO::PARAM_STR);
		$cadastrar -> bindValue(':imagem', $imagemEtapa, PDO::PARAM_STR);
		$cadastrar -> bindValue(':imagem_pb', $imagemEtapa_pb, PDO::PARAM_STR);
		$cadastrar -> bindValue(':peso_minimo', $peso_minimo, PDO::PARAM_STR);
		$cadastrar -> bindValue(':id_cad_unidade_medida', $unidadeMedida, PDO::PARAM_STR);
		$cadastrar -> bindValue(':etapa_moderada', $etapaModerada, PDO::PARAM_STR);
		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function updateEtapa($nomeEtapa, $nomeEtapaApp, $idCadCampeonato, $dataEtapa, $idEtapa, $imagemEtapa, $imagemEtapa_pb, $peso_minimo, $unidadeMedida, $etapaModerada, $qtdBaterias, $datasEtapa, $tipoClassificacao, $uf_realizacao){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("	UPDATE cad_etapas set
											id_cad_campeonato = :idCampeonato,
											descricao = :nome,
											descricao_app = :nomeApp,
											data_etapa = :data,
											imagem = :image_etapa,
											imagem_pb = :imagem_etapa_pb,
											peso_minimo=:peso_minimo,
											id_cad_unidade_medida = :unidadeMedida,
											etapa_moderada = :etapa_moderada,
											qtd_baterias = :qtd_baterias,
											id_cad_tipo_classificacao = :tipoClassificacao,
											ID_CAD_UF_REALIZACAO = :uf_realizacao
										WHERE id=:idEtapa");

		$cadastrar -> bindValue(':idEtapa', $idEtapa, PDO::PARAM_STR);
		$cadastrar -> bindValue(':nomeApp', $nomeEtapaApp, PDO::PARAM_STR);
		$cadastrar -> bindValue(':nome', $nomeEtapa, PDO::PARAM_STR);
		$cadastrar -> bindValue(':idCampeonato', $idCadCampeonato, PDO::PARAM_STR);
		$cadastrar -> bindValue(':data', $dataEtapa, PDO::PARAM_STR);
		$cadastrar -> bindValue(':imagem_etapa_pb', $imagemEtapa_pb, PDO::PARAM_STR);
		$cadastrar -> bindValue(':image_etapa', $imagemEtapa, PDO::PARAM_STR);
		$cadastrar -> bindValue(':peso_minimo', $peso_minimo, PDO::PARAM_STR);
		$cadastrar -> bindValue(':unidadeMedida', $unidadeMedida, PDO::PARAM_STR);
		$cadastrar -> bindValue(':etapa_moderada', $etapaModerada, PDO::PARAM_STR);
		$cadastrar -> bindValue(':qtd_baterias', $qtdBaterias, PDO::PARAM_STR);
		$cadastrar -> bindValue(':tipoClassificacao', $tipoClassificacao, PDO::PARAM_STR);
		$cadastrar -> bindValue(':uf_realizacao', $uf_realizacao, PDO::PARAM_STR);
		$cadastrar -> execute();

		$cadastrar = $pdo -> prepare("DELETE from cad_etapas_datas WHERE id_cad_etapa= :idEtapa");
		$cadastrar -> bindValue(':idEtapa', $idEtapa, PDO::PARAM_STR);
		$cadastrar -> execute();

		//echo $datasEtapa;
		$array = explode(',', $datasEtapa); //split string into array seperated by ', '
		foreach($array as $data_realizacao) //loop over values
		{
			$data_realizacao = DateTime::createFromFormat('d/m/Y', $data_realizacao);
			//echo $data_realizacao->format('Y-m-d'). PHP_EOL;;
			$cadastrar = $pdo -> prepare("INSERT INTO cad_etapas_datas (id_cad_etapa, data_etapa) VALUES (:idEtapa, :data)");
			$cadastrar -> bindValue(':idEtapa', $idEtapa, PDO::PARAM_STR);
			$cadastrar -> bindValue(':data', $data_realizacao->format('Y-m-d'), PDO::PARAM_STR);
			$cadastrar -> execute();
		}

		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function updatePeixesAlvo($idEtapa, $peixesAlvo){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("DELETE from composicao_peixes_alvo WHERE id_cad_etapa= :idEtapa");
		$cadastrar -> bindValue(':idEtapa', $idEtapa, PDO::PARAM_STR);
		$cadastrar -> execute();

		$array = explode(',', $peixesAlvo);
		foreach($array as $peixeAlvo){
			$cadastrar = $pdo -> prepare("INSERT INTO composicao_peixes_alvo (id_cad_etapa, id_cad_especie) VALUES (:idEtapa, :idEspecie)");
			$cadastrar -> bindValue(':idEtapa', $idEtapa, PDO::PARAM_STR);
			$cadastrar -> bindValue(':idEspecie', $peixeAlvo);
			$cadastrar -> execute();
		}

		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function updateEquipe($idEquipe, $nomeEquipe, $idEtapa){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("	UPDATE cad_equipes set
											id_cad_etapa = :idEtapa,
											descricao = :nomeEquipe
										WHERE id=:idEquipe");
		$cadastrar -> bindValue(':idEquipe', $idEquipe, PDO::PARAM_STR);
		$cadastrar -> bindValue(':nomeEquipe', $nomeEquipe, PDO::PARAM_STR);
		$cadastrar -> bindValue(':idEtapa', $idEtapa, PDO::PARAM_STR);
		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function insertEquipe($nomeEquipe, $idEtapa){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("INSERT INTO cad_equipes (descricao, id_cad_etapa) values (:nomeEquipe, :idEtapa)");
		$cadastrar -> bindValue(':nomeEquipe', $nomeEquipe, PDO::PARAM_STR);
		$cadastrar -> bindValue(':idEtapa', $idEtapa, PDO::PARAM_STR);
		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function updateCompetidor($idCompetidor, $nomeCompetidor, $tamanhoCompetidor, $apelidoCompetidor, $cpfCompetidor, $endereco, $num, $complemento, $bairro, $cidade, $uf, $pais, $cep, $id_usuario_modificacao, $celular, $email, $sexo, $data_nascimento, $modelagem){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("	UPDATE cad_competidores
										SET
											nome = :nomeCompetidor,
											tamanho_camiseta = :tamanhoCompetidor,
											modelagem_camiseta = :modelagem,
											apelido = :apelidoCompetidor,
											cpf = :cpfCompetidor,
											endereco = :endereco,
											num = :num,
											complemento = :complemento,
											bairro = :bairro,
											cidade = :cidade,
											uf = :uf,
											pais = :pais,
											data_modificacao = CURRENT_TIMESTAMP,
											cep = :cep,
											id_usuario_modificacao =:id_usuario_modificacao,
											celular = :celular,
											email = :email,
											sexo = :sexo,
											data_nascimento = :data_nascimento
										WHERE
											id =:idCompetidor");


		$cadastrar -> bindValue(':nomeCompetidor', $nomeCompetidor, PDO::PARAM_STR);
		$cadastrar -> bindValue(':tamanhoCompetidor', $tamanhoCompetidor, PDO::PARAM_STR);
		$cadastrar -> bindValue(':modelagem', $modelagem, PDO::PARAM_STR);
		$cadastrar -> bindValue(':apelidoCompetidor', $apelidoCompetidor, PDO::PARAM_STR);
		$cadastrar -> bindValue(':cpfCompetidor', $cpfCompetidor, PDO::PARAM_STR);
        $cadastrar -> bindValue(':endereco', $endereco, PDO::PARAM_STR);
        $cadastrar -> bindValue(':num', $num, PDO::PARAM_STR);
        $cadastrar -> bindValue(':complemento', $complemento, PDO::PARAM_STR);
        $cadastrar -> bindValue(':bairro', $bairro, PDO::PARAM_STR);
        $cadastrar -> bindValue(':cidade', $cidade, PDO::PARAM_STR);
        $cadastrar -> bindValue(':uf', $uf, PDO::PARAM_STR);
        $cadastrar -> bindValue(':pais', $pais, PDO::PARAM_STR);
        $cadastrar -> bindValue(':cep', $cep, PDO::PARAM_STR);
        $cadastrar -> bindValue(':id_usuario_modificacao', $id_usuario_modificacao, PDO::PARAM_STR);
        $cadastrar -> bindValue(':celular', $celular, PDO::PARAM_STR);
        $cadastrar -> bindValue(':email', $email, PDO::PARAM_STR);
        $cadastrar -> bindValue(':sexo', $sexo, PDO::PARAM_STR);
        $cadastrar -> bindValue(':data_nascimento', $data_nascimento, PDO::PARAM_STR);
        $cadastrar -> bindValue(':idCompetidor', $idCompetidor, PDO::PARAM_STR);

		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function insertCampeonato($nomeCampeonato, $nomeCampeonatoApp, $imagemCampeonato, $imagemCampeonato_pb){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("INSERT INTO cad_campeonatos (descricao, descricao_app, imagem, imagem_pb) values (:nome_campeonato, :nomeCampeonatoApp, :imagem_campeonato, :imagem_campeonato_pb)");
		$cadastrar -> bindValue(':nome_campeonato', $nomeCampeonato, PDO::PARAM_STR);
		$cadastrar -> bindValue(':nomeCampeonatoApp', $nomeCampeonatoApp, PDO::PARAM_STR);
		$cadastrar -> bindValue(':imagem_campeonato', $imagemCampeonato, PDO::PARAM_STR);
		$cadastrar -> bindValue(':imagem_campeonato_pb', $imagemCampeonato_pb, PDO::PARAM_STR);
		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function updateCampeonato($nomeCampeonato, $nomeCampeonatoApp, $imagemCampeonato, $imagemCampeonato_pb, $idCampeonato){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("UPDATE cad_campeonatos set descricao = :nome_campeonato, descricao_app = :nome_campeonatoApp, imagem = :imagem_campeonato, imagem_pb = :imagem_campeonato_pb where id=:id_campeonato");
		$cadastrar -> bindValue(':nome_campeonato', $nomeCampeonato, PDO::PARAM_STR);
		$cadastrar -> bindValue(':nome_campeonatoApp', $nomeCampeonatoApp, PDO::PARAM_STR);
		$cadastrar -> bindValue(':imagem_campeonato', $imagemCampeonato, PDO::PARAM_STR);
		$cadastrar -> bindValue(':imagem_campeonato_pb', $imagemCampeonato_pb, PDO::PARAM_STR);
		$cadastrar -> bindValue(':id_campeonato', $idCampeonato, PDO::PARAM_STR);
		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function updateEspecie($nomeEspecie, $idEspecie, $prioridade){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("UPDATE cad_especies set descricao = :descricao, prioridade = :prioridade where id = :id");
		$cadastrar -> bindValue(':descricao', $nomeEspecie, PDO::PARAM_STR);
		$cadastrar -> bindValue(':id', $idEspecie, PDO::PARAM_STR);
		$cadastrar -> bindValue(':prioridade', $prioridade, PDO::PARAM_STR);
		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function updateModalidade($nomeModalidade, $idModalidade){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("UPDATE cad_modalidades set descricao = :descricao where id = :id");
		$cadastrar -> bindValue(':descricao', $nomeModalidade, PDO::PARAM_STR);
		$cadastrar -> bindValue(':id', $idModalidade, PDO::PARAM_STR);
		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function updateIsca($nomeIsca, $idIsca){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("UPDATE cad_iscas set descricao = :descricao where id = :id");
		$cadastrar -> bindValue(':descricao', $nomeIsca, PDO::PARAM_STR);
		$cadastrar -> bindValue(':id', $idIsca, PDO::PARAM_STR);
		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function updateColetor($nomeColetor, $serialColetor, $idColetor){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("UPDATE cad_coletores set descricao = :descricao, serial = :serial where id = :id");
		$cadastrar -> bindValue(':descricao', $nomeColetor, PDO::PARAM_STR);
		$cadastrar -> bindValue(':serial', $serialColetor, PDO::PARAM_STR);
		$cadastrar -> bindValue(':id', $idColetor, PDO::PARAM_STR);
		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function updateFiscal($nomeFiscal, $cpfFiscal, $idFiscal){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("UPDATE cad_fiscais set nome = :nomeFiscal, cpf = :cpfFiscal where id = :idFiscal");
		$cadastrar -> bindValue(':nomeFiscal', $nomeFiscal, PDO::PARAM_STR);
		$cadastrar -> bindValue(':cpfFiscal', $cpfFiscal, PDO::PARAM_STR);
		$cadastrar -> bindValue(':idFiscal', $idFiscal, PDO::PARAM_STR);
		$cadastrar -> execute();
		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaCampeonatos($campeonatoNome, $ativos_inativos_todos){
	$pdo = conecta();
	try {
		$sql = '';
		if($ativos_inativos_todos == 'A'){
			$sql = " cad_campeonatos.ATIVO = 'S'";
		}else{
			if($ativos_inativos_todos == 'I'){
				$sql = " cad_campeonatos.ATIVO = 'N'";
			}
		}

		if ($campeonatoNome != ''){
			if($sql != ''){
				$sql = $sql + " and descricao like '%" . $campeonatoNome . "%'";
			}else{
				$sql = " descricao like '%" . $campeonatoNome . "%'";
			}
		}
		if($sql != ''){
			$sql = 'where' . $sql;
		}

		$consultar = $pdo -> prepare("SELECT id, descricao, ativo from cad_campeonatos " . $sql . " order by descricao");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaColetores($coletoresNome){
	$pdo = conecta();
	try {
		$sql = '';

		if ($coletoresNome != ''){
			if($sql != ''){
				$sql = $sql + " and descricao like '%" . $coletoresNome . "%'";
			}else{
				$sql = " descricao like '%" . $coletoresNome . "%'";
			}
		}
		if($sql != ''){
			$sql = 'where' . $sql;
		}

		$consultar = $pdo -> prepare("SELECT id, descricao, ativo, serial from cad_coletores " . $sql . " order by descricao");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}


function retornaCampeonato($campeonatoId){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("SELECT id, descricao, descricao_app, imagem from cad_campeonatos where id=" . $campeonatoId);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaUFs(){
    $pdo = conecta();
    try {
            $consultar = $pdo -> prepare("SELECT id, sigla, descricao from cad_estados order by sigla");
            $consultar -> execute();
            if ($consultar -> rowCount() > 0) {
                return $consultar -> fetchAll(PDO::FETCH_OBJ);
            } else {
                return false;
            }
    } catch(PDOException $e) {
            echo $e -> getMessage();
    }
}

function retornaEquipesComposicaoAdicionadas($idEtapa, $idEquipe){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("	SELECT
											composicao_equipes.id,
											composicao_equipes.id_pulseira,
											composicao_equipes.id_competidor,
											cad_competidores.NOME competidor,
											cad_competidores.APELIDO,
											composicao_equipes.SUPLENTE
										FROM
											composicao_equipes
											INNER JOIN cad_competidores ON ( cad_competidores.ID = composicao_equipes.id_competidor )
											INNER JOIN composicao_etapas ON ( composicao_equipes.id_equipe = composicao_etapas.ID_CAD_EQUIPE )
										WHERE
											composicao_equipes.id_equipe = :id_equipe
											AND composicao_etapas.ID_CAD_ETAPA = :id_cad_etapa
										ORDER BY
											cad_competidores.nome");

		$consultar -> bindValue(':id_cad_etapa', $idEtapa, PDO::PARAM_STR);
		$consultar -> bindValue(':id_equipe', $idEquipe, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
				if($linha -> id_pulseira == 0){
					$qrCode= '<span class="btn btn-xs btn-danger" onclick="informarQRCode('. $linha -> id .'); event.stopPropagation();"><span class="glyphicon glyphicon-qrcode" aria-hidden="true"></span> N/D</span>';
				}else{
					$qrCode= '<span class="btn btn-xs btn-default" onclick="informarQRCode('. $linha -> id .'); event.stopPropagation();"><span class="glyphicon glyphicon-qrcode" aria-hidden="true"></span> '. $linha -> id_pulseira .'</span>';
				}

				if($linha -> SUPLENTE == 'S'){
					$suplente = '<span class="btn btn-xs btn-warning" onclick="marcaSuplente('. $linha -> id .', \'N\'); event.stopPropagation();" ><span class="glyphicon glyphicon-knight" aria-hidden="true"></span> Suplente</span> ';
				}else{
					$suplente = '<span class="btn btn-xs btn-outline-warning" onclick="marcaSuplente('. $linha -> id .', \'S\'); event.stopPropagation();"><span class="glyphicon glyphicon-knight" aria-hidden="true"></span> Suplente</span> ';
				}

				echo '<a class="list-group-item clearfix">
					' . $linha -> competidor  . '<br><b>Apelido:</b> <span style="cursor:pointer" id="apelidoCompet'.$linha -> id_competidor.'" onclick="alteraApelido('.$linha -> id_competidor.', \''.$linha -> APELIDO.'\')">'.$linha -> APELIDO.'</span>
					<span class="pull-right">
						'.$suplente.$qrCode.'
						<span class="btn btn-xs btn-default" onclick="excluirComposicaoEquipe('. $linha -> id .'); event.stopPropagation();">
							<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						</span>
					</span>
				</a>';
			}
		} else {
			echo '<b>Nenhum competidor nessa equipe!</b>';
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaEquipesComposicaoCompetidores($idEtapa){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("SELECT id, nome from cad_competidores where id not in (SELECT composicao_equipes.id_competidor from composicao_equipes inner join composicao_etapas on (composicao_equipes.id_equipe = composicao_etapas.ID_CAD_EQUIPE) where composicao_etapas.ID_CAD_ETAPA = :id_cad_etapa) order by cad_competidores.data_cadastro desc, cad_competidores.nome limit 25");
		$consultar -> bindValue(':id_cad_etapa', $idEtapa, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
				echo '<a class="list-group-item clearfix">
					' . $linha -> nome . '
					<span class="pull-right">
						<span class="btn btn-xs btn-default" onclick="insereComposicaoEquipe('. $linha -> id .'); event.stopPropagation();">
							<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
						</span>
					</span>
				</a>';
			}
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaColetoresDisponiveisEtapa($idEtapa){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("SELECT
										cad_coletores.id,
										cad_coletores.descricao,
										cad_coletores.serial
									FROM
										cad_coletores
									WHERE
										ativo = 'S'
										AND id NOT IN ( SELECT composicao_etapa_coletor.id_cad_coletor FROM composicao_etapa_coletor WHERE composicao_etapa_coletor.id_cad_etapa = :id_cad_etapa )
									ORDER BY
										cad_coletores.descricao");
		$consultar -> bindValue(':id_cad_etapa', $idEtapa, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaFiscaisDisponiveisEtapa($idEtapa){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("SELECT
										cad_fiscais.id,
										cad_fiscais.nome,
										cad_fiscais.cpf
									FROM
										cad_fiscais
									WHERE
										ativo = 'S'
										AND id NOT IN ( SELECT COALESCE(composicao_etapa_coletor.id_cad_fiscal,0) FROM composicao_etapa_coletor WHERE composicao_etapa_coletor.id_cad_etapa = :id_cad_etapa )
									ORDER BY
										cad_fiscais.nome");
		$consultar -> bindValue(':id_cad_etapa', $idEtapa, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaColetoresEtapa($idEtapa){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("SELECT
										cad_coletores.id,
										cad_coletores.descricao,
										cad_coletores.serial,
										composicao_etapa_coletor.id_cad_fiscal,
										composicao_etapa_coletor.id AS id_composicao,
										cad_fiscais.nome AS fiscal_nome
									FROM
										cad_coletores
										INNER JOIN composicao_etapa_coletor ON ( cad_coletores.id = composicao_etapa_coletor.id_cad_coletor )
										LEFT JOIN cad_fiscais ON ( composicao_etapa_coletor.id_cad_fiscal = cad_fiscais.id )
									WHERE
										composicao_etapa_coletor.id_cad_etapa = :id_cad_etapa");
		$consultar -> bindValue(':id_cad_etapa', $idEtapa, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaBateriasEtapa($idEtapa){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("	SELECT
											baterias.id,
											baterias.id_cad_etapa,
											DATE_FORMAT(baterias.horario_inicio, '%H:%i') horario_inicio,
											DATE_FORMAT(baterias.horario_termino, '%H:%i') horario_termino
										FROM
											cad_etapas_baterias baterias
										WHERE
											baterias.id_cad_etapa = :id_cad_etapa
										ORDER BY
											baterias.horario_inicio");

		$consultar -> bindValue(':id_cad_etapa', $idEtapa, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaBuscaEquipesComposicaoCompetidores($idEtapa, $nome){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("SELECT id, nome from cad_competidores where id not in (SELECT composicao_equipes.id_competidor from composicao_equipes inner join composicao_etapas on (composicao_equipes.id_equipe = composicao_etapas.ID_CAD_EQUIPE) where composicao_etapas.ID_CAD_ETAPA = :id_cad_etapa ) and ((cad_competidores.NOME like :nome_competidor) or (cad_competidores.id = :cod_competidor)) order by cad_competidores.nome");
		$consultar -> bindValue(':id_cad_etapa', $idEtapa, PDO::PARAM_STR);
		$consultar -> bindValue(':nome_competidor', '%'.$nome.'%', PDO::PARAM_STR);
		$consultar -> bindValue(':cod_competidor', $nome, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaQtdContatosAtualizados($dataDe, $dataAte){
    $pdo = conecta();
    try{
        $consultar = $pdo -> prepare("SELECT count(ID) TOTAL, DATE(cad_competidores.DATA_MODIFICACAO) DATA_MODIFICACAO, cad_competidores.UF from cad_competidores where date(DATA_MODIFICACAO) BETWEEN :dataDe and :dataAte group by DATE(cad_competidores.DATA_MODIFICACAO), cad_competidores.UF");
        $consultar -> bindValue(':dataDe', $dataDe, PDO::PARAM_STR);
        $consultar -> bindValue(':dataAte', $dataAte, PDO::PARAM_STR);
        $consultar -> execute();
        if ($consultar -> rowCount() > 0) {
                return $consultar -> fetchAll(PDO::FETCH_OBJ);
        } else {
                return false;
        }
    } catch(PDOException $e) {
            echo $e -> getMessage();
    }
}

function retornaQtdContatosAtualizadosTotalUF($dataDe, $dataAte){
    $pdo = conecta();
    try{
        $consultar = $pdo -> prepare("SELECT count(ID) TOTAL, cad_competidores.UF from cad_competidores where date(DATA_MODIFICACAO) BETWEEN :dataDe and :dataAte group by cad_competidores.UF");
        $consultar -> bindValue(':dataDe', $dataDe, PDO::PARAM_STR);
        $consultar -> bindValue(':dataAte', $dataAte, PDO::PARAM_STR);
        $consultar -> execute();
        if ($consultar -> rowCount() > 0) {
                return $consultar -> fetchAll(PDO::FETCH_OBJ);
        } else {
                return false;
        }
    } catch(PDOException $e) {
            echo $e -> getMessage();
    }
}

function retornaQtdContatosAtualizadosAgrupado($dataDe){
    $pdo = conecta();
    try{
        $consultar = $pdo -> prepare("SELECT count(t.TOTAL) TOTAL from (select count(ID) TOTAL from cad_competidores where date(DATA_MODIFICACAO) = :dataDe group by DATE(cad_competidores.DATA_MODIFICACAO), cad_competidores.UF) t");
        $consultar -> bindValue(':dataDe', $dataDe, PDO::PARAM_STR);
        $consultar -> execute();
        if ($consultar -> rowCount() > 0) {
                return $consultar -> fetch(PDO::FETCH_OBJ);
        } else {
                return false;
        }
    } catch(PDOException $e) {
            echo $e -> getMessage();
    }
}



function excluiEquipeComposicao($idComposicaoEquipe){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("DELETE from composicao_equipes where composicao_equipes.id = :id");
		$consultar -> bindValue(':id', $idComposicaoEquipe, PDO::PARAM_STR);
		$consultar -> execute();
		return true;
	} catch(PDOException $e) {
		echo $e -> getMessage();
		return false;
	}
}

function salvaHistoriaEquipe($idEquipe, $historia){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("UPDATE cad_equipes set HISTORIA = :historia where cad_equipes.id = :idEquipe");
		$consultar -> bindValue(':idEquipe', $idEquipe, PDO::PARAM_STR);
		$consultar -> bindValue(':historia', $historia, PDO::PARAM_STR);

		$consultar -> execute();
		return true;
	} catch(PDOException $e) {
		echo $e -> getMessage();
		return false;
	}
}

function salvaCuriosidadesEquipe($idEquipe, $curiosidades){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("UPDATE cad_equipes set CURIOSIDADES = :curiosidades where cad_equipes.id = :idEquipe");
		$consultar -> bindValue(':idEquipe', $idEquipe, PDO::PARAM_STR);
		$consultar -> bindValue(':curiosidades', $curiosidades, PDO::PARAM_STR);

		$consultar -> execute();
		return true;
	} catch(PDOException $e) {
		echo $e -> getMessage();
		return false;
	}
}

function insereComposicaoEquipe($idEquipe, $idCompetidor){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("INSERT into composicao_equipes (id_equipe, id_competidor) values (:id_cad_equipe, :id_cad_competidor)");
		$consultar -> bindValue(':id_cad_equipe', $idEquipe, PDO::PARAM_STR);
		$consultar -> bindValue(':id_cad_competidor', $idCompetidor, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return true;
		} else {
			return $consultar ;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function insereComposicaoColetor($idColetor, $idEtapa){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("INSERT into composicao_etapa_coletor (id_cad_coletor, id_cad_etapa) values (:idColetor, :idEtapa)");
		$consultar -> bindValue(':idColetor', $idColetor, PDO::PARAM_STR);
		$consultar -> bindValue(':idEtapa', $idEtapa, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return true;
		} else {
			return $consultar ;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function insereBateria($id_cad_etapa, $horario_inicio, $horario_termino){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("INSERT into cad_etapas_baterias (id_cad_etapa, horario_inicio, horario_termino) values (:id_cad_etapa, :horario_inicio, :horario_termino)");
		$consultar -> bindValue(':id_cad_etapa', $id_cad_etapa, PDO::PARAM_STR);
		$consultar -> bindValue(':horario_inicio', $horario_inicio, PDO::PARAM_STR);
		$consultar -> bindValue(':horario_termino', $horario_termino, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return true;
		} else {
			return $consultar ;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function insereTipoOrgRaiasEtapa($idTipoOrgRaias, $idEtapa){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("UPDATE cad_etapas SET ID_CAD_TIPO_ORG_RAIA = :idTipoOrgRaias where ID = :idEtapa");
		$consultar -> bindValue(':idTipoOrgRaias', $idTipoOrgRaias, PDO::PARAM_STR);
		$consultar -> bindValue(':idEtapa', $idEtapa, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return true;
		} else {
			return $consultar ;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function insereComposicaoFiscal($idFiscal, $idComposicao){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("UPDATE composicao_etapa_coletor SET id_cad_fiscal = :idFiscal where id = :idComposicao");
		$consultar -> bindValue(':idFiscal', $idFiscal, PDO::PARAM_STR);
		$consultar -> bindValue(':idComposicao', $idComposicao, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return true;
		} else {
			return $consultar ;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function excluiComposicaoColetor($idColetor, $idEtapa){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("DELETE from composicao_etapa_coletor where id_cad_coletor = :idColetor and id_cad_etapa = :idEtapa");
		$consultar -> bindValue(':idColetor', $idColetor, PDO::PARAM_STR);
		$consultar -> bindValue(':idEtapa', $idEtapa, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return true;
		} else {
			return false ;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function excluiBateriaEtapa($idBateria, $idEtapa){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("DELETE from cad_etapas_baterias where id = :idBateria and id_cad_etapa = :idEtapa");
		$consultar -> bindValue(':idBateria', $idBateria, PDO::PARAM_STR);
		$consultar -> bindValue(':idEtapa', $idEtapa, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return true;
		} else {
			return false ;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function insereComposicaoIdentificacaoCompetidor($idComposicao, $idPulseira){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("UPDATE composicao_equipes set id_pulseira = :id_pulseira where id = :id_composicao");
		$consultar -> bindValue(':id_pulseira', $idPulseira, PDO::PARAM_STR);
		$consultar -> bindValue(':id_composicao', $idComposicao, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return true;
		} else {
			return $consultar ;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function updateApelidoCompetidor($idCompetidor, $novoApelido, $anteriorApelido){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("UPDATE cad_competidores set APELIDO_ANTIGO = :anteriorApelido, APELIDO = :novoApelido where id = :idCompetidor");
		$consultar -> bindValue(':idCompetidor', $idCompetidor, PDO::PARAM_STR);
		$consultar -> bindValue(':novoApelido', $novoApelido, PDO::PARAM_STR);
		$consultar -> bindValue(':anteriorApelido', $anteriorApelido, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return true;
		} else {
			return $consultar ;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function marcaComposicaoSuplenteCompetidor($idComposicao, $marcacao){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("UPDATE composicao_equipes set SUPLENTE = :marcacao where id = :idComposicao");
		$consultar -> bindValue(':marcacao', $marcacao, PDO::PARAM_STR);
		$consultar -> bindValue(':idComposicao', $idComposicao, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return true;
		} else {
			return $consultar ;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function alteraPatrocinadorEquipe($idEquipe, $patrocinador){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("UPDATE cad_equipes set PATROCINADOR = :patrocinador where id = :idEquipe");
		$consultar -> bindValue(':patrocinador', $patrocinador, PDO::PARAM_STR);
		$consultar -> bindValue(':idEquipe', $idEquipe, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return true;
		} else {
			return $consultar ;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function insereComposicaoIdentificacaoRaia($idComposicao, $idRaiaInicial){
	$pdo = conecta();
	try{

		$consultar = $pdo -> prepare("UPDATE composicao_etapas set RAIA_INICIAL = :idRaiaInicial where ID = :id_composicao");
		$consultar -> bindValue(':idRaiaInicial', empty($idRaiaInicial) ? NULL : $idRaiaInicial, PDO::PARAM_INT);
		$consultar -> bindValue(':id_composicao', $idComposicao, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return true;
		} else {
			return $consultar ;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaEtapasComposicaoAdicionadas($idEtapa){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("SELECT composicao_etapas.id, composicao_etapas.ID_CAD_EQUIPE, cad_equipes.DESCRICAO equipe from composicao_etapas inner join cad_equipes on (cad_equipes.ID = composicao_etapas.ID_CAD_EQUIPE) where composicao_etapas.ID_CAD_ETAPA = ".$idEtapa." order by cad_equipes.descricao");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
				echo "<li class='ui-state-highlight form-control' value=". $linha -> id .">" . $linha -> equipe . "</li>";
			}
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaEtapasComposicaoDisponiveis($idEtapa){
	$pdo = conecta();
	try{
		$consultar = $pdo -> prepare("SELECT cad_equipes.id, cad_equipes.DESCRICAO from cad_equipes where id not in ( SELECT composicao_etapas.ID_CAD_EQUIPE from composicao_etapas) and ID_CAD_ETAPA = " . $idEtapa);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			while ($linha = $consultar -> fetch(PDO::FETCH_OBJ)) {
				echo "<li class='ui-state-highlight form-control' value=". $linha -> id .">" . $linha -> DESCRICAO . "</li>";
			}
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}


function retornaEtapas($etapaNome, $idCampeonato){
	$pdo = conecta();
	try {
		$sql = '';
		if ($etapaNome != ''){
			$sql = " cad_etapas.DESCRICAO like '%" . $etapaNome . "%'";
		}
		if ($idCampeonato > 0){
			if($sql != ''){
				$sql = $sql . " and cad_etapas.id_cad_campeonato = " . $idCampeonato;
			}else{
				$sql = " cad_etapas.id_cad_campeonato = " . $idCampeonato;
			}
		}

		if($sql != ''){
			$sql = 'where' . $sql;
		}
		$consultar = $pdo -> prepare("SELECT cad_etapas.id, cad_etapas.DESCRICAO, cad_etapas.DATA_ETAPA, cad_etapas.ID_CAD_CAMPEONATO, cad_campeonatos.DESCRICAO campeonato from cad_etapas inner join cad_campeonatos on (cad_campeonatos.id = cad_etapas.ID_CAD_CAMPEONATO) ".$sql." order by cad_etapas.DATA_ETAPA desc, cad_etapas.ID_CAD_CAMPEONATO  ");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}


function retornaEtapa($etapaId){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("SELECT cad_etapas.*, count(cad_equipes.ID) QTD_EQUIPES from cad_etapas inner join cad_equipes on (cad_equipes.ID_CAD_ETAPA = cad_etapas.ID) where cad_etapas.id=" . $etapaId);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaDatasEtapa($etapaId){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("	SELECT
											GROUP_CONCAT( DATE_FORMAT(ced.data_etapa, '%d/%m/%Y') SEPARATOR ', ') DATAS_REALIZACAO,
											COUNT(ced.id) TOTAL_DATAS
										FROM
											cad_etapas_datas ced
										WHERE
											ced.id_cad_etapa = " . $etapaId);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaModalidades($modalidadeNome){
	$pdo = conecta();
	try {
		$sql = '';
		if ($modalidadeNome != ''){
			$sql = " descricao like '%" . $modalidadeNome . "%'";
		}
		if($sql != ''){
			$sql = 'where' . $sql;
		}
		$consultar = $pdo -> prepare("SELECT id, descricao from cad_modalidades " . $sql . " order by descricao");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaIscas($iscaNome){
	$pdo = conecta();
	try {
		$sql = '';
		if ($iscaNome != ''){
			$sql = " descricao like '%" . $iscaNome . "%'";
		}
		if($sql != ''){
			$sql = 'where' . $sql;
		}
		$consultar = $pdo -> prepare("SELECT id, descricao from cad_iscas " . $sql . " order by descricao");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaFiscais($fiscalNome){
	$pdo = conecta();
	try {
		$sql = '';
		if ($fiscalNome != ''){
			$sql = " nome like '%" . $fiscalNome . "%'";
		}
		if($sql != ''){
			$sql = 'where' . $sql;
		}
		$consultar = $pdo -> prepare("SELECT id, nome, cpf from cad_fiscais " . $sql . " order by nome");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaUnidadesMedida($idUnidadeMedida){
	$pdo = conecta();
	try {
		$sql = '';
		if ($idUnidadeMedida != ''){
			$sql = " id = " . $idUnidadeMedida;
		}
		if($sql != ''){
			$sql = 'where' . $sql;
		}

		$consultar = $pdo -> prepare("SELECT id, descricao, abreviacao from cad_unidades_medida " . $sql . " order by descricao");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaUnidadeMedidaEtapa($idEtapa){
	$pdo = conecta();
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$consultar = $pdo -> prepare("SELECT cad_unidades_medida.id, cad_unidades_medida.descricao, cad_unidades_medida.abreviacao from cad_unidades_medida inner join cad_etapas on (cad_unidades_medida.id = cad_etapas.id_cad_unidade_medida) where cad_etapas.ID = " . $idEtapa);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaSeModeradaEtapa($idEtapa){
	$pdo = conecta();
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$consultar = $pdo -> prepare("SELECT id from cad_etapas where cad_etapas.ID = " . $idEtapa . " and ETAPA_MODERADA='S' order by id");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaTipoClassificacao($idEtapa){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("SELECT ID_CAD_TIPO_CLASSIFICACAO from cad_etapas where id = " . $idEtapa);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaModalidade($modalidadeId){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("SELECT id, descricao from cad_modalidades where id = " . $modalidadeId);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaIsca($iscaId){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("SELECT id, descricao from cad_iscas where id = " . $iscaId);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaColetor($coletorId){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("SELECT id, descricao, ativo, serial from cad_coletores where id = " . $coletorId);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaFiscal($fiscalId){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("SELECT id, nome, cpf from cad_fiscais where id = " . $fiscalId);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaEspecies($especieNome, $prioridade){
	$pdo = conecta();
	try {
		$sql = '';
		if ($especieNome != ''){
			$sql = " descricao like '%" . $especieNome . "%'";
		}
		if($prioridade != ''){
			if($sql == ''){
				$sql = " prioridade = " . $prioridade ;
			}else{
				$sql .= " and prioridade = " . $prioridade ;
			}
		}

		if($sql != ''){
			$sql = 'where' . $sql;
		}
		$consultar = $pdo -> prepare("SELECT id, descricao, prioridade from cad_especies " . $sql . " order by descricao");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaEspecie($especieId){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("SELECT id, descricao, prioridade from cad_especies where id = " . $especieId . " order by descricao");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaPeixesAlvo($idEtapa){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("SELECT GROUP_CONCAT( composicao_peixes_alvo.ID_CAD_ESPECIE SEPARATOR ', ' ) PEIXES_ALVO from composicao_peixes_alvo where id_cad_etapa = " . $idEtapa );
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}

}

function retornaEtapaHoje($moderada){
	$pdo = conecta();
	try {
		if($moderada==null){
			$consultar = $pdo -> prepare("SELECT ce.* FROM cad_etapas ce inner join cad_etapas_datas de on (ce.ID = de.id_cad_etapa) WHERE de.data_etapa = CURRENT_DATE");
		}else{
			$consultar = $pdo -> prepare("SELECT ce.* FROM cad_etapas ce inner join cad_etapas_datas de on (ce.ID = de.id_cad_etapa) WHERE de.data_etapa = CURRENT_DATE and ETAPA_MODERADA='S'");
		}

		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaTiposOrgRaias(){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("SELECT * FROM cad_tipos_organizacao_raias");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaResultadosLive($ultimaVerificacao, $idEtapa, $tela, $idUltimaVerificacao){
	$pdo = conecta();
	$etapaModerada = retornaSeModeradaEtapa($idEtapa);
	$sql = '';
	if(empty($idUltimaVerificacao))  {
		$idUltimaVerificacao = 0;
	}
	try {
		if($tela == 'live'){
			if($etapaModerada == true){
				$sql = ' and cad_etapas.ETAPA_MODERADA = "S" and pesagens.MODERADO_EM IS NOT NULL ' ;
			}

			if($ultimaVerificacao == ''){
				$consultar = $pdo -> prepare("	SELECT
													pesagens.id,
													pesagens.ID_ETAPA,
													cad_etapas.DESCRICAO ETAPA_NOME,
													pesagens.ID_CAD_COMPETIDOR,
													cad_competidores.NOME,
													cad_competidores.APELIDO,
													pesagens.COMPROVANTE,
													pesagens.ID_CAD_ESPECIE,
													cad_especies.DESCRICAO ESPECIE_NOME,
													pesagens.PONTO_PESAGEM,
													pesagens.ID_CAD_MODALIDADE,
													pesagens.EXCLUIDO,
													pesagens.OBS,
													cad_modalidades.DESCRICAO MODALIDADE_NOME,
													cad_iscas.DESCRICAO ISCA_NOME,
													pesagens.PESO,
													pesagens.DATA_HORA,
													cad_equipes.DESCRICAO EQUIPE_NOME,
													cad_equipes.ID EQUIPE_ID
												FROM
													pesagens
													INNER JOIN cad_competidores ON ( cad_competidores.ID = pesagens.ID_CAD_COMPETIDOR )
													INNER JOIN cad_especies ON ( cad_especies.ID = pesagens.ID_CAD_ESPECIE )
													INNER JOIN cad_iscas ON ( cad_iscas.id = pesagens.id_cad_isca )
													INNER JOIN cad_modalidades ON ( cad_modalidades.ID = pesagens.ID_CAD_MODALIDADE )
													INNER JOIN cad_etapas ON ( cad_etapas.ID = pesagens.ID_ETAPA )
													INNER JOIN cad_equipes ON ( pesagens.ID_CAD_EQUIPE = cad_equipes.ID )
												WHERE
													pesagens.id > $idUltimaVerificacao AND
													DATE( pesagens.DATA_HORA ) <= DATE( NOW( ) )
													AND pesagens.ID_ETAPA = ".$idEtapa. $sql . "
												ORDER BY
													pesagens.DATA_HORA DESC");
			}else{
				$consultar = $pdo -> prepare("	SELECT
													pesagens.id,
													pesagens.ID_ETAPA,
													cad_etapas.DESCRICAO ETAPA_NOME,
													pesagens.ID_CAD_COMPETIDOR,
													cad_competidores.NOME,
													cad_competidores.APELIDO,
													pesagens.COMPROVANTE,
													pesagens.ID_CAD_ESPECIE,
													cad_especies.DESCRICAO ESPECIE_NOME,
													pesagens.PONTO_PESAGEM,
													pesagens.ID_CAD_MODALIDADE,
													pesagens.EXCLUIDO,
													pesagens.OBS,
													cad_modalidades.DESCRICAO MODALIDADE_NOME,
													cad_iscas.DESCRICAO ISCA_NOME,
													pesagens.PESO,
													pesagens.DATA_HORA,
													cad_equipes.DESCRICAO EQUIPE_NOME,
													cad_equipes.ID EQUIPE_ID
												FROM
													pesagens
													INNER JOIN cad_competidores ON ( cad_competidores.ID = pesagens.ID_CAD_COMPETIDOR )
													INNER JOIN cad_especies ON ( cad_especies.ID = pesagens.ID_CAD_ESPECIE )
													INNER JOIN cad_iscas ON ( cad_iscas.id = pesagens.id_cad_isca )
													INNER JOIN cad_modalidades ON ( cad_modalidades.ID = pesagens.ID_CAD_MODALIDADE )
													INNER JOIN cad_etapas ON ( cad_etapas.ID = pesagens.ID_ETAPA )
													INNER JOIN cad_equipes ON ( pesagens.ID_CAD_EQUIPE = cad_equipes.ID )
												WHERE
													pesagens.id > $idUltimaVerificacao AND
													DATE( pesagens.DATA_HORA ) = DATE( NOW( ) )
													AND TIME( pesagens.IMPORTADO_EM ) > TIME( '".$ultimaVerificacao."' )
													AND pesagens.ID_ETAPA = ".$idEtapa. $sql . "
												ORDER BY
													pesagens.DATA_HORA DESC");
			}

		}else{
			if($tela = 'mod'){
				if($etapaModerada == true){
					$sql = ' and cad_etapas.ETAPA_MODERADA = "S" ' ;
				}
			}

			if($ultimaVerificacao == ''){
				$consultar = $pdo -> prepare("	SELECT
													pesagens.id,
													pesagens.ID_ETAPA,
													cad_etapas.DESCRICAO ETAPA_NOME,
													pesagens.ID_CAD_COMPETIDOR,
													cad_competidores.NOME,
													cad_competidores.APELIDO,
													pesagens.COMPROVANTE,
													pesagens.ID_CAD_ESPECIE,
													cad_especies.DESCRICAO ESPECIE_NOME,
													pesagens.PONTO_PESAGEM,
													pesagens.ID_CAD_MODALIDADE,
													pesagens.EXCLUIDO,
													pesagens.OBS,
													cad_modalidades.DESCRICAO MODALIDADE_NOME,
													cad_iscas.DESCRICAO ISCA_NOME,
													pesagens.PESO,
													pesagens.DATA_HORA,
													cad_equipes.DESCRICAO EQUIPE_NOME
												FROM
													pesagens
													INNER JOIN cad_competidores ON ( cad_competidores.ID = pesagens.ID_CAD_COMPETIDOR )
													INNER JOIN cad_especies ON ( cad_especies.ID = pesagens.ID_CAD_ESPECIE )
													INNER JOIN cad_iscas ON ( cad_iscas.id = pesagens.id_cad_isca )
													INNER JOIN cad_modalidades ON ( cad_modalidades.ID = pesagens.ID_CAD_MODALIDADE )
													INNER JOIN cad_etapas ON ( cad_etapas.ID = pesagens.ID_ETAPA )
													INNER JOIN cad_equipes ON ( pesagens.ID_CAD_EQUIPE = cad_equipes.ID )
												WHERE
													pesagens.id > $idUltimaVerificacao AND
													( DATE( pesagens.MODERADO_EM ) <= DATE( NOW( ) ) OR ( pesagens.MODERADO_EM IS NULL ) )
													AND pesagens.ID_ETAPA = ".$idEtapa. $sql . "
												ORDER BY
													pesagens.MODERADO_EM DESC");
			}else{
				$consultar = $pdo -> prepare("	SELECT
													pesagens.id,
													pesagens.ID_ETAPA,
													cad_etapas.DESCRICAO ETAPA_NOME,
													pesagens.ID_CAD_COMPETIDOR,
													cad_competidores.NOME,
													cad_competidores.APELIDO,
													pesagens.COMPROVANTE,
													pesagens.ID_CAD_ESPECIE,
													cad_especies.DESCRICAO ESPECIE_NOME,
													pesagens.PONTO_PESAGEM,
													pesagens.ID_CAD_MODALIDADE,
													pesagens.EXCLUIDO,
													pesagens.OBS,
													cad_modalidades.DESCRICAO MODALIDADE_NOME,
													cad_iscas.DESCRICAO ISCA_NOME,
													pesagens.PESO,
													pesagens.DATA_HORA,
													cad_equipes.DESCRICAO EQUIPE_NOME
												FROM
													pesagens
													INNER JOIN cad_competidores ON ( cad_competidores.ID = pesagens.ID_CAD_COMPETIDOR )
													INNER JOIN cad_especies ON ( cad_especies.ID = pesagens.ID_CAD_ESPECIE )
													INNER JOIN cad_iscas ON ( cad_iscas.id = pesagens.id_cad_isca )
													INNER JOIN cad_modalidades ON ( cad_modalidades.ID = pesagens.ID_CAD_MODALIDADE )
													INNER JOIN cad_etapas ON ( cad_etapas.ID = pesagens.ID_ETAPA )
													INNER JOIN cad_equipes ON ( pesagens.ID_CAD_EQUIPE = cad_equipes.ID )
												WHERE
													pesagens.id > $idUltimaVerificacao AND
													(
													DATE( pesagens.MODERADO_EM ) = DATE( NOW( ) )
													AND TIME( pesagens.MODERADO_EM ) > TIME( '".$ultimaVerificacao."' )
													OR ( pesagens.MODERADO_EM IS NULL )
													)
													AND pesagens.ID_ETAPA = ".$idEtapa. $sql . "
												ORDER BY
													pesagens.MODERADO_EM DESC");
			}

		}


		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaIdsEquipes($idEtapa){
	$pdo = conecta();
	try {
		$sql = "SELECT cad_equipes.ID, cad_equipes.DESCRICAO FROM cad_equipes WHERE cad_equipes.ID_CAD_ETAPA = " . $idEtapa . " order by DESCRICAO";
		$consultar = $pdo -> prepare($sql);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}

}

function retornaIdsEPeixesEquipes($idEtapa){
	$pdo = conecta();
	try {
		$sql = "SELECT cad_equipes.ID, cad_equipes.DESCRICAO, contador
				FROM cad_equipes
				LEFT JOIN
					(SELECT pesagens.ID_CAD_EQUIPE, COUNT(pesagens.ID) as contador
					FROM pesagens
					WHERE pesagens.ID_ETAPA = ".$idEtapa." and pesagens.EXCLUIDO = 'NAO'
					GROUP BY pesagens.ID_CAD_EQUIPE) AS QTD
				ON QTD.ID_CAD_EQUIPE = cad_equipes.ID
				WHERE cad_equipes.ID_CAD_ETAPA = ".$idEtapa."
				order by DESCRICAO";
		$consultar = $pdo -> prepare($sql);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}

}

function retornaResultadosLiveNew2($ultimaVerificacao, $idEtapa, $tela, $idUltimaVerificacao){
	$pdo = conecta();
	$etapaModerada = retornaSeModeradaEtapa($idEtapa);
	$sql = '';
	try {
		if($tela == 'live'){
			if($etapaModerada == true){
				$sql = ' and cad_etapas.ETAPA_MODERADA = "S" and pesagens.MODERADO_EM IS NOT NULL ' ;
			}

			if($ultimaVerificacao == ''){
				$consultar = $pdo -> prepare("	SELECT
													pesagens.id,
													pesagens.ID_ETAPA,
													cad_etapas.DESCRICAO ETAPA_NOME,
													pesagens.ID_CAD_COMPETIDOR,
													pesagens.ID_CAD_EQUIPE,
													cad_competidores.NOME,
													cad_competidores.APELIDO,
													pesagens.COMPROVANTE,
													pesagens.ID_CAD_ESPECIE,
													cad_especies.DESCRICAO ESPECIE_NOME,
													pesagens.PONTO_PESAGEM,
													pesagens.ID_CAD_MODALIDADE,
													pesagens.EXCLUIDO,
													pesagens.OBS,
													cad_modalidades.DESCRICAO MODALIDADE_NOME,
													cad_iscas.DESCRICAO ISCA_NOME,
													pesagens.PESO,
													pesagens.DATA_HORA,
													cad_equipes.DESCRICAO EQUIPE_NOME
												FROM
													pesagens
													INNER JOIN cad_competidores ON ( cad_competidores.ID = pesagens.ID_CAD_COMPETIDOR )
													INNER JOIN cad_especies ON ( cad_especies.ID = pesagens.ID_CAD_ESPECIE )
													INNER JOIN cad_iscas ON ( cad_iscas.id = pesagens.id_cad_isca )
													INNER JOIN cad_modalidades ON ( cad_modalidades.ID = pesagens.ID_CAD_MODALIDADE )
													INNER JOIN cad_etapas ON ( cad_etapas.ID = pesagens.ID_ETAPA )
													INNER JOIN cad_equipes ON ( pesagens.ID_CAD_EQUIPE = cad_equipes.ID )
												WHERE
													DATE( pesagens.DATA_HORA ) <= DATE( NOW( ) )
													AND pesagens.ID_ETAPA = ".$idEtapa. $sql . "
												ORDER BY
													pesagens.DATA_HORA DESC");
			}else{
				$consultar = $pdo -> prepare("	SELECT
													pesagens.id,
													pesagens.ID_ETAPA,
													cad_etapas.DESCRICAO ETAPA_NOME,
													pesagens.ID_CAD_COMPETIDOR,
													pesagens.ID_CAD_EQUIPE,
													cad_competidores.NOME,
													cad_competidores.APELIDO,
													pesagens.COMPROVANTE,
													pesagens.ID_CAD_ESPECIE,
													cad_especies.DESCRICAO ESPECIE_NOME,
													pesagens.PONTO_PESAGEM,
													pesagens.ID_CAD_MODALIDADE,
													pesagens.EXCLUIDO,
													pesagens.OBS,
													cad_modalidades.DESCRICAO MODALIDADE_NOME,
													cad_iscas.DESCRICAO ISCA_NOME,
													pesagens.PESO,
													pesagens.DATA_HORA,
													cad_equipes.DESCRICAO EQUIPE_NOME
												FROM
													pesagens
													INNER JOIN cad_competidores ON ( cad_competidores.ID = pesagens.ID_CAD_COMPETIDOR )
													INNER JOIN cad_especies ON ( cad_especies.ID = pesagens.ID_CAD_ESPECIE )
													INNER JOIN cad_iscas ON ( cad_iscas.id = pesagens.id_cad_isca )
													INNER JOIN cad_modalidades ON ( cad_modalidades.ID = pesagens.ID_CAD_MODALIDADE )
													INNER JOIN cad_etapas ON ( cad_etapas.ID = pesagens.ID_ETAPA )
													INNER JOIN cad_equipes ON ( pesagens.ID_CAD_EQUIPE = cad_equipes.ID )
												WHERE
													DATE( pesagens.DATA_HORA ) = DATE( NOW( ) )
													AND TIME( pesagens.DATA_HORA ) > TIME( '".$ultimaVerificacao."' )
													AND pesagens.ID_ETAPA = ".$idEtapa. $sql . "
												ORDER BY
													pesagens.DATA_HORA DESC");
			}

		}else{
			if($tela = 'mod'){
				if($etapaModerada == true){
					$sql = ' and cad_etapas.ETAPA_MODERADA = "S" ' ;
				}
			}

			if($ultimaVerificacao == ''){
				$consultar = $pdo -> prepare("	SELECT
													pesagens.id,
													pesagens.ID_ETAPA,
													cad_etapas.DESCRICAO ETAPA_NOME,
													pesagens.ID_CAD_COMPETIDOR,
													pesagens.ID_CAD_EQUIPE,
													cad_competidores.NOME,
													cad_competidores.APELIDO,
													pesagens.COMPROVANTE,
													pesagens.ID_CAD_ESPECIE,
													cad_especies.DESCRICAO ESPECIE_NOME,
													pesagens.PONTO_PESAGEM,
													pesagens.ID_CAD_MODALIDADE,
													pesagens.EXCLUIDO,
													pesagens.OBS,
													cad_modalidades.DESCRICAO MODALIDADE_NOME,
													cad_iscas.DESCRICAO ISCA_NOME,
													pesagens.PESO,
													pesagens.DATA_HORA,
													cad_equipes.DESCRICAO EQUIPE_NOME
												FROM
													pesagens
													INNER JOIN cad_competidores ON ( cad_competidores.ID = pesagens.ID_CAD_COMPETIDOR )
													INNER JOIN cad_especies ON ( cad_especies.ID = pesagens.ID_CAD_ESPECIE )
													INNER JOIN cad_iscas ON ( cad_iscas.id = pesagens.id_cad_isca )
													INNER JOIN cad_modalidades ON ( cad_modalidades.ID = pesagens.ID_CAD_MODALIDADE )
													INNER JOIN cad_etapas ON ( cad_etapas.ID = pesagens.ID_ETAPA )
													INNER JOIN cad_equipes ON ( pesagens.ID_CAD_EQUIPE = cad_equipes.ID )
												WHERE
													( DATE( pesagens.MODERADO_EM ) <= DATE( NOW( ) ) OR ( pesagens.MODERADO_EM IS NULL ) )
													AND pesagens.ID_ETAPA = ".$idEtapa. $sql . "
												ORDER BY
													pesagens.MODERADO_EM DESC");
			}else{
				$consultar = $pdo -> prepare("	SELECT
													pesagens.id,
													pesagens.ID_ETAPA,
													cad_etapas.DESCRICAO ETAPA_NOME,
													pesagens.ID_CAD_COMPETIDOR,
													pesagens.ID_CAD_EQUIPE,
													cad_competidores.NOME,
													cad_competidores.APELIDO,
													pesagens.COMPROVANTE,
													pesagens.ID_CAD_ESPECIE,
													cad_especies.DESCRICAO ESPECIE_NOME,
													pesagens.PONTO_PESAGEM,
													pesagens.ID_CAD_MODALIDADE,
													pesagens.EXCLUIDO,
													pesagens.OBS,
													cad_modalidades.DESCRICAO MODALIDADE_NOME,
													cad_iscas.DESCRICAO ISCA_NOME,
													pesagens.PESO,
													pesagens.DATA_HORA,
													cad_equipes.DESCRICAO EQUIPE_NOME
												FROM
													pesagens
													INNER JOIN cad_competidores ON ( cad_competidores.ID = pesagens.ID_CAD_COMPETIDOR )
													INNER JOIN cad_especies ON ( cad_especies.ID = pesagens.ID_CAD_ESPECIE )
													INNER JOIN cad_iscas ON ( cad_iscas.id = pesagens.id_cad_isca )
													INNER JOIN cad_modalidades ON ( cad_modalidades.ID = pesagens.ID_CAD_MODALIDADE )
													INNER JOIN cad_etapas ON ( cad_etapas.ID = pesagens.ID_ETAPA )
													INNER JOIN cad_equipes ON ( pesagens.ID_CAD_EQUIPE = cad_equipes.ID )
												WHERE
													(
													DATE( pesagens.MODERADO_EM ) = DATE( NOW( ) )
													AND TIME( pesagens.MODERADO_EM ) > TIME( '".$ultimaVerificacao."' )
													OR ( pesagens.MODERADO_EM IS NULL )
													)
													AND pesagens.ID_ETAPA = ".$idEtapa. $sql . "
												ORDER BY
													pesagens.MODERADO_EM DESC");
			}

		}


		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaCompetidores($competidorNome, $competidorApelido, $competidoresCPF){
	$pdo = conecta();
	try {
		$sql = '';
		if ($competidorNome != ''){
			$sql = " nome like '%" . $competidorNome . "%'";
		}

		if ($competidoresCPF != ''){
			if($sql != ''){
				$sql = $sql . " and CPF = '" . $competidoresCPF . "'";
			}else{
				$sql = " CPF = '" . $competidoresCPF . "'";
			}
		}

		if ($competidorApelido != ''){
			if($sql != ''){
				$sql = $sql . " and APELIDO like '%" . $competidorApelido . "%'";
			}else{
				$sql = " APELIDO like '%" . $competidorApelido . "%'";
			}
		}

		if($sql != ''){
			$sql = 'where' . $sql;
		}
		$consultar = $pdo -> prepare("SELECT * from cad_competidores " . $sql . " order by nome");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaCompetidor($idCompetidor){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("SELECT * from cad_competidores where id=" . $idCompetidor);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaEtapasCompetidor($idCompetidor){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("	SELECT
											cad_campeonatos.DESCRICAO CAMPEONATO,
											cad_etapas.DESCRICAO ETAPA,
											cad_etapas.DATA_ETAPA
										FROM
											cad_etapas
											INNER JOIN composicao_etapas ON ( composicao_etapas.ID_CAD_ETAPA = cad_etapas.ID )
											INNER JOIN composicao_equipes ON ( composicao_equipes.id_equipe = composicao_etapas.ID_CAD_EQUIPE )
											inner join cad_campeonatos on (cad_campeonatos.ID = cad_etapas.ID_CAD_CAMPEONATO)
										WHERE
											composicao_equipes.id_competidor = :idUsuario
										ORDER BY cad_etapas.DATA_ETAPA");

		$consultar -> bindValue(':idUsuario', $idCompetidor, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaEquipes($equipeNome, $idCampeonato, $idEtapa, $idPulseira, $nomeCompetidor){
	$pdo = conecta();
	try {
		$sql = '';
		if ($equipeNome != ''){
			$sql = " cad_equipes.descricao like '%" . $equipeNome . "%'";
		}
		if ($idCampeonato > 0){
			if($sql != ''){
				$sql = $sql . " and cad_etapas.id_cad_campeonato = " . $idCampeonato;
			}else{
				$sql = " cad_etapas.id_cad_campeonato = " . $idCampeonato;
			}
		}
		if ($idEtapa > 0){
			if($sql != ''){
				$sql = $sql . " and cad_etapas.id = " . $idEtapa;
			}else{
				$sql = " cad_etapas.id = " . $idEtapa;
			}
		}
		if($sql != ''){
			$sql = 'where' . $sql;
		}

		/*$consultar = $pdo -> prepare("SELECT cad_equipes.id, cad_equipes.DESCRICAO equipe, cad_equipes.PATROCINADOR, cad_etapas.DESCRICAO etapa, cad_campeonatos.DESCRICAO campeonato FROM cad_equipes inner join cad_etapas on (cad_equipes.ID_CAD_ETAPA = cad_etapas.ID) inner join cad_campeonatos on (cad_campeonatos.ID = cad_etapas.ID_CAD_CAMPEONATO)" . $sql . " order by cad_etapas.ID_CAD_CAMPEONATO, cad_equipes.descricao");*/

		if($idPulseira > 0){
			$filtroPulseira = "WHERE pulseiras LIKE '%".$idPulseira."%'";
		}else{
			$filtroPulseira = "";
		}

		if($nomeCompetidor != ''){
			if($filtroPulseira == ''){
				$filtroPulseira = "WHERE competidores LIKE '%".$nomeCompetidor."%'";
			}else{
				$filtroPulseira .= "and competidores LIKE '%".$nomeCompetidor."%'";
			}
		}

		$consultar = $pdo -> prepare("	SELECT
											*
										FROM
											(
											SELECT
												cad_equipes.id,
												cad_equipes.DESCRICAO equipe,
												cad_equipes.PATROCINADOR,
												cad_etapas.DESCRICAO etapa,
												cad_campeonatos.DESCRICAO campeonato,
												( SELECT GROUP_CONCAT( id_pulseira ) FROM composicao_equipes WHERE id_equipe = cad_equipes.id ) pulseiras,
												( SELECT
													GROUP_CONCAT( cad_competidores.NOME, cad_competidores.APELIDO )
													FROM
														cad_competidores
													WHERE
														cad_competidores.ID IN ( SELECT ( id_competidor ) FROM composicao_equipes WHERE id_equipe = cad_equipes.id )
													) competidores
											FROM
												cad_equipes
												INNER JOIN cad_etapas ON ( cad_equipes.ID_CAD_ETAPA = cad_etapas.ID )
												INNER JOIN cad_campeonatos ON ( cad_campeonatos.ID = cad_etapas.ID_CAD_CAMPEONATO )
											" . $sql . "
											ORDER BY
												cad_etapas.ID_CAD_CAMPEONATO,
												cad_equipes.descricao
												) equipes
										" . $filtroPulseira);


		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaEquipe($idEquipe){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("SELECT cad_equipes.ID, cad_equipes.DESCRICAO, cad_equipes.ID_CAD_ETAPA, cad_etapas.ID_CAD_CAMPEONATO from cad_equipes inner join cad_etapas on (cad_equipes.ID_CAD_ETAPA = cad_etapas.ID) where cad_equipes.id=" . $idEquipe);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaDadosEquipe($idEquipe){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("	SELECT
										cp_eq.id_competidor ID_COMPETIDOR,
										cd_comp.NOME,
										cd_comp.APELIDO,
										CASE
										 	WHEN LENGTH(cd_comp.DATA_NASCIMENTO) = 0 OR cd_comp.DATA_NASCIMENTO IS NULL THEN 'N/I'
											ELSE cd_comp.DATA_NASCIMENTO
										END AS DATA_NASCIMENTO,
										COALESCE(YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cd_comp.DATA_NASCIMENTO))),0) AS IDADE,
										CASE
										 	WHEN LENGTH(cd_comp.CIDADE) = 0 OR cd_comp.CIDADE IS NULL THEN 'N/I'
										 	ELSE cd_comp.CIDADE
										END AS CIDADE,
										CASE
										 	WHEN LENGTH(cd_comp.UF) = 0 OR cd_comp.UF IS NULL THEN 'N/I'
										 	ELSE cd_comp.UF
										END AS UF,
										CASE
										 	WHEN LENGTH(cd_eq.HISTORIA) = 0 OR cd_eq.HISTORIA IS NULL THEN 'N/I'
										 	ELSE cd_eq.HISTORIA
										END AS HISTORIA,
										CASE
										 	WHEN LENGTH(cd_eq.CURIOSIDADES) = 0 OR cd_eq.CURIOSIDADES IS NULL THEN 'N/I'
										 	ELSE cd_eq.CURIOSIDADES
										END AS CURIOSIDADES,
										CASE
										 	WHEN LENGTH(cd_eq.PATROCINADOR) = 0 OR cd_eq.PATROCINADOR IS NULL THEN 'N/I'
										 	ELSE cd_eq.PATROCINADOR
										END AS PATROCINADOR
									FROM
										composicao_equipes cp_eq
										INNER JOIN cad_competidores cd_comp ON ( cd_comp.ID = cp_eq.id_competidor )
										INNER JOIN cad_equipes cd_eq ON ( cd_eq.ID = cp_eq.id_equipe )
									WHERE
										cp_eq.id_equipe = :idEquipe
										AND cp_eq.SUPLENTE = 'N'");

		$consultar -> bindValue(':idEquipe', $idEquipe, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	}catch(PDOException $e) {
		echo $e -> getMessage();
	}
}



function retonaResultadoEtapaGeral($idEtapa, $horario){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}
		if($horario == ''){
			$consultar = $pdo -> prepare("SELECT count(pesagens.ID) total_peixes, COALESCE(FORMAT(sum(pesagens.PESO),3),0.000) total_pesado from pesagens where pesagens.EXCLUIDO='NAO' and pesagens.ID_ETAPA = " . $idEtapa . $sql);
		}else{
			$consultar = $pdo -> prepare("SELECT count(pesagens.ID) total_peixes, COALESCE(FORMAT(sum(pesagens.PESO),3),0.000) total_pesado from pesagens where pesagens.EXCLUIDO='NAO' and pesagens.ID_ETAPA = " . $idEtapa . $sql . " and pesagens.DATA_HORA <= '" . $horario . "'");
		}

		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retonaResultadoCampeonato($idCampeonato){
	$pdo = conecta();
	$sql = '';
	try {
		if($idCampeonato == ''){
			$idCampeonato = 0;
		}

		$consultar = $pdo -> prepare("	SELECT
											cad_etapas.ID ID_ETAPA,
											cad_etapas.DESCRICAO DESCRICAO_ETAPA,
											cad_estados.SIGLA UF,
											DATE_FORMAT( cad_etapas.DATA_ETAPA, '%d/%m/%Y' ) DATA_ETAPA,
											count( pesagens.ID ) TOTAL_PEIXES,
											COALESCE ( FORMAT( sum( pesagens.PESO ), 3 ), 0.000 ) TOTAL_PESADO
										FROM
											cad_etapas
											LEFT JOIN pesagens ON ( cad_etapas.ID = pesagens.ID_ETAPA AND pesagens.EXCLUIDO = 'NAO' )
											LEFT JOIN cad_estados ON (cad_etapas.ID_CAD_UF_REALIZACAO = cad_estados.ID)
										WHERE
											cad_etapas.ID_CAD_CAMPEONATO = :idCampeonato
										GROUP BY
											cad_etapas.ID");

		$consultar -> bindValue(':idCampeonato', $idCampeonato, PDO::PARAM_STR);

		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaResultadoEtapaMaiorPeixe($idEtapa, $horario, $idEquipe){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		if($idEquipe == ''){
			$idEquipe = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}

		//para pegar o mp de alguma dupla especifica
		if($idEquipe != 0){
			$sql .= ' and pesagens.ID_CAD_EQUIPE = ' . $idEquipe;
		}

		if($horario == ''){
            $consultar = $pdo -> prepare("SELECT distinct pesagens.ID, pesagens.ID_CAD_COMPETIDOR, pesagens.ID_ETAPA, format(pesagens.PESO,3) peso, composicao_equipes.id_equipe, pesagens.ID_CAD_ESPECIE, cad_especies.DESCRICAO especie, cad_equipes.DESCRICAO equipe, cad_modalidades.descricao modalidade, cad_iscas.descricao isca from pesagens inner join cad_modalidades on (cad_modalidades.id = pesagens.id_cad_modalidade) inner join cad_iscas on (cad_iscas.id = pesagens.id_cad_isca) inner join composicao_equipes on (composicao_equipes.id_competidor = pesagens.ID_CAD_COMPETIDOR) inner join composicao_etapas on (composicao_etapas.ID_CAD_ETAPA = pesagens.ID_ETAPA and composicao_etapas.ID_CAD_EQUIPE = composicao_equipes.id_equipe) inner join cad_especies on (pesagens.ID_CAD_ESPECIE = cad_especies.ID) inner join cad_equipes on (cad_equipes.ID = composicao_equipes.id_equipe) where pesagens.EXCLUIDO='NAO' and pesagens.ID_ETAPA = ".$idEtapa. $sql . " order by pesagens.peso desc limit 1");
            	//$consultar = $pdo -> prepare("select distinct pesagens.ID, pesagens.ID_CAD_COMPETIDOR, pesagens.ID_ETAPA, format(pesagens.PESO,3) peso, composicao_equipes.id_equipe, pesagens.ID_CAD_ESPECIE, cad_especies.DESCRICAO especie, cad_equipes.DESCRICAO equipe, cad_modalidades.descricao modalidade, cad_iscas.descricao isca from pesagens inner join cad_modalidades on (cad_modalidades.id = pesagens.id_cad_modalidade) inner join cad_iscas on (cad_iscas.id = pesagens.id_cad_isca) inner join composicao_equipes on (composicao_equipes.id_competidor = pesagens.ID_CAD_COMPETIDOR) inner join cad_especies on (pesagens.ID_CAD_ESPECIE = cad_especies.ID) inner join cad_equipes on (cad_equipes.ID = composicao_equipes.id_equipe) where pesagens.EXCLUIDO='NAO' and pesagens.ID_ETAPA = ".$idEtapa." order by pesagens.peso desc limit 1");
		}else{
            $consultar = $pdo -> prepare("SELECT distinct pesagens.ID, pesagens.ID_CAD_COMPETIDOR, pesagens.ID_ETAPA, format(pesagens.PESO,3) peso, composicao_equipes.id_equipe, pesagens.ID_CAD_ESPECIE, cad_especies.DESCRICAO especie, cad_equipes.DESCRICAO equipe, cad_modalidades.descricao modalidade, cad_iscas.descricao isca from pesagens inner join cad_modalidades on (cad_modalidades.id = pesagens.id_cad_modalidade) inner join cad_iscas on (cad_iscas.id = pesagens.id_cad_isca) inner join composicao_equipes on (composicao_equipes.id_competidor = pesagens.ID_CAD_COMPETIDOR) inner join composicao_etapas on (composicao_etapas.ID_CAD_ETAPA = pesagens.ID_ETAPA and composicao_etapas.ID_CAD_EQUIPE = composicao_equipes.id_equipe) inner join cad_especies on (pesagens.ID_CAD_ESPECIE = cad_especies.ID) inner join cad_equipes on (cad_equipes.ID = composicao_equipes.id_equipe) where pesagens.EXCLUIDO='NAO' and pesagens.ID_ETAPA = ".$idEtapa. $sql ." and pesagens.DATA_HORA <= '".$horario."' order by pesagens.peso desc limit 1");
            	//$consultar = $pdo -> prepare("select distinct pesagens.ID, pesagens.ID_CAD_COMPETIDOR, pesagens.ID_ETAPA, format(pesagens.PESO,3) peso, composicao_equipes.id_equipe, pesagens.ID_CAD_ESPECIE, cad_especies.DESCRICAO especie, cad_equipes.DESCRICAO equipe, cad_modalidades.descricao modalidade, cad_iscas.descricao isca from pesagens inner join cad_modalidades on (cad_modalidades.id = pesagens.id_cad_modalidade) inner join cad_iscas on (cad_iscas.id = pesagens.id_cad_isca) inner join composicao_equipes on (composicao_equipes.id_competidor = pesagens.ID_CAD_COMPETIDOR) inner join cad_especies on (pesagens.ID_CAD_ESPECIE = cad_especies.ID) inner join cad_equipes on (cad_equipes.ID = composicao_equipes.id_equipe) where pesagens.EXCLUIDO='NAO' and pesagens.ID_ETAPA = ".$idEtapa." and pesagens.DATA_HORA <= '".$horario."' order by pesagens.peso desc limit 1");
		}
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaResultadoEtapaRankingHorario($idEtapa, $horario){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		$tipoClassificacao = retornaTipoClassificacao($idEtapa);

		if(empty($tipoClassificacao)){
			return false;
		}else{
			if($tipoClassificacao -> ID_CAD_TIPO_CLASSIFICACAO == 0){
				return false;
			}else{
				if($etapaModerada == true){
					$sql .= ' and pesagens.MODERADO_EM IS NOT NULL ' ;
				}
				if($horario != ''){
					$sql .= ' and pesagens.DATA_HORA <= "'.$horario.'"';
				}

				/*CONSIDERAR TODOS OS EXEMPLARES, EXCETO CANCELADOS*/
				if($tipoClassificacao -> ID_CAD_TIPO_CLASSIFICACAO == 1){
					$consultar = $pdo -> prepare("	SELECT
														*
													FROM
														(
													SELECT
														count( pesagens.ID_CAD_COMPETIDOR ) quantidade,
														format( sum( pesagens.PESO ), 3 ) peso,
														sum( pesagens.PESO ) peso2,
														composicao_equipes.id_equipe,
														pesagens.id_etapa,
														cad_equipes.DESCRICAO equipe
													FROM
														pesagens
														INNER JOIN composicao_equipes ON ( composicao_equipes.id_competidor = pesagens.ID_CAD_COMPETIDOR AND composicao_equipes.id_equipe = pesagens.ID_CAD_EQUIPE )
														INNER JOIN cad_equipes ON ( cad_equipes.ID = composicao_equipes.id_equipe )
														INNER JOIN composicao_etapas ON ( composicao_etapas.ID_CAD_EQUIPE = composicao_equipes.id_equipe )
													WHERE
														pesagens.EXCLUIDO = 'NAO'
														AND pesagens.ID_ETAPA = ".$idEtapa.$sql."
													GROUP BY
														id_equipe,
														equipe
													ORDER BY
														peso2 DESC
														LIMIT 10
														) AS `table`
													ORDER BY
														peso2 ASC");
				}else{
					/*CONSIDERAR 3 MAIORES EXEMPLARES, EXCETO CANCELADOS*/
					if($tipoClassificacao -> ID_CAD_TIPO_CLASSIFICACAO == 2){

						$consultar = $pdo -> prepare("SET @contador = 0;");
						$consultar -> execute();
						$consultar = $pdo -> prepare("SET @agrupador = 0;");
						$consultar -> execute();

						$consultar = $pdo -> prepare("	SELECT
															count( x.ID_CAD_EQUIPE ) quantidade,
															format( sum( x.PESO ), 3 ) peso,
															sum( x.PESO ) peso2,
															composicao_equipes.id_equipe,
															x.id_etapa,
															cad_equipes.DESCRICAO equipe
														FROM
															(
															SELECT
																ID_CAD_EQUIPE,
																PESO,
																id_etapa,
																ID_CAD_COMPETIDOR,
																(
																	@contador :=
																IF
																	( @agrupador = `ID_CAD_EQUIPE`, @contador + 1, IF ( @agrupador := `ID_CAD_EQUIPE`, 1, 1 ) )
																) row_number
															FROM
																pesagens t
															WHERE
																t.EXCLUIDO='NAO' and
																t.id_etapa = ".$idEtapa.$sql."
															ORDER BY
																id_cad_equipe,
																peso DESC
															) AS x
															INNER JOIN composicao_equipes ON ( composicao_equipes.id_competidor = x.ID_CAD_COMPETIDOR AND composicao_equipes.id_equipe = x.ID_CAD_EQUIPE )
															INNER JOIN cad_equipes ON ( cad_equipes.ID = composicao_equipes.id_equipe )
															INNER JOIN composicao_etapas ON ( composicao_etapas.ID_CAD_EQUIPE = composicao_equipes.id_equipe )
														WHERE
															x.row_number <= 3
														GROUP BY
															id_equipe,
															equipe
														ORDER BY
															peso2 ASC
														LIMIT 10");
					}

				}

				$consultar -> execute();
				if ($consultar -> rowCount() > 0) {
					return $consultar -> fetchAll(PDO::FETCH_OBJ);
				} else {
					return false;
				}
			}
		}

	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaResultadoEtapaRankingHorarioTudo($idEtapa, $horario){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}
		if($horario == ''){
			$consultar = $pdo -> prepare("SELECT * FROM ( select count(pesagens.ID_CAD_COMPETIDOR) quantidade, format(sum(pesagens.PESO),3) peso, sum(pesagens.PESO) peso2, composicao_equipes.id_equipe, pesagens.id_etapa, cad_equipes.DESCRICAO equipe from pesagens inner join composicao_equipes on (composicao_equipes.id_competidor = pesagens.ID_CAD_COMPETIDOR  and composicao_equipes.id_equipe = pesagens.ID_CAD_EQUIPE) inner join cad_equipes on (cad_equipes.ID = composicao_equipes.id_equipe) inner join composicao_etapas on (composicao_etapas.ID_CAD_EQUIPE = composicao_equipes.id_equipe) where pesagens.EXCLUIDO='NAO' and pesagens.ID_ETAPA = ".$idEtapa.$sql." group by id_equipe, equipe ORDER BY peso2 DESC ) AS `table` ORDER by peso2 ASC");
		}else{
			$consultar = $pdo -> prepare("SELECT * FROM ( select count(pesagens.ID_CAD_COMPETIDOR) quantidade, format(sum(pesagens.PESO),3) peso, sum(pesagens.PESO) peso2, composicao_equipes.id_equipe, pesagens.id_etapa, cad_equipes.DESCRICAO equipe from pesagens inner join composicao_equipes on (composicao_equipes.id_competidor = pesagens.ID_CAD_COMPETIDOR  and composicao_equipes.id_equipe = pesagens.ID_CAD_EQUIPE) inner join cad_equipes on (cad_equipes.ID = composicao_equipes.id_equipe) inner join composicao_etapas on (composicao_etapas.ID_CAD_EQUIPE = composicao_equipes.id_equipe) where pesagens.EXCLUIDO='NAO' and pesagens.ID_ETAPA = ".$idEtapa.$sql." and pesagens.DATA_HORA <= '".$horario."' group by id_equipe, equipe ORDER BY peso2 DESC) AS `table` ORDER by peso2 ASC");
		}
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaRankingPublico($idEtapa){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}

		$consultar = $pdo -> prepare ("SET @contador := 0;");
		$consultar -> execute();

		$consultar = $pdo -> prepare("	SELECT
											@contador := @contador + 1 AS posicao,
											`table`.*
										FROM
											(
										SELECT
											count( pesagens.ID_CAD_COMPETIDOR ) quantidade,
											format( sum( pesagens.PESO ), 3 ) peso,
											sum( pesagens.PESO ) peso2,
											composicao_equipes.id_equipe,
											pesagens.id_etapa,
											cad_equipes.DESCRICAO equipe
										FROM
											pesagens
											INNER JOIN composicao_equipes ON ( composicao_equipes.id_competidor = pesagens.ID_CAD_COMPETIDOR AND composicao_equipes.id_equipe = pesagens.ID_CAD_EQUIPE )
											INNER JOIN cad_equipes ON ( cad_equipes.ID = composicao_equipes.id_equipe )
											INNER JOIN composicao_etapas ON ( composicao_etapas.ID_CAD_EQUIPE = composicao_equipes.id_equipe )
										WHERE
											pesagens.EXCLUIDO = 'NAO'
											AND pesagens.ID_ETAPA = ".$idEtapa.$sql."
										GROUP BY
											id_equipe,
											equipe
										ORDER BY
											peso2 DESC
											) AS `table`
										ORDER BY
											peso2 DESC");


		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaPosicaoRankingPublico($idEtapa, $idEquipe){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}

		$consultar = $pdo -> prepare("	SELECT
											*
										FROM
											(
										SELECT
											@rownum := @rownum + 1 AS posicao,
											`table`.*
										FROM
											(
										SELECT
											count( pesagens.ID_CAD_COMPETIDOR ) quantidade,
											format( sum( pesagens.PESO ), 3 ) peso,
											sum( pesagens.PESO ) peso2,
											composicao_equipes.id_equipe,
											pesagens.id_etapa,
											cad_equipes.DESCRICAO equipe
										FROM
											pesagens
											INNER JOIN composicao_equipes ON ( composicao_equipes.id_competidor = pesagens.ID_CAD_COMPETIDOR AND composicao_equipes.id_equipe = pesagens.ID_CAD_EQUIPE )
											INNER JOIN cad_equipes ON ( cad_equipes.ID = composicao_equipes.id_equipe )
											INNER JOIN composicao_etapas ON ( composicao_etapas.ID_CAD_EQUIPE = composicao_equipes.id_equipe )
										WHERE
											pesagens.EXCLUIDO = 'NAO'
											AND pesagens.ID_ETAPA = :idEtapa
										GROUP BY
											id_equipe,
											equipe
										ORDER BY
											peso2 DESC
											) AS `table`,
											( SELECT @rownum := 0 ) r
										ORDER BY
											peso2 DESC
											) ranking
										WHERE
											ranking.id_equipe = :idEquipe");

		$consultar -> bindValue(':idEtapa', $idEtapa, PDO::PARAM_STR);
		$consultar -> bindValue(':idEquipe', $idEquipe, PDO::PARAM_STR);

		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaPosicaoEquipe($idEtapa, $idEquipe){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}

		$consultar = $pdo -> prepare("	SELECT
											*
										FROM
											(
										SELECT
											@rownum := @rownum + 1 AS posicao,
											`table`.*
										FROM
											(
										SELECT
											count( pesagens.ID_CAD_COMPETIDOR ) quantidade,
											format( sum( pesagens.PESO ), 3 ) peso,
											sum( pesagens.PESO ) peso2,
											composicao_equipes.id_equipe,
											pesagens.id_etapa,
											cad_equipes.DESCRICAO equipe
										FROM
											pesagens
											INNER JOIN composicao_equipes ON ( composicao_equipes.id_competidor = pesagens.ID_CAD_COMPETIDOR AND composicao_equipes.id_equipe = pesagens.ID_CAD_EQUIPE )
											INNER JOIN cad_equipes ON ( cad_equipes.ID = composicao_equipes.id_equipe )
											INNER JOIN composicao_etapas ON ( composicao_etapas.ID_CAD_EQUIPE = composicao_equipes.id_equipe )
										WHERE
											pesagens.EXCLUIDO = 'NAO'
											AND pesagens.ID_ETAPA = :idEtapa
										GROUP BY
											id_equipe,
											equipe
										ORDER BY
											peso2 DESC
											) AS `table`,
											( SELECT @rownum := 0 ) r
										ORDER BY
											peso2 DESC
											) ranking
										WHERE
											ranking.id_equipe = :idEquipe");

		$consultar -> bindValue(':idEtapa', $idEtapa, PDO::PARAM_STR);
		$consultar -> bindValue(':idEquipe', $idEquipe, PDO::PARAM_STR);

		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaResultadosEquipe($idEquipe){
	$pdo = conecta();
	$sql = '';
	try {
		$consultar = $pdo -> prepare("	SELECT RIGHT( pesagens.COMPROVANTE, 6 ) COMPROVANTE,
											cad_especies.DESCRICAO ESPECIE,
											DATE_FORMAT( pesagens.DATA_HORA, '%H:%i' ) HORA,
											format(pesagens.PESO,3) PESO,
											pesagens.OBS
										FROM
											pesagens
											INNER JOIN cad_especies ON ( cad_especies.ID = pesagens.ID_CAD_ESPECIE )
										WHERE
											pesagens.ID_CAD_EQUIPE = :idEquipe ORDER BY pesagens.DATA_HORA");

		$consultar -> bindValue(':idEquipe', $idEquipe, PDO::PARAM_STR);

		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}


function retornaResultadosEtapa($idEtapa){
	$pdo = conecta();
	$sql = '';
	try {
		$consultar = $pdo -> prepare("	SELECT RIGHT( pesagens.COMPROVANTE, 6 ) COMPROVANTE,
											cad_especies.DESCRICAO ESPECIE,
											DATE_FORMAT( pesagens.DATA_HORA, '%H:%i' ) HORA,
											format(pesagens.PESO,3) PESO,
											pesagens.OBS
										FROM
											pesagens
											INNER JOIN cad_especies ON ( cad_especies.ID = pesagens.ID_CAD_ESPECIE )
										WHERE
											pesagens.ID_ETAPA = :idEtapa ORDER BY pesagens.DATA_HORA");

		$consultar -> bindValue(':idEtapa', $idEtapa, PDO::PARAM_STR);

		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}



function retornaResultadoEtapaRanking($idEtapa){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}
		$consultar = $pdo -> prepare("SELECT * FROM ( select count(pesagens.ID_CAD_COMPETIDOR) quantidade, format(sum(pesagens.PESO),3) peso, sum(pesagens.PESO) peso2, composicao_equipes.id_equipe, cad_equipes.DESCRICAO equipe from pesagens inner join composicao_equipes on (composicao_equipes.id_competidor = pesagens.ID_CAD_COMPETIDOR  and composicao_equipes.id_equipe = pesagens.ID_CAD_EQUIPE) inner join cad_equipes on (cad_equipes.ID = composicao_equipes.id_equipe) inner join composicao_etapas on (composicao_etapas.ID_CAD_EQUIPE = composicao_equipes.id_equipe) where pesagens.EXCLUIDO='NAO' and pesagens.ID_ETAPA = ".$idEtapa.$sql." group by id_equipe, equipe ORDER BY peso2 DESC limit 10 ) AS `table` ORDER by peso2 ASC");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaResultadoEtapaRankingTodos($idEtapa){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}
		$consultar = $pdo -> prepare("SELECT * FROM ( select count(pesagens.ID_CAD_COMPETIDOR) quantidade, format(sum(pesagens.PESO),3) peso, sum(pesagens.PESO) peso2, composicao_equipes.id_equipe, cad_equipes.DESCRICAO equipe from pesagens inner join composicao_equipes on (composicao_equipes.id_competidor = pesagens.ID_CAD_COMPETIDOR  and composicao_equipes.id_equipe = pesagens.ID_CAD_EQUIPE) inner join cad_equipes on (cad_equipes.ID = composicao_equipes.id_equipe) inner join composicao_etapas on (composicao_etapas.ID_CAD_EQUIPE = composicao_equipes.id_equipe) where pesagens.EXCLUIDO='NAO' and pesagens.ID_ETAPA = ".$idEtapa.$sql." group by id_equipe, equipe ORDER BY peso2 DESC  ) AS `table` ORDER by peso2 ASC");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaDetalhesEtapaModalidade($idEtapa){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}
		$consultar = $pdo -> prepare("SELECT cad_modalidades.DESCRICAO modalidade, count(pesagens.id) total FROM pesagens INNER JOIN cad_modalidades ON (cad_modalidades.ID = pesagens.ID_CAD_MODALIDADE) WHERE pesagens.ID_ETAPA = ".$idEtapa.$sql." and pesagens.EXCLUIDO = 'NAO' group by pesagens.ID_CAD_MODALIDADE order by total desc");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaDetalhesEquipeModalidade($idEtapa, $idEquipe){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}
		$consultar = $pdo -> prepare("SELECT cad_modalidades.DESCRICAO modalidade, count(pesagens.id) total FROM pesagens INNER JOIN cad_modalidades ON (cad_modalidades.ID = pesagens.ID_CAD_MODALIDADE) WHERE pesagens.ID_ETAPA = ".$idEtapa.$sql." and pesagens.ID_CAD_EQUIPE = ".$idEquipe.$sql." and pesagens.EXCLUIDO = 'NAO' group by pesagens.ID_CAD_MODALIDADE order by total desc");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaDetalhesEtapaEspecie($idEtapa){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}
        $consultar = $pdo -> prepare("SELECT cad_especies.DESCRICAO especie, count(pesagens.id) total FROM pesagens INNER JOIN cad_especies ON (cad_especies.ID = pesagens.ID_CAD_ESPECIE) WHERE pesagens.ID_ETAPA = ".$idEtapa.$sql." and pesagens.EXCLUIDO = 'NAO' group by pesagens.ID_CAD_ESPECIE order by total desc");
        $consultar -> execute();
        if ($consultar -> rowCount() > 0) {
                return $consultar -> fetchAll(PDO::FETCH_OBJ);
        } else {
                return false;
        }
	} catch(PDOException $e) {
        echo $e -> getMessage();
	}
}

function retornaDetalhesEquipeEspecie($idEtapa, $idEquipe){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}
        $consultar = $pdo -> prepare("SELECT cad_especies.DESCRICAO especie, count(pesagens.id) total FROM pesagens INNER JOIN cad_especies ON (cad_especies.ID = pesagens.ID_CAD_ESPECIE) WHERE pesagens.ID_ETAPA = ".$idEtapa.$sql." and pesagens.ID_CAD_EQUIPE = ".$idEquipe.$sql." and pesagens.EXCLUIDO = 'NAO' group by pesagens.ID_CAD_ESPECIE order by total desc");
        $consultar -> execute();
        if ($consultar -> rowCount() > 0) {
                return $consultar -> fetchAll(PDO::FETCH_OBJ);
        } else {
                return false;
        }
	} catch(PDOException $e) {
        echo $e -> getMessage();
	}
}

function retornaDetalhesEtapaIsca($idEtapa){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}
		$consultar = $pdo -> prepare("SELECT cad_iscas.DESCRICAO isca, count(pesagens.id) total FROM pesagens INNER JOIN cad_iscas ON (cad_iscas.ID = pesagens.ID_CAD_ISCA) WHERE pesagens.ID_ETAPA = ".$idEtapa.$sql." and pesagens.EXCLUIDO = 'NAO' group by pesagens.ID_CAD_ISCA order by total desc");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaDetalhesEquipeIsca($idEtapa, $idEquipe){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}
		$consultar = $pdo -> prepare("SELECT cad_iscas.DESCRICAO isca, count(pesagens.id) total FROM pesagens INNER JOIN cad_iscas ON (cad_iscas.ID = pesagens.ID_CAD_ISCA) WHERE pesagens.ID_ETAPA = ".$idEtapa.$sql." and pesagens.ID_CAD_EQUIPE = ".$idEquipe.$sql." and pesagens.EXCLUIDO = 'NAO' group by pesagens.ID_CAD_ISCA order by total desc");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaDetalhesEtapaHora($idEtapa){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}
		$consultar = $pdo -> prepare("SELECT concat(concat(HOUR(pesagens.DATA_HORA),':00 - '),concat(HOUR(pesagens.DATA_HORA),':59')) hora, count(*) total FROM pesagens WHERE id_etapa = ".$idEtapa.$sql." and pesagens.EXCLUIDO = 'NAO' GROUP BY hour( pesagens.DATA_HORA ) ");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaDetalhesEquipeHora($idEtapa, $idEquipe){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}
		$consultar = $pdo -> prepare("SELECT concat(concat(HOUR(pesagens.DATA_HORA),':00 - '),concat(HOUR(pesagens.DATA_HORA),':59')) hora, count(*) total FROM pesagens WHERE id_etapa = ".$idEtapa.$sql." and pesagens.ID_CAD_EQUIPE = ".$idEquipe.$sql." and pesagens.EXCLUIDO = 'NAO' GROUP BY hour( pesagens.DATA_HORA ) ");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaDetalhesQuadrantes($idEtapa){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql = ' and p.MODERADO_EM IS NOT NULL ' ;
		}
		$consultar = $pdo -> prepare("	SELECT
										    count( t.ID ) QTD_PESAGENS,
										    sum( t.PESO ) TOTAL_PESAGENS,
										    t.QUADRANTE,
										    t.BATERIA
										FROM
										    (
										SELECT
										    p.ID,
										    p.COMPROVANTE,
										    p.ID_CAD_COMPETIDOR,
										    p.DATA_HORA,
										    p.PESO,
										    ( SELECT RETORNA_BATERIA_PESAGEM ( p.id_etapa, p.DATA_HORA ) ) BATERIA,
										    ( SELECT RETORNA_RAIA_PESAGEM ( p.ID ) ) RAIA,
										    ( SELECT RETORNA_QUADRANTE_PESAGEM ( RAIA, p.id_etapa ) ) QUADRANTE
										FROM
										    pesagens p
										WHERE
										    p.id_etapa = ".$idEtapa.$sql."  AND p.EXCLUIDO = 'NAO'
										ORDER BY
										    p.ID,
										    BATERIA
										    ) t
										GROUP BY
										    t.BATERIA,
										    t.QUADRANTE");

		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaDetalhesEstatisticasEquipes($idEtapa){
	$pdo = conecta();
	$sql = '';
	try {
		if($idEtapa == ''){
			$idEtapa = 0;
		}

		$consultar = $pdo -> prepare("	SELECT
											'EQUIPES' DESCRICAO,
											COUNT( DISTINCT ce.ID ) TOTAL
										FROM
											composicao_etapas ce
										WHERE
											ce.ID_CAD_ETAPA = :idEtapa1 UNION ALL
										SELECT
											'EQUIPES COM PESAGENS' DESCRICAO,
											count( DISTINCT p.ID_CAD_EQUIPE ) TOTAL
										FROM
											pesagens p
										WHERE
											p.ID_ETAPA = :idEtapa2 AND p.EXCLUIDO = 'NAO' UNION ALL
										SELECT
											'EQUIPES SEM PESAGENS' DESCRICAO,
											( COUNT( DISTINCT ce.ID ) - count( DISTINCT p.ID_CAD_EQUIPE ) ) TOTAL
										FROM
											composicao_etapas ce
											LEFT JOIN pesagens p ON ( p.ID_CAD_EQUIPE = ce.ID_CAD_EQUIPE )
										WHERE
											ce.ID_CAD_ETAPA = :idEtapa3 ");

		$consultar -> bindValue(':idEtapa1', $idEtapa, PDO::PARAM_STR);
		$consultar -> bindValue(':idEtapa2', $idEtapa, PDO::PARAM_STR);
		$consultar -> bindValue(':idEtapa3', $idEtapa, PDO::PARAM_STR);

		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaLancamentosEtapa($idEtapa, $horario, $idModalidade, $idIsca, $idEquipe){
	$pdo = conecta();
	try {

		if($idEtapa == ''){
			$idEtapa = 0;
		}
		$sql = "pesagens.ID_ETAPA = ".$idEtapa;

		if($horario != ''){
            $sql .= " and pesagens.DATA_HORA <= '".$horario."'";
		}

		if($idModalidade != 0){
            $sql .= " and pesagens.ID_CAD_MODALIDADE = ".$idModalidade;
		}

		if($idIsca != 0){
            $sql .= " and pesagens.ID_CAD_ISCA = ".$idIsca;
		}

		if($idEquipe != 0){
            $sql .= " and pesagens.ID_CAD_EQUIPE = ".$idEquipe;
		}

		$etapaModerada = retornaSeModeradaEtapa($idEtapa);
		if($etapaModerada == true){
			$sql .= ' and pesagens.MODERADO_EM IS NOT NULL ' ;
		}


		if($sql != ''){
            $sql = ' where ' . $sql;
		}else{
            $sql = '';
		}

		$consultar = $pdo -> prepare("SELECT pesagens.ID, pesagens.ID_CAD_COMPETIDOR, cad_equipes.DESCRICAO EQUIPE, cad_competidores.NOME COMPETIDOR, pesagens.PESO, pesagens.DATA_HORA, TIME(pesagens.DATA_HORA) HORA, pesagens.ID_CAD_ESPECIE, cad_especies.DESCRICAO ESPECIE, pesagens.ID_CAD_MODALIDADE, cad_modalidades.DESCRICAO MODALIDADE, cad_iscas.descricao ISCA, pesagens.COMPROVANTE, pesagens.ID_ETAPA, cad_etapas.DESCRICAO ETAPA, cad_campeonatos.DESCRICAO CAMPEONATO, pesagens.EXCLUIDO, pesagens.OBS, pesagens.nome_fiscal NOME_FISCAL FROM pesagens inner join cad_modalidades on (cad_modalidades.ID = pesagens.ID_CAD_MODALIDADE) inner join cad_iscas on (cad_iscas.id = pesagens.id_cad_isca) inner join cad_especies on (cad_especies.ID = pesagens.ID_CAD_ESPECIE) inner join cad_equipes on (cad_equipes.ID = pesagens.ID_CAD_EQUIPE) inner join cad_competidores on (cad_competidores.ID = pesagens.ID_CAD_COMPETIDOR) inner join cad_etapas on (cad_etapas.ID = pesagens.ID_ETAPA) inner join cad_campeonatos on (cad_campeonatos.ID = cad_etapas.ID_CAD_CAMPEONATO) ".$sql." order by pesagens.DATA_HORA, pesagens.COMPROVANTE");
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
            return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
            return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaSePulseiraJaCadastrada($idPulseira){
	$pdo = conecta();
	try {
		if($idPulseira==0){
			return false;
		}else{
			$consultar = $pdo -> prepare("SELECT * FROM composicao_equipes WHERE composicao_equipes.id_pulseira = :idPulseira");
			$consultar -> bindValue(':idPulseira', $idPulseira, PDO::PARAM_STR);
			$consultar -> execute();
			if ($consultar -> rowCount() > 0) {
				return true;
			} else {
				return false;
			}
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaCompetidoresEtapa($idEtapa){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("	SELECT
											cad_competidores.NOME,
											cad_equipes.descricao EQUIPE,
											cad_competidores.CPF,
											cad_competidores.TAMANHO_CAMISETA,
											cad_competidores.APELIDO,
											composicao_equipes.ID_PULSEIRA,
											cad_equipes.PATROCINADOR,
											composicao_equipes.SUPLENTE
										FROM
											cad_competidores
											INNER JOIN composicao_equipes ON ( composicao_equipes.id_competidor = cad_competidores.ID )
											INNER JOIN composicao_etapas ON ( composicao_etapas.ID_CAD_EQUIPE = composicao_equipes.id_equipe )
											INNER JOIN cad_equipes ON ( cad_equipes.id = composicao_equipes.id_equipe )
										WHERE
											composicao_etapas.ID_CAD_ETAPA = :idEtapa
										ORDER BY
											cad_equipes.DESCRICAO,
											cad_competidores.NOME");

		$consultar -> bindValue(':idEtapa', $idEtapa, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaCompetidoresAgrupadosEtapa($idEtapa){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("	SELECT
											t.ID_CAD_EQUIPE,
											t.NOME_EQUIPE,
											t.ID_COMPETIDORES,
											t.ID_PULSEIRAS,
											t.RAIA_INICIAL,
											t.ID_COMP_ETAPA,
											( SELECT GROUP_CONCAT( c.APELIDO SEPARATOR ' & ' ) FROM cad_competidores c WHERE FIND_IN_SET( c.ID, t.ID_COMPETIDORES ) ) APELIDOS_COMPETIDORES,
											( SELECT GROUP_CONCAT( c.NOME SEPARATOR ' e ' ) FROM cad_competidores c WHERE FIND_IN_SET( c.ID, t.ID_COMPETIDORES ) ) NOMES_COMPETIDORES
										FROM
											(
										SELECT
											cet.id ID_COMP_ETAPA,
											cet.ID_CAD_EQUIPE,
											caeq.descricao NOME_EQUIPE,
											GROUP_CONCAT( DISTINCT ceq.id_competidor ) ID_COMPETIDORES ,
											GROUP_CONCAT( DISTINCT ceq.id_pulseira ) ID_PULSEIRAS,
											COALESCE(cet.RAIA_INICIAL,0) RAIA_INICIAL
										FROM
											composicao_etapas cet
											INNER JOIN composicao_equipes ceq ON ceq.id_equipe = cet.ID_CAD_EQUIPE
											INNER JOIN cad_equipes caeq on caeq.ID = cet.ID_CAD_EQUIPE
										WHERE
											cet.ID_CAD_ETAPA = :idEtapa
										GROUP BY
											cet.ID_CAD_EQUIPE
											) t");
		$consultar -> bindValue(':idEtapa', $idEtapa, PDO::PARAM_STR);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetchAll(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaNomesEquipe($idEquipe){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("	SELECT
											composicao_equipes.id_competidor,
											cad_competidores.NOME competidor,
											substring_index( cad_competidores.NOME, ' ', 1 ) AS primeiro_nome,
											cad_competidores.apelido,
											cad_competidores.CIDADE,
											cad_competidores.UF,
											DATE_FORMAT( cad_competidores.DATA_NASCIMENTO, '%d/%m/%Y' ) DATA_NASCIMENTO,
											TIMESTAMPDIFF( YEAR, cad_competidores.DATA_NASCIMENTO, CURDATE( ) ) IDADE,
											cad_equipes.HISTORIA,
											cad_equipes.CURIOSIDADES,
											cad_equipes.DESCRICAO
										FROM
											composicao_equipes
											INNER JOIN cad_competidores ON ( cad_competidores.ID = composicao_equipes.id_competidor )
											INNER JOIN cad_equipes ON ( cad_equipes.ID = composicao_equipes.id_equipe )
										WHERE
											composicao_equipes.id_equipe = :idEquipe and composicao_equipes.suplente = 'N'
										ORDER BY
	cad_competidores.APELIDO");

		$consultar -> bindValue(':idEquipe', $idEquipe, PDO::PARAM_STR);
		$consultar -> execute();
		return $consultar -> fetchAll(PDO::FETCH_OBJ);
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaQtdNaEquipe($idEquipe){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("	SELECT
											COUNT(composicao_equipes.id_competidor) PARTICIPANTES
										FROM
											composicao_equipes
										WHERE
											composicao_equipes.id_equipe = :idEquipe");

		$consultar -> bindValue(':idEquipe', $idEquipe, PDO::PARAM_STR);
		$consultar -> execute();
		return $consultar -> fetch(PDO::FETCH_OBJ);
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaIdentificacaoEquipe($idEquipe){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("	SELECT
											count( composicao_equipes.id ) zerados
										FROM
											composicao_equipes
										WHERE
											composicao_equipes.id_equipe = :idEquipe
											AND composicao_equipes.id_pulseira = 0");

		$consultar -> bindValue(':idEquipe', $idEquipe, PDO::PARAM_STR);
		$consultar -> execute();
		return $consultar -> fetch(PDO::FETCH_OBJ);
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}


function retornaNomeDaEquipe($idEquipe){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("SELECT cad_equipes.DESCRICAO, cad_equipes.HISTORIA, cad_equipes.CURIOSIDADES from cad_equipes where ID = " . $idEquipe);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function retornaDadosUsuario($idUsuario){
	$pdo = conecta();
	try {
		$consultar = $pdo -> prepare("SELECT * from cad_usuarios where ID = " . $idUsuario);
		$consultar -> execute();
		if ($consultar -> rowCount() > 0) {
			return $consultar -> fetch(PDO::FETCH_OBJ);
		} else {
			return false;
		}
	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function updatePerfilUsuario($idUsuario, $senhaUsuario){
	$pdo = conecta();
	try {
		$cadastrar = $pdo -> prepare("UPDATE cad_usuarios SET senha = :senha WHERE id = :idUsuario");
		$cadastrar -> bindValue(':senha', $senhaUsuario, PDO::PARAM_STR);
		$cadastrar -> bindValue(':idUsuario', $idUsuario, PDO::PARAM_STR);
		$cadastrar -> execute();

		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}

	} catch(PDOException $e) {
		echo $e -> getMessage();
	}
}

function updatePesagensModeracao($idPesagem, $idUsuario, $status, $obs){
	$pdo = conecta();
	try{
		if($status == 'aprova'){
			$cadastrar = $pdo -> prepare("UPDATE pesagens SET MODERADO_EM = current_timestamp, MODERADO_ID_CAD_USUARIO = :idUsuario WHERE id = :idPesagem");
		}else{
			if($status == 'reprova'){
				$cadastrar = $pdo -> prepare("UPDATE pesagens SET MODERADO_EM = current_timestamp, MODERADO_ID_CAD_USUARIO = :idUsuario, EXCLUIDO = 'SIM', OBS = 'REPROVADO PELA MODERAÇÃO - ".$obs."' WHERE id = :idPesagem");
			}
		}
		$cadastrar -> bindValue(':idUsuario', $idUsuario, PDO::PARAM_STR);
		$cadastrar -> bindValue(':idPesagem', $idPesagem, PDO::PARAM_STR);
		$cadastrar -> execute();

		if ($cadastrar -> rowCount() > 0) {
			return true;
		} else {
			return false;
		}

	}catch(PDOException $e){
		echo $e -> getMessage();
	}

}


?>
