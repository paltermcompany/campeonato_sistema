"use strict";
let globalData = null;
var connection = null;
var isPreview = false;
var etapa_hoje = null;
var equipes_recebido = null;
var equipes_raias = null;
var ranking_recebido = null;
var ranking_indexado = null;
var grafico_recebido = null;
var allinfo = null;
if(window.location.search=='?preview') isPreview = true;

    function heartbeat() {
      clearTimeout(this.pingTimeout);

      // Use `WebSocket#terminate()`, which immediately destroys the connection,
      // instead of `WebSocket#close()`, which waits for the close timer.
      // Delay should be equal to the interval at which your server
      // sends out pings plus a conservative assumption of the latency.
      this.pingTimeout = setTimeout(() => {
        this.terminate();
        connection = null;
      }, 30000 + 1000);
    }


    // for better performance - to avoid searching in DOM
    // my name sent to the server
    function conecta() {
        var myName = 'Cliente';
        // if user is running mozilla then use it's built-in WebSocket
        window.WebSocket = window.WebSocket || window.MozWebSocket;
        // if browser doesn't support WebSocket, just show
        // some notification and exit
        if (!window.WebSocket) {
            console.log('Sorry, but your browser doesn\'t support WebSocket.');
            return;
        }
        // open connection
        connection = new WebSocket('ws://' + config.ip +':'+config.port);

        //connection.onping = heartbeat.bind(connection);
        connection.onclose = function () {
            console.log("Conexão fechada");
            connection = null;
        };

        connection.onopen = function () {
            console.log('Conexão aberta');
            //heartbeat().bind(connection);
        };

        connection.onerror = function (error) {
            // just in there were some problems with connection...
            console.log(error);
            connection = null;
        };

        // most important part - incoming messages
        connection.onmessage = function (message) {
            // try to parse JSON message. Because we know that the server
            // always returns JSON this should work without any problem but
            // we should make sure that the massage is not chunked or
            // otherwise damaged.
            try {
                var json = JSON.parse(message.data);
            } catch (e) {
                console.log('Invalid JSON: ', message.data);
                return;
            }

            // NOTE: if you're not sure about the JSON structure
            // check the server source code above
            // first response from the server with user's color
            if (json.type === 'hello') {
                var previewmode = (isPreview?'preview':'producao');
                connection.send(JSON.stringify({type: 'identify', data: previewmode }));
                // from now user can start sending messages
            }  else if (json.type === 'message') { // it's a single message
                // let the user write another message

                if( json.data.destino == 'todos' ||
                    (isPreview  && json.data.destino == 'preview' ) ||
                    (!isPreview && json.data.destino == 'producao' )
                    ) {

                    switch( json.data.acao ){
                        case 'mostrar': mostrarGC( json.data.gc, json.data.dados  ); break;
                        case 'fechar' : fecharGC(  json.data.gc );  break;
                        case 'remover': removerGC( json.data.gc ); break;
                        default:
                            var gc_classe = eval('new '+capitalize(json.data.gc)+'()');
                                eval('gc_classe.'+json.data.acao+'('+json.data.dados+')');
                                console.info('gc_classe.'+json.data.acao+'('+json.data.dados+')');
                        break;
                    }
                }
            }  else if (json.type === 'ranking') { // it's a single message
                ranking_recebido = json.data.resultados;
                console.log(json.data.resultados);
                indexaRanking();
                Top10_ranking._instancia.ranking(json.data);
                if( typeof $top10_full != 'undefined' ) $top10_full.atualiza();
                if( typeof $competidores != 'undefined' ) $competidores.atualiza();
                if( typeof $dados_equipe != 'undefined' ) $dados_equipe.atualiza();
                console.log('informações do ranking.');
            }  else if (json.type === 'etapa_hoje') { // it's a single message
                console.log('informações da etapa de hoje');
                etapaGC(json.data);
            }  else if (json.type === 'atualiza_form') { // it's a single message
                console.log('informações do formulario '+json.data.gc);
                dadosGC(json.data.gc, json.data.dados);
            }  else if (json.type === 'equipes_etapa') { // it's a single message
                console.log('informações das equipes');
                equipeGC(json.data.resultados);
            }  else if (json.type === 'allinfo') { // it's a single message
                console.log('informações das estatisticas');
                estatisticasInfo(json.data);
            }  else if (json.type === 'status_anterior') { // it's a single message
                msgStatusAnterior(json.data);
            }  else if (json.type === 'syncCountdown') { // it's a single message
                msgSyncCountdown(json.data);
            } else if(json.type === 'grafico'){
                grafico_recebido = json.data;
                indexaGrafico();
            }else {
                //console.log('Hmm..., I\'ve never seen JSON like this:', json);
            }
        };

        // send the message as an ordinary text
        //    connection.send(msg);
        /**
        * Add message to the chat window
        */
    }

$(function () {
    conecta();
    setInterval(function checkConnection() {
        if(connection == null ) {
            conecta();
        }
    }, 1000);

    if(isPreview) {
        $('body').css('zoom','0.5');
    }
});


/********************************************************************************************************************
*     Funções de comando
*/

const capitalize = (s) => {
  if (typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}

function dadosGC( gc, dados ) {
    var gc_classe = eval('new '+capitalize(gc)+'()');
    gc_classe.preenche( dados );
}

function mostrarGC( gc, dados ) {
    var gc_classe = eval('new '+capitalize(gc)+'()');
    gc_classe.mostrar( dados );
}

function fecharGC( gc ) {
    var gc_classe = eval('new '+capitalize(gc)+'()');
    gc_classe.fechar();
}

function removerGC( gc ) {
    var gc_classe = eval('new '+capitalize(gc)+'()');
    gc_classe.remover();
}

function retornoGC( gc, data ){
    if(connection == null ){
        conecta();
    } else {
        connection.send(JSON.stringify({fonte: isPreview ? 'preview' : 'producao', gc: gc, data: data}));
    }
}

function estatisticasInfo( data = null ){
    if(data != null) {
        allinfo = data;
        $card3d.atualizaDados(data);
    }
}

function equipeGC( data = null ){
    if(data!=null) {
        var raias = [];
        raias.push(0);
        var recebido = [];
        data.forEach(function( el ){
            el.nomes_completos = el.competidor1+'<br>'+el.competidor2;
            recebido[el.id_equipe] = el;
            raias.push(el.id_equipe);
        });
        equipes_recebido = recebido;
        equipes_raias = raias;
    }

    if(typeof $competidores != 'undefined') {
        $competidores.atualiza();
    }
}

function indexaRanking(){
    var equipes = [];
    if(ranking_recebido) {
        ranking_recebido.forEach(function (el){
            equipes[el.id_equipe] = el;
        });
    }
    ranking_indexado = equipes;
}

function indexaGrafico(){
    
}

function etapaGC( data = null ){
    if(data!=null) {
        etapa_hoje = data;
    }
    $('body')
        .removeClass('gaucho')
        .removeClass('catarinense')
        .removeClass('paranaense')
        .removeClass('mineiro')
        .removeClass('goiano')
        .removeClass('paulista')
    ;

    if(!etapa_hoje){
        console.log('ERRO! SEM ETAPA SELECIONADA NO SISTEMA!');
        return;
    }

    var selecionado = etapa_hoje.UF;
    if (etapa_hoje.DESCRICAO.indexOf('FINAL') >= 0) {
        selecionado = 'BR';
    }
    switch(selecionado){
        case 'RS': selecionado = 'gaucho';      break;
        case 'GO': selecionado = 'goiano';      break;
        case 'MG': selecionado = 'mineiro';     break;
        case 'SP': selecionado = 'paulista';    break;
        case 'PR': selecionado = 'paranaense';  break;
        case 'SC': selecionado = 'catarinense'; break;
        default:   selecionado = null;
    }

    etapa_hoje.etapa = selecionado;
    if(config.producao) {
        $('body').addClass(selecionado);
    }

    localStorage.setItem( "regional", selecionado );
    if( typeof $menu != 'undefined' ) {
        $menu.regional(selecionado);
    }
    Top10_ranking._instancia.atualiza_descricao();
    $top10_full.atualiza_descricao();
    $graficos.atualiza_descricao();

}


/*
function msgSetaBotaoGC( gc, novostatus ){
    var target = $('#botao_gc_'+gc);

    var form = $('#gc-form-'+gc);
    if(novostatus == 'fechado' ){
        form.addClass('d-none');
    } else {
        form.removeClass('d-none');
    }

    target.removeClass('button-preview').removeClass('button-producao').removeClass('button-fechado');
    target.attr('data-status', novostatus);
    target.addClass('button-'+novostatus);
}*/

function msgStatusAnterior( gcs ){
    var gcs_index = Object.keys(gcs);
    gcs_index.forEach(function(key){
        var gc = gcs[key];
        if(!isPreview && gc.producao == 'mostrar'){
            mostrarGC(key,gc.data);
        } else if( isPreview && gc.preview == 'mostrar'){
            mostrarGC(key,gc.data);
        }
    });
}

function msgSyncCountdown( data ){
    //console.log(data.countdown);
    Top10_ranking._instancia.sync_relogio(data.countdown)
}
