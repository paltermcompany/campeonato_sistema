config = {
    producao: true,
    //ip: '127.0.0.1',
    ip: '10.0.1.17',
    port: 1337,
    api: '/sistema/json/',
    endpoint_ranking: 'gc_resultados.php',
    endpoint_etapa:   'gc_etapa_hoje.php',
    endpoint_equipes: 'gc_equipes_etapa.php',
    endpoint_grafico: 'gc_agregador_grafico.php',
    endpoint_allinfo: 'gc_agregador.php'
};


config.getEndpoint = function( endpoint ){
    return 'http://' + "brasileiroempesqueiros.com.br" + config.api + config["endpoint_"+endpoint];
}

if( typeof exports !== 'undefined' ) {
    exports.config = function() {
        return  config;
    };
}
