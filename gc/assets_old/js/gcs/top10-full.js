$top10_full = null;
function Top10_full ( params ) {
    if($top10_full){
        return $top10_full;
    }
    this.loaded    = false;
    $top10_full  = this;
    this.id        = 'top10_full';
    this.num_slide = 0;
    this.intervalMP = null;
    this.pesoMP = '';
    this.posicaoMP = 0;
    this.fechandoTimeout = null;
}

Top10_full.prototype = {
    constructor: Top10_full,

    init:function() {
        if(this.loaded) return;
        this.loaded = true;
    },

    mostrar:function( ) {
        if(!ranking_recebido) {
            $top10_full.remover();
            return;
        }
        if( $top10_full.fechandoTimeout != null) {
            clearTimeout($top10_full.fechandoTimeout);
        }
        $top10_full.fechandoTimeout = null;
        $top10_full.atualiza();
        $('#top10_full').removeClass('escondido');
        $('#top10_full').removeClass('fechar');
        $('#top10_full').removeClass('nodelay');
    },

    fechar: function() {
        $('#top10_full').removeClass('nodelay');
        $('#top10_full').addClass('fechar');
        if( $top10_full.fechandoTimeout != null) {
            clearTimeout($top10_full.fechandoTimeout);
        }
        $top10_full.fechandoTimeout = setTimeout(function(){
            $('#top10_full').addClass('escondido');
            $top10_full.pararMP();
        }, 1000);
    },

    remover: function() {
        $('#top10_full').removeClass('nodelay');
        $('#top10_full').addClass('fechar');
        $('#top10_full').addClass('escondido');
        $top10_full.pararMP();
        if( $top10_full.fechandoTimeout != null) {
            clearTimeout($top10_full.fechandoTimeout);
        }
        $top10_full.fechandoTimeout = null;
    },

    atualizaMP: function(){
        if(!$top10_full.posicaoMP) return;
        var el_mp = $('#top10_full .isMP');
        $('#top10_full').addClass('nodelay');
        if(el_mp.hasClass('mp')){
            el_mp.removeClass('trocar').offset();
            el_mp.addClass('trocar');
            setTimeout(function(){
                el_mp.removeClass('mp');
                el_mp.removeClass('trocar');
                el_mp.find('.peso p').html( ranking_recebido[$top10_full.posicaoMP].total );
            },300);
        } else {
            el_mp.removeClass('trocar').offset();
            el_mp.addClass('trocar');
            setTimeout(function(){
                el_mp.addClass('mp');
                el_mp.removeClass('trocar');
                el_mp.find('.peso p').html( ranking_recebido[0].total );
            },300);

        }
    },

    iniciarMP: function(){
        if( $top10_full.intervalMP != null ) return;
        $top10_full.intervalMP = setInterval( $top10_full.atualizaMP, 5000 );
    },

    pararMP: function(){
        if( $top10_full.intervalMP != null ) {
            clearInterval($top10_full.intervalMP);
        }
        $top10_full.intervalMP = null;
        $('#top10_full .isMP').removeClass('mp');
    },

    atualiza: function(){
        if(!ranking_recebido) return;
        var mp = ranking_recebido[0];
        var pos_mp = false;
        if(mp.id_equipe == ranking_recebido[1].id_equipe ) {
            $('#top10_full .ranking').removeClass('posicao-extra');
            pos_mp = 1;
            $top10_full.iniciarMP();
        } else if(mp.id_equipe == ranking_recebido[2].id_equipe ) {
            $('#top10_full .ranking').removeClass('posicao-extra');
            pos_mp = 2;
            $top10_full.iniciarMP();
        } else {
            $('#top10_full .ranking').addClass('posicao-extra');
            $top10_full.pararMP();
        }
        $top10_full.posicaoMP = pos_mp;
            //$('#top10_full .ranking').addClass('posicao-extra');
        $('#top10_full .dupla[data-posicao-full]').each( function ( i, el ) {
            var el_dupla = $(el);
            var posicao = el_dupla.data('posicaoFull');

            if(!pos_mp){
                //Não tem MP
                if(posicao == 3) {
                    posicao = 0;
                } else if(posicao > 3 ){
                    posicao--;
                }
            } else {
                //tem MP, pula sexto
                if(posicao > 6 ){
                    posicao--;
                }
            }
            var pos = ranking_recebido[ posicao ];

            if(pos){
                var nomes = pos.nome_competidores;
                if(equipes_recebido && typeof equipes_recebido[ pos.id_equipe ] !== 'undefined') {
                    nomes = equipes_recebido[ pos.id_equipe ].nomes_completos;
                }
                el_dupla.find('.nome').html( nomes );
                el_dupla.find('.peso p').html( pos.total );
                var el_posicao = el_dupla.closest('.posicao');
                el_posicao.find('.num span').html( pos.posicao );
                el_posicao.removeClass('desclassificado');
                if(posicao == pos_mp || (posicao == 0 && pos_mp == 3) ){
                    if( posicao == 0 ) el_posicao.addClass('mp');
                    el_posicao.addClass('isMP');
                } else {
                    el_posicao.removeClass('isMP');
                    el_posicao.removeClass('mp');
                }
            } else {
                el_dupla.find('.nome').html( '' );
                el_dupla.find('.peso p').html( '' );
                var el_posicao = el_dupla.closest('.posicao');
                el_posicao.find('.num span').html( '-' );
                el_posicao.addClass('desclassificado');
                el_posicao.removeClass('isMP');
                el_posicao.removeClass('mp');
            }
        });
    },

    atualiza_descricao( ){
        $('#top10_full .conteudo header>div:last-child h2').html('ETAPA '+etapa_hoje.ETAPA);
        $('#top10_full .conteudo header>div:last-child p em').html(etapa_hoje.DESCRICAO);
    }

}
$top10_full = new Top10_full();
