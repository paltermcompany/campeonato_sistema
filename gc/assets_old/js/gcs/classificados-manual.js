$classificados_manual = null;
function Classificados_manual ( params ) {
    if($classificados_manual){
        return $classificados_manual;
    }
    this.loaded    = false;
    $classificados_manual  = this;
    this.id        = 'classificados_manual';
    this.num_slide = 0;
    this.posicoes  = 0;
    this.pesoMP    = '';
    this.posicaoMP = 0;
    this.carregadoMP = 0;
    this.dados = null;
    this.fechandoTimeout = null;
}

Classificados_manual.prototype = {
    constructor: Classificados_manual,

    init:function() {
        if(this.loaded) return;
        this.loaded = true;
    },

    mostrar:function( ) {
        $classificados_manual.remover();
        $classificados_manual.atualiza_descricao();
        $('#classificados_manual').removeClass('nodelay');
        $('#classificados_manual').removeClass('escondido');
        $('#classificados_manual').removeClass('carregado');
        $('#classificados_manual').removeClass('fechar');
    },

    fechar: function() {
        $('#classificados_manual').removeClass('nodelay');
        $('#classificados_manual').addClass('fechar');
        $classificados_manual.fechandoTimeout = setTimeout(function(){
            $classificados_manual.remover();
        }, 1000);
    },

    remover: function() {
        if($classificados_manual.fechandoTimeout != null) {
            clearTimeout($classificados_manual.fechandoTimeout);
            $classificados_manual.fechandoTimeout = null;
        }
        if($classificados_manual.carregadoMP != null) {
            clearTimeout($classificados_manual.carregadoMP);
            $classificados_manual.carregadoMP = null;
        }
        $classificados_manual.pararMP();
        $('#classificados_manual').removeClass('nodelay');
        $('#classificados_manual').addClass('fechar');
        $('#classificados_manual').addClass('escondido');
        $('#classificados_manual .carregado').removeClass('carregado');
        $('#classificados_manual .mp').removeClass('mp');
        $('#classificados_manual .isMP').removeClass('isMP');

    },

    atualizaMP: function(){
        if(!$classificados_manual.posicaoMP) return;
        var el_mp = $('#classificados_manual .isMP');
        $('#classificados_manual').addClass('nodelay');
        if(el_mp.hasClass('mp')){
            el_mp.find('.peso').removeClass('carregado');
            setTimeout(function(){
                el_mp.removeClass('mp');
                $('#classificados_manual .dupla[data-posicao-full=1] .peso p').html(dados['primeiro_lugar_peso']);
                $('#classificados_manual .dupla[data-posicao-full=2] .peso p').html(dados['segundo_lugar_peso']);
                $('#classificados_manual .dupla[data-posicao-full=3] .peso p').html(dados['terceiro_lugar_peso']);
            },300);
        } else {
            el_mp.addClass('mp');
            setTimeout(function(){
                el_mp.find('.peso').addClass('carregado');
                el_mp.find('.peso p').html( $classificados_manual.dados['peso_mp'] );
            },300);

        }
    },

    iniciarMP: function(){
        if( $classificados_manual.intervalMP != null ) return;
        $classificados_manual.intervalMP = setInterval( $classificados_manual.atualizaMP, 6000 );
    },

    pararMP: function(){
        if( $classificados_manual.intervalMP != null ) {
            clearInterval($classificados_manual.intervalMP);
        }
        $classificados_manual.intervalMP = null;
        $('#classificados_manual .isMP').removeClass('mp');
    },

    preenche: function( dados ) {
        $classificados_manual.dados = dados;
        if(!$('#classificados_manual').hasClass('escondido')) {
            $classificados_manual.atualiza_descricao();
        }

    },
    atualiza_descricao: function() {
        dados = $classificados_manual.dados;
        document.getElementById('classificados_manual').classList = dados['etapa'];
        $('#classificados_manual .conteudo header>div:last-child h2').html(   dados['descricao_etapa'] );
        $('#classificados_manual .conteudo header>div:last-child p em').html( dados['local_etapa']     );

        $('#classificados_manual .dupla[data-posicao-full]').each( function ( i, el ) {
            var el_dupla = $(el);
            var posicao = el_dupla.data('posicaoFull');
            var nome = '', peso = '';

            switch(posicao){
                case 1: nome = dados['primeiro_lugar']; peso = dados['primeiro_lugar_peso']; break;
                case 2: nome = dados['segundo_lugar']; peso = dados['segundo_lugar_peso']; break;
                case 3: nome = dados['terceiro_lugar']; peso = dados['terceiro_lugar_peso']; break;
            }
            nome = nome.replace('&','<br>');
            el_dupla.find('.nome').html( nome );
            el_dupla.find('.peso p').html( peso );
            el_posicao = el_dupla.closest('.posicao');


            if( dados['posicao_mp'] == posicao ) {//Sou o mp?
                $classificados_manual.posicaoMP = posicao;
                if( posicao == 3 ) {
                    $classificados_manual.pararMP();
                } else {
                    $classificados_manual.iniciarMP();
                }
                if( posicao == 3 ) {
                    el_posicao.addClass('mp');
                    el_dupla.find('.peso');
                    el_dupla.find('.peso p').html( dados['peso_mp'] );
                    if($classificados_manual.carregadoMP == null ){
                        console.log('Iniciado timeout');
                        $classificados_manual.carregadoMP = setTimeout( function(){
                            console.log('Finlizado timeout');
                            $classificados_manual.carregadoMP = null;
                            el_dupla.find('.peso').addClass('carregado');
                        }, 4000 );
                    }
                }
                el_posicao.addClass('isMP');
            } else {
                el_posicao.removeClass('isMP');
                el_posicao.removeClass('mp');
                el_dupla.find('.peso').removeClass('carregado');
            }
        });

        //$classificados_manual.atualiza();
    },

}
$classificados_manual = new Classificados_manual();
