$graficos = null;
function Graficos(params){
    if($graficos){
        return $graficos;
    }
    this.loaded = false;
    $graficos = this;
    this.id = 'graficos';
    this.dataset = 'ESPECIE';
    this.fechandoTimeout = null;
}

Graficos.prototype = {
    constructor: Graficos,

    init:function(){
        if(this.loaded) return;
        this.loaded = true;
    },

    mostrar:function( dataset = null){

        var ctx = $('#grafico-gc');
        var nomeGrafico;
        
        if($graficos.dataset == 'ESPECIES'){
            nomeGrafico = 'Espécies';
        }else if($graficos.dataset == 'ISCAS'){
            nomeGrafico = 'Iscas';
        }else if($graficos.dataset == 'MODALIDADES'){
            nomeGrafico = 'Modalidades';
        }

        if(grafico_recebido == null){
            $graficos.remover();
            return;
        }

        if(dataset == null){
            dataset = this.dataset;
        }

        if( $graficos.fechandoTimeout != null) {
            clearTimeout($graficos.fechandoTimeout);
        }
        $graficos.fechandoTimeout = null;

        $('#graficos').attr('data-dataset', this.dataset);

        $graficos.atualiza();
        $('#graficos').removeClass('fechar');
        $('#graficos').removeClass('escondido');
        $('#graficos').removeClass('nodelay');

        this.criaGrafico(ctx, grafico_recebido, $graficos.dataset, nomeGrafico);
    
    },

    fechar:function(){
        $('#graficos').removeClass('nodelay');
        $('#graficos').addClass('fechar');
        if( $graficos.fechandoTimeout != null) {
            clearTimeout($graficos.fechandoTimeout);
        }
        $graficos.fechandoTimeout = setTimeout(function(){
            $('#graficos').addClass('escondido');
        }, 1000);
    },

    atualiza:function(){
        var ctx = $('#grafico-gc');
        var nomeGrafico;
        
        if($graficos.dataset == 'ESPECIES'){
            nomeGrafico = 'Espécies';
        }else if($graficos.dataset == 'ISCAS'){
            nomeGrafico = 'Iscas';
        }else if($graficos.dataset == 'MODALIDADES'){
            nomeGrafico = 'Modalidades';
        }

        this.criaGrafico(ctx, grafico_recebido, $graficos.dataset, nomeGrafico);
    },

    remover:function(){

        $('#graficos').removeClass('nodelay');
        $('#graficos').addClass('fechar');
        $('#graficos').addClass('escondido');
        if( $graficos.fechandoTimeout != null) {
            clearTimeout($graficos.fechandoTimeout);
        }
        $graficos.fechandoTimeout = null;

    },

    preenche:function(dados){
        $graficos.dataset = dados['dados-grafico'];
        $graficos.atualiza();
        $graficos.atualiza_descricao();    
    },

    

    criaGrafico:function(ctx, dados, opcao, nomeGrafico){
        var labels = dados["TEMPOS"];
        //var cores = ["#0074D9", "#FF4136", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"];
        var datasetGrafico = [];
        /*
        var total = [
            [1, 2, 3, 4],
            [5, 6, 7, 8],
            [9, 10, 11, 12],
            [13, 1, 2, 5],
            [13, 1, 2, 5],
            [13, 1, 2, 0]
          ];
          */
        //datasetGrafico.push(labelsGrafico);

        for(i in dados[opcao]){

            //var total = [];
            
            /*
            for( j in dados.ESPECIES[i].TOTAL){
                total.push(dados.ESPECIES[i].TOTAL);
            }
            */

            var dataset = {
                label: dados[opcao][i].DESCRICAO,
                data: dados[opcao][i].QTDS_TEMPO,
                backgroundColor: dados[opcao][i].COR_GC_HEX 
                //backgroundColor: dados.ESPECIES[i].COR_GC_HEX
            };

            datasetGrafico.push(dataset);

        }

        new Chart(ctx, {
            type: 'bar',
            data:{
                labels:labels,
                datasets: datasetGrafico
            },
            options:{
                responsive: false,
                title:{
                    display: true,
                    text: nomeGrafico,
                    fontSize: 45,
                    fontColor: "white",
                    fontFamily: 'Montserrat',
                    fontStyle: 'bold'
                },
                scales:{
                    xAxes:[{
                        stacked:true,
                        gridLines:{
                            display:false
                        },
                        ticks:{
                            fontColor: "white",
                            fontSize: 26,
                            fontFamily: 'Montserrat',
                            fontStyle: 'bold'
                        }
                    }],
                    yAxes:[{
                        stacked:true,
                        ticks:{
                            beginAtZero: true,
                            fontColor: "white",
                            fontSize: 26,
                            fontFamily: 'Montserrat',
                            fontStyle: 'bold'
                        }
                    }]
                },
                legend: {
                    display: true,
                    labels: {
                        fontColor: 'rgb(255, 255, 255)',
                        fontSize: 25,
                        fontFamily: 'Montserrat',
                        fontStyle: 'bold'
                    }
                },
                events:[]
            }
            
        });

        /*
        dados.ESPECIES.forEach(element => {
            console.log(element);
        });
        */

    },



    atualiza_descricao( ){
        $('#graficos .conteudo header>div:last-child h2').html('ETAPA '+etapa_hoje.ETAPA);
        $('#graficos .conteudo header>div:last-child p em').html(etapa_hoje.DESCRICAO);
    }

    
}

$graficos = new Graficos();