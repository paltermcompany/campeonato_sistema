$apresentadores = null;
function Apresentadores ( params ) {
    if($apresentadores){
        return $apresentadores;
    }
    this.loaded     = false;
    $apresentadores = this;
    this.id         = 'apresentadores';
    this.posicao    = 'esquerda';
}

Apresentadores.prototype = {
    constructor: Apresentadores,

    init:function() {
        if(this.loaded) return;
        this.loaded = true;
    },

    mostrar:function( posicao = null ) {
        $('#apresentadores').removeClass('fechar');
        $('#apresentadores').removeClass('escondido');

        if( posicao == null ) {
            posicao = this.posicao;
        }

        $('#apresentadores').attr('data-posicao', this.posicao);
    },

    fechar: function() {
        if($('#apresentadores').hasClass('escondido')) return;
        $('#apresentadores').addClass('escondido');
        $('#apresentadores').offset();
        $('#apresentadores').removeClass('escondido');
        $('#apresentadores').addClass('fechar');
        setTimeout(function(){
            $('#apresentadores').addClass('escondido');
        }, 1500);

    },

    remover: function() {
        $('#apresentadores').addClass('fechar');
        $('#apresentadores').addClass('escondido');
    },

    preenche: function( dados_novos ) {
        $('#apresentadores .conteudo .nome').html(dados_novos['apresentador-titulo']);
        $('#apresentadores .conteudo .legenda').html(dados_novos['apresentador-texto']);
        this.posicao = dados_novos['apresentador-posicao'];
        $('#apresentadores').attr('data-posicao', this.posicao);
    },

}

$apresentadores = new Apresentadores();

