function random_range( min, max ){
    return min + (Math.random() * (max-min));
}
// utilities
function getLength(x0, y0, x1, y1) {
    // returns the length of a line segment
    const x = x1 - x0;
    const y = y1 - y0;
    return Math.sqrt(x * x + y * y);
}

function getDegAngle(x0, y0, x1, y1) {
    const y = y1 - y0;
    const x = x1 - x0;
    return Math.atan2(y, x) * (180 / Math.PI);
}

// some constants
const DECAY   = 110;        // confetti decay in seconds
const FADEIN  = 30;        // confetti decay in seconds
const FADEOUT = 45;        // confetti decay in seconds
const SPREAD  = 30;      // degrees to spread from the angle of the cannon
const SPREADX = 50;      // degrees to spread from the angle of the cannon
const GRAVITY = 100;
const FRICTION = 0.001;
const VELOCIDADEY = 4;
class ConfettiCannon {
    constructor() {
        // setup a canvas
        this.lastid = 0;
        this.canvas = document.getElementById('explosao');
        this.dpr = window.devicePixelRatio || 1;
        this.ctx = this.canvas.getContext('2d');
        this.ctx.scale(this.dpr, this.dpr);

        // add confetti here
        this.confettiSprites = [];

        // bind methods
        this.render = this.render.bind(this);
        this.setCanvasSize = this.setCanvasSize.bind(this);
        this.tickInterval = null;
        this.setCanvasSize();
    }

    atira() {
        this.addConfettiParticles(200, 90, VELOCIDADEY,  550, 300 );
    }

    start() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.confettiSprites = [];
        if(this.tickInterval != null ){
            clearInterval( this.tickInterval );
        }
        this.tickInterval = setInterval(this.render,1000/30);
    }

    stop() {
        if(this.tickInterval != null ){
            clearInterval( this.tickInterval );
        }
        this.tickInterval = null;
    }

    setCanvasSize() {
        this.canvas.width = 1100;
        this.canvas.height = 650;
        this.canvas.style.width = '1100px';
        this.canvas.style.height = '650px';
    }

    addConfettiParticles(amount, angle, velocity, pos_x, y) {
        const dourados = [[255,223,0],[212,175,55],[207,181,59],[197,179,88],[230,190,138],[153,101,21]];
        let lastAlive = 0;
        let i = 0;
        while (i++ < amount) {
            // sprite
            const r = random_range(4, 6)/2;
            const d = random_range(15, 25)/2;
            const cor = parseInt(random_range(0,5));
            const cr = dourados[cor][0];
            const cg = dourados[cor][1];
            const cb = dourados[cor][2];

            const tilt = random_range(10, -10);
            const tiltAngleIncremental = random_range(0.07, 0.05);
            const tiltAngle = random_range(-1, 1);
            const decay = DECAY+random_range(0,25);
            const ax = random_range(-velocity,velocity);
            const ay = random_range(-velocity,-velocity*0.8);
            const x = pos_x + random_range(-SPREADX, SPREADX);

            const sprite = {
                decay,
                angle,
                velocity,
                x,
                y,
                ax,
                ay,
                r,
                d,
                cr,
                cg,
                cb,
                tilt,
                tiltAngleIncremental,
                tiltAngle,
            };


            for( ; lastAlive < this.confettiSprites.length; lastAlive++ ) {
                if(this.confettiSprites[lastAlive].decay<=0){
                    break;
                }

            }
            if(lastAlive < this.confettiSprites.length ){
                delete this.confettiSprites[lastAlive];
                this.confettiSprites[lastAlive] = sprite;
            } else {
                this.confettiSprites.push(sprite);
            }
            //this.tweenConfettiParticle(id);
        }
    }

    updateConfettiParticle(sprite) {

        const tiltAngle = 0.0005 * sprite.d;

        sprite.angle += 0.01;
        sprite.tiltAngle += tiltAngle;
        sprite.tiltAngle += sprite.tiltAngleIncremental;
        sprite.tilt = (Math.sin(sprite.tiltAngle - (sprite.r / 2))) * sprite.r * 2;
        sprite.y += sprite.ay + Math.sin(sprite.angle + sprite.r / 2) * 2 ;
        sprite.x += sprite.ax;

        sprite.ax += (0 - sprite.ax) * FRICTION;
        sprite.ay += (GRAVITY - sprite.ay) * FRICTION;
        if(sprite.decay > 0 ) sprite.decay--;
    }

    drawConfetti() {
        for( var id=0; id < this.confettiSprites.length; id++ ) {
            const sprite = this.confettiSprites[id];
            if(sprite.decay<=0) continue;
            if(sprite.decay>DECAY) {
                this.updateConfettiParticle(sprite);
                continue;
            }
            let opacity = 1;
            if(sprite.decay >= DECAY-FADEIN){
                 opacity = ( DECAY - sprite.decay )/ FADEIN;
            } else if( sprite.decay < FADEOUT ) {
                opacity = (sprite.decay / FADEOUT);
            }

            this.ctx.beginPath();
            this.ctx.lineWidth = sprite.d / 2;
            this.ctx.strokeStyle = `rgba(${sprite.cr}, ${sprite.cg}, ${sprite.cb}, ${opacity})`;
            this.ctx.moveTo(sprite.x + sprite.tilt + sprite.r, sprite.y);
            this.ctx.lineTo(sprite.x + sprite.tilt, sprite.y + sprite.tilt + sprite.r);
            this.ctx.stroke();
            this.updateConfettiParticle(sprite);
        };
    }

    render() {
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.drawConfetti();
        //var dc = document.getElementById('dupla_campea');dc.style.transform="rotate("+this.lastid+"deg)";this.lastid++;
    }
}

$dupla_campea = null;
function Dupla_campea ( params ) {
    if($dupla_campea) {
        return $dupla_campea;
    }
    this.loaded    = false;
    $dupla_campea  = this;
    this.id        = 'dupla_campea';
    this.posicao   = 1;
    this.fogos     = 0;
    this.num_slide = 0;
    this.confetti = null;
    this.canhaoInterval = null;
    this.txtposicao_manual = '';
    this.txtcompetidores_manual = '';
    this.fogos_manual = 0;
    $(document).ready(this.init.bind(this));
}

Dupla_campea.prototype = {
    constructor: Dupla_campea,

    init:function() {
        if(this.loaded) return;
        console.log('init dupla_campea');
        this.loaded = true;
        this.confetti = new ConfettiCannon();
        //setInterval(function(){ this.confetti.addConfettiParticles(1, 90, 1,  550, 300 );}.bind(this),16)
    },

    mostrar:function( dupla ) {
        if(dupla) {
            this.preenche( dupla );
        }
        $('#dupla_campea').removeClass('escondido');
        $('#dupla_campea').removeClass('fechar');
        this.confetti.start();
        if( this.canhaoInterval != null ) clearInterval(this.canhaoInterval);
        if( $dupla_campea.fogos == 1 ){
            this.confetti.atira();
            this.canhaoInterval = setInterval( function() { this.confetti.atira(); }.bind(this), 3000 );
        }
        //setInterval(function(){ this.confetti.addConfettiParticles(10, 90, 1,  350 + random_range(-SPREAD, SPREAD), 300 ); }.bind(this),16)
    },

    fechar: function() {
        setTimeout(function(){
            $('#dupla_campea').addClass('fechar');
        }, 100);
        if( this.canhaoInterval != null ) clearInterval(this.canhaoInterval);
        this.canhaoInterval = setInterval(this.remover.bind(this),1500);
    },

    remover: function() {
        $('#dupla_campea').addClass('fechar');
        $('#dupla_campea').addClass('escondido');
        if( this.canhaoInterval != null ) clearInterval(this.canhaoInterval);
        this.canhaoInterval = null;
        this.confetti.stop();
    },

    preenche: function( dupla ){
        console.log(dupla);
        var posicao = parseInt(dupla['id_dupla_campea']);
        //{"id_dupla_campea":"-1","dupla_campea_titulo":"Jemiroquei","dupla_campea_nomes":"Lesmialdo","dupla_campea_fogos":"on"}
        if(dupla['dupla_campea_titulo']) $dupla_campea.txtposicao_manual = dupla['dupla_campea_titulo'];
        if(dupla['dupla_campea_nomes']) {$dupla_campea.txtcompetidores_manual = dupla['dupla_campea_nomes']; $dupla_campea.fogos_manual = 0; }
        if(dupla['dupla_campea_fogos'] == 'on') { $dupla_campea.fogos_manual = 1; }
        if(dupla['dupla_campea_nomes'] && dupla['dupla_campea_titulo'] ) return;

        var equipe = null;
        if(posicao >= 0) {
            equipe = ranking_recebido[posicao];
        }
        $dupla_campea.fogos   = 0;
        switch(posicao){
            case -1: //manual
                    txtposicao = $dupla_campea.txtposicao_manual;
                    txtcompetidores = $dupla_campea.txtcompetidores_manual;
                    $dupla_campea.fogos = $dupla_campea.fogos_manual;
            break;
            case 0:  txtposicao = "MAIOR PEIXE";    txtcompetidores = equipe.nome_competidores; break;
            case 1:  txtposicao = "PRIMEIRO LUGAR"; txtcompetidores = equipe.nome_competidores; $dupla_campea.fogos = 1;break;
            case 2:  txtposicao = "SEGUNDO LUGAR";  txtcompetidores = equipe.nome_competidores; break;
            case 3:  txtposicao = "TERCEIRO LUGAR"; txtcompetidores = equipe.nome_competidores; break;
            default: txtposicao =  equipe.posicao + 'º Lugar'; txtcompetidores = equipe.nome_competidores; break;
        }
        console.log('mudando para', txtposicao, txtcompetidores );
        $('#dupla_campea .conteudo .titulo').html( txtposicao );
        $('#dupla_campea .conteudo .texto').html( txtcompetidores );
        $dupla_campea.posicao = posicao;
    }

}

$dupla_campea = new Dupla_campea();
/*setTimeout(function(){
    $dupla_campea.init();
    //$dupla_campea.mostrar();
}, 100);*/

