$card3d = null;
function Card3d ( params ) {
    if($card3d) {
        return $card3d;
    }
    this.loaded = false;
    $card3d       = this;
    this.id     = 'card3d';
    this.lastAtivo = '';
}

Card3d.prototype = {
    constructor: Card3d,

    init:function() {
        if(this.loaded) return;
        this.loaded = true;
    },

    mostrar:function() {
        $('#card3d').removeClass('escondido');
        $('#card3d').removeClass('fechar');
    },

    fechar: function() {
        $("#card3d").addClass("fechar");
        setTimeout(function(){
            $card3d.remover();
        }, 500);
    },

    remover: function() {
        $card3d.lastAtivo = '';
        $('#card3d').addClass('fechar');
        $('#card3d').addClass('escondido');
        $("#card3d .card").removeClass("details");
        $("#card3d").removeClass("abrir");
    },

    preenche: function( dados_novos ) {
        var ativo = dados_novos["card3d-ativo"];
        if (ativo == $card3d.lastAtivo) {
            return;
        }
        $card3d.lastAtivo = ativo;
        var alvo = $('#card3d #card3d-'+ativo);
        var abertos = $("#card3d .details").length;
        if(alvo.length>0) {
            $("#card3d .card").removeClass("details");
            $("#card3d").removeClass("abrir");
            var timeout = (abertos == 0) ? 50 : 1000;
            setTimeout(() => {
                $("#card3d").addClass("abrir");
                alvo.addClass("details");
            }, timeout);
        } else {
            $("#card3d .card").removeClass("details");
            $("#card3d").removeClass('abrir');
        }

        //$('#card3d .conteudo .titulo').html(dados_novos['info-titulo']);
        //$('#card3d .conteudo .texto').html(dados_novos['info-texto']);
    },
    atualizaDados: function (alldata) {
        Object.keys(alldata).forEach(index => {
            var arr = alldata[index];
            var alvo = $("#card3d-"+index.toLowerCase());
            var resumo = [];
            var total = 0;
            var maximo = 0;
            var pos = 1;
            arr.forEach( item => {

                if(index == 'TEMPOS') {
                    var valor = parseInt(item['TOTAL_PESAGENS']);
                    var valorStr = valor + '<br>Kg';
                } else {
                    var valorStr = item['TOTAL'];
                    var valor = parseInt(item['TOTAL']);
                }

                if (valor > maximo) maximo = valor;
                total += valor;

                resumo.push({
                    descricao: item[(Object.keys(item)[0])],
                    valor: valor,
                    valorStr: valorStr,
                    cor: item['COR_GC_HEX'] || null,
                });
            })

            if( index != 'TEMPOS') {
                resumo.sort((a, b) => { return a.valor - b.valor; });
            }

            alvo.find('.bar').addClass('disabled');
            resumo.forEach( item => {
                item.valor_escalado = 1+(item.valor/(maximo/99));
                var barra = alvo.find('.bar'+pos);
                barra.html('<span>' + item.descricao + '</span><em>' + item.valorStr +'</em>');
                barra.removeClass();
                barra.addClass('bar');
                barra.addClass('bar' + pos);
                barra.addClass('barsize-' + parseInt(item.valor_escalado));
                pos++;
            });

            if (index == 'TEMPOS') {
                alvo.find('h3').html(total + ' Kg');
            } else {
                alvo.find('h3').html(total);
            }
            //console.log(alvo, resumo);
        });
    }

}

$card3d = new Card3d();

