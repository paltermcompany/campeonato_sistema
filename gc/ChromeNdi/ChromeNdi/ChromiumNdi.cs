﻿using CefSharp;
using CefSharp.OffScreen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChromeNdi
{
    public partial class ChromiumNdi : Form
    {
        private long lastFrameNumber = 0;
        private long lastCallTime = 0;
        private ChromiumWebBrowser browser = null;
        private NDIRenderer renderer = null;
        string address = "http://127.0.0.1/gc";

        public ChromiumNdi()
        {
            InitializeComponent();

            this.toolStripStatusLabel1.Text = "Inicializando";
        }

        public void setBrowser(ChromiumWebBrowser browser)
        {
            this.toolStripStatusLabel1.Text = "Inicializado";
            this.browser = browser;
        }
        public void setRenderer(NDIRenderer renderer)
        {
            this.renderer = renderer;
        }

        public void onInicializouBrowser()
        {
            this.toolStripStatusLabel1.Text = "Endereço: " + this.address;
            
        }

        private void SairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            

        }

        private void RecarregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (browser != null)
            {
                browser.Load(browser.Address);
            }
        }

        private void TimerUi_Tick(object sender, EventArgs e)
        {
            if (renderer != null)
            {
                this.lblFrameCounter.Text = "Frame: " + renderer.currentFrame;
            }
        }

        private void AbrirDevToolsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            browser?.ShowDevTools();
        }
    }
}
