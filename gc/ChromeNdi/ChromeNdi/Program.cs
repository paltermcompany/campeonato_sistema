// Copyright © 2014 The CefSharp Authors. All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.

using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.OffScreen;
using CefSharp.Structs;
//using CefSharp.Wpf;

using Size = System.Drawing.Size;

namespace ChromeNdi
{
    static class Program
    {
        private const string TestUrl = "http://127.0.0.1/gc";
        private static NDIRenderer renderer = null;
        private static ChromiumNdi chromiumNdi = null;
        [STAThread]
        static void Main()
        {

            chromiumNdi = new ChromiumNdi();
            // You need to replace this with your own call to Cef.Initialize();
            Init(new CefSettings(), browserProcessHandler: new BrowserProcessHandler());
            //MainAsync();
            new Thread(new ThreadStart(MainAsync)).Start();
            //Demo showing Zoom Level of 3.0
            //Using seperate request contexts allows the urls from the same domain to have independent zoom levels
            //otherwise they would be the same - default behaviour of Chromium
            //MainAsync("cache\\path2", 3.0);

            // We have to wait for something, otherwise the process will exit too soon.
            Application.Run( chromiumNdi );

            // Clean up Chromium objects.  You need to call this in your application otherwise
            // you will get a crash when closing.            
            Cef.Shutdown();
            renderer?.StopNDI();
            //Success
            return;
        }



        //TODO: Revert after https://bitbucket.org/chromiumembedded/cef/issues/2685/networkservice-custom-scheme-unable-to
        //has been fixed.
        // Use when debugging the actual SubProcess, to make breakpoints etc. inside that project work.
        private static readonly bool DebuggingSubProcess = Debugger.IsAttached;

        public static void Init(AbstractCefSettings settings, IBrowserProcessHandler browserProcessHandler)
        {
            //settings.RemoteDebuggingPort = 8088;
            //The location where cache data will be stored on disk. If empty an in-memory cache will be used for some features and a temporary disk cache for others.
            //HTML5 databases such as localStorage will only persist across sessions if a cache path is specified. 
            //settings.CachePath = "cache";
            //settings.UserAgent = "CefSharp Browser" + Cef.CefSharpVersion; // Example User Agent
            //settings.CefCommandLineArgs.Add("renderer-process-limit", "1");
            //settings.CefCommandLineArgs.Add("renderer-startup-dialog");
            //settings.CefCommandLineArgs.Add("enable-media-stream"); //Enable WebRTC
            //settings.CefCommandLineArgs.Add("no-proxy-server"); //Don't use a proxy server, always make direct connections. Overrides any other proxy server flags that are passed.
            //settings.CefCommandLineArgs.Add("debug-plugin-loading"); //Dumps extra logging about plugin loading to the log file.
            settings.CefCommandLineArgs.Add("disable-plugins-discovery", "1"); //Disable discovering third-party plugins. Effectively loading only ones shipped with the browser plus third-party ones as specified by --extra-plugin-dir and --load-plugin switches
            //settings.CefCommandLineArgs.Add("enable-system-flash"); //Automatically discovered and load a system-wide installation of Pepper Flash.
            //settings.CefCommandLineArgs.Add("allow-running-insecure-content"); //By default, an https page cannot run JavaScript, CSS or plugins from http URLs. This provides an override to get the old insecure behavior. Only available in 47 and above.

            //settings.CefCommandLineArgs.Add("enable-logging"); //Enable Logging for the Renderer process (will open with a cmd prompt and output debug messages - use in conjunction with setting LogSeverity = LogSeverity.Verbose;)
            //settings.LogSeverity = LogSeverity.Verbose; // Needed for enable-logging to output messages

            settings.CefCommandLineArgs.Add("disable-extensions", "1"); //Extension support can be disabled
                                                                        //settings.CefCommandLineArgs.Add("disable-pdf-extension", "1"); //The PDF extension specifically can be disabled

            if (settings.WindowlessRenderingEnabled)
            {
                settings.CefCommandLineArgs.Add("disable-renderer-accessibility", "1");
                settings.CefCommandLineArgs.Add("off-screen-rendering-enabled", "1");
                //settings.CefCommandLineArgs.Add("off-screen-frame-rate", "30");
                //settings.CefCommandLineArgs.Add("disable-gpu", "1");
                settings.CefCommandLineArgs.Add("disable-gpu-compositing", "1");
                settings.CefCommandLineArgs.Add("disable-gpu-vsync", "1");
                settings.CefCommandLineArgs.Add("enable-begin-frame-scheduling", "1");
                //settings.CefCommandLineArgs.Add("show-fps-counter", "1");
                
                //settings.CefCommandLineArgs.Add("enable-media-stream", "1");
                settings.UncaughtExceptionStackSize = 10;
            
            // Off Screen rendering (WPF/Offscreen)

                //settings.SetOffScreenRenderingBestPerformanceArgs();
                //settings.CefCommandLineArgs.Add("disable-gpu","1");
                //settings.CefCommandLineArgs.Add("disable-gpu-vsync", "1"); //Disable Vsync


                //Disable Direct Composition to test https://github.com/cefsharp/CefSharp/issues/1634
                //settings.CefCommandLineArgs.Add("disable-direct-composition", "1");

                // DevTools doesn't seem to be working when this is enabled
                // http://magpcss.org/ceforum/viewtopic.php?f=6&t=14095
                //settings.CefCommandLineArgs.Add("enable-begin-frame-scheduling", "1");
            }


            //Exit the subprocess if the parent process happens to close
            //This is optional at the moment
            //https://github.com/cefsharp/CefSharp/pull/2375/
            CefSharpSettings.SubprocessExitIfParentProcessClosed = true;


            if (!Cef.Initialize(settings, performDependencyCheck: !DebuggingSubProcess, browserProcessHandler: browserProcessHandler))
            {
                throw new Exception("Unable to Initialize Cef");
            }

            //Cef.AddCrossOriginWhitelistEntry(BaseUrl, "https", "cefsharp.com", false);
        }


        private static async void MainAsync()
        {
            int bwWidth = 1920;
            int bwHeight = 1080;

            var browserSettings = new BrowserSettings();
            //Reduce rendering speed to one frame per second so it's easier to take screen shots
            browserSettings.WindowlessFrameRate = 60;
            //browserSettings.BackgroundColor = 0x00FFFF00;
            var requestContextSettings = new RequestContextSettings();

            // RequestContext can be shared between browser instances and allows for custom settings
            // e.g. CachePath
            var requestContext = new RequestContext(requestContextSettings);
            var browser = new ChromiumWebBrowser(TestUrl, browserSettings, requestContext);
            
            browser.Size = new Size(bwWidth, bwHeight);
            renderer = new NDIRenderer(browser);
            browser.RenderHandler = renderer;
            chromiumNdi.setBrowser(browser);
            chromiumNdi.setRenderer(renderer);
            
            await LoadPageAsync(browser);
            
        }

        private static void renderhandler_onpaint(PaintElementType arg1, Rect arg2, IntPtr arg3, int arg4, int arg5)
        {
            throw new NotImplementedException();
        }

        public static Task LoadPageAsync(IWebBrowser browser, string address = null)
        {
            var tcs = new TaskCompletionSource<bool>(TaskCreationOptions.RunContinuationsAsynchronously);
            
            EventHandler<LoadingStateChangedEventArgs> handler = null;
            handler = (sender, args) =>
            {
                //Wait for while page to finish loading not just the first frame
                if (!args.IsLoading)
                {
                    browser.LoadingStateChanged -= handler;
                    //Important that the continuation runs async using TaskCreationOptions.RunContinuationsAsynchronously
                    tcs.TrySetResult(true);
                }
            };

            browser.LoadingStateChanged += handler;

            if (!string.IsNullOrEmpty(address))
            {
                browser.Load(address);
            }
            return tcs.Task;
        }

        
        private static void DisplayBitmap(Task<Bitmap> task)
        {
            
            // Make a file to save it to (e.g. C:\Users\jan\Desktop\CefSharp screenshot.png)
            var screenshotPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "CefSharp screenshot" + DateTime.Now.Ticks + ".png");

            Console.WriteLine();
            Console.WriteLine("Screenshot ready. Saving to {0}", screenshotPath);

            var bitmap = task.Result;

            // Save the Bitmap to the path.
            // The image type is auto-detected via the ".png" extension.
            bitmap.Save(screenshotPath);

            // We no longer need the Bitmap.
            // Dispose it to avoid keeping the memory alive.  Especially important in 32-bit applications.
            bitmap.Dispose();

            Console.WriteLine("Screenshot saved.  Launching your default image viewer...");

            // Tell Windows to launch the saved image.
            Process.Start(screenshotPath);

            Console.WriteLine("Image viewer launched.  Press any key to exit.");
        }

    }
}
