﻿namespace ChromeNdi
{
    partial class ChromiumNdi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChromiumNdi));
            this.lblFrameCounter = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.recarregarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirDevToolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timerUi = new System.Windows.Forms.Timer(this.components);
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblFrameCounter
            // 
            this.lblFrameCounter.AutoSize = true;
            this.lblFrameCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrameCounter.ForeColor = System.Drawing.Color.LightGray;
            this.lblFrameCounter.Location = new System.Drawing.Point(12, 40);
            this.lblFrameCounter.Name = "lblFrameCounter";
            this.lblFrameCounter.Size = new System.Drawing.Size(177, 31);
            this.lblFrameCounter.TabIndex = 1;
            this.lblFrameCounter.Text = "Inicializando";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 91);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(246, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.recarregarToolStripMenuItem,
            this.abrirDevToolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(246, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // recarregarToolStripMenuItem
            // 
            this.recarregarToolStripMenuItem.Name = "recarregarToolStripMenuItem";
            this.recarregarToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.recarregarToolStripMenuItem.Text = "Recarregar";
            this.recarregarToolStripMenuItem.Click += new System.EventHandler(this.RecarregarToolStripMenuItem_Click);
            // 
            // abrirDevToolsToolStripMenuItem
            // 
            this.abrirDevToolsToolStripMenuItem.Name = "abrirDevToolsToolStripMenuItem";
            this.abrirDevToolsToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.abrirDevToolsToolStripMenuItem.Text = "Abrir DevTools";
            this.abrirDevToolsToolStripMenuItem.Click += new System.EventHandler(this.AbrirDevToolsToolStripMenuItem_Click);
            // 
            // timerUi
            // 
            this.timerUi.Enabled = true;
            this.timerUi.Tick += new System.EventHandler(this.TimerUi_Tick);
            // 
            // ChromiumNdi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(246, 113);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.lblFrameCounter);
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ChromiumNdi";
            this.Text = "ChromiumNdi";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblFrameCounter;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem recarregarToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Timer timerUi;
        private System.Windows.Forms.ToolStripMenuItem abrirDevToolsToolStripMenuItem;
    }
}