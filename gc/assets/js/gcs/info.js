$info = null;
function Info(params) {
  if ($info) {
    return $info;
  }
  this.loaded = false;
  $info = this;
  this.id = "info";
}

Info.prototype = {
  constructor: Info,

  init: function () {
    if (this.loaded) return;
    this.loaded = true;
  },

  mostrar: function () {
    $("#info").removeClass("escondido");
    $("#info").removeClass("fechar");
  },

  fechar: function () {
    if ($("#info").hasClass("escondido")) return;
    $("#info").addClass("escondido");
    $("#info").offset();
    $("#info").removeClass("escondido");
    $("#info").addClass("fechar");
    setTimeout(function () {
      $("#info").addClass("escondido");
    }, 1500);
  },

  remover: function () {
    $("#info").addClass("fechar");
    $("#info").addClass("escondido");
  },

  preenche: function (dados_novos) {
    console.log(dados_novos);
    $("#info .conteudo .nome").html(dados_novos["info-titulo"]);
    $("#info .conteudo .legenda").html(dados_novos["info-texto"]);
  },
};

$info = new Info();
