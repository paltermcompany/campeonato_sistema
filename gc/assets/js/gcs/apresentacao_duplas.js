$apresentacao_duplas = null;
function Apresentacao_duplas ( params ) {
    if($apresentacao_duplas){
        return $apresentacao_duplas;
    }
    this.loaded          = false;
    $apresentacao_duplas = this;
    this.id              = 'apresentacao_duplas';
    this.num_slide       = 0;
    this.resultados = {
        // 'RS': [
        //     {
        //         // Aqui adiciona a classe do logo regional, exemplo logo-gaucho
        //         logo: ''
        //     },
        //     {
        //         competidor1: 'Alexsandro',
        //         competidor2: 'Claudio',
        //         patrocinador: 'buffalo-motores',
        //     },
        //     {
        //         competidor1: 'André',
        //         competidor2: 'João',
        //         patrocinador: 'cloter-rytos',
        //     },
        //     {
        //         competidor1: 'Cleber',
        //         competidor2: 'Halisson',
        //         patrocinador: 'combat-fishing',
        //     },
        //     {
        //         competidor1: 'DUPLA 4',
        //         competidor2: 'DUPLA 4',
        //         patrocinador: 'faca-na-rede',
        //     },
        //     {
        //         competidor1: 'DUPLA 5',
        //         competidor2: 'DUPLA 5',
        //         patrocinador: 'rf-equipamentos',
        //     },
        //     {
        //         competidor1: 'DUPLA 6',
        //         competidor2: 'DUPLA 6',
        //         patrocinador: 'latittude',
        //     },
        // ],
        'SC': [
            {
                logo: ''
            },
            {
                competidor1: 'JOEL',
                competidor2: 'JOEL JUNIOR',
                patrocinador: 'ebf',
            },
            {
                competidor1: 'JULIANO',
                competidor2: 'MARCOS',
                patrocinador: 'bigua',
            },
            {
                competidor1: 'JOSÉ MARQUES',
                competidor2: 'RODRIGO',
                patrocinador: 'silicones-paulista',
            },
            {
                competidor1: 'ALEXNALDO',
                competidor2: 'JOSÉ SILVA',
                patrocinador: 'security',
            },
            {
                competidor1: 'BENEDETTI',
                competidor2: 'PAULINHO',
                patrocinador: 'silicones-paulista',
            },
            {
                competidor1: 'DIDI',
                competidor2: 'EDUARDO',
                patrocinador: 'lifek',
            },
        ],
        'PR': [
            {
                logo: ''
            },
            {
                competidor1: 'ALDO',
                competidor2: 'SCHMOELLER',
                patrocinador: 'hidea',
            },
            {
                competidor1: 'DAIANE',
                competidor2: 'RAFAEL',
                patrocinador: 'hidea',
            },
            {
                competidor1: 'JEFERSON',
                competidor2: 'RICARDO',
                patrocinador: 'bigua',
            },
            {
                competidor1: 'ANDRÉ',
                competidor2: 'JOÃO GRANDO',
                patrocinador: 'haxea',
            },
            {
                competidor1: 'INESIO',
                competidor2: 'SANDRO',
                patrocinador: 'acezo',
            },
            {
                competidor1: 'ELISANDRA',
                competidor2: 'RODRIGO',
                patrocinador: 'security',
            },
        ],
        'MG': [
            {
                logo: ''
            },
            {
                competidor1: 'NONI',
                competidor2: 'TATOS',
                patrocinador: 'albatroz',
            },
            {
                competidor1: 'FABIANO',
                competidor2: 'JOÃO VITOR',
                patrocinador: 'zeeo',
            },
            {
                competidor1: 'MARCÃO',
                competidor2: 'GLAUTER',
                patrocinador: 'lumis',
            },
            {
                competidor1: 'CLEBER',
                competidor2: 'HALISON',
                patrocinador: 'maruri',
            },
            {
                competidor1: 'INÁCIO',
                competidor2: 'JEDEAN',
                patrocinador: 'cloter',
            },
            {
                competidor1: 'ROGÉRIO',
                competidor2: 'JULIANA',
                patrocinador: 'all-fishing',
            },
        ],
        'GO': [
            {
                logo: ''
            },
            {
                competidor1: 'CARLOS',
                competidor2: 'MARCELO',
                patrocinador: 'maruri',
            },
            {
                competidor1: 'TEREZA',
                competidor2: 'THIAGO',
                patrocinador: 'lumis',
            },
            {
                competidor1: 'ADAN',
                competidor2: 'MÁRCIO',
                patrocinador: 'boias-barao',
            },
            {
                competidor1: 'ANDERSON',
                competidor2: 'KAMILA',
                patrocinador: 'albatroz',
            },
            {
                competidor1: 'DIVINO',
                competidor2: 'EMANUEL',
                patrocinador: 'maruri',
            },
            {
                competidor1: 'ANDRÉ',
                competidor2: 'EDSON',
                patrocinador: 'ebf',
            },
        ],
        'SP': [
            {
                logo: ''
            },
            {
                competidor1: 'JOÃO BATISTA',
                competidor2: 'LETÍCIA',
                patrocinador: 'cloter',
            },
            {
                competidor1: 'DANILO',
                competidor2: 'RYAN',
                patrocinador: 'silicones-paulista',
            },
            {
                competidor1: 'JOÃO BOSCO',
                competidor2: 'RAFAEL',
                patrocinador: 'cloter',
            },
            {
                competidor1: 'ALEX JUNIOR',
                competidor2: 'CLAUDINHO',
                patrocinador: 'acezo',
            },
            {
                competidor1: 'LUKINHAS',
                competidor2: 'WAGNER',
                patrocinador: 'haxea',
            },
            {
                competidor1: 'MÁRCIO',
                competidor2: 'PATRICIA',
                patrocinador: 'haxea',
            },
            {
                competidor1: 'FÁBIO',
                competidor2: 'JONAS',
                patrocinador: 'acezo',
            },
            {
                competidor1: 'LEO',
                competidor2: 'YURI ARAUJO',
                patrocinador: 'jem',
            },
            {
                competidor1: 'JÚLIO',
                competidor2: 'PEDRINHO',
                patrocinador: 'all-fishing',
            },
            {
                competidor1: 'ARRUDA',
                competidor2: 'REI COSTA',
                patrocinador: 'jem',
            },
            {
                competidor1: 'NEY',
                competidor2: 'JUNINHO',
                patrocinador: 'lifek',
            },
            {
                competidor1: 'DIEGO',
                competidor2: 'EDUARDO LEITE',
                patrocinador: 'lifek',
            },
            {
                competidor1: 'JOÃO FARIAS',
                competidor2: 'ROGERIO',
                patrocinador: 'albatroz',
            },
            {
                competidor1: 'MARCEL',
                competidor2: 'TIAGO',
                patrocinador: 'lumis',
            },
            {
                competidor1: 'FILLIPE',
                competidor2: 'VICTOR',
                patrocinador: 'boias-barao',
            },
        ],
    };
    // this.estado='RS';
    this.estado='SC';
    this.etapa=1;
}

Apresentacao_duplas.prototype = {
    constructor: Apresentacao_duplas,

    init:function() {
        if(this.loaded) return;
        this.loaded = true;
    },

    mostrar:function( ) {
        $apresentacao_duplas.atualiza();
        $('#apresentacao_duplas').removeClass('escondido');
        $('#apresentacao_duplas').removeClass('fechar');
    },

    fechar: function() {
        $('#apresentacao_duplas').addClass('fechar');
        setTimeout(function(){
            $('#apresentacao_duplas').addClass('escondido');
        }, 1000);
    },

    remover: function() {
        $('#apresentacao_duplas').addClass('fechar');
        $('#apresentacao_duplas').addClass('escondido');
    },

    preenche: function (etapa) {
        console.log(etapa);
        var posicao = parseInt(etapa['id_apresentacao_duplas']);

        switch(posicao)
        {
            // case 0:  etapa=1; estado='RS'; break;
            // case 1:  etapa=2; estado='RS'; break;
            // case 2:  etapa=1; estado='SC'; break;
            // case 3:  etapa=2; estado='SC'; break;
            // case 4:  etapa=1; estado='PR'; break;
            // case 5:  etapa=2; estado='PR'; break;
            // case 6:  etapa=1; estado='MG'; break;
            // case 7:  etapa=2; estado='MG'; break;
            // case 8:  etapa=1; estado='GO'; break;
            // case 9:  etapa=2; estado='GO'; break;
            // case 10: etapa=1; estado='SP'; break;
            // case 11: etapa=2; estado='SP'; break;
            // case 12: etapa=3; estado='SP'; break;
            // case 13: etapa=4; estado='SP'; break;

            case 0:  etapa=1; estado='SC'; break;
            case 1:  etapa=2; estado='SC'; break;
            case 2:  etapa=1; estado='PR'; break;
            case 3:  etapa=2; estado='PR'; break;
            case 4:  etapa=1; estado='MG'; break;
            case 5:  etapa=2; estado='MG'; break;
            case 6:  etapa=1; estado='GO'; break;
            case 7:  etapa=2; estado='GO'; break;
            case 8:  etapa=1; estado='SP'; break;
            case 9:  etapa=2; estado='SP'; break;
            case 10: etapa=3; estado='SP'; break;
            case 11: etapa=4; estado='SP'; break;
            case 12: etapa=5; estado='SP'; break;
        }

        $apresentacao_duplas.estado = estado;
        $apresentacao_duplas.etapa = etapa;
        $apresentacao_duplas.atualiza();
    },
    atualiza: function(){
        var offset = ($apresentacao_duplas.etapa-1)*3;
        var estado = $apresentacao_duplas.estado;
        var logo = $apresentacao_duplas.resultados[estado][0].logo;
        console.log(logo);
        $('#apresentacao_duplas .apresentacao-header .logo').removeClass().addClass('logo').addClass(logo);

        $('#apresentacao_duplas .posicao[data-posicao-final]').each( function ( i, el ) {
            if( i < 3) {
                var el_dupla = $(el);
                var pos = el_dupla.data('posicaoFinal') + offset;
                var patrocinador = $apresentacao_duplas.resultados[estado][pos].patrocinador;
                var resultado = $apresentacao_duplas.resultados[estado][pos];
                var nomes = resultado.competidor1 + '<br>'+resultado.competidor2;

                var bg    = 'url(assets/img/apresentacao-duplas/'+estado+'/'+pos+'-mascara.png) top center no-repeat';
                var fotos = 'url(assets/img/apresentacao-duplas/'+estado+'/'+pos+'.png) top center no-repeat';
                var patrocinadorUrl = 'url(assets/img/apresentacao-duplas/patrocinadores/'+patrocinador+'.png)';

                el_dupla.find('.nomes').html( nomes );
                el_dupla.find('.foto-bg').css('background', bg );
                el_dupla.find('.foto-png').css('background', fotos );
                el_dupla.find('.logo').css('background-image', patrocinadorUrl);
            }
        });

        $('#apresentacao_duplas .apresentacao-header .etapa .nome').html(etapa_hoje.DESCRICAO);
        $('#apresentacao_duplas .apresentacao-header .etapa .local').html('CBP4');
    },
}

$apresentacao_duplas = new Apresentacao_duplas();
