$apresentadores = null;
function Apresentadores(params) {
  if ($apresentadores) {
    return $apresentadores;
  }
  this.loaded = false;
  $apresentadores = this;
  this.id = "apresentadores";
}

Apresentadores.prototype = {
  constructor: Apresentadores,

  init: function () {
    if (this.loaded) return;
    this.loaded = true;
  },

  mostrar: function (posicao = null) {
    $("#apresentadores").removeClass("fechar");
    $("#apresentadores").removeClass("escondido");
  },

  fechar: function () {
    if ($("#apresentadores").hasClass("escondido")) return;
    $("#apresentadores").addClass("escondido");
    $("#apresentadores").offset();
    $("#apresentadores").removeClass("escondido");
    $("#apresentadores").addClass("fechar");
    setTimeout(function () {
      $("#apresentadores").addClass("escondido");
    }, 1500);
  },

  remover: function () {
    $("#apresentadores").addClass("fechar");
    $("#apresentadores").addClass("escondido");
  },

  preenche: function (dados_novos) {
    $("#apresentadores .conteudo .nome").html(
      dados_novos["apresentador-titulo"]
    );
    $("#apresentadores .conteudo .legenda").html(
      dados_novos["apresentador-texto"]
    );
  },
};

$apresentadores = new Apresentadores();
