$clima = null;
function Clima(params) {
  if ($clima) {
    return $clima;
  }
  this.loaded = false;
  $clima = this;
  this.id = "clima";
  this.trocas = null;
  this.dados = [];
  this.num_slide = 0;
}

Clima.prototype = {
  constructor: Clima,

  init: function () {
    if (this.loaded) return;
    this.loaded = true;
    // this.atualizaDados();
  },

  mostrar: function () {
    $("#clima").removeClass("escondido");
    $("#clima").removeClass("fechar");
    $clima.num_slide = 0;
    $clima.atualizaDados();
    $clima.timeoutTroca();
  },

  timeoutTroca() {
    if ($clima.trocas != null) {
      clearTimeout($clima.trocas);
    }
  },

  fechar: function () {
    if ($("#clima").hasClass("escondido")) return;
    $("#clima").addClass("escondido");
    $("#clima").removeClass("fechar");
    $("#clima").removeClass("trocar");
    $("#clima").offset();
    $("#clima").removeClass("escondido");
    $("#clima").addClass("fechar");
    if ($clima.trocas) clearInterval($clima.trocas);
    $clima.trocas = null;
    setTimeout(function () {
      $("#clima").addClass("escondido");
    }, 400);
  },

  remover: function () {
    if ($clima.trocas) clearInterval($clima.trocas);
    $clima.trocas = null;
    $("#clima").addClass("fechar");
    $("#clima").addClass("escondido");
  },

  atualizaDados() {
    $dia = "DIA";
		$icon = "sunny";
    switch ($clima.dados["clima-dia"]) {
      case "sol":
        $dia = "ENSOLARADO";
				$icon = "sunny";
        break;
      case "nublado":
        $dia = "NUBLADO";
				$icon = "cloudy";
        break;
      case "chuva":
        $dia = "CHUVA";
				$icon = "rainy";
        break;
      case "chuva-forte-com-raio":
        $dia = "CHUVA COM RAIOS";
				$icon = "thunderstorm";
        break;
    }

    $("#clima-descricao").html($dia);
    $("#clima-info-temperatura .texto").html($clima.dados["clima-temperatura"] + " ºC");
    $("#clima-info-pressao-barimetrica .texto").html($clima.dados["clima-pressao-barimetrica"] + " atm");
    $("#clima-info-velocidade-vento .texto").html($clima.dados["clima-velocidade-vento"] + " km/h");

    $("#clima-icone").html($icon);

		$('#agua-descricao').html($clima.dados['agua-visibilidade']);
		$('#agua-info-temperatura .texto').html($clima.dados['agua-temperatura-agua']+" ºC");
		$('#agua-info-oxigenio .texto').html('Oxigênio '+$clima.dados['agua-oxigenio']);
		$('#agua-info-ph .texto').html('pH ' + $clima.dados['agua-ph']);
		$('#agua-info-amonia .texto').html('Amônia ' + $clima.dados['agua-amonia']);

		console.log($clima.dados);
  },
  preenche: function (dados_novos) {
    $clima.dados = dados_novos;
    $clima.atualizaDados();
  },
};

$clima = new Clima();
