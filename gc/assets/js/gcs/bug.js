$bug = null;

function Bug ( params ) {
    if($bug){
        return $bug;
    }
    this.loaded = false;
    $bug       = this;
    this.id     = 'bug';
}

Bug.prototype = {
    constructor: Bug,

    init:function() {
        if(this.loaded) return;
        this.loaded = true;
    },

    mostrar:function() {
        $('#bug').removeClass('escondido');
        $('#bug').removeClass('fechar');
    },

    fechar: function() {
        $('#bug').addClass('fechar');
        $('#bug').addClass('escondido');
    },

    remover: function() {
        $('#bug').addClass('fechar');
        $('#bug').addClass('escondido');
    }

}

$bug = new Bug();
