$dados_equipe = null;
function Dados_equipe(params) {
  if ($dados_equipe) {
    return $dados_equipe;
  }
  this.loaded = false;
  $dados_equipe = this;
  this.id = "dados_equipe";
  this.num_slide = 0;
  this.id_equipe = null;
}

Dados_equipe.prototype = {
  constructor: Dados_equipe,

  init: function () {
    if (this.loaded) return;
    this.loaded = true;
  },

  mostrar: function (equipe = null) {
    if (equipe) this.preenche(equipe);

    $("#dados_equipe").removeClass("escondido");
    $("#dados_equipe").removeClass("fechar");
  },

  fechar: function () {
    if ($("#dados_equipe").hasClass("escondido")) return;
    $("#dados_equipe").addClass("escondido");
    $("#dados_equipe").offset();
    $("#dados_equipe").removeClass("escondido");
    $("#dados_equipe").addClass("fechar");
    setTimeout(function () {
      $("#dados_equipe").addClass("escondido");
    }, 1500);
  },

  remover: function () {
    $("#dados_equipe").addClass("fechar");
    $("#dados_equipe").addClass("escondido");
  },

  preenche: function (dados_novos) {
    $dados_equipe.id_equipe = dados_novos["dados_equipe-equipe"];
    $dados_equipe.atualiza();
  },
  atualiza: function () {
    if ($dados_equipe.id_equipe == null) return;

    var id_equipe = $dados_equipe.id_equipe;
    var equipe = equipes_recebido[id_equipe];
    // $('#dados_equipe .conteudo .texto').html( equipe.nomes_completos );
    $("#dados_equipe .conteudo .nome").html(equipe.nomes_completos);

    var equipe_ranking =
      ranking_indexado != null ? ranking_indexado[id_equipe] : false;
    if (equipe_ranking) {
      // $('#dados_equipe .conteudo p.peso').html(  equipe_ranking.total );
      $("#dados_equipe .conteudo .peso b").html(equipe_ranking.total);
      // $('#dados_equipe .conteudo .posicao').html( equipe_ranking.posicao );
      $("#dados_equipe .conteudo .num b").html(equipe_ranking.posicao);

      // if (equipe_ranking.posicao >= 10) {
      //   $("#dados_equipe .conteudo .bolinha").css("right", "3px");
      // } else {
      //   $("#dados_equipe .conteudo .bolinha").css("right", "6px");
      // }
    } else {
      // $('#dados_equipe .conteudo p.peso').html(  '------ Kg' );
      $("#dados_equipe .conteudo .peso b").html("------ Kg");
      // $('#dados_equipe .conteudo .posicao').html( '' );
      $("#dados_equipe .conteudo .num b").html("");
      // $('#dados_equipe .conteudo .bolinha').css('right','-100px');
    }
    //$('#info .conteudo .texto').html(dados_novos['info-texto']);
  },
};

dados_equipe = new Dados_equipe();
