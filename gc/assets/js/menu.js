$menu          = null;
function Menu ( params ) {
    if($menu) {
        return $menu;
    }
    this.loaded    = false;
    $menu          = this;
    this.id        = 'menu';
    this.num_slide = 0;

    this.timer = null;
}

Menu.prototype = {
    constructor: Menu,
    init:function() {
        if(this.loaded) return;
        this.loaded = true;
        if(config.producao || isPreview == true){
            localStorage.setItem( "modoDev", 'off' );
            localStorage.setItem( "mostra_timer", '0' );
            localStorage.setItem( "fundo_escuro", '0' );
            $('#menu').css('display','none');
            $('#div_fundo_escuro').css('display','none');
            $('#safe-areas').css('display','none');
        } else {
            this.events();
        }
    },

    events:function () {
        $('#menu a').on('click', function (e) {
            e.preventDefault();
            $(this).parent().toggleClass('aberto');
        });

        $('#menu button').on('click', function (e) {
            e.preventDefault();
            var acao = $(this).data('acao');
            var gc   = $(this).data('gc');

            if(acao){
                switch(acao) {
                    case 'modoDev':
                        var modoDev = localStorage.getItem("modoDev");
                        console.info('modoDev', modoDev);
                        localStorage.setItem( "modoDev", (modoDev == 'off' || !modoDev) ? "on" : "off" );
                        $menu.modoDev();
                        $menu.gc();
                    break;
                    case 'regional':
                        var selecionado = $(this).data('regional');
                        var regional    = localStorage.getItem("regional");
                        localStorage.setItem( "regional", (regional == selecionado) ? "" : selecionado );
                        $menu.regional(selecionado);
                        $menu.gc();
                    break;
                }
            }else if( gc ){
                var posicao = $(this).data('posicao');

                if(posicao){
                    var _gc = localStorage.getItem("gc-"+gc+"|"+posicao);
                    for( i in localStorage ){
                        if( i.indexOf( gc ) > -1 ){
                            localStorage.removeItem(i);

                            var posicao_selecionada = i.split('|');
                            if(posicao_selecionada.length > 1){
                                var selecionado     = posicao_selecionada[0].replace('gc-','');
                                posicao_selecionada = posicao_selecionada[1];
                                $('button[data-gc="'+selecionado+'"][data-posicao="'+posicao_selecionada+'"]').removeClass('ativo');
                            }
                        }
                    }
                }else{
                    var _gc = localStorage.getItem("gc-"+gc);

                }

                localStorage.setItem("gc-"+gc+( posicao ? '|'+posicao : '' ), (_gc == 'off' || !_gc) ? "on" : "off");

                console.info( posicao );
                $('#'+gc).attr('data-posicao', posicao);
                $menu.gc();
            }
        });

        var timer = localStorage.getItem("mostra_timer");
        if(timer == 1) $('#mostra_timer').prop('checked', true);

        $('#mostra_timer').on('change', function () {
            console.info('mostra_timer');
            var selecionado  = $('#mostra_timer').is(':checked') ? 1 : 0;
            localStorage.setItem( "mostra_timer", selecionado );
        })

        $('#safe_area').on('change', function () {
            console.info('safe_area');
            var selecionado  = $('#safe_area').is(':checked') ? 1 : 0;
            localStorage.setItem( "safe_area", selecionado );
            $('#safe-areas').css('display', selecionado == 1 ? 'block' : 'none');
        })

        var fundo_escuro = localStorage.getItem("fundo_escuro");
        if(fundo_escuro == 1) $('#fundo_escuro').prop('checked', true);

        $('#fundo_escuro').on('change', function () {
            console.info('fundo_escuro');
            var selecionado  = $('#fundo_escuro').is(':checked') ? 1 : 0;
            localStorage.setItem( "fundo_escuro", selecionado );
         })

        $menu.modoDev();
        $menu.gc();
        $menu.regional();
    },
    regional:function( selecionado ){
        var regional    = localStorage.getItem("regional");
        var selecionado = $('#menu button[data-regional].ativo').data('regional');
        $('body').removeClass(selecionado);
        $('#menu button[data-regional]').attr('class','');

        if(regional){
            $('#menu button[data-regional="'+regional+'"]').attr('class','ativo');
            $('body').addClass(regional);
        }
    },
    gc:function(){

        clearInterval($menu.timer);
        $('#timer .loader').css('width', '1px');
        $('#timer .time').text('0ms');

        if($('#mostra_timer').is(':checked')){
            $menu.timer = setInterval(function() {
                var w = Math.round(parseFloat($('#timer .loader').width()+1));
                $('#timer .loader').css('width', w+'px');
                $('#timer .time').text(w*100+'ms');
            }, 100);
        }

        if($('#fundo_escuro').is(':checked')){
            $('#div_fundo_escuro').show();
        } else {
            $('#div_fundo_escuro').hide();
        }

        $.each(localStorage, function(k,v){
            if(k.substr(0,3) == 'gc-'){
                var gc      = k.substr(3);
                var posicao = gc.split('|');

                if(posicao.length > 1){
                    gc      = posicao[0];
                    posicao = posicao[1];
                }else{
                    posicao = false;
                }

                var gc_classe = eval('new '+capitalize(gc)+'()');
                if(v == 'off'){
                    // gc_classe.esconder();
                    $('#'+gc).addClass('escondido');
                    $('#menu button[data-gc="'+gc+'"]').attr('class','');
                }else{
                    gc_classe.init();
                    gc_classe.mostrar( posicao );

                    if(posicao){
                        $('#menu button[data-gc="'+gc+'"][data-posicao="'+posicao+'"]').attr('class','ativo');
                    }else{
                        $('#menu button[data-gc="'+gc+'"]').attr('class','ativo');
                    }
                }
            }
        });
    },

    modoDev:function(){
        var modoDev = localStorage.getItem("modoDev");
        console.info('modoDev:', modoDev);
        if((modoDev == 'off' || !modoDev)){
            $('#menu button[data-acao="modoDev"]').attr('class','');
            $('body').removeClass('viewport');
        }else{
            $('#menu button[data-acao="modoDev"]').attr('class','ativo');
            $('body').addClass('viewport');
        }
    }
}

$menu = new Menu();
setTimeout(function(){
    $menu.init();
}, 100);
