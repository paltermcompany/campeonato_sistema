    //"use strict";
    let globalData = null;
    var connection = null;
    var equipes_recebido = null;
    var presets_info     = [];

    function heartbeat() {
      clearTimeout(this.pingTimeout);

      // Use `WebSocket#terminate()`, which immediately destroys the connection,
      // instead of `WebSocket#close()`, which waits for the close timer.
      // Delay should be equal to the interval at which your server
      // sends out pings plus a conservative assumption of the latency.
      this.pingTimeout = setTimeout(() => {
        this.terminate();
        connection = null;
      }, 30000 + 1000);
    }
    // for better performance - to avoid searching in DOM
    // my name sent to the server
    function conecta() {
        if( connection != null ) {
            return;
        }
        var myName = 'Controlador';
        // if user is running mozilla then use it's built-in WebSocket
        window.WebSocket = window.WebSocket || window.MozWebSocket;
        // if browser doesn't support WebSocket, just show
        // some notification and exit
        if (!window.WebSocket) {
            console.log('Sorry, but your browser doesn\'t support WebSocket.');
            return;
        }
        // open connection
        connection = new WebSocket('ws://' + config.ip +':'+config.port);
        //connection.onping = heartbeat.bind(connection);
        connection.onclose = function () {
            console.log("Conexão fechada");
            connection = null;
        };

        connection.onopen = function () {
            console.log('Conexão aberta');
            //heartbeat().bind(connection);
        };

        connection.onerror = function (error) {
            // just in there were some problems with connection...
            console.log(error);
            connection = null;
        };

        // most important part - incoming messages
        connection.onmessage = function (message) {
            // try to parse JSON message. Because we know that the server
            // always returns JSON this should work without any problem but
            // we should make sure that the massage is not chunked or
            // otherwise damaged.
            try {
                var json = JSON.parse(message.data);
            } catch (e) {
                console.log('Invalid JSON: ', message.data);
                return;
            }

            // NOTE: if you're not sure about the JSON structure
            // check the server source code above
            // first response from the server with user's color
            if (json.type === 'hello') {
                connection.send(JSON.stringify({type: 'identify', data: 'touch'}));
                // from now user can start sending messages
            }  else if (json.type === 'message') { // it's a single message
                msgAtualizaBotaoGC( json.data );
            }  else if (json.type === 'etapa_hoje') { // it's a single message

            }  else if (json.type === 'atualiza_form') { // it's a single message
                recebiForm( json.data.gc, json.data.dados );
            }  else if (json.type === 'presets') { // it's a single message
                recebiPresets( json.data.gc, json.data.dados );
            }  else if (json.type === 'equipes_etapa') { // it's a single message
                recebiEquipes(json.data.resultados);
            }  else if (json.type === 'ranking') { // it's a single message

            }  else if (json.type === 'allinfo') { // it's a single message

            }  else if (json.type === 'syncCountdown') { // it's a single message
                msgSyncCountdown(json.data);
            }  else if (json.type === 'status_anterior') { // it's a single message
                msgStatusAnterior(json.data);
            }  else if(json.type === 'grafico'){
                //console.log(json.data);
            } else {
                console.log('Hmm..., I\'ve never seen JSON like this:', json);

            }
        };

        // send the message as an ordinary text
        //    connection.send(msg);
        /**
        * Add message to the chat window
        */
    }

$(function () {
    setInterval(function checkConnection() {
        if(connection == null ) {
            conecta();
        }
    }, 1000);
});


/********************************************************************************************************************
*     Recebimento de mmensagem
*/
function recebeuMensagem( type, data ) {
    console.log('Recebi mensagem de '+type);
    switch(type){
      case 'ranking_status': ranking_status( data );
    }
}


function ranking_status( data ){
  console.log(data);
  switch(data){
    case 'idle'    : $('button[data-gc="ranking"]').prop('disabled',false);  break;
    case 'animando': $('button[data-gc="ranking"]').prop('disabled','disabled'); break;
  }
}


function recebiEquipes( data = null ){
    if(data!=null) {
        var recebido = [];
        data.forEach(function( el ){
            el.nomes_completos = el.competidor1+'<br>'+el.competidor2;
            recebido[el.id_equipe] = el;
        });
        equipes_recebido = recebido;
        equipes_recebido.sort(function(a,b){ return parseInt(a.nome_equipe) < parseInt(b.nome_equipe) ? -1 : 1; });
    }
    if(!equipes_recebido){
        console.log('Sem equipes recebidas!');
        return;
    }

    var botoes = '';
    var opcoes = '';
    var n      = 1;
    equipes_recebido.forEach(function(el){
            opcoes += '<option value="'+el.id_equipe+'">'+el.nome_equipe+') '+ el.competidor1+' & '+el.competidor2+'</option>';
            //+ (n < 10 ? '0'+n : n) +
            var nome = el.nome_equipe;//+ ' '+ el.competidor1 + ' & '+ el.competidor2;
            botoes += '<button type="button" data-equipe="'+el.id_equipe+'" class="btn btn-info acionamento-interno" style="margin: 2px; font-size: 15px;">'+ nome +'</button>' ;
            n++;
    });
    document.getElementById('botoes-dados_equipe').innerHTML = botoes;
    document.getElementById('lista-equipes').innerHTML       = opcoes;
    document.getElementById('classificados_primeiro').innerHTML       = opcoes;
    document.getElementById('classificados_segundo').innerHTML       = opcoes;
    document.getElementById('classificados_terceiro').innerHTML       = opcoes;

    $('.acionamento-interno').click(function(ev){ botaoAcionamentoInterno($(ev.currentTarget)); } );
}


function recebiPresets( gc = null, data = null ){
    if(gc != 'info') {
        console.log('só os presets de info estão implementados');
        return;
    }

    if(data!=null) {
        presets_info = data;
    }
    var pos = document.getElementById('info-presets').value;
    console.log('pos: '+pos);
    var opcoes = '<option value="-1" >-- NOVO --</option>';
    var n      = 1;
    if(presets_info){
        presets_info.forEach(function(el, i){
                opcoes += '<option value="'+i+'" '+(i == pos? 'selected': '')+'>'+el.titulo+' | '+el.texto+'</option>';
        });
    }
    document.getElementById('info-presets').innerHTML = opcoes;
}
recebiPresets('info');

function enviarMensagem( destino, acao, gc, dados ){
    if(connection == null ){
        conecta();
    } else {
        connection.send(JSON.stringify({
            type:'comando_gc',
            destino: destino,
            acao: acao,
            gc: gc,
            dados: dados
        }));
    }
}

function enviarMensagemTipo( tipo, gc, dados ){
    if(connection == null ){
        conecta();
    } else {
        connection.send(JSON.stringify({
            type: tipo,
            gc: gc,
            dados: dados
        }));
    }
}

/********************************************************************************************************************
*     Botões de acionamento
*/
var conflitos = {
    "agua":                 ["apresentacao_duplas", "card3d", "classificados_manual", "classificados_final", "apresentadores","clima", "competidores","dados_equipe","dupla_campea","info","top10_full", "graficos"],
    "apresentadores":       ["apresentacao_duplas", "card3d", "classificados_manual", "classificados_final", "clima","agua","competidores","dados_equipe","dupla_campea","info","top10_full","top10_ranking", "graficos"],
    "apresentacao_duplas":  ["apresentacao_duplas", "card3d", "classificados_manual", "classificados_final", "apresentadores","clima","agua","competidores","dados_equipe","dupla_campea","info","top10_ranking", "graficos"],
    "card3d":               ["apresentacao_duplas", "card3d", "classificados_manual", "classificados_final", "apresentadores","clima","agua","competidores","dados_equipe","dupla_campea","info","top10_ranking", "top10_full", "graficos"],
    "classificados_final":  ["apresentacao_duplas", "card3d", "classificados_manual", "classificados_final", "apresentadores","clima","agua","competidores","dados_equipe","dupla_campea","info","top10_ranking", "top10_full", "graficos"],    "clima":                ["apresentacao_duplas", "classificados_final", "apresentadores","agua", "competidores","dados_equipe","dupla_campea","info","top10_full", "graficos"],
    "classificados_manual": ["apresentacao_duplas", "card3d", "classificados_manual", "classificados_final", "apresentadores","clima","agua","competidores","dados_equipe","dupla_campea","info","top10_ranking", "top10_full", "graficos"],    "clima":                ["apresentacao_duplas", "classificados_final", "apresentadores","agua", "competidores","dados_equipe","dupla_campea","info","top10_full", "graficos"],
    "bug":                  [], // **
    "clima":                ["apresentacao_duplas", "card3d", "classificados_manual", "classificados_final", "apresentadores","agua", "competidores","dados_equipe","dupla_campea","info","top10_full", "graficos"],
    "competidores":         ["apresentacao_duplas", "card3d", "classificados_manual", "classificados_final", "apresentadores","clima","agua","dados_equipe","dupla_campea","info","top10_full","top10_ranking", "graficos"],
    "dados_equipe":         ["apresentacao_duplas", "card3d", "classificados_manual", "classificados_final", "apresentadores","clima","agua","competidores","dupla_campea","info","top10_full","top10_ranking", "graficos"],
    "dupla_campea":         ["apresentacao_duplas", "card3d", "classificados_manual", "classificados_final", "apresentadores","clima","agua","competidores","dados_equipe","info","top10_full", "graficos"],
    "info":                 ["apresentacao_duplas", "card3d", "classificados_manual", "classificados_final", "apresentadores","clima","agua","competidores","dados_equipe","dupla_campea","top10_full", "graficos"],
    "top10_full":           ["apresentacao_duplas", "card3d", "classificados_manual", "classificados_final", "apresentadores","clima","agua","competidores","dados_equipe","dupla_campea","info","top10_ranking", "graficos"],
    "top10_ranking":        ["apresentacao_duplas", "card3d", "classificados_manual", "classificados_final", "apresentadores","competidores","dados_equipe","top10_full", "graficos"],
    "graficos":             ["apresentacao_duplas", "card3d", "classificados_manual", "classificados_final", "apresentadores","clima","agua","dados_equipe","dupla_campea","info","top10_full","top10_ranking", "graficos"],
};

console.info();
function conflita( aberto, novo ){
    if( aberto == novo ) return false;
    if( conflitos[aberto].indexOf( novo ) > -1 ) return true;
    return false;
}

function fechaConflitantes( local, gc_selecionado ){
    //Fecha todos os gcs abertos que são conflitantes. Essa função é legal se não rodar async
    var acionados = document.querySelectorAll('.acionamento:not([data-status=fechado])');
    for( var i = 0; i < acionados.length; i++) {
        var gc_el     = $(acionados[i]);
        var gc_nome   = gc_el.attr('data-gc');
        var gc_status = gc_el.attr('data-status');

        if( conflita( gc_nome, gc_selecionado) ) {
            var status = gc_el.attr('data-status') || 'fechado';
            if(gc_status == 'producao' && local == 'preview') {
                //Estou mexendo no preview, mas este está em prod.
                //Vou remover ele somente do preview
                enviarMensagem('preview', 'remover', gc_nome, '');
                fechaInternos('preview', gc_nome);
                continue;
            } else {
                gc_el.removeClass('button-'+status).attr('data-status', 'fechado');
                enviarMensagem(local, 'remover', gc_nome, '');
                fechaInternos(local, gc_nome);
            }
        }
    }
}

function fechaInternos( local, gc ){
    if(local == 'todos' || local == 'preview' ){
        $('#gc-form-'+gc).find('.button-preview').removeClass('button-preview');
    }
    if(local == 'todos' || local == 'producao' ){
        $('#gc-form-'+gc).find('.button-producao').removeClass('button-producao');
    }
}

function botaoAcionamento(target) {
    var tem_filhos = target.hasClass('com-filhos');
    var gc         = target.attr('data-gc');
    var status     = target.attr('data-status') || 'fechado';
    var dados      = '';
    if( typeof question != 'undefined'  ) {
        dados = prompt(question);
    }
    target.removeClass('button-'+status);

    $('.forms form[id!="#gc-form-'+gc+'"]').addClass('d-none');
    var form = $('#gc-form-'+gc);
    var ok = true;
    if(form.length!=0 && !form.is(':valid')) {
        ok = false;
    }
    //enviarMensagem
    switch(status){
        default:
        case 'fechado':
            form.removeClass('d-none');
            status = 'preview';
            fechaConflitantes( 'preview', gc );
            enviarMensagem('preview', 'mostrar', gc, dados);
        break;

        case 'preview':
            form.removeClass('d-none');
            if(!tem_filhos && ok){
                status = 'producao';
                fechaConflitantes( 'todos', gc );
                enviarMensagem('producao', 'mostrar', gc, dados);
            }
        break;
        case 'producao':
            form.addClass('d-none');
            status = 'fechado';
            console.log('Fechando', gc);
            fechaInternos('todos', gc);
            enviarMensagem('todos', 'fechar', gc, dados);
        break;
    }
    target.attr('data-status', status);
    target.addClass('button-'+status);
};


function botaoAcionamentoInterno( target ){
    var form   = target.closest('form');
    var gc     = form.attr('data-gc');
    var num    = parseInt(target.data('equipe'));
    var status = target.hasClass('button-preview') ? 'preview' : ( target.hasClass('button-producao') ? 'producao' : 'fechado' );
    var pai = $('#botao_gc_'+gc);
    var status_pai = pai.attr('data-status') || 'fechado';

    target.removeClass('button-'+status);
    //target;
        switch(status){
        default:
        case 'fechado':
            form.find('.acionamento-interno').removeClass('button-preview');
            status = 'preview';
            enviarMensagem('preview', 'mostrar', gc, {'dados_equipe-equipe': num});
        break;

        case 'preview':
            status = 'producao';
            fechaConflitantes( 'todos', gc );
            form.find('.acionamento-interno').removeClass('button-producao');
            form.find('select').val( num );
            form.submit();
            enviarMensagem('producao', 'mostrar', gc, {'dados_equipe-equipe': num});
        break;
        case 'producao':
            status = 'fechado';
            if(status_pai == 'producao') {
                botaoAcionamento(pai);
                form.removeClass('d-none');
            }
            form.find('.acionamento-interno').removeClass('button-producao');
            enviarMensagem('todos', 'fechar', gc, '');
        break;
    }
    target.addClass('button-'+status);
};

function msgAtualizaBotaoGC( data ){
    var target = $('#botao_gc_'+data.gc);
    var status = target.attr('data-status') || 'fechado';
    var novostatus= 'fechado';
    switch( data.acao ){
        case 'mostrar':
            switch( data.destino ){
                case 'preview':  novostatus= 'preview'; break;
                case 'producao': novostatus= 'producao'; break;
            }
        break;
        case 'remover':
        case 'fechar' :
            novostatus= 'fechado';
        break;
        default:
            return;
    }

    if(status == 'producao' && data.destino == 'preview') return;
    //if(status == 'producao' && data.destino == 'preview' )

    if(novostatus != status ){
        //$('.forms form[id!="#gc-form-'+data.gc+'"]').addClass('d-none');
        var form = $('#gc-form-'+data.gc);
        if(novostatus == 'fechado' ){
            form.addClass('d-none');
            form.find('.button-producao').removeClass('button-producao');
            form.find('.button-preview').removeClass('button-preview');
        } else {
            $('.forms form[id!="#gc-form-'+data.gc+'"]').addClass('d-none');
            form.removeClass('d-none');
        }
        target.removeClass('button-'+status);
        target.attr('data-status', novostatus);
        target.addClass('button-'+novostatus);
    }
}

function recebiForm( gc, data ){
    var inputs = $('#gc-form-'+gc+' input,select');
    inputs.each( function(i, el){
        if(typeof data[el.id] != 'undefined') {
            el.value = data[el.id];
        }
    });
}

function msgSetaBotaoGC( gc, novostatus ){
    var target = $('#botao_gc_'+gc);

    var form = $('#gc-form-'+gc);
    if(novostatus == 'fechado' ){
        form.addClass('d-none');
        form.find('.acionamento-interno').removeClass('button-producao');
        form.find('.acionamento-interno').removeClass('button-preview');
    } else {
        $('.forms form[id!="#gc-form-'+gc+'"]').addClass('d-none');
        form.removeClass('d-none');
    }

    target.removeClass('button-preview').removeClass('button-producao').removeClass('button-fechado');
    target.attr('data-status', novostatus);
    target.addClass('button-'+novostatus);
}

function msgStatusAnterior( gcs ){
    if(!gcs){
        console.log('Status anterior com erro');
        return;
    }
    var gcs_index = Object.keys( gcs );
    gcs_index.forEach(function(key){
        var gc = gcs[key];
        if(gc.producao == 'mostrar'){
            msgSetaBotaoGC(key,'producao');
        } else if(gc.preview == 'mostrar'){
            msgSetaBotaoGC(key,'preview');
        }
    });
}

var countdown = 0;
var countdownPausado = true;
function msgSyncCountdown( data ){
    countdown = data.countdown;
    countdownPausado = false;
}


function updateCountDown(){
    if( countdown <= 0 ) countdownPausado = true;
    if(!countdownPausado && countdown > 0 ) {
        countdown--;
        var segundo = parseInt( countdown % 60 );
        var minuto  = parseInt( ((countdown - segundo) / 60) % 60 );
        var hora    = parseInt( countdown / 3600 );
        var tempo   =  hora.toString().padStart(2,'0') + ':' + minuto.toString().padStart(2,'0') + ':' + segundo.toString().padStart(2,'0');
        $('#tempo_restante').val(tempo);
    }
}

function setCountdownFromForm(){
    var tempo = $('#tempo_restante').val().split(':');
    var hora    = parseInt( tempo[0] );
    var minuto  = parseInt( tempo[1] );
    var segundo = parseInt( tempo[2] || 0 );
    countdown = segundo + (minuto * 60) + (hora * 3600);
    console.log('Atualizando countdown para '+countdown);
}


/* EVENTOS */
    $('#gc-form-top10_ranking .save-in').click(setCountdownFromForm);
    $('#gc-form-top10_ranking').submit(setCountdownFromForm);
    setInterval(updateCountDown,1000);

    /* TOP 10 ESQUERDA */
        $('.btn-play-countDown').on('click', function () {
            enviarMensagem('todos', 'playCountdown', 'top10_ranking', '');
            countdownPausado = false;
        });
        $('.btn-pause-countDown').on('click', function () {
            enviarMensagem('todos', 'pauseCountdown', 'top10_ranking', '');
            countdownPausado = true;
        });
    /* / TOP 10 ESQUERDA */

    $('.btn-force-out').on('click', function () {
        $('.botoes button').each(function(i, e) {
            enviarMensagem('todos', 'remover', $(e).data('gc'), '');
        });
    });

    $('.btn-clear-preview').on('click', function () {
        console.info('clear-preview')
        //$('.botoes button').each(function(i, e) {
        $('.botoes .button-preview').each(function(i, e) {
            //if($(e).data('status') == 'preview')
                enviarMensagem('preview', 'remover', $(e).data('gc'), '');
        });
    });

    $('.btn-out-all').on('click', function () {
        console.info('clear-out-all')
        $('.botoes button').each(function(i, e) {
            //if($(e).hasClass('button-producao') )
                enviarMensagem('todos', 'fechar', $(e).data('gc'), '');
        });
    });

    $('.acionamento').click(function(ev){
        botaoAcionamento($(ev.currentTarget));
    });

    $('.btn-dupla_campea').click(function(ev){
        var target = $(ev.currentTarget);
        var form   = target.closest('form');
        var gc     = form.attr('data-gc');
        var num    = parseInt(target.data('pos'));
        var status = target.hasClass('button-preview') ? 'preview' : ( target.hasClass('button-producao') ? 'producao' : 'fechado' );
        var pai = $('#botao_gc_'+gc);
        var status_pai = pai.attr('data-status') || 'fechado';

        target.removeClass('button-'+status);
        //target;
            switch(status){
            default:
            case 'fechado':
                form.find('.btn-dupla_campea').removeClass('button-preview');
                status = 'preview';
                enviarMensagem('preview', 'mostrar', gc, {'id_dupla_campea': num});
            break;

            case 'preview':
                status = 'producao';
                fechaConflitantes( 'todos', gc );
                form.find('.btn-dupla_campea').removeClass('button-producao');
                form.find('input#id_dupla_campea').val( num );
                form.submit();
                enviarMensagem('producao', 'mostrar', gc, {'id_dupla_campea': num});
            break;
            case 'producao':
                status = 'fechado';
                if(status_pai == 'producao') {
                    botaoAcionamento(pai);
                    form.removeClass('d-none');
                }
                form.find('.btn-dupla_campea').removeClass('button-producao');
                enviarMensagem('todos', 'fechar', gc, '');
            break;
        }
        target.addClass('button-'+status);
    });

    $('.btn-apresentacao_duplas').click(function(ev){
        var target = $(ev.currentTarget);
        var form   = target.closest('form');
        var gc     = form.attr('data-gc');
        var num    = parseInt(target.data('pos'));
        var estado   = parseInt(target.data('estado'));
        var etapa    = parseInt(target.data('etapa'));
        var status = target.hasClass('button-preview') ? 'preview' : ( target.hasClass('button-producao') ? 'producao' : 'fechado' );
        var pai = $('#botao_gc_'+gc);
        var status_pai = pai.attr('data-status') || 'fechado';

        target.removeClass('button-'+status);
        //target;
            switch(status){
            default:
            case 'fechado':
                form.find('.btn-apresentacao_duplas').removeClass('button-preview');
                status = 'preview';
                    enviarMensagem('preview', 'mostrar', gc, { 'id_apresentacao_duplas': num, estado, etapa});
            break;

            case 'preview':
                status = 'producao';
                fechaConflitantes( 'todos', gc );
                form.find('.btn-apresentacao_duplas').removeClass('button-producao');
                form.find('input#id_apresentacao_duplas').val( num );
                form.submit();
                    enviarMensagem('producao', 'mostrar', gc, { 'id_apresentacao_duplas': num, estado, etapa});
            break;
            case 'producao':
                status = 'fechado';
                if(status_pai == 'producao') {
                    botaoAcionamento(pai);
                    form.removeClass('d-none');
                }
                form.find('.btn-apresentacao_duplas').removeClass('button-producao');
                enviarMensagem('todos', 'fechar', gc, '');
            break;
        }
        target.addClass('button-'+status);
    });

    $('.btn-raias').click(function(ev){
        var target = $(ev.currentTarget);
        var form   = target.closest('form');
        var gc     = form.attr('data-gc');
        var num    = parseInt(target.data('pos'));
        var status = target.hasClass('button-preview') ? 'preview' : ( target.hasClass('button-producao') ? 'producao' : 'fechado' );
        var pai = $('#botao_gc_'+gc);
        var status_pai = pai.attr('data-status') || 'fechado';

        target.removeClass('button-'+status);
        //target;
            switch(status){
            default:
            case 'fechado':
                form.find('.btn-raias').removeClass('button-preview');
                status = 'preview';
                enviarMensagem('preview', 'mostrar', gc, {'pagina': num});
            break;

            case 'preview':
                status = 'producao';
                fechaConflitantes( 'todos', gc );
                form.find('.btn-raias').removeClass('button-producao');
                form.find('input').val( num );
                form.submit();
                enviarMensagem('producao', 'mostrar', gc, {'pagina': num});
            break;
            case 'producao':
                status = 'fechado';
                if(status_pai == 'producao') {
                    botaoAcionamento(pai);
                    form.removeClass('d-none');
                }
                form.find('.btn-raias').removeClass('button-producao');
                enviarMensagem('todos', 'fechar', gc, '');
            break;
        }
        target.addClass('button-'+status);
    });

    $('.save-in').click(function(ev){
        var form = $(ev.currentTarget).closest('form');
        form.submit();
        var gc     = form.attr('data-gc');
        var pai = $('#botao_gc_'+gc);
        var status_pai = pai.attr('data-status') || 'fechado';
        if(status_pai == 'preview') {
            botaoAcionamento(pai);
        }
    });

    $('.forms form').submit(function( ev ){
        ev.preventDefault();
        var gc = ev.currentTarget.id.substr(8);
        var formData = new FormData(ev.currentTarget);
        var object = {};
        formData.forEach(function(value, key){
            object[key] = value;
        });
        var json = object;
        enviarMensagemTipo('atualiza_form', gc, json);
    });

    $('.btn-abre-form').click(function( ev ){
        ev.preventDefault();
        var form = ev.currentTarget.dataset.form;
        console.log(form);
        $('.forms form[id!="#'+form+'"]').addClass('d-none');
        $('#'+form).removeClass('d-none');
    });

    $('.preset-store').click(function(ev){
        ev.preventDefault();
        var selecionado = $('#info-presets').val();
        var form = $('#gc-form-info');
        const dados = {
            titulo: $('#info-titulo').val(),
            texto:  $('#info-texto').val()
        };

        if( selecionado < 0  || !presets_info[ selecionado ] ) {
            presets_info.push(dados);
        } else {
            presets_info[selecionado] = dados;
        }
        //console.log(presets_info);
        enviarMensagemTipo( 'presets', 'info', presets_info );
    });
    $('.preset-load').click(function(ev){
        ev.preventDefault();
        var selecionado = $('#info-presets').val();
        var form = $('#gc-form-info');
        if( selecionado < 0 || !presets_info[ selecionado ]) {
            console.log(selecionado);
            return;
        }
        var sel = presets_info[ selecionado ];
        $('#info-titulo').val(sel.titulo);
        $('#info-texto').val(sel.texto);
    });
/* / EVENTOS */
