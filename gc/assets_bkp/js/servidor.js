"use strict";
const fs = require('fs');
// Optional. You will see this name in eg. 'ps' or 'top' command
process.title = 'Relay GC';
// Port where we'll run the websocket server
// websocket and http servers
//var webSocketServer = require('websocket').server;
const WebSocket = require('ws');
const logFile = 'logs/msgs-'+Date.now()+'.json';
var config = require('./config').config();
const got = require('got');
var webSocketsServerPort = config.port;
var etapa_hoje = '';
var ranking = '';
var equipes_etapa = '';
var grafico = '';
var allinfo = '';

var countdownPausado = true;
var countdown = 0;
var form_dados_gc = [];
var gc_status = {};
var presets = [];
/**
 * Global variables
 */

function timestamp(){
    var date = new Date()
    return date.getHours() + ":"+date.getMinutes()+":"+date.getSeconds()+"| ";
}


/**
 *  Keepalive
 */
function noop() {}

function heartbeat() {
    this.isAlive = true;
}

function sendAndLog(connection, data ){
    connection.send(data);
    fs.appendFile( logFile, 'X|'+Date.now()+'|'+ data +'\r\n', 'utf8', function(){});
}


/**
 * WebSocket server
 */
var wsServer = new WebSocket.Server({ port: webSocketsServerPort } );

wsServer.on('connection', function(connection) {

    connection.userName = Math.random().toString(36).substring(7);
    connection.isAlive = true;
    sendAndLog(connection, JSON.stringify({ type:'hello' }) );
    sendAndLog(connection, JSON.stringify({ type:'ranking', data: ranking }) );
    sendAndLog(connection, JSON.stringify({ type:'etapa_hoje', data: etapa_hoje }) );
    sendAndLog(connection, JSON.stringify({ type:'equipes_etapa', data: equipes_etapa }) );
    sendAndLog(connection, JSON.stringify({ type:'grafico', data: grafico}))
    sendAndLog(connection, JSON.stringify({ type:'allinfo', data: allinfo }) );


    var keys = Object.keys(form_dados_gc);
    keys.forEach(function( gc_key ){
        sendAndLog(connection, JSON.stringify({ type: 'atualiza_form', data: {gc: gc_key, dados: form_dados_gc[gc_key] } }));
    });

    var keys = Object.keys(presets);
    keys.forEach(function( gc_key ){
        sendAndLog(connection, JSON.stringify({ type: 'presets', data: {gc: gc_key, dados: presets[gc_key] } }));
    });

    sendAndLog(connection, JSON.stringify( { type:'status_anterior', data: gc_status  }) );

    /*form_dados_gc.each(dados){
        console.log(dados);
        //connection.send( JSON.stringify({ type:'equipes_etapa', data: equipes_etapa }) );
    }*/
    connection.on('pong', heartbeat);
    connection.on('message', processaMensagem.bind(connection) );
    connection.on('closedconnection', conexaoFechou);
    console.log(timestamp() + 'Conexão de ' + connection.userName );
});


const interval = setInterval(function ping() {
  wsServer.clients.forEach(function each(ws) {
    if (ws.isAlive === false) return ws.terminate();

    ws.isAlive = false;
    ws.ping(noop);
  });
}, 30000);


function conexaoFechou(id) {
    console.log(timestamp() + 'fechou a conexão id '+id);
};


function broadcastMensagem(type, mensagem ){
    // broadcast message to all connected clients
    fs.appendFile( logFile, 'O|'+Date.now()+'|'+ JSON.stringify({ type:type, data: mensagem })+'\r\n', 'utf8', function(){});
    var json = JSON.stringify({ type:type, data: mensagem });
    wsServer.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
            console.log('cliente: ' +client.userName);
            client.send(json);
        }
    });
}

function processaMensagem(message) {
    // first message sent by user is their name
    console.log(timestamp() + 'Received Message from ' + this.userName + ': ' + message);
    fs.appendFile( logFile, 'I|'+Date.now()+'|'+ message +'\r\n', 'utf8', function(){});
    var msg = JSON.parse(message);
    var relay = false;
    switch(msg.type){
        case 'identify':
            this.tipo = msg.data;
            this.userName+='#' +msg.data;
        break;
        case 'atualiza_form':
            recebeu_atualiza_form(msg.gc, msg.dados);
        break;
        case 'presets':
            recebeu_presets(msg.gc, msg.dados);
        break;
        case 'comando_gc':
            relay = true;
            recebeu_comando_gc(msg.destino,msg.acao,msg.gc,msg.dados);
        break;
        //case 'ranking_status': break;
        default: relay = true;
    }
    if( relay ) {
        // broadcast message to all connected clients
        var json = JSON.stringify({ type:'message', data: msg });
        fs.appendFile( logFile, 'O|'+Date.now()+'|'+json+'\r\n', 'utf8', function(){});
        wsServer.clients.forEach(function each(client) {
            if (client != this && client.readyState === WebSocket.OPEN) {
                client.send(json);
            }
        });
    }
}


function compara_objetos(obj1, obj2) {
    if(JSON.stringify(obj1) === JSON.stringify(obj2) ) return true;
    return false;
}

function recebeu_atualiza_form( gc, dados ){
    if(gc == 'top10_ranking') {
        countDownForm(dados);
    }
    broadcastMensagem('atualiza_form', {gc: gc, dados: dados } );
    form_dados_gc[gc] = dados;
    console.log(form_dados_gc[gc]);
}

function recebeu_presets( gc, dados ){
    broadcastMensagem('presets', {gc: gc, dados: dados } );
    presets[gc] = dados;
}

function recebeu_comando_gc( destino, acao, gc, dados){
    if(typeof gc_status[gc] == 'undefined') {
        gc_status[gc] = {'preview': 'esconder', 'producao': 'esconder', 'dados':''};
    }

    gc_status[gc].dados = dados;
    if(destino == 'preview' || destino == 'todos' ){
        gc_status[gc].preview = acao;
    }
    if(destino == 'producao' || destino == 'todos'){
        gc_status[gc].producao = acao;
    }
    if(acao == 'playCountdown') {
        countdownPausado = false;
    } else if( acao == 'pauseCountdown'){
        countdownPausado = true;
    }
}

function countDownForm(dados){
    if(!dados['tempo_restante']) return;
    var tempo = dados['tempo_restante'].split(':');
    var hora    = parseInt( tempo[0] );
    var minuto  = parseInt( tempo[1] );
    var segundo = parseInt( tempo[2] || 0 );
    countdown = segundo + (minuto * 60) + (hora * 3600);
    console.log('Countdown setado para ' + countdown );
}

function updateCountDown(){
    if( countdown <= 0 ) countdownPausado = true;
    if(!countdownPausado && countdown > 0 ) {
        countdown--;
        if(countdown % 10 == 0 ){
            broadcastMensagem('syncCountdown', {'countdown': countdown } );
        }

        /*
        var segundo = parseInt( countdown % 60 );
        var minuto  = parseInt( ((countdown - segundo) / 60) % 60 );
        var hora    = parseInt( countdown / 3600 );

        var tempo   =  hora.toString().padStart(2,'0') + ':' + minuto.toString().padStart(2,'0') + ':' + segundo.toString().padStart(2,'0');
        form_dados_gc['top10_ranking']['tempo_restante'] = tempo;
        */

    }
}

var intervalEtapaAtual = setInterval(function(){
    got( config.getEndpoint('etapa') , { json: true }).then(response => {
        clearInterval( intervalEtapaAtual );
        etapa_hoje = response.body.ETAPA_HOJE[0] ;
        broadcastMensagem('etapa_hoje', etapa_hoje);
        console.log(timestamp() + 'Preparado para a etapa "'+etapa_hoje.DESCRICAO+'"' );
    }).catch(error => {
        console.log('Não consegui pegar a etapa atual em '+config.api + config.endpoint_etapa);
    });
}, 1000)

var intervalEquipesAtual = setInterval(function(){
    got( config.getEndpoint('equipes') , { json: true }).then(response => {
        clearInterval( intervalEquipesAtual );
        equipes_etapa = response.body;
        broadcastMensagem('equipes_etapa', equipes_etapa);
        console.log(timestamp() + 'Recebido equipes_etapa ');
    }).catch(error => {
        console.log('Não consegui pegar as equipes em '+config.api + config.endpoint_equipes);
    });
}, 1500)

var intervalGrafico = setInterval(function(){
    got( config.getEndpoint('grafico') , {json: true}).then(response => {
        var novo = response.body;
        if(!compara_objetos(grafico, novo)){
            grafico = novo;
            broadcastMensagem('grafico', grafico);
            console.log(timestamp() + 'Recbeido grafico');
        }
        clearInterval(intervalGrafico);
    }).catch(error => {
        console.log('Não consegui pegar o grafico em '+config.api + config.endpoint_grafico);
    });
}, 1700);

setInterval(function() {
    got( config.getEndpoint('ranking'), { json: true }).then(response => {
        var novo = response.body;
        if(!compara_objetos(ranking, novo)){
            ranking = novo;
            broadcastMensagem('ranking', ranking);
            console.log(timestamp() + 'ranking alterado!');
        }
    }).catch(error => {
        console.log(timestamp() + error);
    });
},2000);

setInterval(function() {
    got( config.getEndpoint('allinfo'), { json: true }).then(response => {
        var novo = response.body;
        if (JSON.stringify(allinfo) !== JSON.stringify(novo)){
            allinfo = novo;
            broadcastMensagem('allinfo', allinfo);
            console.log(timestamp() + 'allinfo alterado!');
        }
    }).catch(error => {
        console.log(timestamp() + error);
    });
},10000);


setInterval(updateCountDown, 1000);


/*

gc_especies.php
gc_iscas.php
gc_modalidades.php
gc_tempos.php
*/
