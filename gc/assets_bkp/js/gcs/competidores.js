$competidores = null;

function Competidores ( params ) {
    if($competidores){
        return $competidores;
    }
    this.loaded    = false;
    $competidores  = this;
    this.id        = 'competidores';
    this.num_slide = 0;
    this.pagina    = 0;
}

Competidores.prototype = {
    constructor: Competidores,

    init:function() {
        if(this.loaded) return;
        this.loaded = true;
    },

    mostrar:function( dados = null ) {
        if( dados != null ){
            console.log(dados);
            if(dados.pagina) {
                $competidores.pagina = parseInt(dados.pagina) - 1;
            }
        }
        $competidores.atualiza();
        $('#competidores').removeClass('escondido');
        $('#competidores').removeClass('fechar');

        // setTimeout(function(){
        //     $competidores.fechar();
        // }, 9000);
    },

    fechar: function() {
        $('#competidores').addClass('fechar');
        setTimeout(function(){
            $('#competidores').addClass('escondido');
        }, 1200);
    },

    remover: function() {
        $('#competidores').addClass('fechar').addClass('escondido');
    },

    atualiza: function(){
        if(!equipes_recebido) return;
        if(!equipes_raias) return;
        var offset = $competidores.pagina * 20;
        $('#competidores .raia[data-raia]').each( function ( i, el ) {
            var el_raia = $(el);
            var raia = el_raia.data('raia') + offset;
            var nome = '';
            var equipe = equipes_raias[ raia ];
            if(equipe) {
                var pos = equipes_recebido[ equipe ] ;
                if(pos){
                    nome = pos.nomes_completos;
                }
            }
            el_raia.find('.bg .texto').html( nome );
            el_raia.find('.bg-raia .texto').html('RAIA '+raia.toString().padStart(2,'0') );
        });
    },
    preenche: function( dupla ){
        $competidores.pagina = parseInt(dupla['pagina']) - 1;

    }

}

$competidores = new Competidores();
