class Top10_ranking {

    constructor(){
        if( Top10_ranking._instancia != null ) {
            return Top10_ranking._instancia;
        }
        console.log('construindo top10');
        Top10_ranking._instancia = this;
        this.loaded    = false;
        this.id        = 'top10_ranking';
        this.id_sel    = '#top10_ranking';

        //Faz o refresh do ranking
        this.ranking_atual = null;
        this.rankingTimeout = null;
        this.posicao_mp = 99;
        this.peso_mp = 0;
        this.status_ranking = 'idle';
        this.bateria = 1;
        this.ranking_em_espera = null;
        this.countdown = 0;
        this.hora_final       = false;
        this.countDownPausado = true;

        setTimeout(function () {
            this.el_hora = $(this.id_sel+' .hora');
            if(this.el_hora.length){
                setInterval( this.atualiza_relogio.bind(this), 1000 );
            }
        }.bind(this), 500)

    }

    init() {
        if(this.loaded) return;
        this.loaded = true;
    }

    mostrar() {
        if(this.ranking_atual == null) {
            console.log('não tenho dados para mostrar o ranking');
            if(config.producao){
                return;
            }
        }
        $(this.id_sel +' .nodelay').removeClass('nodelay');
        $(this.id_sel).removeClass('escondido');
        $(this.id_sel).removeClass('fechar');
        if(this.rankingTimeout != null){
            clearTimeout( this.rankingTimeout );
            this.rankingTimeout = null;
        }
        this.ranking_refresh();
        this.atualiza_descricao();
    }

    fechar() {
        $(this.id_sel +' .nodelay').removeClass('nodelay');
        $(this.id_sel).addClass('fechar');
        if(this.rankingTimeout != null){
            clearTimeout( this.rankingTimeout );
        }
        this.rankingTimeout = setTimeout(this.remover.bind(this),4000);
    }

    remover() {
        $(this.id_sel).addClass('fechar');
        $(this.id_sel).addClass('escondido');
        if(this.rankingTimeout != null){
            clearTimeout( this.rankingTimeout );
            this.rankingTimeout = null;
        }
    }

    preenche( dados_novos ) {
        console.info(dados_novos);

        if(dados_novos['bateria']) {
            this.bateria = dados_novos['bateria'];
            this.atualiza_descricao();
        }
        if(dados_novos['tempo_restante']) {
            var tempo = dados_novos['tempo_restante'].split(':');
            var hora    = parseInt( tempo[0] );
            var minuto  = parseInt( tempo[1] );
            var segundo = parseInt( tempo[2] || 0 );
            this.countdown = segundo + (minuto * 60) + (hora * 3600);
        }

        //this.setHoraFinal(dados_novos['hora_final']);
        //$('#top10_ranking #hora_final').html(dados_novos['hora_final']);
    }

    setHoraFinal( hora_final ){
        //this.hora_final = hora_final;
    }

    playCountdown(){
        this.countDownPausado = false;
    }

    pauseCountdown(){
        this.countDownPausado = true;
    }

/************************************************************************************************************************************************

                        ABAIXO ESTÃO AS FUNÇÕES QUE CALCULAM E MOVIMENTAM O RANKING. ENTRE A SEU PRÓPRIO RISCO!


 _            ,'|     _ |`,      _            ,'|     _ |`,      _       _            ,'|     _ |`,      _            ,'|     _ |`,      _
(:\          ///     /:) \\\    (:\          ///     /:) \\\    (:\     (:\          ///     /:) \\\    (:\          ///     /:) \\\    (:\
 \:\        )//     /:/   \\(    \:\        )//     /:/   \\(    \:\     \:\        )//     /:/   \\(    \:\        )//     /:/   \\(    \:\
==\:(======/:(=====):/=====):\====\:(======/:(=====):/=====):\====\:(=====\:(======/:(=====):/=====):\====\:(======/:(=====):/=====):\====\:(===
   )\\    /:/     //(       \:\    )\\    /:/     //(       \:\    )\\     )\\    /:/     //(       \:\    )\\    /:/     //(       \:\    )\\
    \\\  (:/     ///         \:)    \\\  (:/     ///         \:)    \\\     \\\  (:/     ///         \:)    \\\  (:/     ///         \:)    \\\
     `.|  "     |.'           "      `.|  "     |.'           "      `.|     `.|  "     |.'           "      `.|  "     |.'           "      `.|
************************************************************************************************************************************************/
    atualiza_descricao(){
        if( etapa_hoje == null ) {
            console.log('sem informação da etapa hoje, retentando');
            setTimeout(this.atualiza_descricao.bind(this),2000);
            return;
        }

        $( this.id_sel+' .rodape p small span' ).html(etapa_hoje.DESCRICAO);
        $( this.id_sel+' .rodape p b' ).html(etapa_hoje.UF+' - TEMPO '+this.bateria);
    }

    ranking( data ){
        if(!data){
            console.log('Ranking nulo!');
            return false;
        }

        if(this.status_ranking != 'idle'){
            //Estou fazendo alguma animação, então vou esperar para entrar
            this.ranking_em_espera = data;
            return false;
        }

        this.status_ranking = 'updating';
        var novo_ranking = this.formata_ranking(data);

        if(this.ranking_atual == null ) {
            this.ranking_atual = Object.assign({}, novo_ranking);
            this.preenche_ranking(this.ranking_atual);
            setTimeout(this.set_idle.bind(this),3500);
            return true;
        }

        var keys = Object.keys( novo_ranking );       
        var num_equipes = keys.length;                
        if(num_equipes>11) num_equipes = 11;          
        var trocas = [];                              
        var troca_externa = false;                    
        for(var i = 0; i < num_equipes; i++ ){        
            var novo =  novo_ranking[keys[i]];        
            var antigo = this.ranking_atual[keys[i]]; 
            if( typeof antigo != 'undefined' &&       
                typeof novo != 'undefined'   &&       
                novo.id_equipe != antigo.id_equipe ) {
                var posicao_antiga = -1;              
                for(var j = 0; j < num_equipes; j++ ){
                    if( typeof this.ranking_atual[keys[j]] != 'undefined' && 
                        this.ranking_atual[keys[j]].id_equipe == novo.id_equipe ){
                        posicao_antiga = j;
                        break;
                    }
                }
                if(posicao_antiga == -1 ){
                    troca_externa = true;
                    break;
                }
                var item_a = parseInt(keys[posicao_antiga]);
                var item_b = parseInt(keys[i]);
                //console.log(item_a, ' => ', item_b);
                if( typeof trocas[item_a] != 'undefined' ){
                    console.log('me embanananei tenando colocar ',item_b, ' onde já tem o ',trocas[item_a],' na posicao ',item_a);
                    troca_externa = true;
                    break;
                }
                trocas[item_a] = item_b;
                //troca_posicoes(item_a, item_b);
                //setTimeout(function(){ troca_posicoes(item_a, item_b);}, 3000 );
            } else if(typeof antigo == 'undefined' || typeof novo == 'undefined') {
                troca_externa = true;
            }
        }

        var newkeys = Object.keys( novo_ranking );
        for(var i = 0; i< newkeys.length; i++) {
            if(trocas[i]){
                newkeys[i] = parseInt(keys[trocas[i]]);
            } else {
                newkeys[i] = parseInt(newkeys[i]);
            }
        }
        newkeys.sort();
        for(var i = 0; i< newkeys.length-1; i++) {
            if(newkeys[i] == newkeys[i+1]) {
                troca_externa=true;
            }
        }

        //debugger;
        if(troca_externa) {
            this.set_animating();
            this.troca_posicao_externa( novo_ranking );
        } else {
            if(trocas.length!=0 ) {
                this.set_animating();
                this.troca_posicoes(trocas);
            } else {
                // this.set_idle();
                console.info( novo_ranking );
                this.set_animating();
                this.troca_posicao_externa( novo_ranking );
            }
        }

        this.atualiza_colocacoes( novo_ranking );
        this.ranking_atual = Object.assign({}, novo_ranking);
        return true;
    }

    sec_to_time( time ){
        var sec_num = parseInt(time, 10); // don't forget the second param
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = "0"+minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}
        return hours+':'+minutes+':'+seconds;
    }

    sync_relogio( data ){
        //Ta perto de sincar
        this.countDownPausado = false; //Se veio sync é porque não está pausado
        if( this.countdown > data - 5 && this.countdown < data + 5 ) return;
        this.countdown = data;
    }

    atualiza_relogio(){
        this.el_hora.text( this.sec_to_time(this.countdown) );
        if( this.countdown <= 0 ) this.countDownPausado = true;
        if( this.countdown <= 0 || this.countDownPausado) return;
        this.countdown--;
    }

    set_animating(){
        //console.log('Animando...');
        if(!isPreview){
            retornoGC('ranking', 'animando');
        }
        this.status_ranking = 'animando';
    }

    set_idle() {
        //console.log('Status Idle');
        if(!isPreview){
            retornoGC('ranking', 'idle');
        }
        this.status_ranking = 'idle';
    }

    ranking_refresh(){
        if($(this.id_sel).hasClass('escondido') ) {
            console.log('Parando refresh do ranking lateral');
            clearTimeout(this.rankingTimeout);
            this.rankingTimeout = null;
            return;
        }
        if(this.status_ranking != 'idle') {
            this.rankingTimeout = setTimeout( this.ranking_refresh.bind(this), 3000 );
            return;
        }

       if(this.posicao_mp <= 3 && !this.apresentou_mp ) {
            this.apresenta_mp();
            this.apresentou_mp = true;
            this.rankingTimeout = setTimeout( this.ranking_refresh.bind(this), 5000 );
            return;
       }

       this.apresentou_mp = false;
       if(this.ranking_em_espera != null) {
            if( this.ranking( this.ranking_em_espera ) ){
                this.ranking_em_espera = null;
            }
       }
       this.rankingTimeout = setTimeout( this.ranking_refresh.bind(this), 5000 );
    }

    apresenta_mp(){
        var mp = this.posicao_mp;
        if(mp<=3 ){
            this.set_animating();
            var el = $(this.id_sel+' .nomes .posicao[data-posicao='+mp+']');
            var col = $(this.id_sel+' .posicoes .posicao:nth-child('+mp+')');
            var peso = el.find('.peso b');
            peso.text( this.peso_mp );

            col.addClass('mp');
            this.tamanho_maior(el);
            setTimeout(
                function(){
                    this.ajusta_tamanho(el);
                    col.removeClass('mp');
                    col.addClass('mpremoved');
                    setTimeout( this.set_idle.bind(this), 500);
                    peso.text(this.ranking_atual[mp].total);
                    //setTimeout( function(){this.ranking(result2_move);console.log('XANGE');}.bind(this), 800);
                }.bind(this),
                4000
            );
        }
    }

    formata_ranking( data ) {
        var novo_ranking = {};

        this.posicao_mp = 99;
        //console.info(data.resultados, mp);
        var mp = data.resultados[0];
        this.peso_mp = mp.total;
        //Onde está o Mp?
        for(var i = 1; i < data.resultados.length; i++ ){
            if( mp.id_equipe == data.resultados[i].id_equipe) {
                this.posicao_mp = i;
            }
        }

        for(var i = 1; i < data.resultados.length; i++ ){
            var posicao = data.resultados[i].posicao;
            if( this.posicao_mp > 3 ){
                if( posicao >= 3) posicao++;
            }
            novo_ranking[ posicao ] = data.resultados[i];
        }

        if( this.posicao_mp > 3 ){
            novo_ranking[ 3 ] = mp;
        }

        return novo_ranking;
    }

    preenche_ranking( ranking_atual ) {
        $(this.id_sel+' .ranking .nomes .posicao').each(function( i, el ){
            var pos = $(el).attr('data-posicao');
            var eu = ranking_atual[ pos ];
            if(!eu) {
                $(el).find('.nome').text( '--' );
                $(el).find('.peso b').text('--');
            } else {
                $(el).find('.nome').text( eu.nome_competidores );
                //$(el).find('.colocacao span').text( eu.posicao );
                var peso = $(el).find('.peso b');
                if( peso.text() != eu.total ){
                    peso.text( eu.total );
                    setTimeout(function(){
                        peso.removeClass('trocar');
                    },1000);
                }
            }
            //this.ajusta_tamanho($(el));
        });
        this.atualiza_colocacoes( ranking_atual );
    }

    atualiza_colocacoes( novo_ranking ){
        var mp = this.posicao_mp;
        $(this.id_sel+' .mp').removeClass('mp');
        $(this.id_sel+' .desclassificado').removeClass('desclassificado');

        var ultima_posicao = 0;
        for(var i = 1; i<= 11; i++ ){
            var colocacao = $(this.id_sel+' .posicoes .posicao:nth-child('+i+')');
            //console.log(colocacao);
            if(typeof novo_ranking[i] == 'undefined') {
                colocacao.addClass('desclassificado');
                var nom = $(this.id_sel+' .nomes .posicao[data-posicao='+i+']');
                nom.addClass('desclassificado');
                //nom.find('.peso').html('--');
                //nom.find('.nome').html('--');
                ultima_posicao++;
                colocacao.find('.num .texto b').text(ultima_posicao);
                continue;
            }
            if(novo_ranking[i].posicao == 'mp') {
                colocacao.addClass('mp');
            } else {
                ultima_posicao = novo_ranking[i].posicao;
                colocacao.find('.num .texto b').text(ultima_posicao);
                if(novo_ranking[i].posicao == 11){
                    colocacao.addClass('desclassificado');
                }
            }
        }
    }

    troca_posicao_externa( novo_ranking ){
        for(var pos=1; pos<=11;pos++) {
            //console.log(pos,': ',novo_ranking[pos],ranking_atual[pos]);
            if( typeof novo_ranking[pos] == 'undefined' ||
                typeof this.ranking_atual[pos] == 'undefined' ||
                novo_ranking[pos].id_equipe != this.ranking_atual[pos].id_equipe ||
                novo_ranking[pos].total != this.ranking_atual[pos].total) {
                this.atualiza_inplace( pos );
            }
        }
        setTimeout( this.set_idle.bind(this), 500);
    }

    atualiza_inplace( posicao_board ){
        var el = $(this.id_sel+' .nomes .posicao[data-posicao='+parseInt(posicao_board)+']');
        var nome = el.find('.nome');
        var peso = el.find('.peso b');
        var degrade = el;
        degrade.removeClass('nodelay');
        degrade.addClass('alternar');
        setTimeout(function(){
            degrade.removeClass('alternar');
            degrade.offset();
            degrade.addClass('nodelay');
            if( typeof this.ranking_atual[posicao_board] != 'undefined'){
                nome.text(this.ranking_atual[posicao_board].nome_competidores);
                peso.text(this.ranking_atual[posicao_board].total);
                if(this.ranking_atual[posicao_board].posicao >= 11 ) {
                    el.addClass('desclassificado');
                } else {
                    el.removeClass('desclassificado');
                }
            } else {
                nome.text('--');
                peso.text('--');
                el.addClass('desclassificado');
            }
        }.bind(this), 400 );
    }

    troca_posicoes( arr_trocas ){
        var posprincipal = 0;
        var diff_troca = 0;
        for(var i=0; i< arr_trocas.length; i++) {
            if( (i - arr_trocas[i]) > diff_troca ){
                diff_troca = i - arr_trocas[i];
                posprincipal = i;
            }
        }

        var elprincipal = $(this.id_sel+' .nomes .posicao[data-posicao='+posprincipal+']');
        elprincipal.find('.peso').addClass('trocar');
        //console.log(elorigem, eldestino );
        this.tamanho_normal( elprincipal );

        setTimeout(function(){
            var elements = [];

            for(var i=0; i< arr_trocas.length; i++) {
                //elements[i].css('display','none');
                //console.log(i,'=>', arr_trocas[i]);
                if(!arr_trocas[i]) continue;
                var el = $(this.id_sel+' .nomes .posicao[data-posicao='+i+']');
                this.mover_posicao_pequeno(el, parseInt(arr_trocas[i]) );
            }

            setTimeout(function(){

                $(this.id_sel+' .nomes .posicao[data-nova-posicao]').each(function( i, el) {;
                    var el = $(el);
                    var np = el.attr('data-nova-posicao');
                    el.attr('data-posicao', np );
                    el.removeAttr('data-nova-posicao');
                });

                this.ajusta_tamanho( elprincipal );
                this.preenche_ranking(this.ranking_atual);
                setTimeout( this.set_idle.bind(this) , 500);
            }.bind(this), 1000);
        }.bind(this),1000 );
    }

    //Helpers de movimentação
    calcula_posicao( pos ){
        //top: #{ ( $i - 1  )  * if($i<4, 79, 74) + if($i<4, 0, 15) }px;
        return ( pos - 1  ) * ( pos < 4? 79: 74) + ( pos < 4 ? 0 : 15 );
    }

    mover_posicao_pequeno(el, posdestino ){
        var top = this.calcula_posicao( posdestino );
        el.css('top', top);
        el.attr('data-nova-posicao',posdestino);
        this.ajusta_tamanho(el);
    }

    ajusta_tamanho(el){
        var posicao = parseInt(el.attr('data-nova-posicao'));
        if(!posicao) posicao = parseInt(el.attr('data-posicao'));

        if( posicao <= 3){
            el.css('transform', 'scale(1) translate(0, 0)');
            el.css('z-index','80');
        } else {
            el.css('transform', 'scale(0.9) translate(-18px, 0)');
            el.css('z-index','80');
        }
    }

/*transform: scale(1.1) translate(18px,0);
transform: scale(1.2) translate(28px,0);
*/
    tamanho_normal(el){
        el.css('transform', 'scale(1) translate(0, 0)');
        el.css('z-index','120');
    }

    tamanho_maior(el){
        el.css('transform', 'scale(1.05) translate( 8px, 0)');
        el.css('z-index','120');
    }

}

Top10_ranking._instancia = new Top10_ranking();




/********************************************************************************************************************
*   Daqui pra baixo é tudo trabalho de exploração e descoberta para o gc de ranking
*********************************************************************************************************************/
var result2 = JSON.parse('{"resultados":['+
    '{"posicao":"mp","id_equipe":"55","nome_equipe":"051","nome_competidores":"CHRISTIAN & GIOVANI","total":"7,200 Kg"},'+
    '{"posicao":1,"id_equipe":"37","nome_equipe":"033","nome_competidores":"MARCO & CRIS","total":"54,278 Kg"},'+
    '{"posicao":2,"id_equipe":"38","nome_equipe":"034","nome_competidores":"MARTINELLI & MARTINELLI","total":"52,420 Kg"},'+
    '{"posicao":3,"id_equipe":"35","nome_equipe":"031","nome_competidores":"EVERSON & PAULO","total":"51,820 Kg"},'+
    '{"posicao":4,"id_equipe":"34","nome_equipe":"030","nome_competidores":"BIGUÁ & JACARÉ","total":"43,840 Kg"},'+
    '{"posicao":5,"id_equipe":"16","nome_equipe":"012","nome_competidores":"LEO & MIRES","total":"39,780 Kg"},'+
    '{"posicao":6,"id_equipe":"47","nome_equipe":"043","nome_competidores":"GILSON JR & GILSON LEAL","total":"35,144 Kg"},'+
    '{"posicao":7,"id_equipe":"48","nome_equipe":"044","nome_competidores":"RUDI & RODRIGO","total":"28,880 Kg"},'+
    '{"posicao":8,"id_equipe":"36","nome_equipe":"032","nome_competidores":"MATEUS & CHAIANE","total":"23,240 Kg"},'+
    '{"posicao":9,"id_equipe":"1","nome_equipe":"001","nome_competidores":"PIOVESAN & DUDU","total":"20,520 Kg"},'+
    '{"posicao":10,"id_equipe":"62","nome_equipe":"058","nome_competidores":"WAGNER & LEANDRO","total":"18,820 Kg"}'+
    ']}');

//Mudou a posicao de alguns
var result2_move = JSON.parse('{"resultados":['+
    '{"posicao":"mp","id_equipe":"55","nome_equipe":"051","nome_competidores":"CHRISTIAN & GIOVANI","total":"7,200 Kg"},'+
    '{"posicao":1,"id_equipe":"37","nome_equipe":"033","nome_competidores":"MARCO & CRIS","total":"54,278 Kg"},'+
    '{"posicao":2,"id_equipe":"48","nome_equipe":"044","nome_competidores":"RUDI & RODRIGO","total":"28,880 Kg"},'+
    '{"posicao":3,"id_equipe":"38","nome_equipe":"034","nome_competidores":"MARTINELLI & MARTINELLI","total":"52,420 Kg"},'+
    '{"posicao":4,"id_equipe":"35","nome_equipe":"031","nome_competidores":"EVERSON & PAULO","total":"51,820 Kg"},'+
    '{"posicao":5,"id_equipe":"34","nome_equipe":"030","nome_competidores":"BIGUÁ & JACARÉ","total":"43,840 Kg"},'+
    '{"posicao":6,"id_equipe":"16","nome_equipe":"012","nome_competidores":"LEO & MIRES","total":"39,780 Kg"},'+
    '{"posicao":7,"id_equipe":"47","nome_equipe":"043","nome_competidores":"GILSON JR & GILSON LEAL","total":"35,144 Kg"},'+
    '{"posicao":8,"id_equipe":"36","nome_equipe":"032","nome_competidores":"MATEUS & CHAIANE","total":"23,240 Kg"},'+
    '{"posicao":9,"id_equipe":"1","nome_equipe":"001","nome_competidores":"PIOVESAN & DUDU","total":"20,520 Kg"},'+
    '{"posicao":10,"id_equipe":"62","nome_equipe":"058","nome_competidores":"WAGNER & LEANDRO","total":"18,820 Kg"}'+
    ']}');

//Entrou um copetidor novo voando
var result3 = JSON.parse('{"resultados":['+
    '{"posicao":"mp","id_equipe":"55","nome_equipe":"051","nome_competidores":"CHRISTIAN & GIOVANI","total":"7,200 Kg"},'+
    '{"posicao":1,"id_equipe":"37","nome_equipe":"033","nome_competidores":"MARCO & CRIS","total":"54,278 Kg"},'+
    '{"posicao":2,"id_equipe":"38","nome_equipe":"034","nome_competidores":"MARTINELLI & MARTINELLI","total":"52,420 Kg"},'+
    '{"posicao":3,"id_equipe":"35","nome_equipe":"031","nome_competidores":"EVERSON & PAULO","total":"51,820 Kg"},'+
    '{"posicao":4,"id_equipe":"34","nome_equipe":"030","nome_competidores":"BIGUÁ & JACARÉ","total":"43,840 Kg"},'+
    '{"posicao":5,"id_equipe":"16","nome_equipe":"012","nome_competidores":"LEO & MIRES","total":"39,780 Kg"},'+
    '{"posicao":6,"id_equipe":"47","nome_equipe":"043","nome_competidores":"GILSON JR & GILSON LEAL","total":"35,144 Kg"},'+
    '{"posicao":7,"id_equipe":"48","nome_equipe":"044","nome_competidores":"RUDI & RODRIGO","total":"28,880 Kg"},'+
    '{"posicao":8,"id_equipe":"69","nome_equipe":"099","nome_competidores":"FAFA DE BELEM","total":"25,820 Kg"},'+
    '{"posicao":9,"id_equipe":"36","nome_equipe":"032","nome_competidores":"MATEUS & CHAIANE","total":"23,240 Kg"},'+
    '{"posicao":10,"id_equipe":"1","nome_equipe":"001","nome_competidores":"PIOVESAN & DUDU","total":"20,520 Kg"}'+
    ']}');

var result_mp_3o = JSON.parse('{"resultados":['+
    '{"posicao":"mp","id_equipe":"35","nome_equipe":"031","nome_competidores":"EVERSON & PAULO","total":"7,820 Kg"},'+
    '{"posicao":1,"id_equipe":"37","nome_equipe":"033","nome_competidores":"MARCO & CRIS","total":"54,278 Kg"},'+
    '{"posicao":2,"id_equipe":"38","nome_equipe":"034","nome_competidores":"MARTINELLI & MARTINELLI","total":"52,420 Kg"},'+
    '{"posicao":3,"id_equipe":"35","nome_equipe":"031","nome_competidores":"EVERSON & PAULO","total":"51,820 Kg"},'+
    '{"posicao":4,"id_equipe":"34","nome_equipe":"030","nome_competidores":"BIGUÁ & JACARÉ","total":"43,840 Kg"},'+
    '{"posicao":5,"id_equipe":"16","nome_equipe":"012","nome_competidores":"LEO & MIRES","total":"39,780 Kg"},'+
    '{"posicao":6,"id_equipe":"47","nome_equipe":"043","nome_competidores":"GILSON JR & GILSON LEAL","total":"35,144 Kg"},'+
    '{"posicao":7,"id_equipe":"48","nome_equipe":"044","nome_competidores":"RUDI & RODRIGO","total":"28,880 Kg"},'+
    '{"posicao":8,"id_equipe":"36","nome_equipe":"032","nome_competidores":"MATEUS & CHAIANE","total":"23,240 Kg"},'+
    '{"posicao":9,"id_equipe":"1","nome_equipe":"001","nome_competidores":"PIOVESAN & DUDU","total":"20,520 Kg"},'+
    '{"posicao":10,"id_equipe":"62","nome_equipe":"058","nome_competidores":"WAGNER & LEANDRO","total":"18,820 Kg"},'+
    '{"posicao":11,"id_equipe":"12","nome_equipe":"054","nome_competidores":"DESCLASSIFILDO","total":"-320 Ton"}'+
    ']}');


var result_mp_2o = JSON.parse('{"resultados":['+
    '{"posicao":"mp","id_equipe":"38","nome_equipe":"031","nome_competidores":"EVERSON & PAULO","total":"8,820 Kg"},'+
    '{"posicao":1,"id_equipe":"37","nome_equipe":"033","nome_competidores":"MARCO & CRIS","total":"54,278 Kg"},'+
    '{"posicao":2,"id_equipe":"38","nome_equipe":"034","nome_competidores":"MARTINELLI & MARTINELLI","total":"52,420 Kg"},'+
    '{"posicao":3,"id_equipe":"35","nome_equipe":"031","nome_competidores":"EVERSON & PAULO","total":"51,820 Kg"},'+
    '{"posicao":4,"id_equipe":"34","nome_equipe":"030","nome_competidores":"BIGUÁ & JACARÉ","total":"43,840 Kg"},'+
    '{"posicao":5,"id_equipe":"16","nome_equipe":"012","nome_competidores":"LEO & MIRES","total":"39,780 Kg"},'+
    '{"posicao":6,"id_equipe":"47","nome_equipe":"043","nome_competidores":"GILSON JR & GILSON LEAL","total":"35,144 Kg"},'+
    '{"posicao":7,"id_equipe":"48","nome_equipe":"044","nome_competidores":"RUDI & RODRIGO","total":"28,880 Kg"},'+
    '{"posicao":8,"id_equipe":"36","nome_equipe":"032","nome_competidores":"MATEUS & CHAIANE","total":"23,240 Kg"},'+
    '{"posicao":9,"id_equipe":"1","nome_equipe":"001","nome_competidores":"PIOVESAN & DUDU","total":"20,520 Kg"},'+
    '{"posicao":10,"id_equipe":"62","nome_equipe":"058","nome_competidores":"WAGNER & LEANDRO","total":"18,820 Kg"},'+
    '{"posicao":11,"id_equipe":"12","nome_equipe":"054","nome_competidores":"DESCLASSIFILDO","total":"-320 Ton"}'+
    ']}');



var result_mp_6o = JSON.parse('{"resultados":['+
    '{"posicao":"mp","id_equipe":"47","nome_equipe":"043","nome_competidores":"GILSON JR & GILSON LEAL","total":"35,144 Kg"},'+
    '{"posicao":1,"id_equipe":"37","nome_equipe":"033","nome_competidores":"MARCO & CRIS","total":"54,278 Kg"},'+
    '{"posicao":2,"id_equipe":"38","nome_equipe":"034","nome_competidores":"MARTINELLI & MARTINELLI","total":"52,420 Kg"},'+
    '{"posicao":3,"id_equipe":"35","nome_equipe":"031","nome_competidores":"EVERSON & PAULO","total":"51,820 Kg"},'+
    '{"posicao":4,"id_equipe":"34","nome_equipe":"030","nome_competidores":"BIGUÁ & JACARÉ","total":"43,840 Kg"},'+
    '{"posicao":5,"id_equipe":"16","nome_equipe":"012","nome_competidores":"LEO & MIRES","total":"39,780 Kg"},'+
    '{"posicao":6,"id_equipe":"47","nome_equipe":"043","nome_competidores":"GILSON JR & GILSON LEAL","total":"38,144 Kg"},'+
    '{"posicao":7,"id_equipe":"48","nome_equipe":"044","nome_competidores":"RUDI & RODRIGO","total":"28,880 Kg"},'+
    '{"posicao":8,"id_equipe":"36","nome_equipe":"032","nome_competidores":"MATEUS & CHAIANE","total":"23,240 Kg"},'+
    '{"posicao":9,"id_equipe":"1", "nome_equipe":"001","nome_competidores":"PIOVESAN & DUDU","total":"20,520 Kg"},'+
    '{"posicao":10,"id_equipe":"62","nome_equipe":"058","nome_competidores":"WAGNER & LEANDRO","total":"18,820 Kg"},'+
    '{"posicao":11,"id_equipe":"12","nome_equipe":"054","nome_competidores":"DESCLASSIFILDO","total":"-320 Ton"}'+
    ']}');


