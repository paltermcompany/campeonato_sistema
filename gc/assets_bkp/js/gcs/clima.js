$clima = null;
function Clima ( params ) {
    if($clima){
        return $clima;
    }
    this.loaded    = false;
    $clima         = this;
    this.id        = 'clima';
    this.trocas    = null;
    this.dados     = [];
    this.num_slide = 0;
}

Clima.prototype = {
    constructor: Clima,

    init:function() {
        if(this.loaded) return;
        this.loaded = true;
        // this.atualizaDados();
    },

    mostrar:function() {
        $('#clima').removeClass('escondido');
        $('#clima').removeClass('fechar');
        $clima.num_slide = 0;
        $clima.atualizaDados();
        $clima.timeoutTroca();
    },

    timeoutTroca(){
        if($clima.trocas != null ){
            clearTimeout( $clima.trocas );
        }

       /* $clima.trocas = setTimeout(function(){
            $('#clima').removeClass('trocar');
            $('#clima').offset();
            $('#clima').addClass('trocar');
            setTimeout($clima.atualizaDados,600);
            $clima.timeoutTroca();
        }, 2500);*/

    },
    fechar: function() {
        if($('#clima').hasClass('escondido')) return;
        $('#clima').addClass('escondido');
        $('#clima').removeClass('fechar');
        $('#clima').removeClass('trocar');
        $('#clima').offset();
        $('#clima').removeClass('escondido');
        $('#clima').addClass('fechar');
        if($clima.trocas) clearInterval( $clima.trocas );
        $clima.trocas=null
        setTimeout(function() {
            $('#clima').addClass('escondido');
        }, 1000);

    },

    remover: function() {
        if($clima.trocas) clearInterval( $clima.trocas );
        $clima.trocas=null;
        $('#clima').addClass('fechar');
        $('#clima').addClass('escondido');
    },
    atualizaDados(){
        $dia='DIA';
        switch($clima.dados['clima-dia']){
            case 'sol': $dia = 'ENSOLARADO'; break;
            case 'nublado': $dia = 'NUBLADO'; break;
            case 'chuva': $dia = 'CHUVA'; break;
            case 'chuva-forte-com-raio': $dia = 'CHUVA COM RAIOS'; break;
        }
        $('#clima-descricao').html( $dia );
        $('#clima-info-1 .texto').html($clima.dados['clima-temperatura']+" ºC");
        $('#clima-info-2 .texto').html($clima.dados['clima-pressao-barimetrica']);
        $('#clima-info-3 .texto').html($clima.dados['clima-velocidade-vento']+' km/h');
        document.querySelector('#clima-icone').classList = 'icone '+$clima.dados['clima-dia'];
        document.querySelector('#clima-info-1 .icone').classList = 'icone temperatura-do-dia';
        document.querySelector('#clima-info-2 .icone').classList = 'icone pressao-barimetrica';
        document.querySelector('#clima-info-3 .icone').classList = 'icone vento';
        /*
        switch( $clima.num_slide){
            default:
            case 0:
                $('#clima-descricao').html($clima.dados['clima-duplas'] + ' DUPLAS');
                $('#clima-info-1 .texto').html('Inicio às '+$clima.dados['clima-hora-inicio']);
                $('#clima-info-2 .texto').html('Término às '+$clima.dados['clima-hora-fim']);
                document.querySelector('#clima-icone').classList = 'icone dupla';
                document.querySelector('#clima-info-1 .icone').classList = 'icone relogio';
                document.querySelector('#clima-info-2 .icone').classList = 'icone relogio';
                $clima.num_slide = 1;
            break;
            case 1:
                $dia='DIA';
                switch($clima.dados['clima-dia']){
                    case 'sol': $dia = 'ENSOLARADO'; break;
                    case 'nublado': $dia = 'NUBLADO'; break;
                    case 'chuva': $dia = 'CHUVA'; break;
                    case 'chuva-forte-com-raio': $dia = 'CHUVA COM RAIOS'; break;
                }
                $('#clima-descricao').html( $dia );
                $('#clima-info-1 .texto').html($clima.dados['clima-temperatura']+" ºC");
                $('#clima-info-2 .texto').html($clima.dados['clima-velocidade-vento']+' km/h');
                document.querySelector('#clima-icone').classList = 'icone '+$clima.dados['clima-dia'];
                document.querySelector('#clima-info-1 .icone').classList = 'icone temperatura-do-dia';
                document.querySelector('#clima-info-2 .icone').classList = 'icone vento';
                $clima.num_slide=2;
            break;
            case 2:
                $('#clima-descricao').html('ÀGUA');
                $('#clima-info-1 .texto').html('Visibilidade '+$clima.dados['clima-visibilidade']);
                $('#clima-info-2 .texto').html($clima.dados['clima-temperatura-agua']+" ºC");
                document.querySelector('#clima-icone').classList = 'icone visibilidade-'+$clima.dados['clima-visibilidade'];
                document.querySelector('#clima-info-1 .icone').classList = 'icone visibilidade-'+$clima.dados['clima-visibilidade'];
                document.querySelector('#clima-info-2 .icone').classList = 'icone temperatura-do-lago';
                $clima.num_slide=0;
            break;
        }*/
        //console.log($clima.dados);
    },
    preenche: function( dados_novos ) {
        $clima.dados = dados_novos;
        $clima.atualizaDados();
    },
}

$clima = new Clima();
