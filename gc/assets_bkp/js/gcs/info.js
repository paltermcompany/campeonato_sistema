$info = null;
function Info ( params ) {
    if($info) {
        return $info;
    }
    this.loaded = false;
    $info       = this;
    this.id     = 'info';
}

Info.prototype = {
    constructor: Info,

    init:function() {
        if(this.loaded) return;
        this.loaded = true;
    },

    mostrar:function() {
        $('#info').removeClass('escondido');
        $('#info').removeClass('fechar');
    },

    fechar: function() {
        setTimeout(function(){
            $('#info').addClass('fechar');
        }, 100);
    },

    remover: function() {
        $('#info').addClass('fechar');
        $('#info').addClass('escondido');
    },

    preenche: function( dados_novos ) {
        $('#info .conteudo .titulo').html(dados_novos['info-titulo']);
        $('#info .conteudo .texto').html(dados_novos['info-texto']);
    },

}

$info = new Info();

