$agua = null;
function Agua ( params ) {
    if($agua){
        return $agua;
    }
    this.loaded    = false;
    $agua         = this;
    this.id        = 'clima';
    this.trocas    = null;
    this.dados     = [];
    this.num_slide = 0;
}

Agua.prototype = {
    constructor: Agua,

    init:function() {
        if(this.loaded) return;
        this.loaded = true;
        // this.atualizaDados();
    },

    mostrar:function() {
        $('#agua').removeClass('escondido');
        $('#agua').removeClass('fechar');
        $agua.num_slide = 0;
        $agua.atualizaDados();
        $agua.timeoutTroca();
    },

    timeoutTroca(){
        if($agua.trocas != null ){
            clearTimeout( $agua.trocas );
        }

        $agua.trocas = setTimeout(function(){
            $('#agua').removeClass('trocar');
            $('#agua').offset();
            $('#agua').addClass('trocar');
            setTimeout($agua.atualizaDados,300);
            $agua.timeoutTroca();
        }, 5000);

    },
    fechar: function() {
        if($('#agua').hasClass('escondido')) return;
        $('#agua').addClass('escondido');
        $('#agua').removeClass('fechar');
        $('#agua').removeClass('trocar');
        $('#agua').offset();
        $('#agua').removeClass('escondido');
        $('#agua').addClass('fechar');
        if($agua.trocas) clearInterval( $agua.trocas );
        $agua.trocas=null
        setTimeout(function() {
            $('#agua').addClass('escondido');
        }, 1000);

    },

    remover: function() {
        if($agua.trocas) clearInterval( $agua.trocas );
        $agua.trocas=null;
        $('#agua').addClass('fechar');
        $('#agua').addClass('escondido');
    },
    atualizaDados(){
        //console.log($agua.dados);
        console.log($agua.num_slide);
        switch( $agua.num_slide){
            default:
            case 0:
                $agua.num_slide = 1;
                $('#agua-descricao').html('ÁGUA');
                document.querySelector('#agua-icone').classList = 'icone visibilidade-'+$agua.dados['agua-visibilidade'];
                document.querySelector('#agua-info-1 .icone').classList = 'icone temperatura-do-lago';
                document.querySelector('#agua-info-2 .icone').classList = 'icone oxigenio';
                $('#agua-info-1 .texto').html($agua.dados['agua-temperatura-agua']+" ºC");
                $('#agua-info-2 .texto').html('Oxigênio '+$agua.dados['agua-oxigenio']);
            break;
            case 1:
                $agua.num_slide=0;
                document.querySelector('#agua-icone').classList = 'icone visibilidade-'+$agua.dados['agua-visibilidade'];
                document.querySelector('#agua-info-1 .icone').classList = 'icone ph';
                document.querySelector('#agua-info-2 .icone').classList = 'icone amonia';

                $('#agua-descricao').html('ÁGUA');
                $('#agua-info-1 .texto').html('pH ' + $agua.dados['agua-ph']);
                $('#agua-info-2 .texto').html('Amônia ' + $agua.dados['agua-amonia']);

            break;
        }

    },
    preenche: function( dados_novos ) {
        $agua.dados = dados_novos;
    },
}

$agua = new Agua();
