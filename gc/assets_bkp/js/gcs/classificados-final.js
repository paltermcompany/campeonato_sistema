$classificados_final = null;
function Classificados_final ( params ) {
    if($classificados_final){
        return $classificados_final;
    }
    this.loaded    = false;
    $classificados_final  = this;
    this.id        = 'classificados_final';
    this.num_slide = 0;
    this.posicoes  = 0;
    this.pesoMP    = '';
    this.posicaoMP = 0;
    this.carregadoMP = 0;
    this.fechandoTimeout = null;
}

Classificados_final.prototype = {
    constructor: Classificados_final,

    init:function() {
        if(this.loaded) return;
        this.loaded = true;
    },

    mostrar:function( ) {
        $classificados_final.remover();
        $classificados_final.atualiza();
        $('#classificados_final').removeClass('nodelay');
        $('#classificados_final').removeClass('escondido');
        $('#classificados_final').removeClass('fechar');
    },

    fechar: function() {
        $('#classificados_final').removeClass('nodelay');
        $('#classificados_final').addClass('fechar');
        $classificados_final.fechandoTimeout = setTimeout(function(){
            $classificados_final.remover();
        }, 1000);
    },

    remover: function() {
        if($classificados_final.fechandoTimeout != null) {
            clearTimeout($classificados_final.fechandoTimeout);
            $classificados_final.fechandoTimeout = null;
        }

        if($classificados_final.carregadoMP != null) {
            clearTimeout($classificados_final.carregadoMP);
            $classificados_final.carregadoMP = null;
        }
        $classificados_final.pararMP();
        $('#classificados_final').removeClass('nodelay');
        $('#classificados_final').addClass('fechar');
        $('#classificados_final').addClass('escondido');
        $('#classificados_final .carregado').removeClass('carregado');
        $('#classificados_final .mp').removeClass('mp');
        $('#classificados_final .isMP').removeClass('isMP');

    },

    atualizaMP: function(){
        if(!$classificados_final.posicaoMP) return;
        var el_mp = $('#classificados_final .isMP');
        $('#classificados_final').addClass('nodelay');
        if(el_mp.hasClass('mp')){
            el_mp.find('.peso').removeClass('carregado');
            setTimeout(function(){
                el_mp.removeClass('mp');
                el_mp.find('.peso p').html( ranking_recebido[ $classificados_final.posicaoMP ].total );
            },300);
        } else {
            el_mp.addClass('mp');
            setTimeout(function(){
                el_mp.find('.peso').addClass('carregado');
                el_mp.find('.peso p').html( ranking_recebido[ 0 ].total );
            },300);

        }
    },

    iniciarMP: function(){
        if( $classificados_final.intervalMP != null ) return;
        $classificados_final.intervalMP = setInterval( $classificados_final.atualizaMP, 6000 );
    },

    pararMP: function(){
        if( $classificados_final.intervalMP != null ) {
            clearInterval($classificados_final.intervalMP);
        }
        $classificados_final.intervalMP = null;
        $('#classificados_final .isMP').removeClass('mp');
    },

    atualiza: function(){
        if(!ranking_recebido) return;
        var equipe_mp = ranking_recebido[0].id_equipe;

        $('#classificados_final .dupla[data-posicao-full]').each( function ( i, el ) {
            var el_dupla = $(el);
            var posicao = el_dupla.data('posicaoFull');
            var rank = $classificados_final.posicoes[ posicao - 1 ];
            var pos = ranking_indexado[ rank ];
            $classificados_final.posicaoMP = 10;
            if(pos && pos.id_equipe == equipe_mp){
                $classificados_final.posicaoMP = posicao;
                if( posicao == 3 ) {
                    $classificados_final.pararMP();
                } else {
                    $classificados_final.iniciarMP();
                }
            }
            if(pos){
                var nomes = pos.nome_competidores;
                if(equipes_recebido && typeof equipes_recebido[ pos.id_equipe ] !== 'undefined') {
                    nomes = equipes_recebido[ pos.id_equipe ].nomes_completos;
                }
                el_dupla.find('.nome').html( nomes );
                el_dupla.find('.peso p').html( pos.total );
                el_posicao = el_dupla.closest('.posicao');
                if(pos.id_equipe == equipe_mp){
                    if( posicao == 3 ) {
                        el_posicao.addClass('mp');
                        el_dupla.find('.peso');
                        el_dupla.find('.peso p').html( ranking_recebido[0].total );
                        if($classificados_final.carregadoMP == null ){
                            console.log('Iniciado timeout');
                            $classificados_final.carregadoMP = setTimeout( function(){
                                console.log('Finlizado timeout');
                                $classificados_final.carregadoMP = null;
                                el_dupla.find('.peso').addClass('carregado');
                            }, 4000 );
                        }
                    }
                    el_posicao.addClass('isMP');
                } else {
                    el_posicao.removeClass('isMP');
                    el_posicao.removeClass('mp');
                    el_dupla.find('.peso').removeClass('carregado');
                }
            }
        });
    },

    preenche: function( dados ) {
        $classificados_final.posicoes = [dados['classificados_primeiro'],dados['classificados_segundo'],dados['classificados_terceiro']];
        $classificados_final.atualiza_descricao();
        $classificados_final.atualiza();
    },
    atualiza_descricao( ){
        $('#classificados_final .conteudo header>div:last-child h2').html('ETAPA '+etapa_hoje.ETAPA);
        //$('#classificados_final .conteudo header>div:last-child p em').html(etapa_hoje.DESCRICAO);
    }

}
$classificados_final = new Classificados_final();
