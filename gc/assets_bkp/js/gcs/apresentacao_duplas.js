$apresentacao_duplas = null; 
function Apresentacao_duplas ( params ) {
    if($apresentacao_duplas){
        return $apresentacao_duplas;
    }
    this.loaded          = false;
    $apresentacao_duplas = this;
    this.id              = 'apresentacao_duplas';
    this.num_slide       = 0;
    this.resultados = {
        SC: [
            {
                logo: "logo-catarinense",
            },
            {
                competidor1: "Isidoro Cisielski",
                competidor2: "Marcelo Dalmolin",
                patrocinador: "1albatroz-fishing",
            },
            {
                competidor1: "Ivan Souza",
                competidor2: "Marcio Feliciano",
                patrocinador: "1ghost",
            },
            {
                competidor1: "Diego Lima",
                competidor2: "Judiney Rosa",
                patrocinador: "1repemax-natucare",
            },
            {
                competidor1: "Fabrício de Lima",
                competidor2: "Jaqueline da Silva",
                patrocinador: "1lifek",
            },
            {
                competidor1: "Domingos Feder",
                competidor2: "Guilherme Goedert",
                patrocinador: "1cloter-rytos",
            },
            {
                competidor1: "Daniel Urbanik",
                competidor2: "Tatiane Kluska",
                patrocinador: "1repemax-natucare",
            },
        ],
        PR: [
            {
                logo: "logo-paranaense",
            },
            {
                //competidor1: "Alexandre Rocha",
                competidor1: "Carlos Monfredini",
                competidor2: "Juliano Ribeiro",
                patrocinador: "1mercadao-da-pesca",
            },
            {
                competidor1: "Rafael Rossi",
                competidor2: "Rodrigo Santos",
                patrocinador: "1boias-barao",
            },
            {
                competidor1: "Pablo Suchara",
                competidor2: "Tiago Rohweder",
                patrocinador: "1patola",
            },
            {
                competidor1: "Jonathan Camargo",
                competidor2: "Luiz Laverde",
                patrocinador: "1ghost",
            },
            {
                competidor1: "Kamila Bigua",
                competidor2: "Marcelia Takahashi",
                patrocinador: "1albatroz-fishing",
            },
            {
                competidor1: "Cláudio de Souza",
                competidor2: "Paulo Xavier",
                patrocinador: "1massa-cordeiro",
            },
        ],
        MG: [
            {
                logo: "logo-mineiro",
            },
            {
                competidor1: "Marcos Santos",
                competidor2: "Rafael Silvério",
                patrocinador: "1maruri",
            },
            {
                competidor1: "Jedean Rodrigues",
                competidor2: "Silfarney Martins",
                patrocinador: "1agropesca-petshop",
            },
            {
                competidor1: "Glauter Pinto",
                competidor2: "Inácio Zingel",
                patrocinador: "1lumis",
            },
            {
                competidor1: "Flávio Clementino",
                competidor2: "Tadeu Neves",
                patrocinador: "1cloter-rytos",
            },
            {
                competidor1: "Kleiton Ferreira",
                competidor2: "Müller Nogueira",
                patrocinador: "1canoeiros",
            },
            {
                //competidor1: "Glauter Pinto",
                competidor1: "Victor Teixeira",
                competidor2: "Sergio Filho",
                patrocinador: "1dewar",
            },
        ],
        GO: [
            {
                logo: "logo-goiano",
            },
            {
                competidor1: "Emanuel Santos",
                competidor2: "Geraldo Filho",
                patrocinador: "1lifek",
            },
            {
                competidor1: "Eli Silva",
                competidor2: "Waldiney Vieira",
                patrocinador: "1mx-pesca",
            },
            {
                competidor1: "Luciana Sallas",
                competidor2: "Irineu Neto",
                patrocinador: "1maruri",
            },
            {
                competidor1: "Márcio Arikado",
                competidor2: "Patricia Arikado",
                patrocinador: "1lumis",
            },
            {
                competidor1: "Jhonata Uhde",
                competidor2: "Maiquel Uhde",
                patrocinador: "1dewar",
            },
            {
                competidor1: "Adriano Rodrigues",
                competidor2: "Arthur Cruvinel",
                patrocinador: "1patola",
            },
        ],
        SP: [
            {
                logo: "logo-paulista",
            },
            {
                competidor1: "Eduardo França",
                competidor2: "Antonio Piovesan",
                patrocinador: "1jem-comercial",
            },
            {
                competidor1: "Lucas Giacomeli",
                competidor2: "Richard Oliveira",
                patrocinador: "1lumis",
            },
            {
                competidor1: "Cesar Okazaki",
                competidor2: "Henrique Koja",
                patrocinador: "1namazu",
            },
            {
                competidor1: "Karina Miyayaciki",
                competidor2: "Mauro Miyayaciki",
                patrocinador: "1namazu",
            },
            {
                competidor1: "Adan Santos",
                competidor2: "Marcelo Costa",
                patrocinador: "1boias-barao",
            },
            {
                competidor1: "Frederico Daenekas",
                //competidor2: "Paola Daenekas",
                competidor2: "Carmem Daenekas",
                patrocinador: "1mx-pesca",
            },
            {
                competidor1: "Fernando Battani",
                competidor2: "William Araújo",
                patrocinador: "1maruri",
            },
            {
                competidor1: "Alexandre Benedetti",
                competidor2: "Eduardo Leite",
                patrocinador: "1kamaro-joias",
            },
            {
                competidor1: "Alex Morais",
                competidor2: "Henrique Bartoli",
                patrocinador: "1canoeiros",
            },
            {
                competidor1: "Eduardo Granna",
                competidor2: "Marcelo Ferracini",
                patrocinador: "1agropesca-petshop",
            },
            {
                competidor1: "André Rodrigues",
                competidor2: "Edson Santos",
                patrocinador: "1kamaro-joias",
            },
            {
                //competidor1: "Marcelo Costa",
                competidor1: "Pedro Oliveira",
                competidor2: "Rodrigo Araujo",
                patrocinador: "1patola",
            },
            {
                competidor1: "Lucas Mendonça",
                competidor2: "Wagner Mendonça",
                patrocinador: "1dewar",
            },
            {
                competidor1: "Caio Santos",
                competidor2: "Ricardo Fernandes",
                patrocinador: "1lifek",
            },
            {
                competidor1: "Fernando Marino",
                competidor2: "Kathlyn Hidaka",
                patrocinador: "1repemax-natucare",
            },
            {
                competidor1: "Adão Santos",
                competidor2: "Roberto Gianfrancesco",
                patrocinador: "1pegando-gigantes",
            },
            {
                competidor1: "Alexandre Rocha",
                competidor2: "Anderson Rocha",
                patrocinador: "1albatroz-fishing",
            },
            {
                competidor1: "Newton Koeke",
                competidor2: "Sergio Freitas",
                patrocinador: "1pegando-gigantes",
            },
            {
                competidor1: "Alex Reis",
                //competidor2: "Marcelo Costa",
                competidor2: "Douglas de Souza",
                patrocinador: "1jem-comercial",
            },
            {
                //competidor1: "Adan Santos",
                competidor1: "Rafael Rinaldi",
                competidor2: "Márcio Silva",
                patrocinador: "1paunogato",
            },
            {
                competidor1: "Guilherme Azevedo",
                competidor2: "José Silva",
                patrocinador: "1massa-cordeiro",
            },
            {
                //competidor1: "Inácio Zingel",
                competidor1: "Aldo Scramosin",
                competidor2: "Inácio Junior",
                patrocinador: "1jem-comercial",
            },
            {
                competidor1: "Fillipe Pereira",
                competidor2: "Victor Duarte",
                patrocinador: "1mercadao-da-pesca",
            },
            {
                //competidor1: "Anderson Rocha",
                competidor1: "Deividi Pinto",
                competidor2: "Carlos Valentim",
                patrocinador: "1paunogato",
            },
        ],
    };
    this.estado='RS';
    this.etapa=1;
}

Apresentacao_duplas.prototype = {
    constructor: Apresentacao_duplas,

    init:function() {
        if(this.loaded) return;
        this.loaded = true;
    },

    mostrar:function( ) {
        $apresentacao_duplas.atualiza();
        $('#apresentacao_duplas').removeClass('escondido');
        $('#apresentacao_duplas').removeClass('fechar');
    },

    fechar: function() {
        $('#apresentacao_duplas').addClass('fechar');
        setTimeout(function(){
            $('#apresentacao_duplas').addClass('escondido');
        }, 1000);
    },

    remover: function() {
        $('#apresentacao_duplas').addClass('fechar');
        $('#apresentacao_duplas').addClass('escondido');
    },

    preenche: function (etapa) {
        console.log(etapa);
        var posicao = parseInt(etapa['id_apresentacao_duplas']);

        switch(posicao)
        {
            case 0:  etapa=1; estado='RS'; break;
            case 1:  etapa=2; estado='RS'; break;
            case 2:  etapa=1; estado='SC'; break;
            case 3:  etapa=2; estado='SC'; break;
            case 4:  etapa=1; estado='PR'; break;
            case 5:  etapa=2; estado='PR'; break;
            case 6:  etapa=1; estado='MG'; break;
            case 7:  etapa=2; estado='MG'; break;
            case 8:  etapa=1; estado='GO'; break;
            case 9:  etapa=2; estado='GO'; break;
            case 10: etapa=1; estado='SP'; break;
            case 11: etapa=2; estado='SP'; break;
            case 12: etapa=3; estado='SP'; break;
            case 13: etapa=4; estado='SP'; break;
            case 14: etapa=5; estado='SP'; break;
            case 15: etapa=6; estado='SP'; break;
            case 16: etapa=7; estado='SP'; break;
            case 17: etapa=8; estado='SP'; break;
        }

        $apresentacao_duplas.estado = estado;
        $apresentacao_duplas.etapa = etapa;
        $apresentacao_duplas.atualiza();
    },
    atualiza: function(){
        var offset = ($apresentacao_duplas.etapa-1)*3;
        var estado = $apresentacao_duplas.estado;
        var logo = $apresentacao_duplas.resultados[estado][0].logo;
        $('#apresentacao_duplas header .logo').removeClass().addClass('logo').addClass(logo);

        $('#apresentacao_duplas .posicao[data-posicao-final]').each( function ( i, el ) {
            if( i < 3) {
                var el_dupla= $(el);
                var pos = el_dupla.data('posicaoFinal') + offset;
                var patrocinador = $apresentacao_duplas.resultados[estado][pos].patrocinador;
                var resultado = $apresentacao_duplas.resultados[estado][pos];
                var nomes = resultado.competidor1 + '<br>'+resultado.competidor2;

                var bg    = 'url(assets/img/apresentacao-duplas/'+estado+'/'+pos+'-mascara.png) top center no-repeat';
                var fotos = 'url(assets/img/apresentacao-duplas/'+estado+'/'+pos+'.png) top center no-repeat';
                var patrocinadorUrl = 'url(assets/img/apresentacao-duplas/patrocinadores/'+patrocinador+'.png)';

                el_dupla.find('.nomes').html( nomes );
                el_dupla.find('.foto-bg').css('background', bg );
                el_dupla.find('.foto-png').css('background', fotos );
                el_dupla.find('.logo').css('background-image', patrocinadorUrl);
            }
        });
    }
}

$apresentacao_duplas = new Apresentacao_duplas();