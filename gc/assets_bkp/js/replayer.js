"use strict";
const fs = require('fs');
const readline = require('readline');
// Optional. You will see this name in eg. 'ps' or 'top' command
process.title = 'Replayer GC';
// Port where we'll run the websocket server
// websocket and http servers
//var webSocketServer = require('websocket').server;
const WebSocket = require('ws');
const logFile = 'log-replay.json';
var config = require('./config').config();
var webSocketsServerPort = config.port;

var $lines = null;
try{
    $lines = fs.readFileSync( logFile, 'UTF-8' ).split(/\r?\n/);
} catch( err ){
    console.error('Erro lendo arquivo '+logFile );
}

/**
 * Global variables
 */

function timestamp(){
    var date = new Date()
    return date.getHours() + ":"+date.getMinutes()+":"+date.getSeconds()+"| ";
}

function Streamer ( connection ) {
    var linha = $lines[0].split('|');
    this.posicao = 0;
    this.timer = null;
    this.connection = connection;
    this.nextLine();
}

Streamer.prototype = {
    constructor: Streamer,
    nextLine:function() {
        if(!$lines[this.posicao]) return;
        var linha = $lines[this.posicao].split('|');
        this.connection.send( linha[2] );

        if(!$lines[this.posicao + 1]) {console.log('Fim de log');return;}
        var proximoTempo = ($lines[this.posicao+1].split('|'))[1];
        var tempo = (proximoTempo - linha[1]);
        console.log(tempo);
        this.posicao++;
        setTimeout( this.nextLine.bind(this), tempo );
    },
}

/**
 * WebSocket server
 */
var wsServer = new WebSocket.Server({ port: webSocketsServerPort } );

wsServer.on('connection', function(connection) {
    new Streamer( connection );
});


function conexaoFechou(id) {
    console.log(timestamp() + 'fechou a conexão id '+id);
};


function broadcastMensagem(type, mensagem ){
    // broadcast message to all connected clients

    var json = JSON.stringify({ type:type, data: mensagem });
    wsServer.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
            client.send(json);
        }
    });
}

function processaMensagem(message) {
    // first message sent by user is their name

}
